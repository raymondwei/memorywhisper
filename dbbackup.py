# Copyright (C) 2014 Adacro Inc. - All Rights Reserved
# -----------------------------------------------------------------------------
# 3 use cases:
#   1. download all FTSUPER-RP-* to <ROOT>/tools/__pcodes.py
#        python dbbackup.py pcode
#   2. download fid-DB to <ROOT>/dbbackups/<fid><ts>-FTSUPER-ADMIN.data 
#        python dbbackup.py <fid>
#   3. verify fid/DB integrity, output report to <ROOT>/report_<ts>_<fid>.py
#        python dbbackup.py <fid> verify
#      admin check the report file, if errs in DB? If yes, repair it by:
#      python repair_db.py <report-file.py>
# -----------------------------------------------------------------------------
import sys, os
from modules import ftsuper, utils

# verify ent for iddict[keyname] where iddict[keyname] is list of ids
def verify_idlst(id, iddict, keyname, id_pool, report):
    msg0 = report[id[8:10]]['errs'].get(id,'')# get err-string for id, if exists
    msg = msg0 # copy original err-string. it may grow here in length
    if keyname in iddict:
        for e in iddict[keyname]:
            es = e.strip('+-')
            if es not in id_pool:
                msg += 'ms:' + es + ','
    else:
        msg += 'no:' + keyname + ',' # keyname not in iddict
    if len(msg) > 0 and msg != msg0: # err-string did grow here
        report[id[8:10]]['errs'][id] = msg.strip(',')

# verify ent for iddict[keyname] where iddict[keyname] is id-string
def verify_idsingle(id, iddict, keyname, id_pool, report):
    msg0 = report[id[8:10]]['errs'].get(id,'')
    msg = msg0
    if keyname in iddict:
        # id(iddict[keyname]) can be '', but if len>0, it must be in id_pool
        if len(iddict[keyname]) > 0 and iddict[keyname] not in id_pool:
            msg += 'ms:' + iddict[keyname] + ','
    else: # keyname must exist in iddict
        msg += 'no:' + keyname + ','
    if len(msg) > 0 and msg != msg0: # nsg did grow here
        report[id[8:10]]['errs'][id] = msg.strip(',')

def verify(fid, cid, db, ids, report):
    rows = db._get_cid_recs(fid, cid)
    for row in rows: # row: (rec, eid, ts)
        ent = utils.json.loads(row[0])
        report[cid]['total'] += 1
        if cid == 'PA':
            # now check pa.iddict
            verify_idlst(ent['_id'], ent['iddict'], 'parents', ids, report)
            verify_idlst(ent['_id'], ent['iddict'], 'spouses', ids, report)
            verify_idsingle(ent['_id'],ent['iddict'], 'nutshell', ids, report)
            verify_idsingle(ent['_id'],ent['iddict'], 'portrait', ids, report)
        elif cid == 'BA':
            verify_idlst(ent['_id'],ent['iddict'],'children',ids, report)
            # check if male/female ids are okay. dont report if no-id
            verify_idsingle(ent['_id'],ent['iddict'],'male',ids,report)
            verify_idsingle(ent['_id'],ent['iddict'],'female',ids,report)
        elif cid == 'IP':
            verify_idlst(ent['_id'],ent['iddict'],'folders',ids,report)
            verify_idlst(ent['_id'],ent['iddict'],'docs',ids,report)
            verify_idsingle(ent['_id'],ent['iddict'],'owner',ids,report)
        elif cid == 'IT':
            verify_idlst(ent['_id'],ent['iddict'],'folders',ids,report)
            verify_idlst(ent['_id'],ent['iddict'],'docs',ids,report)
            verify_idlst(ent['_id'],ent['iddict'],'facets',ids,report)
            verify_idsingle(ent['_id'],ent['iddict'],'owner',ids,report)
            verify_idsingle(ent['_id'],ent['iddict'],'ownerpa',ids,report)
        elif cid == 'FC':
            verify_idsingle(ent['_id'],ent['iddict'],'owner',ids,report)
            verify_idsingle(ent['_id'],ent['iddict'],'rs',ids,report)
        elif cid == 'TD':
            verify_idlst(ent['_id'],ent['iddict'],'holders',ids,report)
            #verify_idlst(ent['_id'],ent['iddict'],'chidren',ids,report)
        elif cid == 'RS':
            verify_idlst(ent['_id'],ent['iddict'],'holders',ids,report)


def verify_db(fid, db):
    utils.action_log("debugA", "verifying DB for "+fid, db)
    #utils.set_trace()
    report = {
        'SUM':{'total':0, 'errs':{}}, # sumary {total:N, errs:{'PA':M,}}
        # for PA-errs: ms:<id> id has no ent in db; no:<name> iddict has no 
        # id-string under the name in iddict.
        'PA':{'total':0,'errs':{}},# {total:N,errs:{<pa-id>:"ms:<id>,no:<name>"}
        'BA':{'total':0,'errs':{}}, # {total:N,errs:{<ba-id>:"",}}
        'IP':{'total':0,'errs':{}}, # {total:N,errs:{<ba-id>:"",}}
        'IT':{'total':0,'errs':{}}, # {total:N,errs:{<ba-id>:"",}}
        'FC':{'total':0,'errs':{}}, # {total:N,errs:{<ba-id>:"",}}
        'TD':{'total':0,'errs':{}}, # {total:N,errs:{<ba-id>:"",}}
        'RS':{'total':0,'errs':{}}  # {total:N,errs:{<ba-id>:"",}}
    }
    ct = db.get_ent(fid + '-CT-' + fid)
    if not ct:
        report['SUM']['errs']['general'] = "ct missing"
    # snapshots:{<id>:<ts>,..}.keys() -> list of all ids (except ct-id, fa-id)
    ids = list(ct['snapshots'].keys()) # ids of all entities (except CT/FA-id)
    for cid in ('PA','BA','IP','IT','FC','TD','RS'):
        verify(fid, cid, db, ids, report)
    for cid in ('PA','BA','IP','IT','FC','TD','RS'):
        total_cnt = cnt = 0
        dic = report[cid]
        total_cnt += dic['total']
        report['SUM']['errs'][cid] = len(dic['errs'])
        report['SUM']['total'] += total_cnt

    # print out python-importable report file
    now_ts = utils.get_ts18()[0:13] # 20171216T1223
    output_report(report,fid+'_report' +'.py')

def output_report(report, filename):
    ofile = open(filename, 'w')
    from pprint import pprint
    # summary
    ofile.write('sum = ')
    pprint(report['SUM'], stream=ofile)

    # pas
    ofile.write('pas = ')
    pprint(report['PA'], stream=ofile)
    
    # bas
    ofile.write('bas = ')
    pprint(report['BA'], stream=ofile)
    
    # ips
    ofile.write('ips = ')
    pprint(report['IP'], stream=ofile)
    
    # For its
    ofile.write('its = ')
    pprint(report['IT'], stream=ofile)

    # fcs
    ofile.write('fcs = ')
    pprint(report['FC'], stream=ofile)

    # tds
    ofile.write('tds = ')
    pprint(report['TD'], stream=ofile)

    # rss
    ofile.write('rss = ')
    pprint(report['RS'], stream=ofile)        

    ofile.close()

if __name__ == '__main__':
    if len(sys.argv) < 2:
        print("usage: python dbbackup.py pcode|<fid>")
    else:
        ft = ftsuper.FTSUPER()
        if sys.argv[1] == 'pcode':
            ft.download_rp()
        elif len(sys.argv) == 3 and sys.argv[2] == 'verify':
            verify_db(sys.argv[1], ft.db)
        else:
            namebase = ft.backup_db(sys.argv[1])
            open('tmp.txt','w').write(namebase)
