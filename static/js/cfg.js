var AC_CFG = (function(){
    function AC_CFG(fd){
        this.fd = fd;
        this.AJ_div = $("<div style='width:100%; height: 100%;'></div>");
    };// end of AC_CFG constructor
    
    AC_CFG.prototype.AF_initialize = function(){
        this.AV_kvpairs = [];
        this.AJ_table = $('<table class="cfg-form"></table>')
                      .appendTo(this.AJ_div);
        var msg = '<tr><th class="cfg-form-col1"><span class="LS" tag="M1509">';
        msg += this.fd.AV_lang.face("M1509");                   // M1509: key
        msg += '</span></th><th class="cfg-form-col2"><span class="LS" ';
        // M1510: value
        msg += 'tag="M1510">'+this.fd.AV_lang.face('M1510')+'</span></th></tr>';
        $(msg).appendTo(this.AJ_table);   // title-row
        
        msg = '<tr class="button-row"><td colspan="2"><button id="add-new-row">';
        msg += '<span class="LS" tag="M1519">Merge</span></button></td></tr>';
        $(msg).appendTo(this.AJ_table);
    };// end of AC_CFG.prototype.AF_initialize
    
    AC_CFG.prototype.AF_setup = function(pa){
        this.AF_initialize();
        this.pa = pa;
        this.AV_actbuffer = this.fd.AV_actbuffer.AF_init();
        this.AV_orig = pa.AF_cfg();
        this.AV_cfgdic = $.extend(true, {}, this.AV_orig);
        this.AV_delta = this.AV_actbuffer.AF_get_update_dic(pa);
        this.AV_delta.cfg = {};
        // col1 shows the keys of form entries. Not keys in cfg
        var AV_keys = ["M1508",    // language         - value: string
                    "M7519",    // color theme      - value: string
                    "M1502",    // email            - value: string
                    "M7534",    // trustee          - value: array
                    "M1504",    // password - value: {} of string | array
                    "M7518",    // children (exposure) value: M1504
                    "M7517",    // spouses exposure    value: M1504
                    "M7516"];   // ancestry exposure   value: M1504
        for(var i=0; i<AV_keys.length; i++){
            this.AV_kvpairs.push(new AC_KVPair(this, 
                AV_keys[i],
                // null for children|spouses|ancestry. It is not used anyway
                this.AV_cfgdic[AV_keys[i]]));
        }
        this.AF_enable_save_btn(false);
    };// end of AC_CFG.prototype.AF_setup
    
    AC_CFG.prototype.AF_enable_save_btn = function(AV_enable){
        var AV_disable_clss = "ui-button-disabled ui-state-disabled";
        if(!AV_enable) this.AJ_save_btn.addClass(AV_disable_clss);
        else        this.AJ_save_btn.removeClass(AV_disable_clss);
    };// end of AC_CFG.prototype.AF_enable_save_btn
    
    // compare cfgdic with orig, update delta
    AC_CFG.prototype.AF_update_delta = function(AV_cfgkeys){
        var i, k, AV_orival, AV_changed;
        if(!AV_cfgkeys){
            AV_cfgkeys = Object.keys(this.AV_cfgdic);
        }
        if(!('cfg' in this.AV_delta)) 
            this.AV_delta.cfg = {};
        for(i=0; i<AV_cfgkeys.length; i++){
            k = AV_cfgkeys[i];
            AV_orival = this.AV_orig[k];
            AV_changed = false;
            if(typeof(AV_orival) === 'string'){
                AV_changed = this.AV_orig[k] !== this.AV_cfgdic[k];
            } else if($.isArray(AV_orival)){
                AV_changed = U.AF_array_equal(this.AV_orig[k], this.AV_cfgdic[k]);
            } else { // dictionary
                AV_changed = JSON.stringify(this.AV_orig[k]) !== 
                             JSON.stringify(this.AV_cfgdic[k]);
            }
            if(AV_changed){
                this.AV_delta.cfg[k] = this.AV_cfgdic[k];
            }else {
                delete this.AV_delta.cfg[k];
            }
        }
        if(Object.keys(this.AV_delta.cfg).length > 0){
            this.AV_actbuffer.AF_add_update(this.pa, this.AV_delta);
        } else {
            this.AV_actbuffer.AF_remove_update(this.pa);
        }
        this.AF_enable_save_btn(Object.keys(this.AV_delta.cfg).length > 0);
    };// end of AC_CFG.prototype.AF_update_delta
    
    AC_CFG.prototype.AF_save_update = function(){
        if('M7519' in this.AV_delta.cfg){
            this.AV_actbuffer.AF_add_call(FD.AF_switch_theme, 
                FD, [this.AV_delta.cfg.M7519]);
        }
        var dic = this.AV_actbuffer.AF_get_reqdic('putdocs');
        this.fd.AV_lserver.AF_post('putdocs',dic, this);
    };// end of AC_CFG.prototype.AF_save_update
    
    AC_CFG.prototype.AF_handle_success = function(data, self){
        self.AV_actbuffer.AF_execute();
    };// end of AC_CFG.prototype.AF_handle_success
    
    AC_CFG.prototype.AF_handle_err = function(data, self){
        U.log(1, 'save failed:' + data);
    };// end of CDF.prototype.AF_handle_err
    
    AC_CFG.prototype.AF_popup_dialog = function(pa){
        var self = this;
        if(!self.mergewizard)
            self.mergewizard = new AC_MergeWizard(self.fd);
        self.AJ_div.empty();
        self.dlg = self.AJ_div.dialog({
            title: self.fd.AV_lang.face("M7121"), // 
            autoOpen: false,
            width: 800,
            height: 650,
            modal: true,
            show: false,         // animated going away
            close: function(e){ self.dlg.dialog('destroy'); },
            open: function(e){ 
                self.AJ_save_btn = $('#cfg-ok-button');
                self.AF_setup(pa);
                return U.AF_stop_event(e);
            },
            buttons: {
                'ok-button': {
                    id: 'cfg-ok-button',
                    text: 'OK',
                    click: function(e){
                        self.AF_save_update();
                        self.AJ_div.dialog('close');
                        return U.AF_stop_event(e);
                    },
                },
                'cancel-button': {
                    id: 'cfg-cancel-button',
                    text: 'CANCEL',
                    click: function(e){ 
                        self.AJ_div.dialog('close'); 
                        return U.AF_stop_event(e);
                    }
                }
            }
        });
        self.dlg.dialog('open');

        self.dlg.keyup(function(e){
            if(e.keyCode === 13 && self.AJ_name.val().length > 0){
                // ENTER -> OK
                $('#cfg-ok-button').trigger('click');
            }
            if(e.keyCode === 27){ // ESCAPE -> Cancel
                $('#cfg-cancel-button').trigger('click');
            }
        });
        self.dlg.dialog('widget').attr('id', 'item-wizard-popup');
        this.fd.AV_lang.AF_set_dlg('item-wizard-popup',{
            '.ui-dialog-title': "M7121",
            '#cfg-ok-button': 'M0002',
            '#cfg-cancel-button': 'M1003'
        });
    };// end of  AC_CFG.prototype.AF_popup_dialog
    
    return AC_CFG;
})();