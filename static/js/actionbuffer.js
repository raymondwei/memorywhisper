﻿/*
 * Copyright (C) 2014 Adacro Inc. - All Rights Reserved
 */
var AC_ActionBuffer = (function(){
    function AC_ActionBuffer(AV_store){ 
        this.AV_store = AV_store;
        this.AF_init();
    }
    AC_ActionBuffer.prototype.AF_init = function(){
        this.AV_saves = [];        // [<ent>,..]
        this.AV_deletes = [];      // [<id>,<id>,..]
        this.AV_updates = [];      // [[ent,dic],[ent,dic],..]
        this.AV_assignments = [];  //[[<owner>,<name>,<value>],[],..]
        this.AV_calls = [];        // [[<function-ref>,<args-array>],[],..]
        this.reqdic = {};
        return this;
    };// end of AC_ActionBuffer.prototype.AF_init

    //============== preparation for request ===============
    AC_ActionBuffer.prototype.AF_out_rs_data = function(AV_dict){
        var d = $.extend(true, {}, AV_dict);
        d.data = '';
        return d;
    };// end of AC_ActionBuffer.prototype.AF_out_rs_data
    
    AC_ActionBuffer.prototype.AF_get_reqdic = function(reqmsg){
        var i, ent, d, dic = {}, // tx is 10 digits long - secs not milisecs
            tx = (__UTC() + '').substr(0,10); // 10 gidits (secs) utc-time
        var act, actlog = ""; // "C|U|D:<id>,..""
        for(i=0; i<this.AV_saves.length; i++){
            //this.AV_saves[i].tx(tx); // put utc-secs as tx
            ent = this.AV_saves[i];
            act = 'C:' + ent.id();
            actlog += act + ','
            ent.tx(tx);  // ent-tx set to be the same as session's tx
            d = ent.toJSON();
            if(ent.cid() === 'RS'){
                // null doesn't work for python-dict. replace with ""
                dic[act] = JSON.stringify(this.AF_out_rs_data(d))
                                        .replace(/null/g,'""');
            } else {
                dic[act] = JSON.stringify(d).replace(/null/g,'""');
            }
        }
        // in case of delete, dic has only _id.
        for(i=0; i<this.AV_deletes.length; i++){
            act = 'D:' + this.AV_deletes[i];
            actlog += act + ','
            dic[act] = JSON.stringify({'_id':this.AV_deletes[i]});
        }
        i=0;
        while(i < this.AV_updates.length){
            d = this.AV_updates[i][1];        // change-dict
            if(Object.keys(d).length === 0){  // is this update empty?
                this.AV_updates.splice(i, 1); // remove this empty update
            } else {
                ent = this.AV_updates[i][0];  // for extracting id
                act = 'U:'+ent.id();
                actlog += act + ','
                d['tx'] = tx;                 // se to current time-stamp
                if(ent.cid() === 'RS'){
                    dic[act] = JSON.stringify(this.AF_out_rs_data(d))
                                             .replace(/null/g,'""');
                } else {
                    dic[act]= JSON.stringify(d).replace(/null/g,'""');
                }
                i++;
            }
        }
        if(actlog.length > 0 && FD._se){
            actlog = actlog.slice(0,-1); // trim off last "," char
            FD._se.add_log(actlog,tx);   // this will also update tx
            dic['C:'+FD.AF_sid()] = JSON.stringify(FD._se);
        }
        return dic;
    };// end of AC_ActionBuffer.prototype.AF_get_reqdic
    
    //============== preparation for aftermath ===============
    AC_ActionBuffer.prototype.AF_add_save = function(ent){
        if($.isArray(ent)){
            for(var i=0; i<ent.length; i++){ this.AF_add_save(ent[i]); }
        } else { // it is a single ent
            if(this.AV_saves.indexOf(ent) === -1) // add it only if not in
                this.AV_saves.push(ent);
        }
        return this;
    };// end of AC_ActionBuffer.prototype.AF_add_save
    
    AC_ActionBuffer.prototype.AF_get_save_index = function(ent_id){
        var i;
        for(i=0; i<this.AV_saves.length; i++){
            if(this.AV_saves[i].id() === ent_id) 
                return i;
        }
        return -1;
    };// end of AC_ActionBuffer.prototype.AF_get_save_index
    
    AC_ActionBuffer.prototype.AF_remove_save = function(ent){//entity/id-string
        var id = typeof(ent)==='string'? ent : ent.id(),
            ind = this.AF_get_save_index(id);
        if(ind > -1)
            this.AV_saves.splice(ind,1);
        return this;
    };// end of AC_ActionBuffer.prototype.AF_remove_save
    
    // this.AV_updates is a list of tuples (ent, dic) where dic is k/v pair
    // for changes to be made for ent. This call gets the index pointing to
    // the tuple with ent in it.
    AC_ActionBuffer.prototype.AF_get_update_index = function(ent){
        var eid = typeof(ent) === 'string'? ent : ent.id();
        for(var i=0; i<this.AV_updates.length; i++){
            if(this.AV_updates[i][0].id() === eid) 
                return i;
        }
        return -1;
    };// end of AC_ActionBuffer.prototype.AF_get_update_index
    
    // return the update-dict keyed by ent. If ent-key no-existent/ent not in 
    // buffer, create it and return its dict(ref) - this is a reference
    // it doesn't harm to have empty dict(s), get_reqdict will skip it/them
    AC_ActionBuffer.prototype.AF_get_update_dic = function(ent){
        var ind = this.AF_get_update_index(ent);
        if(ind === -1){
            var e = typeof(ent) === 'string'? M.e(ent) : ent;
            this.AV_updates.push([e, {}]);
            return this.AF_get_update_dic(ent);
        } else
            return this.AV_updates[ind][1];
    };// end of AC_ActionBuffer.prototype.AF_get_update_dic
    
    // add/replace delta-dic for ent in actbuffer. ent must pre-exist in 
    AC_ActionBuffer.prototype.AF_add_update = function(ent, dic){
        // if ent not event in store, nothing to update: do nothing.
        if(!this.AV_store.AF_id_is_in(ent.id())) 
            return this;
        // find index in [[ent,{delta-dic}],..]
        var ind = this.AF_get_update_index(ent);
        if(ind === -1){
            var entity = (typeof(ent) === 'string')? M.e(ent) : ent;
            this.AV_updates.push([entity, dic]);
        } else { // an update-entry with ent as key already exists.
            // overwrite dict with dic
            this.AV_updates[ind][1] = dic;
        }
        return this;
    };// end of AC_ActionBuffer.prototype.AF_add_update 
    
    AC_ActionBuffer.prototype.AF_remove_update = function(ent){
        if(typeof(ent) === 'number'){
            if(ent < this.AV_updates.length && ent > -1)
                this.AV_updates.splice(ent,1);
        } else { // ent is entity or id-string
            var ind = this.AF_get_update_index(ent);
            if(ind > -1 && ind < this.AV_updates.length)
                this.AV_updates.splice(ind,1);
        }
        return this;
    };// end of AC_ActionBuffer.prototype.AF_remove_update
    
    // ent can be array of ids, in that case put all ids to deletion
    AC_ActionBuffer.prototype.AF_add_delete = function (ent) {
        if ($.isArray(ent)) {
            for (var i = 0; i < ent.length; i++) { this.AF_add_delete(ent[i]); }
        } else {
            if (typeof (ent) === 'string')
                this.AV_deletes.push(ent);
            else
                this.AV_deletes.push(ent.id());
        }
        return this;
    };// end of AC_ActionBuffer.prototype.AF_add_delete
    
    AC_ActionBuffer.prototype.AF_add_assignment = function(
        AV_left_container, AV_name, AV_value){
        this.AV_assignments.push([AV_left_container, AV_name, AV_value]);
        return this;
    };// end of AC_ActionBuffer.prototype.AF_add_assignment
    AC_ActionBuffer.prototype.AF_add_call = function(funref, ctx, args){
        this.AV_calls.push([funref, ctx, args]);
        return this;
    };// end of AC_ActionBuffer.prototype.AF_add_call
    AC_ActionBuffer.prototype.AF_execute = function(){
        // in case pa created(saved)/updated/deleted, collect ces to delete
        // from indexedDB/store, so they will be missing and fetched from 
        // server anew, when next time needed.
        var i, ces2delete = []; // list of ce-id: <fid>-CE-<2char month>
        var idb_puts = [], update_logo = false;

        // do the saves
        for(i=0; i<this.AV_saves.length; i++){
            if(this.AV_saves[i].cid() === 'RS'){
                FD.AV_bom.AF_ensure_data(this.AV_saves[i]);
            } else {
                if(this.AV_saves[i].cid() == 'PA'){
                    // get all ces (Calendar-Entrie) touched by pa into list
                    M.AF_collect_dirty_ces('C', this.AV_saves[i], ces2delete);
                    M.AF_blur_pwd(this.AV_saves[i]);
                }
                if(this.AV_saves[i].cid() == 'IT')
                    M.AF_blur_pwd(this.AV_saves[i]);
            }
            this.AV_store.AF_put_ent(this.AV_saves[i]);
            idb_puts.push(this.AV_saves[i]); // collecting saves to puts
        }// for(i=0; i<this.AV_saves.length; i++)
        // do the updates
        for(i=0; i<this.AV_updates.length; i++){
            //  an update can be [ent,dic] or [<id-string>, dic]. get ent
            var ent = typeof(this.AV_updates[i][0]) === 'object'? 
                this.AV_updates[i][0] : M.e(this.AV_updates[i][0]);
            if(!this.AV_store.AF_id_is_in(ent.id())){
                alert('ent 2b updated not in AV_store!');
            } else {
                // in case tname is updated, it maybe loginpv's tname.
                // update logo-text and login-username
                if('tname' in this.AV_updates[i][1]){
                    update_logo = true;
                }
                // fill in update onto ent, so entity has the update delta in
                ent.update(this.AV_updates[i][1]);
                if(ent.cid() === 'RS'){
                    FD.AV_bom.AF_ensure_data(ent); // save into azure DB
                }
                if(ent.cid() == 'PA'){
                    // give [old-pa,delta-dic] to collect months
                    M.AF_collect_dirty_ces('U', this.AV_updates[i], ces2delete);
                    // before put into store, blur pwd, if exist
                    M.AF_blur_pwd(ent);
                }
                if(ent.cid() == 'IT')
                    M.AF_blur_pwd(ent);
                idb_puts.push(ent); // collecting updates to puts
            }
        }// for(i=0; i<this.AV_updates.length; i++)

        if(update_logo){
            FD.AV_tbar.AF_set_logoword();
        }
        
        for(i=0; i<this.AV_assignments.length; i++){
            var asm = this.AV_assignments[i];
            asm[0][asm[1]] = asm[2];
        }
        for(i=0; i<this.AV_calls.length; i++){
            this.AV_calls[i][0]
                .apply(this.AV_calls[i][1], this.AV_calls[i][2]);
        }
        // do the deletes
        var idb_dels = [];
        for (i = 0; i < this.AV_deletes.length; i++) {
            this.AV_store.AF_delEntity(this.AV_deletes[i]);
            idb_dels.push(this.AV_deletes[i]);
            if(this.AV_deletes[i].substr(8,2) == 'PA'){
                M.AF_collect_dirty_ces('D', this.AV_deletes[i], ces2delete);
            }
        }

        FD.AV_lserver.AF_get_idb(FD.fid(), function(){
            var idb = FD.idbs[FD.fid()];
            idb.AF_put_ents(idb_puts);   // puts/saves into idb        
            idb.AF_delete_ents(idb_dels);// commit deletes to idb    
        });

        // delete all dirty ces in store and indexedDB
        FD.AV_lserver.AF_delete_local_ces(ces2delete);
        return {puts: idb_puts, dels: idb_dels};
    };// end of AC_ActionBuffer.prototype.AF_execute
    
    // class-method: ent's next level nodes for unfolding
    // missing ids put in list, existing ents in updates:{<id>:<tx>,,}
    AC_ActionBuffer.AF_nextlevel_ids = function(ent, list){
        var lst = list? list : [];
        if('ips' in ent.iddict)      // list of ids of ips (in pa)
            FD.AV_store.AF_add2lst(ent.iddict['ips'], lst);
        if('nutshell' in ent.iddict) // single id of nutshell (td in pa)
            FD.AV_store.AF_add2lst(ent.iddict['nutshell'], lst);
        if('portrait' in ent.iddict) // single id of picture (rs in pa)
            FD.AV_store.AF_add2lst(ent.iddict['portrait'], lst);
        if('folders' in ent.iddict)  // list of ids of itFs (in ip or it) 
            FD.AV_store.AF_add2lst(ent.iddict['folders'], lst);
        if('docs' in ent.iddict)     // list of ids of itDs (in ip or it)
            FD.AV_store.AF_add2lst(ent.iddict['docs'], lst);
        if('facets' in ent.iddict)   // list of ids of fcs (in itD)
            FD.AV_store.AF_add2lst(ent.iddict['facets'], lst);
        return lst;
    };// end of AC_ActionBuffer.AF_nextlevel_ids class method
    
    // ids of: all pa's 1st level containees, pa's preferred ls,
    // all parent-bas, all spouses-ba
    AC_ActionBuffer.AF_focus_ids = function(pa, lst){
        var lst = lst? lst : [];
        AC_ActionBuffer.AF_nextlevel_ids(pa, lst);
        // add first parent-ba-id
        FD.AV_store.AF_add2lst(
            U.AF_trim_adopt_sign(pa.iddict['parents']),lst);
        // add pa's cfg.M1508(prefered language) lst
        FD.AV_store.AF_add2lst('FTSUPER-LS-' + pa.AF_cfg().M1508, lst);
        // ad all spouse-bas
        FD.AV_store.AF_add2lst(pa.iddict['spouses'], lst);
        return lst;
    };// end of AC_ActionBuffer.AF_focus_ids
    
    // pa has been focused before. Now ids of all spouse-pas, children
    AC_ActionBuffer.AF_family_ids = function(pa){
        var lst = [], ba, opa;
        // other pa in every spouse, and children
        for(var i=0; i<pa.iddict.spouses.length; i++){
            ba = M.e(pa.iddict.spouses[i]);
            opid = ba.AF_other_pid(pa.id());
            if(opid)
                FD.AV_store.AF_add2lst(opid, lst);
            FD.AV_store
              .AF_add2lst(U.AF_trim_adopt_sign(ba.iddict.children), lst);
        }
        return lst;
    };// end of AC_ActionBuffer.AF_family_ids
    
    return AC_ActionBuffer;
})();