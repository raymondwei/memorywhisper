/*
 * Copyright (C) 2014 Adacro Inc. - All Rights Reserved
 */
// when constructed with fa given, the purpose of this popup is for edit/update
// of an existing fa. Modifieravles: lcode, theme, title(name) for diff langs
// if not given, this.fa==null, and this.newfa will be made for creation
var AC_FtWizard = (function(){
    function AC_FtWizard(){
        var self = this;
        this.fa_tlcode = FD.AF_lcode();
        this.AV_abuffer = FD.AV_actbuffer;
        this.AF_load_template();
        this.AF_setup_input_handler();
        // dic: elements without class==LS and tag
        this.AV_setlang_dic = {
            '.ui-dialog-title': 'M7116',
            '#new-ft-submit-button': 'M0002',
            '#new-ft-cancel-button': 'M1003'
        }
    };// end of AC_FtWizard constructor

    AC_FtWizard.prototype.AF_load_template = function(){
        var m = $('<div id="newft_dlg_frame"></div>');
        this.AJ_layout = m;
        $('<span id="promocode-label" class="LS" tag="M1506"></span>')
            .appendTo(m).text(FD.AV_lang.face('M1506'));
        this.AJ_pcode = 
            $('<input type="text" class="newft-input" id="promocode"/>')
            .appendTo(m);
        $('<span id="password-label" class="LS" tag="M1504"></span>')
            .appendTo(m).text(FD.AV_lang.face('M1504'));
        this.AJ_lcpicker = $(
            '<img class="language-picker" id="newft-default-lang"/>')
            .appendTo(m);
        this.AJ_pwd = $(
            '<input type="password" class="newft-input" id="loginpassword"/>')
            .appendTo(m);
        $('<span id="ftwizard-email-label" class="LS" tag="M1502"></span>')
            .appendTo(m).text(FD.AV_lang.face('M1502'));
        this.AJ_email = $(
            '<input type="text" class="newft-input" id="ftwizard-email"/>')
            .appendTo(m);
        $('<span id="ftwizard-tname-label" class="LS" tag="M7101"></span>')
            .appendTo(m).text(FD.AV_lang.face('M7101'));
        this.AJ_tname = $(
            '<input type="text" class="newft-input" id="ftwizard-tname"/>')
            .appendTo(m);
        $('<span id="gender-label" class="LS" tag="M7099"></span>')
            .appendTo(m).text(FD.AV_lang.face('M7099'));
        this.AJ_male = $('<input type="radio" name="gender" id="male-radio"/>')
            .appendTo(m);
        $('<div id="male-icon-label" class="icon-img-bg"></div>')
            .appendTo(m)
            .append($('<img class="male" src="pics/male-head.png"/>'));
        this.AJ_female = $(
            '<input type="radio" name="gender" id="female-radio"/>')
            .appendTo(m);
        $('<div id="female-icon-label" class="icon-img-bg"></div>')
            .appendTo(m)
            .append($('<img class="female" src="pics/female-head.png"/>'));
        $('<span id="newft-lang-label" class="LS" tag="M1511"></span>')
            .appendTo(m).text(FD.AV_lang.face('M1511'));
        $('<span class="LS" tag="M7519" id="newft-theme-label"></span')
            .appendTo(m).text(FD.AV_lang.face('M7519'));// Theme
        this.AJ_theme_sel = $('<select id="newft-theme-select"></select')
            .appendTo(m).append($(
                '<option value="default" selected="selected">default</option>'))
            .append($('<option value="th1">th1</option>'))
            .append($('<option value="th2">th2</option>'))
            .append($('<option value="th3">th3</option>'))
            .append($('<option value="th4">th4</option>'));
    };// end of AC_FtWizard.prototype.AF_load_template
    
    AC_FtWizard.prototype.AF_lang_pick = function(lcode, self){
        self.AJ_lcpicker.attr('src',FD.AV_lang.AF_icon_filepath(lcode));
        self.AV_lcode = lcode;
        var lsid = 'FTSUPER-LS-' + lcode,
            ls = M.e(lsid);
        if(ls){
            self.AF_handle_success(null, self);
        } else {
            self.reqverb = 'get-ls';
            FD.AV_lserver.AF_getdics_helper([lsid], 'get-ls', self);
        }
    };// end of AC_FtWizard.prototype.AF_lang_pick
    
    AC_FtWizard.prototype.AF_clearout = function(){
        this.AJ_pcode.val('');
        this.AJ_pwd.val('');
        this.AJ_email.val('');
        // this.AJ_ft_title.val('');
        this.AJ_tname.val('');
        this.AJ_male.prop('checked',true);
    };// end of AC_FtWizard.AF_clearout method
    
    AC_FtWizard.prototype.AF_setup_input_handler = function(){
        var self = this;
        self.AJ_pcode.unbind('input').bind('input', function(e){
            self.AF_verify_inputs(e); });
        self.AJ_pwd.unbind('input').bind('input', function(e){ 
            self.AF_verify_inputs(e); });
        self.AJ_email.unbind('input').bind('input', function(e){
            self.AF_verify_inputs(e); });
        self.AJ_tname.unbind('input').bind('input', function(e){ 
            self.AF_verify_inputs(e); });
        self.AJ_male.change(function(e){ self.AF_verify_inputs(e); });
        self.AJ_female.change(function (e) { self.AF_verify_inputs(e); });
        this.AJ_theme_sel.change(function(e){
            var t = $(this).val(),
            src = AC_BigButton.AV_themes[t]['fpane-bg'],
            bg = 'url(' + src + ')';
            self.AJ_layout.css('background-image', bg);
        });
    };// end of AC_FtWizard.AF_setup_input_handler method
    
    AC_FtWizard.prototype.AF_verify_inputs = function(e){
        var res = true;
        res = res && ($.trim(this.AJ_pcode.val()).length === 7)
                && ($.trim(this.AJ_pwd.val()).length > 1)
                && U.AF_validateEmail($.trim(this.AJ_email.val()))
                && ($.trim(this.AJ_tname.val()).length > 1);
        var msg = res ? "enable" : "disable";
        $('#new-ft-submit-button').button(msg);
        return res;
    };// end of AC_FtWizard.AF_verify_inputs emthod
    
    AC_FtWizard.prototype.AF_popup = function(){
        var self = this;
        this.AV_lcode = FD.AF_lcode();
        self.dlg = self.AJ_layout.dialog({
            autoOpen: false,
            title: '',
            modal: true,
            resizable: false,
            width: 710,
            height: 360,
            show: true,
            hide: true,
            open: function(e){
                self.AJ_lcpicker.attr('src',
                        FD.AV_lang.AF_icon_filepath(self.AV_lcode));
                self.AJ_lcpicker.languagePicker(self.AF_lang_pick, self);
                $(this).parent().promise().done(function () {
                    self.AF_clearout();
                    self.AF_verify_inputs();
                });
            },
            buttons:[
                {   id: "new-ft-submit-button",
                    tabIndex: -1,   // make sure no button has focus by def.
                    text: "Submit",
                    click: function (){
                        self.AF_create_ft();
                        self.dlg.dialog('close');
                    }
                },
                {   id: "new-ft-cancel-button",
                    tabIndex: -1,   // make sure no button has focus by def.
                    text: "Cancel",
                    click: function () {
                        self.dlg.dialog('close');
                    }
                }
            ]
        });// self.dlg 
        self.dlg.dialog('open');
        self.dlg.keyup(function(e){
            if(e.keyCode === 13 &&
               !$('#new-ft-submit-button').attr('disabled')){
                $('#new-ft-submit-button').trigger('click');
            }
        });
        self.dlg.dialog('widget').attr('id','newft-dlg');
        FD.AV_lang.AF_set_dlg('newft-dlg', self.AV_setlang_dic);
    };// end of AC_FtWizard.AF_popup method
    
    // ------------------------------------------------------------------------
    // Purpose: collect information of new user/ft and post to server
    // --------------------------------------------------------------
    AC_FtWizard.prototype.AF_create_ft = function(){
        var sex = this.AJ_male.prop('checked') ? 'male' : 'female';
        var d = {'sex':sex,                   // gender of initial pa
                // initial pa's login-password
                'pwd': this.AJ_pwd.val().trim(),
                'email': this.AJ_email.val().trim(),
                'name': this.AJ_tname.val().trim(),  // pa tname_face
                'theme': this.AJ_theme_sel.val(),
                'lang': this.AV_lcode,
                'trustee': [],
                'pcode': this.AJ_pcode.val().trim()};// p-code(fid) for this fa
        this.AV_abuffer.AF_init();
        // create centerpa. pa and all its containees put into abuffer
        this.pa = M.AF_create_pa('M1512',  // as initial person - newft
                null,     // itor: null
                d,        // seed_dic
                this.AV_abuffer);
        var dic = this.AV_abuffer.AF_get_reqdic('newft');
        dic['_pcode'] = this.AJ_pcode.val().trim();
        this.reqverb = 'newft';
        FD.AV_lserver.AF_post('newft', dic, this);
    };// end of AC_FtWizard.AF_create_ft method

    AC_FtWizard.prototype.AF_handle_success = function (data, self) {
        self.AV_abuffer.AF_execute();
        if(self.reqverb === 'newft'){
            var url = '?private='+self.pa.id();
            window.location.replace(url);
        } else if(self.reqverb === 'get-ls'){
            var ls = M.e('FTSUPER-LS-' + self.AV_lcode);
            FD.AV_lang.AF_set_dlg('newft-dlg', self.AV_setlang_dic, ls); 
        }
    };// end of AC_FtWizard.AF_handle_success method
    AC_FtWizard.prototype.AF_handle_err = function(data){
        console.log(data);
    }
    return AC_FtWizard;
})();
