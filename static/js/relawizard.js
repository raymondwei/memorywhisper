﻿/*
 * Copyright (C) 2014 Adacro Inc. - All Rights Reserved
 */
var AC_RelationWizard = (function(){
    function AC_RelationWizard(swz){
        this.swz = swz;
        this.AJ_layout = swz.jofferings;// $('<div id="rel-dlg"></div>');
        this.vgwidth = 20; // 1 grid-with 40 px
        this.ricon_height = 16;
        this.lpadding = 5; // 10px left padding
        this.AV_highlight = null;
    };// end of AC_RelationWizard constructor
    
    AC_RelationWizard.prototype.AF_highlight_pa = function(p, hl){
        if(hl){
            p.rpdiv.css('background-image','url(/pics/goldtag2.png)');
            if(this.AV_highlight){
                this.AF_highlight_pa(this.AV_highlight, false);
            }
            this.AV_highlight = p;
            this.swz.jpa_tname.text(p.tname_face());
            this.swz.jpa_dates.text(
                U.AF_date_dashed(p.dob(),'.')+' - '+
                                 U.AF_date_dashed(p.dod(),'.'));
            this.swz.jpa_pob.text(p.pob_face());            
        } else {
            p.rpdiv.css('background-image','url(/pics/goldtag.png)');
        }
    };// end of AC_RelationWizard.prototype.AF_highlight_pa
    
    AC_RelationWizard.prototype.AF_setup_pv = function(p ,_top, _left){
        if(!p.rpdiv){
            p.rpdiv = $('<div class="pv-bg"></div>');
        }
        p.rpdiv.css({top: _top, left: _left}).appendTo(this.AJ_layout);
        var imgsrc = p.sex()==='male'? 
           "pics/male-head.png" : "pics/female-head.png";
        var jimg = p.rpdiv.find('img');
        if(jimg.length === 0){
            $('<img src="'+imgsrc+'"/>').appendTo(p.rpdiv);
        } else {
            jimg.attr('src',imgsrc);
        }
        var span = p.rpdiv.find('span');
        var tname = U.AF_shorten_name(p.tname_face(FD.AF_lcode())); 
        if(span.length === 0){
            $('<span class="fullname">'+tname+'</span>').appendTo(p.rpdiv);
        } else {
            span.addClass('fullname').text(tname);
        }
        var self = this;
        p.rpdiv.unbind('click').bind('click', function(e){
            if(self.AV_highlight !== p){
                self.AF_highlight_pa(p, true);
            }
            var ids = []; 
            AC_ActionBuffer.AF_focus_ids(p, ids);
            self.swz.fd.AV_lserver.AF_getdics_helper(ids, 'pa-focus', self);
        });
    };// end of AC_RelationWizard.prototype.AF_setup_pv
    
    AC_RelationWizard.prototype.AF_handle_success = function(data, self){
        var fd = self.swz.fd; 
        if(self.swz.AV_init_path_index < fd.AV_path_mgr.ci){
            if(fd.AV_path_mgr.ci - self.swz.AV_init_path_index > 1)
                U.log(1, 'search-wizard tpath overusage...');
            else
                fd.AV_path_mgr.AF_pop_last_path();
        }
        // render f-graph for new (highlight)center-pa
        $('div#tab1').scrollTop(0).scrollLeft(0);

        // chop off all paths behind current-index (ci)
        fd.AV_path_mgr.AF_chop_from_ci();
        fd.AV_path_mgr.AF_new_path(self.AV_highlight) // set focus
          .AF_center_pa(self.AV_highlight);
        fd.AV_landing.AF_page().AF_render().AF_update_focus_ppane();
    };// end of AC_RelationWizard.prototype.AF_handle_success
    
    AC_RelationWizard.prototype.AF_setup_ricon = function(r, _top, _left){
        var isrc = 'url(/pics/', newvg = _left; 
        switch(r){
        case 's': 
            isrc += 'spouses-icon.png)'; break;
        case 'p': 
            newvg = _left - this.vgwidth;
            isrc += 'parent-child0.png)'; break;
        case 'c': 
            newvg = _left + this.vgwidth;
            isrc += 'parent-child0.png)'; break;
        }
        // render rela-icon
        var ic = $('<div class="rela-icon"></div>').appendTo(this.AJ_layout);
        ic.css({'top':_top, 'left': newvg+56, 'background-image': isrc});
        return newvg;
    };// end of AC_RelationWizard.prototype.AF_setup_ricon
    
    AC_RelationWizard.prototype.AF_render = function(p0, AV_path){
        var lvg = 0, rvg = 0, vg = 0, ic; // x-direction offset. rela-icon
        var lasty = this.lpadding; // 5px y-padding
        for(var i=0; i<AV_path.length; i++){
            if(AV_path[i].rooting() === 'p'){
                vg -= 1;
                if(lvg > vg) lvg = vg;
            } else if(AV_path[i].rooting() === 'c'){
                vg += 1;
                if(rvg < vg) rvg = vg;
            }
        }
        var vgcount = rvg - lvg + 1;
        //x-starting position of node1
        vg = this.vgwidth*(-lvg) + this.lpadding;
        // render p0
        this.AF_setup_pv(p0, lasty, vg);
        lasty += U.AV_PAV_H; 
        
        for(i=0; i<AV_path.length; i++){
            vg = this.AF_setup_ricon(AV_path[i].rooting(), lasty, vg);
            lasty += this.ricon_height;//20;
            this.AF_setup_pv(AV_path[i], lasty, vg);
            lasty += U.AV_PAV_H; 
        }
        this.AF_result_onheader(p0, AV_path[AV_path.length-1]);
    };// end of AC_RelationWizard.prototype.AF_render
    
    AC_RelationWizard.prototype.AF_result_onheader = function(p0, p1){
        // set title bar text
        $('#search-wizard-popup').find('span.ui-dialog-title')
            .text(this.swz.fd.AV_lang.face('M0012'));
    
        var p0div = this.swz.jrel_startbox.find('div.pa-tag');
        p0div.removeClass('pa-tag').addClass('pv-bg');
        p0div.css('top','2px');
        var pdiv = $('<div class="pv-bg"></div>');
        pdiv.css({'top': '1px', 'right': '2px'});
        var imgsrc = p1.sex()==='male'? 
           "pics/male-head.png" : "pics/female-head.png";
        $('<img src="'+imgsrc+'"/>').appendTo(pdiv);
        var tname = U.AF_shorten_name(p1.tname_face(FD.AF_lcode()));
        $('<span class="fullname">'+tname+'</span>').appendTo(pdiv);
        var rediv = this.swz.jrel_startbox.find('div.pa-relation');
        rediv.css({'width':'170px','color':'black'}).text('=>  ').append(pdiv);
        this.swz.jmore.css('visibility','hidden');
        this.swz.jloaded.css('visibility','hidden');
        $('#pas-loaded').css('visibility','hidden');
        this.swz.jpa_tname.text('');
        this.swz.jpa_dates.text('');
        this.swz.jpa_pob.text('');                
    };// end of AC_RelationWizard.prototype.AF_result_onheader
    
    return AC_RelationWizard
})()