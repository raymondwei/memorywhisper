var AC_LandingPage500 = (function(){
    function AC_LandingPage500(fd, jbox, ctls){
        this.AV_fd = fd;
        this.AV_lcode = fd.AF_lcode();
        this.AJ_div = jbox;
        this.AJ_upper = ctls.up;
        this.AJ_lower = ctls.dn;
        ctls.lb.css('visibility','hidden');
        ctls.rb.css('visibility','hidden');
        ctls.vc.css('visibility','hidden');
        this.AF_init();
    };// end of AC_LandingPage500 constructor

    AC_LandingPage500.prototype.AF_init = function(){
        this.AJ_upper.empty();
        var txt = FD.AV_lang.face('M7145'), // 'Access code'
            m0 = $('<div id="access-code-bg"></div>')
                .appendTo(this.AJ_upper);
            m = $('<div id="access-code-box"></div>').appendTo(m0);
        $('<span class="LS" id="access-code-label" tag="M7145"></span>')
            .appendTo(m).text(txt);
        this.AJ_code = $('<input type="text", id="access-code"/>')
            .appendTo(m);
        this.AJ_gobtn =$('<button class="LS" id="code-submit"></button>')
            .appendTo(m).text(FD.AV_lang.face('M0002')) // OK
            .prop('disabled',true)
            .attr('tag','M0002');
        this.AJ_cancelbtn =$('<button class="LS" id="code-cancel"></button>')
            .appendTo(m).text(FD.AV_lang.face('M1003')) // Cancel
            .prop('disabled',true)
            .attr('tag','M1003');
        this.AJ_ftbtn= $('<button class="LS" tag="M7116" id="new-ft"></button>')
            .appendTo(m0)
            .text(FD.AV_lang.face('M7116')); // Register new
        m = {
            en:'input your personal url to access your family-tree-homepage; '+
            'or hit "Register New" for applying for a new account'+
            '(You will need a promo-code).',
            zh:'输入个人网址，可以前往个人家谱首页；或点击“注册新用户”，'+
            '开通新账号(需要一个有效的注册码)。'
        }
        this.AJ_lower.empty();
        if(this.AV_lcode === 'zh'){
            this.AJ_lower.html(m.zh);
        } else {
            this.AJ_lower.html(m.en);            
        }
        this.AF_setup_handlers();
    };// end of AC_LandingPage500.prototype.AF_init

    AC_LandingPage500.prototype.AF_setup_handlers = function(){
        var self = this;
        this.AJ_code.unbind('input').bind('input', function(e){
            var val = $(this).val(),
                v = val.split('=');
            v = v.length === 1? v[0] : v[1];
            if(val.length > 0){
                self.AJ_cancelbtn.prop('disabled',false);
            } else {
                self.AJ_cancelbtn.prop('disabled',true);
            }
            if(v.length === 30 && v.substr(8,2) === 'PA'){
                    self.AJ_gobtn.prop('disabled', false);
                    self.AV_url = "http://"+window.location.host+"?private="+v;
                    self.AV_pid = v;
                    self.AJ_code.css('background-color','#E6E6E6');
                } else {
                    self.AJ_gobtn.prop('disabled', true);
                    self.AJ_code.css('background-color','pink');
                }
            })
        this.AJ_code.unbind('focus').bind('focus', function(e){
            self.AJ_code.val('');
        });
        this.AJ_cancelbtn.unbind('click').bind('click', function(e){
            self.AJ_code.val('').css('background-color','pink');
            self.AJ_cancelbtn.prop('disabled',true);
        })
        this.AJ_gobtn.unbind('click').bind('click',function(e){
            self.AF_check_personal_url();
        })
        this.AJ_ftbtn.unbind('click').bind('click', function(e){
            if(!self.AV_newftwz){
                self.AV_newftwz = new AC_FtWizard();
            }
            self.AV_newftwz.AF_popup();
        })
    };// end of AC_LandingPage500.prototype.AF_setup_handlers

    AC_LandingPage500.prototype.AF_check_personal_url = function(){
        FD.AV_lserver.AF_post("verifypurl", {pid: this.AV_pid}, this);
    };// end of AC_LandingPage500.prototype.AF_check_personal_url

    AC_LandingPage500.prototype.AF_handle_success = function(data, self){
        window.location.href = self.AV_url;
    };// end of AC_LandingPage500.prototype.AF_handle_success

    AC_LandingPage500.prototype.AF_handle_err = function(data){
        var m = "Wrong access code";
        $('#access-code').css('background-color','pink').val(m);
        this.AJ_gobtn.prop('disabled', true);
        this.AJ_code.css('background-color','pink');
    }

    AC_LandingPage500.prototype.AF_resize = function(w, h){
        this.AJ_div.css({width: w + 'px', height: h + 'px'});
        this.AJ_upper.css({width: (w - 6) + 'px', height: (h - 60) + 'px'});
        // lower(lp-msgpane) has padding 15px both left/right: take 20 off
        this.AJ_lower.css({width: (w - 6 - 30) + 'px', height: 48 + 'px'});
    };// end of AC_LandingPage500.prototype.AF_resize

    return AC_LandingPage500;
})();// end of class AC_LandingPage500
