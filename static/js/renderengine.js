﻿var AC_RenderEngine = (function(){
    function AC_RenderEngine(AV_fgraph){
        this.AV_fgraph = AV_fgraph;
    };// end of AC_RenderEngine constructor
    
    // pv is in vmap. Now expand pv's bv with other-pv
    // make bv and other-pav and put them into vmap. Also put bv into pv.loves
    AC_RenderEngine.prototype.AF_make_expand_bv = function(pv, ba, AV_path){
        var opa = ba.AF_other_pa(pv.model),
            bv, bvtop, opdic, opv;
        if(pv.sex() === 'male'){
            bvtop = pv.AF_top() +U.AF_male_anchortop2bv_top(pv.AV_loves.length);
            bv = new AC_BAVModel({gen: pv.AF_gen(), top: bvtop,
                       host: pv, ba: ba, male_pv: pv, 
                       aindex: pv.AV_loves.length});
            opdic = {pa: opa, gen: pv.AF_gen(), ast: 'shrunk',
                         sex: 'female', top: bvtop + U.AV_BAV_H, mhost: bv};
            opv = new AC_PAVModel(opdic);
            bv.AF_female_pv(opv).AF_reset_lines();
        } else {
            bvtop = pv.AF_top() - U.AF_female_anchortop2bv_top(pv.AV_loves.length);
            bv = new AC_BAVModel({gen: pv.AF_gen(), top: bvtop, 
                                  host: pv, ba: ba, female_pv: pv,
                                  aindex: pv.AV_loves.length});
            opdic = {pa: opa, gen: pv.AF_gen(), ast: 'shrunk',
                     sex: 'male', top: bvtop - U.AV_PAV_H, mhost: bv};
            opv = new AC_PAVModel(opdic);
            bv.AF_male_pv(opv).AF_reset_lines();
        }
        opv.AF_is_anchor(false);
        pv.AV_loves.push(bv.AF_anchor_pv(pv));
        AV_path.AF_add2vmap(bv).AF_add2vmap(opv);
        return this;
    };// end of AC_RenderEngine.prototype.AF_make_expand_bv

    AC_RenderEngine.prototype.AF_adjust_pv_ystart = function(ystart, pv){
        var ydelta = ystart - (pv.AF_upper(true) + U.AV_PAV_YMIND);
        pv.AF_top(pv.AF_top() + ydelta);
        for(var i=0; i<pv.AV_loves.length; i++){
            pv.AV_loves[i].AF_non_anchor_pv().AF_ymove(ydelta);
            // lineup this gen only - don't move bv.children
            pv.AV_loves[i].AF_ymove(ydelta,true);//bv:move me-only:true
        }
    };// end of AC_RenderEngine.prototype.AF_adjust_pv_ystart

    AC_RenderEngine.prototype.AF_make_expand_pvloves = function(pv, AV_path){
        for(var i=0; i<pv.AV_loves.length; i++){
            var sgrp = pv.AV_loves[i].AF_sgroup(),
                ba = pv.AV_loves[i].model;
            if(ba.iddict.children.length === 0)
                continue;
            sgrp.AF_gen().AF_add_block(sgrp);
            if(AV_path.AF_is_exp(ba)){
                this.AF_expand_sgroup_cluster(sgrp);
            }
        }
    };// end of AC_RenderEngine.prototype.AF_make_expand_pvloves

    /**
     * expand a pv cluster with all spouse-bvs and non-anchor-pvs lined up. 
     * ystart is the y-coord, where the top of first pav should be. returns the 
     * new ystart for the next pav. is_eldest is relevant because, 
     * in case all spouses/partners are above(this is a female), move all 
     * down so that the upper edge minus U.AV_PAV_YMIND is ystart
     */
    AC_RenderEngine.prototype.AF_expand_pv_cluster = function(
                ystart, 
                pv,
                AV_is_eldest){  // one child is center-pa(pid)
        var AV_path = FD.AF_tpath(),
            pair = AV_path.AF_centerpa_pair(); // center-pa: parent-ba/pa pair
        // if pa's AC(access-control) allows spouses to be opened 
        // (is owner, or AC = M7117)
        if(!pair.parent){ // no single-ba-expand demanded by center-pa 
            if(!pv.AF_spouse_expandable()){
                // no-expand if not allowed
                AV_path.AF_remove_expand(pv.model, true);
            }
        }
        // expand pv-loves if pa in AV_path
        if(AV_path.AF_is_exp(pv.model)){
            var AV_spouses;
            if(pair.parent &&  // single-ba is in pv's spouses
               pv.model.iddict.spouses.indexOf(pair.parent.id()) > -1){
                AV_spouses = [pair.parent];
            } else {
                AV_spouses = pv.model.spouses();
            }
            for(var i=0; i<AV_spouses.length; i++){
                var bv = this.AF_make_expand_bv(pv, AV_spouses[i], AV_path);
            }// for(var i=0; i<spouses.length; i++)
            // if pv is female and not first child, move pv and is pvs down
            if(!AV_is_eldest &&
               pv.AF_upper(true) + U.AV_PAV_YMIND !== pv.AF_top()){
                this.AF_adjust_pv_ystart(ystart, pv);
            }
            this.AF_make_expand_pvloves(pv, AV_path);
        }
        return pv.AF_lower(true); // true: return extended lower edge
    };// end of AC_RenderEngine.prototype.AF_expand_pv_cluster
    
    // sgrp is freshly created(empty of siblings pvs)
    AC_RenderEngine.prototype.AF_expand_sgroup_cluster = function(sgrp){
        // First PV's upper edge 
        var lasty = sgrp.AF_ynavel() - 0.5*U.AV_PAV_H,
            AV_path = FD.AF_tpath(),
            pair = AV_path.AF_centerpa_pair();
        // loop thru all siblings.
        //var AV_children = sgrp.bv().model.AF_children();
        var AV_chpids = U.AF_trim_adopt_sign(sgrp.bv().model.iddict.children),
            AV_pids;
        // if pair.centerpa is among the siblings, only expand that child
        if(pair.centerpa && AV_chpids.indexOf(pair.centerpa.id()) > -1)
            AV_pids = [pair.centerpa.id()]; // centerpid as the only one child
        else 
            AV_pids = AV_chpids;            // show all children
        var cpa;
        for(var i=0; i<AV_pids.length; i++){
            cpa = M.e(AV_pids[i]);
            var parms = { top: lasty, pa: cpa, 
                          is_anchor: true,
                          gen: sgrp.AF_gen(), 
                          ast: 'expanded', 
                          phost: sgrp.bv()
            };
            var pv = (new AC_PAVModel(parms)).AF_is_anchor(true);
            FD.AF_tpath().AF_add2vmap(pv);
            sgrp.AF_insert_sibling(pv);
            lasty = this.AF_expand_pv_cluster(lasty, pv, i===0);
        }
        if(AV_chpids == AV_pids){
            sgrp.AF_state("expanded")
                .AJ_navel.css('background-image', 'url(/pics/minus.png)');
        } else {
            sgrp.AF_state("shrunk")
                .AJ_navel.css('background-image', 'url(/pics/plus.png)');
        }
        // re-render the vertical line of this sgrp
        return sgrp.AF_reset_vline();
    };// end of AC_RenderEngine.prototype.AF_expand_sgroup_cluster
    
    AC_RenderEngine.prototype.AF_expand_root_pa = function(p, AV_path){
        var parms = {pa: p, top: U.AV_PAV_YMIND, 
                     ast: 'shrunk', is_anchor: true,
                     gen: this.AV_fgraph.AV_gens[0], 
                     phost: AV_path};
        var AV_rootpav = new AC_PAVModel(parms);
        AV_rootpav.phost = AV_path;
        AV_path.AF_vroot(AV_rootpav);
        AV_path.AF_add2vmap(AV_path.AF_vroot()); // put pav into vmap
        // render pv, set is_eldest = false, for female-pv move down, to
        // let the upper bv be on upper edge(PAV_YMIND=28)
        this.AF_expand_pv_cluster(U.AV_PAV_YMIND, 
                                  AV_rootpav, 
                                  false);     // is_eldest
        
        this.AV_fgraph.AV_gens[0].AF_add_block(AV_path.AF_vroot());
    };// end of AC_RenderEngine.prototype.AF_expand_root_pa
    
    // root ba must be virtual - no male or female existing
    AC_RenderEngine.prototype.AF_expand_root_ba = function(b, AV_path){
        var bv = new AC_BAVModel({ba: b, host: AV_path, 
                                  gen: this.AV_fgraph.AV_gens[0], 
                                  top: U.AV_PAV_YMIND + U.AV_PAV_H,
                                  aindex: 0});
            bv.AF_male_pv(new AC_PAVModel({gen: this.AV_fgraph.AV_gens[0], 
                                       top: U.AV_PAV_YMIND,
                                       sex: 'male', mhost: bv}))
              .AF_female_pv(new AC_PAVModel(
                  { gen: this.AV_fgraph.AV_gens[0], 
                    sex: 'female', 
                    mhost: bv, 
                    top: U.AV_PAV_YMIND+U.AV_PAV_H+U.AV_BAV_H}));
        AV_path.AF_add2vmap(bv)             // add bv to vmap
            .AF_add2vmap(bv.AF_male_pv())   // push male_pv into vmap(virtual)
            .AF_add2vmap(bv.AF_female_pv());// push female_pv into vmap(virtual)
        this.AV_fgraph.AV_gens[0].AF_add_block(bv);
        AV_path.AF_vroot(bv);
        var sgrp = bv.AF_sgroup();
        sgrp.AF_gen().AF_add_block(sgrp);
        this.AF_expand_sgroup_cluster(sgrp);
    };// end of AC_RenderEngine.prototype.AF_expand_root_ba
    
    // root can be pa or ba, serving as root; focusp is a pa for focusing.
    AC_RenderEngine.prototype.AF_expand = function(){
        var AV_path = FD.AF_tpath(),
            root_ent = M.e(AV_path.AF_root());
        if(root_ent.cid() === 'PA'){
            this.AF_expand_root_pa(root_ent, AV_path);
        } else {
            this.AF_expand_root_ba(root_ent, AV_path);
        }
        AV_path.AF_focus().AF_focused(true); // set visuals of pav
    };// end of AC_RenderEngine.prototype.AF_expand
    
    // go thru all active gens[1..N] (not 0-th) on fgraph, 
    // centralize every blocks around its ynavel. return last active gen
    AC_RenderEngine.prototype.AF_centralize = function(){
        var ag = g = this.AV_fgraph.AF_lastactive_gen();
        while(g.AF_index() > 0){
            // centralize all sgrps in g.AV_blocks
            for(var i=0; i<g.AV_blocks.length; i++){
                g.AV_blocks[i].AF_centralize();
            }
            g.AF_sort_blocks();
            g = g.AF_prev_gen();
        }
        return ag;
    };// end of AC_RenderEngine.AF_centralize method
    
    // resolve conflicts
    AC_RenderEngine.prototype.AF_arrange_position = function(){
        var g = last_gen = this.AF_centralize();//last gen with at least 1 block
        var conf = g.AF_next_overlap(), gens = this.AV_fgraph.AV_gens;
        while(g.AF_index() > 0){
            if(!conf){
                g = g.AF_prev_gen();
                conf = g.AF_next_overlap();
            }
            conf = g.AF_resolve_conflict(conf);
        }
        // find y-delta the whole tree should be moved down and do the ymove
        var ydelta = 0, i, sgrp;
        for(i=0; i<= last_gen.AF_index(); i++){
            sgrp = gens[i].AV_blocks[0];
            if(-sgrp.AF_upper() > ydelta) ydelta = -sgrp.AF_upper();
        }
        if(gens[0].AV_blocks[0].cid() === 'BAV'){
            if(gens[0].AV_blocks[0].AF_is_virtual()){//both male/female no-exist
                // ymove the virtual-male/vitual-female
                gens[0].AV_blocks[0].AF_male_pv().AF_ymove(ydelta);
                gens[0].AV_blocks[0].AF_female_pv().AF_ymove(ydelta);
                // move bv. This will also move the sgroup(all the children)
                gens[0].AV_blocks[0].AF_ymove(ydelta);
            } else
                gens[0].AV_blocks[0].AF_anchor_pv().AF_ymove(ydelta);
        } else if (gens[0].AV_blocks[0].cid() === 'PAV')
            gens[0].AV_blocks[0].AF_ymove(ydelta);
    };// end of AC_RenderEngine.prototype.AF_arrange_position method
    
    AC_RenderEngine.prototype.AF_adjust_panesize = function(){
        var y = 0, gens = this.AV_fgraph.AV_gens;
        // get max height-stretch of all gen-columns
        for(var i = 0; i < gens.length; i++){
            var l = gens[i].AV_blocks.length;
            if(l>0 && gens[i].AV_blocks[l-1].AF_lower() > y){
                y = gens[i].AV_blocks[l-1].AF_lower();
            }
        }
        if(y < U.AV_MIN_FGRAPH_HEIGHT)
            y = U.AV_MIN_FGRAPH_HEIGHT + 50;
        else 
            y = y + 50;
            
        // stretch to bigger height if needed
        // save it. after .AF_swap_render_pane(flip) set to this
        this.AV_fgraph.AV_curr_height = y;
        
        // check horizontal (col-counts), make sure pane-width is 50 over it
        this.AV_fgraph.AV_curr_width = 950; // 5x175 + 50=925, set min to 950
        if(gens.length > U.AV_DEF_GEN_NUMBER){
            y = gens.length*U.AV_GEN_L + 50;
            this.AV_fgraph.AV_curr_width = y;
        }
    };// end of AC_RenderEngine.AF_adjust_panesize method
    
    AC_RenderEngine.AF_fadeout = function(divs){
        for(var i = 0; i < divs.length; i++){
            divs[i].fadeOut(500, 
                function(e){
                    U.log(2, i + ' disappeared.');
                }
            );
        }
    };// end of AC_RenderEngine.AF_fadeout
    
    return AC_RenderEngine;
})();