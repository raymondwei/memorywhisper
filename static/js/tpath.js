/*
 * Copyright (C) 2014 Adacro Inc. - All Rights Reserved
 */
var AC_TPath = (function(){
    function AC_TPath(focus_pid){
        this.AV_expands = [];
        this._focus = null; // PAV entity. set after available
        this._vroot = null;
        this._root = null;
        this._centerpa_pair = {};
        this.AF_focus_pid(focus_pid);
        // {<pid>:[pav1,pav2],<bid>:[bav1,bav2],..}
        this.AV_vmap = {}; // {<vmodel.id>: vmodel,...}
    };// end of AC_TPath constructor
    AC_TPath.prototype.cid = function(){ return 'PTH'; }

    AC_TPath.prototype.AF_focus_pid = function(pa){
        if(typeof(pa) === 'undefined'){
            // after rendering, _focus is set return the focus-pv.model
            if(this._focus) return this._focus.model.id();
            // before rendering _focus == null return ths intial set pa
            else return this._focus_pid; 
        }
        this._focus_pid = typeof(pa) === 'object'? pa.id() : pa;
        return this;
    };// end of AC_TPath.prototype.AF_focus_pid
    
    // center-pa means: 
    // 1. its parent-ba>anchor-pa is root, if virtual parents-ba that ba is root
    //    (root is in expands too)
    // 2. parents-ba in expands: all children of center-pa's parent-ba shown
    // 3. center-pa is in expands: all spouses shown
    // 4. all apouses-bas are in expands : all children of center-pa shown
    AC_TPath.prototype.AF_center_pa = function(pa, parent_index){
        parent_index = parent_index? parent_index : 0;
        // getter
        if(typeof(pa) === 'undefined') 
            return this._center_pa;
        // setter
        this._center_pa = pa;
        var ba = M.e(U.AF_trim_adopt_sign(pa.iddict.parents[parent_index]));
        if(ba.AF_is_virtual()){
            this.AF_centerpa_pair({parent: null, centerpa: pa});
            this.AF_root(ba);
        } else {
            this.AF_centerpa_pair({parent: ba, centerpa: pa});
            this.AF_add_expand(ba);
            this.AF_root(ba.male() || ba.female());
        }
        this.AF_add_expand(pa);
        var M1504 = pa.AF_cfg().M1504,
        spouses = pa.spouses();
        // if Spouses (M7517) not in protection or is public(M7117)
        if(!('M7517' in M1504) || M1504.M7517 === 'M7117')
            for(var i=0; i<spouses.length; i++){
                // if both spouses open up theri children
                if(M.AF_fadmin() ||
                   spouses[i].AF_children_permission() === true)
                    this.AF_add_expand(spouses[i]);
            }
        return this;
    };// end of AC_TPath.prototype.AF_center_pa
    
    AC_TPath.prototype.AF_add2vmap = function(vmodel){
        this.AV_vmap[vmodel.id] = vmodel;
        if( vmodel.model && 
            vmodel.model.id() === this.AF_focus_pid())
            this.AF_focus(vmodel);
        return this;
    };// end of AC_TPath.prototype.AF_add2vmap

    AC_TPath.prototype.AF_vroot = function(v){
        if(typeof(v) === 'undefined') return this._vroot;
        this._vroot = v;
        return this;
    };// end of AC_TPath.prototype.AF_vroot
    
    // getter: this.AF_focus() returns the pav on focus; 
    // setter: this.AF_focus(v) will set/switch focus on/to v (pav)
    AC_TPath.prototype.AF_focus = function(v){
        if(typeof(v) === 'undefined') // getter
            return this._focus;
        // v is bringing in a value: pav
        if(this._focus && v.id !== this._focus.id){// not the prev pav?
            this._focus.AF_focused(false);
        }
        this._focus = v; 
        if(!v.AF_is_virtual() || FD.AV_loginpv)
            v.AF_update_lclick_items();
        return this;
    };// end of AC_TPath.prototype.AF_focus
    
    // clone a new path, if focus_pid given, set that as the new focus_pid
    // if not use the old focus_pid
    AC_TPath.prototype.AF_clone = function(focus_pid){
        var AV_new_tpath = new AC_TPath(focus_pid? focus_pid : this.AF_focus_pid());
        AV_new_tpath.AF_root(this.AF_root());
        AV_new_tpath.AV_expands = this.AV_expands.slice();
        AV_new_tpath.AF_vroot(this.AF_vroot());
        var oldpair = this.AF_centerpa_pair();
        // clone (on top level) of centerpa-pair object
        AV_new_tpath.AF_centerpa_pair({parent:   oldpair.parent, 
                                       centerpa: oldpair.centerpa});
        return AV_new_tpath;
    };// end of AC_TPath.prototype.AF_clone
    
    AC_TPath.prototype.AF_root = function(v){
        if(v === null || typeof(v) === 'undefined') 
            return this._root;
        var eid = (typeof(v) === 'object')? v.id() : v;
        this._root = eid;
        this.AF_add_expand(eid);
        return this;
    };// end of AC_TPath.AF_root method+
    
    AC_TPath.prototype.AF_add_expand = function(v){   // v: id/array of ids
        if($.isArray(v)){
            for(var i=0; i<v.length; i++){
                this.AF_add_expand(v[i]);
            }
        } else {
            var x = (typeof(v)==='object')? v.id() : v;
            if(v && (M.cid(x) ==='PA' || M.cid(x) ==='BA')){// PA or BA
                if(this.AV_expands.indexOf(x) === -1){     // and not already in
                    this.AV_expands.push(x);
                }
            }
        }
        return this;
    };// end of AC_TPath.AF_add_expand method
    
    // remove v from path. if AV_propagate, remove all expands down-wards
    AC_TPath.prototype.AF_remove_expand = function(v, AV_propagate){
        // find index of v in .AV_expands
        var ind = (typeof(v) === 'object')? 
            this.AV_expands.indexOf(v.id()) : this.AV_expands.indexOf(v);
        if(ind > -1){
            // remove v from expands
            var eid = this.AV_expands.splice(ind,1)[0];
            if(AV_propagate && M.cid(eid) === 'PA'){
                // if pa's love shrunk, shrink all spouses-bas
                var pa = M.e(eid);
                for(var i=0; i<pa.spouses().length; i++){
                    this.AF_remove_expand(pa.spouses()[i], AV_propagate);
                }
            } else if (AV_propagate && M.cid(eid) === 'BA'){
                // if ba's children not to be shown, shrink all their loves
                var ba = M.e(eid);
                for(var i=0; i<ba.AF_children().length; i++){
                    this.AF_remove_expand(ba.AF_children()[i], AV_propagate);
                }
            }
        }
        return this;
    };// end of AC_TPath.AF_remove_expand method
    
    AC_TPath.prototype.AF_is_exp = function(v){
        if(typeof(v) === 'object') return this.AV_expands.indexOf(v.id()) > -1;
        if(typeof(v) === 'string') return this.AV_expands.indexOf(v) > -1;
        return null;
    };// end of AC_TPath.AF_is_exp method
    
    // remove all id(of vmodel) from this.AV_expands if vmodel is not in the 
    // subtree of AV_rootpv 
    AC_TPath.prototype.AF_trim_expands = function(AV_rootpv, vmodel){
        if(vmodel === AV_rootpv || 
           vmodel.AF_gen().AF_index() > AV_rootpv.AF_gen().AF_index())
            return
        if(this.AF_is_exp(vmodel.model))        // in expands?
            this.AF_remove_expand(vmodel.model);//remove,leave subtree untouched
        if(vmodel.cid() === 'BAV'){
            for(var i=0; i<vmodel.AF_sgroup().AV_siblings.length; i++){
                this.AF_trim_expands(AV_rootpv, vmodel.AF_sgroup().AV_siblings[i]);
            }
        } else if(vmodel.cid() === 'PAV'){
            for(var i=0; i<vmodel.AV_loves.length; i++){
                this.AF_trim_expands(AV_rootpv, vmodel.AV_loves[i]);
            }
        }
        return this;
    };// end of AC_TPath.prototype.AF_trim_expands
    
    AC_TPath.prototype.AF_all_viewables = function(){
        var AV_collection = [];
        for(var k in this.AV_vmap){
            AV_collection.push(this.AV_vmap[k]);
        }
        return AV_collection;
    };// end of AC_TPath.prototype.AF_all_viewables
    
    AC_TPath.AF_find_deltas = function(AV_oldpath, AV_newpath){
        var i = 0, j = 0, eid, index, keeps = [], // temps
        deltas = {
        AV_deletes:[], // ids in AV_newpath.AV_vmap but not in AV_oldpath.AV_vmap
        AV_moves:[],   // array of [nvmodel, m, ovmodel], where nv/ov are pav, bav
                    // in new-/old-path. m.x, m.y are position deltas(old=>new)
        AV_creates:[]},// ids in AV_oldpath.AV_vmap but not in AV_newpath.AV_vmap
        nar = Object.keys(AV_newpath.AV_vmap), // array of vids in AV_newpath
        oar = Object.keys(AV_oldpath.AV_vmap); // array of vids in AV_oldpath

        // first look for direct match
        while(i < oar.length){
            index = nar.indexOf(oar[i]);
            if( index > -1){
                // remove index-th vid from nar, i-th vid from oar
                // put the pair into keeps
                keeps.push([oar.splice(i, 1), nar.splice(index, 1)]); 
            } else 
                i++
        }
        i = 0;
        // look for id match (pav.mode.id() or bav.modelid() )
        while(i< oar.length){
            // old-vid like <pid>_s_<mhost-ba.id>, get <pid>
            eid = oar[i].split('_')[0];
            index = -1;
            for(j=0; j<nar.length; j++){
                if(nar[j].substr(0, eid.length) === eid){
                    index = j;
                    break;
                }
            }
            if(index > -1){
                // remove index-th vid from nar, i-th vid from oar
                // put the pair into keeps                
                keeps.push([oar.splice(i, 1), nar.splice(index, 1)]); 
            } else 
                i++;
        }
        // left-overs in oar are the AV_deletes; left-over in nar are AV_creates
        // AV_moves are in keeps: they need to be procecced into right format
        for(i=0; i<oar.length; i++){ // all in oar are AV_deletes
            deltas.AV_deletes.push(AV_oldpath.AV_vmap[oar[i]]);
        }
        for(i=0; i<nar.length; i++){ // all in nar are AV_creates
            deltas.AV_creates.push(AV_newpath.AV_vmap[nar[i]]);
        }
        var kp, unchanged, m;
        // get AV_moves from keeps 
        // (existing both in AV_oldpath and AV_newpath's vmaps
        for(i=0; i<keeps.length; i++){
            kp = [AV_oldpath.AV_vmap[keeps[i][0]],
                  AV_newpath.AV_vmap[keeps[i][1]]];
            unchanged = kp[0].AF_top() === kp[1].AF_top() && 
                        kp[0].AF_left() === kp[1].AF_left();
            if(kp[0].cid() === 'PAV' && unchanged)
                continue;
            // for BAV, even top/left unchanged, still update(sgroup may change)
            m = {};
            if(kp[0].AF_top() !== kp[1].AF_top())     m.y = kp[1].AF_top();
            if(kp[0].AF_left() !== kp[1].AF_left())   m.x = kp[1].AF_left();
            deltas.AV_moves.push([kp[0], m, kp[1]]);
        }
        
        return deltas;
    };// end of AC_TPath.AF_find_deltas

    /** purpose of centerpa-pair:
     * normally, if a pa and its parents-ba(male/female) are to be all rendered,
     * then the parents-ba is put into path.exps array and all the siblings are
     * rendered. But for case of centerpa, only this pa should be expanded as
     * child, not all the sibling. This is sppecial for centerpa-rendering case.
     * in this case parents-ba is in path.exps array, but is this ba is in
     * path.centerpa-pair(ba is pair.parent),  only center-pa is expanded when 
     * render ba's children; and navel has(+) sign. Otherwise, if ba is in the
     * path.exps array, all children will be expanded, navel has (-) sign.
     */ 
    AC_TPath.prototype.AF_centerpa_pair = function(pair){
        // getter: no parameter given
        if(typeof(pair) === 'undefined') 
            return this._centerpa_pair;
        // setter
        for(var k in pair){ // k can be: parent, centerpa
            this._centerpa_pair[k] = pair[k];
        }
        return this;
    };// ned of AC_TPath.prototype.AF_centerpa_pair

    return AC_TPath;
})();
