﻿/*
 * Copyright (C) 2014 Adacro Inc. - All Rights Reserved
 *-----------------------------------------------------------------------
 * Purpose: managing the categories: add/delete/modify cat of a pa
 */
var AC_CatWizard = (function(){
    function AC_CatWizard(infov){
        this.AV_infov = infov;
        this.AV_actbuffer = FD.AV_actbuffer;
        this.AF_load_template();
    };// end of AC_CatWizard constructor

    AC_CatWizard.prototype.AF_load_template = function(){
        this.AJ_layout = $('<div id="newcat-frame"></div>');
    $('<span id="cat-name-label" class="LS" tag="M7123">Category name</span>')
        .appendTo(this.AJ_layout);
        this.jcatname = $('<select id="cat-name" />').appendTo(this.AJ_layout);
        $('<span id="cat-pk-label" class="LS" tag="M7124">Visible to</span>')
        .appendTo(this.AJ_layout);
        this.jcatpk = $('<select id="cat-pk" />').appendTo(this.AJ_layout);
    $('<span id="cat-descr-label" class="LS" tag="M7504">Description</span>')
        .appendTo(this.AJ_layout);
        this.jcatdescr = $('<textarea id="cat-descr" />');
    };// end of AC_CatWizard.prototype.AF_load_template

    /* ------------------------------------------------------------------------
    // mode can be 1.'new-ip' - when add category button click); and
    // 2.'update-ip' - when cate button context-menu>edit is picked
    */// ---------------------------------------------------------------------
    AC_CatWizard.prototype.AF_popup = function(mode){
        this.mode = mode;
        this.ent = this.AV_infov.pa; // pa or ba for ip-container
        var self = this;
        this.AV_actbuffer.AF_init();
        FD.AV_lang.AF_set_all(this.AJ_layout, this.AV_lcode);
        self.update_selects();
        self.dlg = self.AJ_layout.dialog({
            autoOpen: false,
            modal: true,
            resizable: false,
            width: 440,
            height: 360,
            show: true, // animated appearing
            hide: true, // animated disappearing
            close: function(e){ self.dlg.dialog('destroy'); },
            buttons: {
                'save-button': {
                    id: 'newcat-save-button',
                    text: "Save",
                    click: function(e){
                        if(FD.AF_se()){
                            self.AF_save_cat();
                        }
                        self.dlg.dialog('close');
                        return U.AF_stop_event(e);
                    }
                },
                'cancel-button': {
                    id: 'newcat-cancel-button',
                    text: "Cancel", 
                    click: function(e){ 
                        self.dlg.dialog('close'); 
                        return U.AF_stop_event(e);
                    }
                }
            }
        });
        self.dlg.dialog('open');
        self.dlg.keyup(function(e){
            if(e.keyCode === 13){
                $('#newcat-save-button').trigger('click');
            }
        });
        self.dlg.dialog('widget').attr('id','newcat-popup');
        FD.AV_lang.AF_set_dlg('newcat-popup', {
            '.ui-dialog-title': 'M7141',
            '#newcat-save-button': 'M1004',
            '#newcat-cancel-button': 'M1003'
        })
        return false;
    };// end of AC_CatWizard.AF_popup method
    
    // ------------------------------------------------------------------------
    // Purpose: update the cat-name select options, what cats exist,
    // and what are to be offered.
    // ---------------------------
    AC_CatWizard.prototype.update_selects = function(){
        // clone the complete set from BigButton
        this.avail_cats = AC_BigButton.AV_cat_catalog.slice(0); 
        
        // take what ips already has from avail_cats
        var ips = this.ent.ips();
        for(var i=0; i< ips.length; i++){
            var ind = this.avail_cats.indexOf(ips[i].name());
            if(ind === -1){
                U.log(1, "pa has unknown cat: " + ips[i].name());
            } else {
                this.avail_cats.splice(ind, 1); // away from available cats
            }
        }
        // add possible cats to cat-name select>options
        this.jcatname.empty(); // clear away old options
        for(var i=0; i<this.avail_cats.length; i++){
            this.jcatname.append($('<option>',{
                value: this.avail_cats[i],
                text: FD.AV_lang.face(this.avail_cats[i])
            }));
        }
        // add "private","family" and "public" to cat-pk select>options
        this.jcatpk.empty();    // clear away old options
        this.jcatpk.append( // M7117: public
            $('<option>',{value: 'M7117', text: FD.AV_lang.face('M7117')}));
        this.jcatpk.append( // M7118: private
            $('<option>',{value: 'M7118', text: FD.AV_lang.face('M7118')}));
        this.jcatpk.append( // M1513: password protection
            $('<option>',{value: 'M1513', text: FD.AV_lang.face('M1513')}));
        this.ip_count = this.ent.ips.length;
    };// end of AC_CatWizard.update_selects method
    
    AC_CatWizard.prototype.collect_ip = function(){
        var ind = this.ent.ips().length;
        var ttl = {};
        ttl[FD.AF_lcode()] = this.jcatdescr.val();
        var ip = new IP({'_id':M.mid(this.ent.fid(),'IP'),
            'name': this.jcatname.val(),
            'title': ttl,
            'pk':this.jcatpk.val()});
        ip.iddict['owner'] = this.ent.id();
        ip.iddict['ownerpa'] = this.ent.id();
        this.AV_actbuffer.AF_add_save(ip);
        var iddict = $.extend(true, {},this.ent.iddict);
        iddict['ips'].push(ip.id());
        this.AV_actbuffer.AF_add_update(this.ent, {'iddict': iddict});
    };// end of AC_CatWizard.collect_ip method
    
    AC_CatWizard.prototype.AF_handle_success = function(data, self){
        this.AV_actbuffer.AF_execute();
        self.AV_infov.AF_add_cat_button();
    };// end of AC_CatWizard.prototype.AF_handle_success
    
    AC_CatWizard.prototype.AF_handle_err = function(data){
        alert(data);
    };// end of AC_CatWizard.prototype.AF_handle_err

    AC_CatWizard.prototype.AF_save_cat = function(){
        if(this.mode === "new-ip")
            this.collect_ip();
        var req_dict = this.AV_actbuffer.AF_get_reqdic(this.mode);
        FD.AV_lserver.AF_post('putdocs', req_dict, this);
    };// end of AC_CatWizard.AF_save_catupdate method

    return AC_CatWizard;
})();