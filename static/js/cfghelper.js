﻿// A collection (table/tr) of k/v represent pa.config-fc['contdict']
// in CFGForm, the contdict can be modified by the user
var AC_KVPair = (function(){
    function AC_KVPair(AV_theform, k, v){
        this.form = AV_theform;
        var self = this;
        this.k = k;
        this.orig_v = v;
        this.AJ_table = AV_theform.AJ_table;
        this.jtr = $('<tr class="new-row"></tr>');
        
        var col1 = $('<td class="cfc-form-col1"></td>').appendTo(this.jtr);
        var col2 = $('<td class="cfg-form-col2"></td>').appendTo(this.jtr);
        this.AJ_key = $('<span class="LS" tag="'+k+'"/>').appendTo(col1);
        switch(k){
        case 'M1504': // password
            this.AJ_key.text(FD.AV_lang.face(['M1001',k])); // login pwd
            var msg = '<input type="password" class="password-input"/>'
            this.AJ_value=$(msg).appendTo(col2).css('background-color','white');
            this.AJ_value.val('').attr('placeholder','******');
            this.AJ_value.unbind('input').bind('input', function(e){
                self.AF_validate_input(self.AJ_value.val());
            });
            break;
        case 'M1502': // M1502: Email
            this.AJ_key.text(FD.AV_lang.face(k));
            this.AJ_value = $("<input class='form-input-txt' />")
                .appendTo(col2).val(v);
            this.AJ_value.css('background-color','white');            
            this.AJ_value.unbind('input').bind('input', function(e){
                self.AF_validate_input(self.AJ_value.val());
            });
            break;
        case 'M1508': // (default/preferred) language
            this.AJ_key.text(FD.AV_lang.face(k));
            this.langimg = $('<img class="language-picker"></img>')
                .appendTo(col2);
            this.langimg.attr('src', FD.AV_lang.AF_icon_filepath(v));
            this.langimg.languagePicker(self.AF_language_pick, self);
            break;
        case 'M7518': // Children, M7109(Exposure)
            this.AJ_key.text(FD.AV_lang.face(["M7518","M7109"]));
            this.lockrow = new AC_CSALock(this.form, k, col2);
            break;
        case 'M7517': // Spouses. M7109(Exposure)
            this.AJ_key.text(FD.AV_lang.face(["M7517", "M7109"])); 
            this.lockrow = new AC_CSALock(this.form, k, col2);
            break;
        case 'M7516': // Ancestry, M7109(Exposure)
            this.AJ_key.text(FD.AV_lang.face(["M7516","M7109"])); 
            this.lockrow = new AC_CSALock(this.form, k, col2);
            break;
        case 'M7519': this.AJ_key.text(FD.AV_lang.face(k));//M7519: color theme
          this.AJ_value=$('<select id="theme-select"></select>').appendTo(col2);
    $("<option>"+FD.AV_lang.face('M1518')+FD.AV_lang.face('M7519')+"</option>")
                .appendTo(this.AJ_value).val("default");
            $("<option>"+FD.AV_lang.face('M7519')+" 1</option>")
                .appendTo(this.AJ_value).val("th1");
            $("<option>"+FD.AV_lang.face('M7519')+" 2</option>")
                .appendTo(this.AJ_value).val("th2");
            $("<option>"+FD.AV_lang.face('M7519')+" 3</option>")
                .appendTo(this.AJ_value).val("th3");
            $("<option>"+FD.AV_lang.face('M7519')+" 4</option>")
                .appendTo(this.AJ_value).val("th4");
            this.AJ_value.val(v);
            var thm, src;
            src = AC_BigButton.AV_themes[v]['fpane-bg'];
            this.form.AJ_table.css('background-image', 'url('+src+')');
            this.AJ_value.unbind('input').bind('input', function(e){
                thm = self.AJ_value.val();
                self.form.AV_cfgdic[k] = thm;
                src = AC_BigButton.AV_themes[thm]['fpane-bg'];
                self.form.AJ_table.css('background-image', 'url('+src+')');
                self.form.AF_update_delta(['M7519']);
            });
            break;
        case 'M7534': this.AJ_key.text(FD.AV_lang.face(k));//M7534: trustee
            break;
        default: 
            this.AJ_key.text(FD.AV_lang.face(k));
            break;
        }// switch
        
        this.form.AJ_table.find('tr.button-row').before(this.jtr);
    };// end of AC_KVPair constructor
    
    // change of personal language choice for M1508(language preference)
    AC_KVPair.prototype.AF_language_pick = function (lcode, self) {
        self.langimg.attr('src', FD.AV_lang.AF_icon_filepath(lcode));
        self.form.AV_cfgdic.M1508 = lcode;
        self.form.AF_update_delta(['M1508']);
    };// end of AC_KVPair.prototype.AF_language_pick

    // validate input. if valid, call  which
    // will set actbuffer and also activate [OK] button.
    AC_KVPair.prototype.AF_validate_input = function(val){
        if(this.k === 'M1504'){ // login password, in M1504 - a dict
            // must be more than 2 chars, no * in it
            if(val.length < 3 || val.indexOf('*') !== -1){
                this.AJ_value.css('background-color','#E8ADAA');//pink
                this.form.AV_cfgdic.M1504['login'] = // del change
                    this.form.AV_orig.M1504['login']
            } else {
                this.AJ_value.css('background-color','white');
                this.form.AV_cfgdic.M1504['login'] [val,"",""];
            }
            this.form.AF_update_delta(['M1504']);
        } else if(this.k === 'M1502'){ // email - string
            if(!U.AF_validateEmail(val)){
                this.AJ_value.css('background-color','#E8ADAA');//pink
                this.form.AV_cfgdic.M1502 = this.form.AV_orig.M1502;// to orig
            } else {
                this.AJ_value.css('background-color','white');
                this.form.AV_cfgdic['M1502'] = val;
            }
            this.form.AF_update_delta(['M1502']);
        }
    };// end of AC_KVPair.prototype.AF_validate_input
    
    return AC_KVPair;
})();

// Children/Spouses/Ancestry(CSA) -lock
var AC_CSALock = (function(){
    function AC_CSALock(AV_theform, key, jline){
        this.form = AV_theform;
        this.pa = AV_theform.pa;
        this.AV_cfgdic = AV_theform.AV_cfgdic;
        this.key = key;
        this.AJ_line = jline;
        
        this.AJ_pksel = $("<select class='lock-pk-sel' />").appendTo(jline);
        $("<option>",{value: 'M7117', text:FD.AV_lang.face('M7117')})
            .appendTo(this.AJ_pksel);  // add Public
        $("<option>",{value: 'M7118', text:FD.AV_lang.face('M7118')})
            .appendTo(this.AJ_pksel); // add "Private"
        $("<option>",{value: 'M1513', text:FD.AV_lang.face('M1513')})
            .appendTo(this.AJ_pksel);  // add pwd protection
            
        this.AJ_pwd = $("<input type='password' class='lock-pwd-input'/>")
            .appendTo(jline).attr('placeholder',FD.AV_lang.face('M1504'));

        this.AJ_expdate = $("<input type='text' class='lock-exp-date-input' />")
            .appendTo(jline).attr('placeholder', FD.AV_lang.face('M6004'));
        this.jpostpk_label = $("<span class='post-exp-pk-label'/>")
            .text(FD.AV_lang.face('M1515')).appendTo(jline);
        // post expiry pk can only be public(M7117) or private(M7118)
        this.jpostpk = $("<select class='post-exp-pk'/>").appendTo(jline);
        $("<option>",{value:'M7117', text: FD.AV_lang.face('M7117')})
            .appendTo(this.jpostpk);
        $("<option>",{value:'M7118', text: FD.AV_lang.face('M7118')})
            .appendTo(this.jpostpk);
        
        this.AJ_expdate.datepicker({dateFormat: "yy-mm-dd", // 2013-04-17
            changeYear: true,
            yearRange: "+0:+0", 
            changeMonth: true,
            regional: FD.AF_lcode()
        });
        if(key in this.AV_cfgdic.M1504){
            if($.isArray(this.AV_cfgdic.M1504[key])){ // array can be only for
                // pwd-protected: [<pwd/******>,<exp-date>,<post-exp-pk>]
                this.AJ_pksel.val('M1513');          
                this.AJ_pwd.val(this.AV_cfgdic.M1504[key][0]); // show pwd(****)
                this.AJ_expdate.val(this.AV_cfgdic.M1504[key][1]); //exp-date
                this.AV_cfgdic.M1504[key][2]? 
                    this.jpostpk.val(this.AV_cfgdic.M1504[key][2]) :
                    this.jpostpk.val('M7117');
            } else {// in M1504, but not an array - M7118 or M7117. Show it
                this.AJ_pksel.val(this.AV_cfgdic.M1504[key]);
            }
        } else {    // not in M1504 - it is public
            this.AJ_pksel.val('M7117');
        }
        this.AF_set_visibilities(this.AJ_pksel.val()).AF_setup_handlers();
    };// end of AC_CSALock constructor

    // pwd/***, exp-date and post-exp-pk are show only for M1513
    // initial call pk not given: take from orig val - set initial visibilities.
    // when used in handlers, pk is from jpksel.val() - update visibilities
    AC_CSALock.prototype.AF_set_visibilities = function(pk){
        if(pk === 'M7117' || pk === 'M7118'){ // public or private
            this.AJ_pwd.val('').css('visibility','hidden');
            this.AJ_expdate.val('').css('visibility','hidden');
            this.jpostpk_label.css('visibility','hidden');
            this.jpostpk.val('').css('visibility','hidden');
        } else { // M1513/pwd-protection
            this.AJ_pwd.css('visibility','visible');
            if($.trim(this.AJ_pwd.val()).length === 0){
                this.AJ_pwd.css('background-color','pink');
                this.form.AF_enable_save_btn(true);
                this.AJ_expdate.val('').css('visibility','hidden');
                this.jpostpk_label.css('visibility','hidden');
                this.jpostpk.val('').css('visibility','hidden');
            } else {
                this.AJ_pwd.css('background-color','white');
                this.form.AF_enable_save_btn(false);
                this.AJ_expdate.css('visibility','visible')
                    .val(U.AF_date_dashed(this.AV_cfgdic.M1504[this.key][1]));
                if(this.AV_cfgdic.M1504[this.key][1].length > 0){
                    this.jpostpk_label.css('visibility','visible');
                    this.jpostpk.val(this.AV_cfgdic.M1504[this.key][2])
                        .css('visibility','visible');
                } else {
                    this.jpostpk_label.css('visibility','hidden');
                    this.jpostpk.val('').css('visibility','hidden');
                }
            }
        }
        return this;
    };// end of AC_CSALock.prototype.AF_set_visibilities
    
    AC_CSALock.prototype.AF_setup_handlers = function(){
        var self = this, nowstr = TS(true);
        this.AJ_pksel.unbind('change').bind('change', function(e){
            if(self.AJ_pksel.val() === 'M7117' || 
               self.AJ_pksel.val() === 'M7118'){
                self.form.AV_cfgdic.M1504[self.key] = self.AJ_pksel.val();
            } else { // M1513
                self.form.AV_cfgdic.M1504[self.key] = [
                        self.AJ_pwd.val(), 
                        U.AF_date_undashed(self.AJ_expdate.val()),
                        self.jpostpk.val()];
            }
            self.AF_set_visibilities(self.AJ_pksel.val())
                .form.AF_update_delta(['M1504']);
        });
        self.AJ_pwd.unbind('input').bind('input',function(e){
            if(self.AJ_pwd.val().length === 0){
                self.jpostpk.val('');
                self.AJ_expdate.val('');
                self.jpostpk.val('');
            }
            self.form.AV_cfgdic.M1504[self.key] = [self.AJ_pwd.val(),
                                   U.AF_date_undashed(self.AJ_expdate.val()),
                                   self.jpostpk.val()? self.jpostpk.val() : ''];
            self.AF_set_visibilities(self.AJ_pksel.val())
                .form.AF_update_delta(['M1504']);
        });
        self.AJ_expdate.unbind('change').bind('change',function(e){
            var dstr = $.trim(U.AF_date_undashed(self.AJ_expdate.val()));
            if(dstr === '' || dstr <= nowstr){ // not in the future?
                self.form.AV_cfgdic.M1504[self.key][1] = '';
                self.form.AV_cfgdic.M1504[self.key][2] = '';
            } else {
                self.form.AV_cfgdic.M1504[self.key][2]? 
                    self.jpostpk.val(self.M1504[self.key][2]) : 
                    self.jpostpk.val('M7117');
            }
            self.form.AV_cfgdic.M1504[self.key] = [
                self.AJ_pwd.val(), dstr, self.jpostpk.val()];
            self.AF_set_visibilities(self.AJ_pksel.val())
                .form.AF_update_delta(['M1504']);
        });
        self.jpostpk.unbind('change').bind('change',function(e){
            self.form.AV_cfgdic.M1504[self.key][2] = self.jpostpk.val();
            self.AF_set_visibilities(self.AJ_pksel.val())
                .form.AF_update_delta(['M1504']);
        });
    };// end of AC_CSALock.prototype.AF_setup_handlers
    
    return AC_CSALock;
})();