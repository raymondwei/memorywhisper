/*
 * Copyright (C) 2014 Adacro Inc. - All Rights Reserved
 * ------------------------------------------------------
 * jQuery plugin for Pretty looking right click context menu.
 */
jQuery.fn.languagePicker = function(callback,ctx) {
    this.callback = callback;
    this.ctx = ctx;
    var self = this, i,
        items = [], lcs = FD.AV_lang.AF_all_lcodes();
    for(i = 0; i<lcs.length; i++){
        items.push({icon: FD.AV_lang.AF_icon_filepath(lcs[i]), lcode: lcs[i]});
    }    
    // Define default settings
    var settings = {
        contextMenuClass: 'contextMenuPlugin',
        gutterLineClass: 'gutterLine',
        headerClass: 'header',
        seperatorClass: 'divider',
    };

    // Build popup menu HTML
    function createMenu(e) {
        // create a ul with a div in it, append it to doc-window
        var menu = $('<ul class="'+settings.contextMenuClass+'"><div class="'+ 
            settings.gutterLineClass + '"></div></ul>').appendTo(document.body);
        // loop thru items
        items.forEach(function(item) {
            if(item){
                var rowCode='<li><a href="#"><span class="LS"></span></a></li>';
                var row = $(rowCode).appendTo(menu);
                if(item.icon){
                    $('<img class="lang-picker-list-item">')
                        .attr('src', item.icon)
                        .insertBefore(row.find('span'));
                }
                row.find('span').attr('tag', item.lcode);
                if (self.callback) {
                    row.find('a').click(function(){ 
                        $('ul.contextMenuPlugin').remove();
                        $('div.contextMenuPluginShield').remove();
                        self.callback(item.lcode,self.ctx); 
                    });
                }
            } else {
                $('<li class="' + settings.seperatorClass + '"></li>')
                    .appendTo(menu);
            }
        });
        menu.find('.' + settings.headerClass ).text(settings.title);
        FD.AV_lang.AF_set_all(menu);
        return menu;
    }

    //this.live('click', function(e) {
    this.bind('click', function(e) {
        var menu = createMenu(e).show();
        /* nudge to the right, so the pointer is covering the title */
        var left = e.pageX + 5, 
        top = e.pageY;
        if (top + menu.height() >= $(window).height()) {
            if (top > 450) {
                top = Math.max(40, top - menu.height());
            }
            else top = 40;
        }
        if (left + menu.width() >= $(window).width()) {
            left -= menu.width();
        }

        // Create and show menu
        menu.css({zIndex:1000001, left:left, top:top})
            .bind('contextmenu', function() { return false; });

        // Cover rest of page with invisible div that when clicked will cancel 
        // the popup.
        var bg = $('<div class="contextMenuPluginShield"></div>')
            .css({left:0, top:0, width:'100%', height:'100%', 
                position:'absolute', zIndex:1000000})
            .appendTo(document.body)
            .bind('contextmenu click', function() {
                // If click/right click anywhere else on page:remove/clean up.
                bg.remove();
                $('ul.contextMenuPlugin').remove();
                $('div.contextMenuPluginShield').remove();
                menu.remove();
                return false;
            });
        // When clicking on a link in menu: clean up 
        // (in addition to handlers on link already)
        menu.find('a').click(function() {
            bg.remove();
            menu.remove();
        });
        // adjust size and position
        $('ul.contextMenuPlugin li').css('height','22px');
        $('ul.contextMenuPlugin img').css('width','20px');
        $('ul.contextMenuPlugin span')
         .css({position: 'relative', bottom: '1px'});
        // Cancel event, so real browser popup doesn't appear.
        return false;
    });
    return this;
};