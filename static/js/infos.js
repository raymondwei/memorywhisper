/* Copyright (C) 2014 Adacro Inc. - All Rights Reserved
 * ----------------------------------------------------
 * For visuals used in infos div: AC_InfosVModel is for the cat-pane where 
 * categories are shown as big buttons. Whne any of the cat clicked, the 
 * AC_ItemPane is used for showing the content of that cat chosen.
 ----------------------------------------------------------------------------*/
var AC_InfosVModel = (function(){
    function AC_InfosVModel(AJ_ppane){
        this.AJ_ppane = AJ_ppane;
        this.AJ_div = $('<div id="infos"></div>').appendTo(AJ_ppane);
        this.AJ_left = $('<div class="left"></div>')
            .appendTo(this.AJ_div).css('width','0px');
        this.AJ_right = $('<div class="right"></div>')
            .appendTo(this.AJ_div).css('width','0px');
        this.AV_docpane = new AC_DocPane(FD);
        this.AV_itempane = new AC_ItemPane(this);
        this.big_buttons = [];
        this._show_state = 'hidden';//person_pane show-state: hidden | shown
    };// end of AC_InfosVModel constructor

    // get/set personal_pane show state: 'hidden' | 'shown'.
    // v='shown': show left-pane(category-pane), not right(item)-pane 
    // 'hidden': hide away the ppane. 
    AC_InfosVModel.prototype.AF_show_state = function(v){
        if(typeof(v) === 'undefined') // getter
            return this._show_state;
        if(v === this._show_state)    // setter
            return this;              // nothing to do
        if(['hidden','shown'].indexOf(v) > -1)
            this._show_state = v;
        var width = parseInt(this.AJ_ppane.css('width'));
        if(v === 'hidden' && width > 100){ // U.AV_PPANE_WIDTH (254)
            this.AJ_ppane.animate({width: '0px'}, U.AV_animate_duration);
            this.AV_title.AF_reset(); // back to M7000
        } else if(v === 'shown' && width === 0){
            this.AJ_ppane.animate({width: U.AV_PPANE_WIDTH+'px'}, 
                                  U.AV_animate_duration);
        }
        return this;
    };// end of AC_InfosVModel.prototype.AF_show_state

    AC_InfosVModel.prototype.AF_set_tooltip = function(){
        //<div.itembox> is the item-bar. its parent(li) has it-id. get that
        $('div.itembox', this.AJ_div).each(function(i, item){
            var it_id = $(this).parent().attr('id').split('_')[0];
            // it.title_face() is the current-lcode title-string.
            $(this).attr('title',M.e(it_id).title_face());
        });
        $('span.LS', this.AJ_div).each(function(i, item){
            U.AF_short_it_name($(this));
        })
    };// end of AC_InfosVModel.prototype.AF_set_tooltip

    // catpane is left div where big cat-buttons are shown
    AC_InfosVModel.prototype.AF_show_leftpane = function(newpa){
        if(this.viewchoice === "left") return this;
        this.AV_title.AF_reset("M7100"); //'Category'
        this.AF_clear_bigbuttons().AJ_left.empty();
        if(!this.viewchoice){ // initial state: animation done by FD.toggle
            this.AJ_left.css('width','290px');
            this.AJ_right.css('width','0px');
        } else { // it was "right"
            // from item-pane (div.right), turning to cat-pane(div.left)
            this.AJ_left.animate({width:'290px'}, U.AV_animate_duration); 
            this.AJ_right.animate({width:'0px'}, U.AV_animate_duration);  
        }
        this.viewchoice = "left";   // set the view choice
        this.AF_add_cat_button();       // add all ips(cats) of the pa
        var self = this;
        this.AV_title.AJ_back2left.unbind('click').bind('click', function(e){
            // let ppane vanish; parchive-title to initial state
            self.AF_show_state('hidden'); 
            return U.AF_stop_event(e);
        })
    };// end of AC_InfosVModel.AF_show_leftpane method
    
    // it is now in cat(left)-pane, now turn to show item(right)-pane, 
    // fill it from ip
    AC_InfosVModel.prototype.AF_show_rightpane = function(ip){
        var self = this;
        this.AV_itempane.AF_update_ipv(ip);
        this.AV_title.AJ_text.attr('tag', ip.name())
            .text(FD.AV_lang.ls.face(ip.name()));
        if(this.viewchoice === "left"){
            this.AJ_left.animate({width:'0px'}, U.AV_animate_duration);
            this.AJ_right.animate({width:'290px'}, U.AV_animate_duration);
        } else {
            this.AJ_left.css('width','290px');
            this.AJ_right.css('width','0px');
        }
        this.AV_title.AJ_back2left.unbind('click').bind('click', function(e){
            self.AV_itempane.AF_clear();
            self.AF_show_leftpane();
        })
        this.viewchoice = "right";
    };// end of AC_InfosVModel.prototype.AF_show_rightpane
    
    // when add-allowed, add the action button(s) (Reister new, New category)
    AC_InfosVModel.prototype.AF_action_buttons = function(){
        // if action-button exists, remove it
        if( this.big_buttons.length > 0 &&
            this.big_buttons[this.big_buttons.length-1].usage === 1){
            var btn = this.big_buttons.pop();
            btn.AF_destroy();
        }
        if(M.OA(this.pa)){
            this.big_buttons.push(new AC_BigButton(this,'new ip'));
        }
        return this;
    };// end of AC_InfosVModel.AF_action_buttons method
    
    AC_InfosVModel.prototype.AF_clear_bigbuttons = function(){
        for (var i=0; i<this.big_buttons.length; i++){
            this.big_buttons[i].AF_destroy();
        }
        this.big_buttons = [];
        return this;
    };// end of AC_InfosVModel.AF_clear_bigbuttons method
    
    // ------------------------------------------------------------------------
    // Purpose: add a cat-button for ip. If ip==null, add for all this.pa.ips
    // if ip.AF_index() is not the last, remove all buttons and re-add them all
    // so the order is along the ips index
    // --------
    AC_InfosVModel.prototype.AF_add_cat_button = function(ip){
        // make sure last button is dummy button
        if( this.big_buttons.length == 0 ||
            !this.big_buttons[this.big_buttons.length-1].usage){
            this.big_buttons.push(new AC_BigButton(this,'new ip'));
        }
        if (ip){
            var btn_ind = this.higher_ent_ind(ip.AF_index());
            var btn = new AC_BigButton(this, 'ip name', ip);
            this.big_buttons.splice(btn_ind, 0, btn);
        } else { // ip == null: add all ip buttons, if visible(accessible)
            if(this.pa){
                this.AF_clear_bigbuttons();
                var ips = this.pa.ips();
                for(var i=0; i<ips.length; i++){
                    // when !M.OA()only not PV are shown
                    if(ips[i].pk() != 'M7118' || M.OA(ips[i].owner())){
                        // ip.ipv will assigned to ItemPane when clicked
                        ips[i].ipv = null; 
                        var btn = new AC_BigButton(this, 'ip name', ips[i]);
                        this.big_buttons.push(btn);
                    }
                }
            }
        }
        this.AF_action_buttons();
        // add all buttons to div container
        for(var i=0; i<this.big_buttons.length; i++){
            this.big_buttons[i].AF_append2div(this.AJ_left);
        }
    };// end of AC_InfosVModel.AF_add_cat_button emthod
    
    /* ------------------------------------------------------------------------
    // when a name tag on f-graph is clicked upon, the personal info-pane,
    // including left-pane and right-pane need to be initialized. 
    // Also, if the FD.AV_loginpv is the trustee of pa, cat-wizard is active
    // when a bv is clicked, info-pane shows - nothing (TBD)
    */// -------------------------------------------------------
    AC_InfosVModel.prototype.AF_init = function(pa, AV_title){
        this.AJ_right.empty();
        this.AJ_left.empty();  // with no cat-button
        this.viewchoice = null;
        this.AV_title = AV_title;
        this.pa = pa;
        // new pa: update buttons even if viewchoice is already cat-pane
        this.AF_show_state('shown').AF_show_leftpane();

        // if FD.AV_loginpv is a trustee of pa, then cat-wizard = active
        if(M.OA(pa)){
            this.catwizard = new AC_CatWizard(this);
        } else if(this.catwizard){
            this.catwizard = null;
        }
        return this;
    };// end of AC_InfosVModel.AF_init method

    return AC_InfosVModel;
})(); // end of class AC_InfosVModel
