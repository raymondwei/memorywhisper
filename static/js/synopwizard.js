/* Copyright (C) 2014 Adacro Inc. - All Rights Reserved
 * ----------------------------------------------------------------------------
 * class for handling 5 modes:
 * M7129/update-pa: editing pa under focus. trigger: synop-box edit-icon
 * M7132/add-spouse:add new pa as spouse. trigger: pav>ctx-menu "add spouse"
 * M7133/add-child:add new pa as child. trigger bav>ctx-menu "add child"
 * M7134/add-female:fill in a new pa in unknown(virtual) parent(female) of a
 *    login pa. trigger: synop-box edit icon, after virtual pav is in focus
 * M7135/add-male:fill in a new pa in unknown(virtual) parent(male) of a
 *    login pa. trigger: synop-box edit icon, after virtual pav is in focus
 * ----------------------------------------------------------------------------
 */
var AC_SynopWizard = (function(){
    function AC_SynopWizard(synop){
        this.AV_synop = synop;
        this.pa = null;
        this.AV_lcode = FD.AF_lcode();
        this.AJ_lcpicker_img = $('<img class="language-picker"></img>');
        var self = this;
        if(!self.AJ_layout){
            $.get("/dlgs/sywz.html",'',
                function(data){
                    self.AJ_layout = $(data).filter('div#the-sywz');
                    self.AJ_picframe = self.AJ_layout.find('div#sywz-picframe');
                    self.AJ_male = self.AJ_layout.find('#synwzd-male-radio');
                    self.AJ_female =self.AJ_layout.find('#synwzd-female-radio');
                    self.AJ_tname = self.AJ_layout.find('input#sywz-name');
                    self.jdob = self.AJ_layout.find('input#sywz-dob');
                    self.jdod = self.AJ_layout.find('input#sywz-dod');
                    self.jpob = self.AJ_layout.find('input#sywz-pob');
                    self.jpod = self.AJ_layout.find('input#sywz-pod');
                    self.AJ_nutshell = self.AJ_layout.find('#sywz-nutshell');
                    self.AJ_img = self.AJ_layout.find('#portrait-picture');
                    self.AJ_adpin = self.AJ_layout.find('#sywz-adp-in');
                    self.AJ_adpout = self.AJ_layout.find('#sywz-adp-out');
                    self.AJ_file_input = self.AJ_layout.find(
                        'input#the-file-input');
                    self.AJ_addbtn = self.AJ_layout.find('div#add-pic-btn');
            });
        }
    };// end of AC_SynopWizard constructor
    AC_SynopWizard.prototype.cid = function(){ return 'SYW';}

    AC_SynopWizard.prototype.AF_lang_pick = function(lcode, self){
        self.AJ_lcpicker_img.attr('src',FD.AV_lang.AF_icon_filepath(lcode));
        self.AV_lcode = lcode;
        if(lcode in self.pa.tname()){
            self.AJ_tname.val(self.pa.tname_face(self.AV_lcode));
        }
        if(lcode in self.pa.pob()){
            self.jpob.val(self.pa.pob_face(self.AV_lcode));
        }
        if(lcode in self.pa.pod()){
            self.jpod.val(self.pa.pod_face(self.AV_lcode));
        }
        if(lcode in self.td.content()){
            self.AJ_nutshell.val(self.td.AF_content_face(self.AV_lcode));
        }
        self.AF_update_value({
            'tname': self.AJ_tname.val(),
            'pob':   self.jpob.val(),
            'pod':   self.jpod.val(),
            'nutshell': self.AJ_nutshell.val()
        });
    };// end of AC_SynopWizard.prototype.AF_lang_pick

    // fill boxes with pa's data. pa can be a newly created pa, or pa 2b edited
    AC_SynopWizard.prototype.AF_fillin_pa = function(){
        // fill in portrait pic
        var rs = this.pa.portrait();         // get portrait rs
        if(!rs || !rs.signature()){     // no rs or pic-url non-existing
            this.AJ_img.hide();           // hide img
        } else {                        // portrait/url exists
            // fill in the url in img
            this.AJ_img.appendTo(this.AJ_picframe.empty()).show();
            AC_BinObjMgr.AF_feed_media(this.AJ_img, rs);
        }
        if(this.pa.nutshell()){
            this.td = this.pa.nutshell();
            this.AJ_nutshell.val(this.td.AF_content_face(this.AV_lcode));
        } else {
            // in case pa.nutshell td missing, create it,add to pa/actbuffer
            this.td = new TD({'_id':M.mid(this.pa.fid(),'TD'),'name': 'M7111'});
            this.td.AF_add_holder(this.pa.id());
            this.AV_actbuffer.AF_add_save(this.td);
            this.AF_update_value({newtd: this.td.id()});
        }
        // upload + new picture
        var btn_face = FD.AV_lang.face('M7502') +' '+ FD.AV_lang.face('M6017');
        this.AJ_addbtn.text(btn_face);
        
        // setting sex-radio button to pa's sex
        if(this.pa.sex() === "male"){
            this.AJ_male.prop("checked",true);
            this.AJ_female.prop("checked",false);
        } else {
            this.AJ_female.prop("checked",true);
            this.AJ_male.prop("checked",false);
        }
        // if tag-name empty, red-frame it, disable OK button; if not, reverse
        this.AJ_tname.val(this.pa.tname_face(this.AV_lcode));
        if(!this.AJ_tname.val()){
            this.AJ_tname.removeClass('txt').addClass('needy');
            $('#the-sywz-ok-button')
                .attr("disabled", true).addClass("ui-state-disabled");
        } else {
            this.AJ_tname.removeClass('needy').addClass('txt');
            $('#the-sywz-ok-button').attr("disabled", false)
                .removeClass("ui-state-disabled");
        }
        this.jdob.val(U.AF_date_dashed(this.pa.dob()));
        this.jdod.val(U.AF_date_dashed(this.pa.dod()));
        this.jpob.val(this.pa.pob_face(this.AV_lcode));
        this.jpod.val(this.pa.pod_face(this.AV_lcode));

        this.AJ_adpin.prop("checked", this.AV_synop.pav.AF_adp() === '+');
        this.AJ_adpout.prop("checked", this.AV_synop.pav.AF_adp() === '-');
    }// end of AC_SynopWizard.prototype.AF_fillin_pa
    
    // ------------------------------------------------------------------------
    // Purpose: populate this.AJ_layout's input fields with pa's data (tname,
    // dob, dod, nutshel)
    AC_SynopWizard.prototype.AF_prepare_pa = function(){
        // this.mode can be: M7129(edit-pa), 
        // M7132(add-spouse), M7133(add-child) or
        // M7134(add female), M7135(add male)
        if(this.mode === 'M7129') // updating existing pav
            this.pa = this.AV_synop.pav.model;
        this.AF_setup_change_handlers();
        // M7129: update pa; M7134: add female; M7135: add male
        if(['M7134','M7135'].indexOf(this.mode) > -1){
            // M7134/7135. pav is virtual, make a new pa for the that
            var sex = this.mode=='M7135'? 'male' : 'female';
            this.pa = M.AF_create_pa(this.mode, 
                    this.itor, // mhosting bav
                    {sex: sex, 
                        trustee:[FD.AV_loginpv.model.id()]},
                    this.AV_actbuffer);
        } else if(this.mode === 'M7132' || this.mode === 'M7133'){
            // M7133/add child, itor:bav, or M7132/add spouse, itor: pav
            var sex = this.itor.cid() === 'PAV'?
                U.AF_osex(this.itor.sex()) : //add-spouse: op-sex of hosting pav
                'male';                   // ad child: default: male
            this.pa = M.AF_create_pa(this.mode, 
                    this.itor, // PAV(add spouse) or BAV(add child)
                    {sex: sex, 
                     trustee:[FD.AV_loginpv.model.id()]}, 
                    this.AV_actbuffer);
        }
        this.AF_fillin_pa();
        // if sex can be modified
        if(this.mode !== 'M7133'   && // NOT adding a child, AND
           !(this.mode === 'M7129' && // NOT(update pa AND pa has no spouse)
             this.pa.iddict.spouses.length === 0)){ // sex can't be changed
            this.AJ_male[0].disabled = true;
            this.AJ_female[0].disabled = true;
        } else {
            // for M7133(add child) or M7129 with no spouse: sex changeable
            this.AJ_male[0].disabled = false;
            this.AJ_female[0].disabled = false;
        }
        // make datepicker widgets
        this.jdob.datepicker({dateFormat: "yy-mm-dd", // 2013-04-17
                         changeYear: true, 
                         yearRange: "-100:+0",// current year-100 to now
                         changeMonth: true
        });
        this.jdod.datepicker({dateFormat: "yy-mm-dd", // 2013-04-17
                         changeYear: true, 
                         yearRange: "-100:+0",  // current year-100 to now
                         changeMonth: true
        });        
    }// end of AC_SynopWizard.prototype.AF_prepare_pa    
    // ------------------------------------------------------------------------
    // set up event handlers for all input fields(for M7129): changes against 
    // pa will put update in actbuffer. For non-M7129, only setup for tname
    // input: empty input-box will have red-frame and OK button disabled.
    // -------------------------------------------------------
    AC_SynopWizard.prototype.AF_setup_change_handlers = function(){
        var self = this;
        // if tag-name empty, red-frame it, disable OK button; if not, reverse
        self.AJ_tname.unbind('input').bind('input',function(e){
            if(!self.AJ_tname.val()){
                self.AJ_tname.removeClass('txt').addClass('needy');
                $('#the-sywz-ok-button')
                    .attr("disabled", true).addClass("ui-state-disabled");
            } else {
                self.AJ_tname.removeClass('needy').addClass('txt');
                $('#the-sywz-ok-button').attr("disabled", false)
                    .removeClass("ui-state-disabled");
            }
            if(self.mode === 'M7129')
                self.AF_update_value({"tname": $.trim(self.AJ_tname.val()) });
        });
        // adding portrait picture button click
        self.AJ_addbtn.unbind('click').bind('click', function(e){
            self.AJ_file_input.val('').trigger('click');
        });
        self.AJ_file_input.unbind('change').bind('change', function(e){
            var file = this.files[0];
            $('div.wait-icon').css('visibility','visible');
            FD.AV_bom.AF_add_media(file, self);
            return U.AF_stop_event(e);
        });
        self.jdob.unbind('change').bind('change',function(e){
            self.AF_update_value({"dob": U.AF_date_undashed(self.jdob.val())});
        });
        self.jdod.unbind('change').bind('change',function(e){
            self.AF_update_value({"dod": U.AF_date_undashed(self.jdod.val())});
        });
        self.jpob.unbind('change').bind('change',function(e){
            self.AF_update_value({"pob": $.trim(self.jpob.val()) });
        });
        self.jpod.unbind('change').bind('change',function(e){
            self.AF_update_value({"pod": $.trim(self.jpod.val()) });
        });
        self.AJ_nutshell.unbind('change').bind('change',function(e){
            self.AF_update_value({"nutshell": $.trim(self.AJ_nutshell.val()) });
        });
        self.AJ_male.unbind('change').bind('change',function(e){
            if(typeof(self.AJ_male.prop("checked"))){
                self.AF_update_value({"sex": "male" });
            } else {
                self.AF_update_value({"sex": "female" });
            }
        });
        self.AJ_female.unbind('change').bind('change',function(e){
            if(typeof(self.AJ_male.prop("checked"))){
                self.AF_update_value({"sex": "female" });
            } else {
                self.AF_update_value({"sex": "male" });
            }
        });
        var pav = self.AV_synop.pav;
        // only when update-pa/edit-pa and is anchor, 
        // 2x check-boxes for adp are offered
        if(self.mode === 'M7129' && pav.AF_is_anchor()){
            self.AJ_adpin.prop("disabled", false);
            self.AJ_adpout.prop("disabled", false);            

            var adp = self.AV_synop.pav.AF_adp(),
                has_adpout_bid = false, 
                currb = self.AV_synop.pav.phost.model;
            // find bid of adp-out parents-ba
            for(var i=0; i<self.pa.iddict.parents.length; i++){
                if(self.pa.iddict.parents[i].substr(-1) === '-')
                    // trim-off the trailing '-'
                    has_adpout_bid = self.pa.iddict.parents[i].slice(0, -1);
            }
            if(adp &&              // pa is adp-child (- or +)
            has_adpout_bid &&   // pa has adp-out parents-ba
            //adp-out ba NOT the current phost-ba
            currb.id() !== has_adpout_bid){
                // there exists a adp-out(biological) parents-be, and it is
                // NOT the current parents-ba: block both check-boxes
                self.AJ_adpin.prop("disabled", true);
                self.AJ_adpout.prop("disabled", true);
            } else {
                self.AJ_adpin.unbind('change').bind('change', function(e){
                    self.AJ_adpout.prop('checked', false);// can't be both set
                    self.AF_update_value({"adp": self.AF_gui_adp() });
                });
                self.AJ_adpout.unbind('change').bind('change', function(e){
                    self.AJ_adpin.prop('checked', false);// can't be both set
                    self.AF_update_value({"adp": self.AF_gui_adp() });
                });
            }            
        } else {
            self.AJ_adpin.prop("disabled", true);
            self.AJ_adpout.prop("disabled", true);            
        }
    };// end of AC_SynopWizard.AF_setup_change_handlers method

    AC_SynopWizard.prototype.AF_handle_rs = function(rs, is_new){
        $('div.wait-icon').css('visibility','hidden');
        if(is_new){ // rs is new made, but pa already has its iddict.portrait
            rs.AF_add_holder(this.pa.id());
            if(this.mode === 'M7129'){
                // sice rs is new, and pa is not, add rs to actbuf.saves
                this.AV_actbuffer.AF_add_save(rs);
                // pa's old portrait rs modified in this, also pa.portrait
                this.AF_update_value({'portrait': rs.id()});
            } else { // mode: M7132/7133/7134/7135 pa being added 
                rs.id(this.pa.iddict.portrait); // use pa.portrait.id
                // pa is newly created, pa and its rs in actbuf.saves
                // remove it. save anew, 
                this.AV_actbuffer.AF_remove_save(rs).AF_add_save(rs); 
            }
        } else { // rs is old, existing one, with the same sig - a re-use rs
            // add pa-id to rs.holders
            var iddict = $.extend(true, {}, rs.iddict);
            if(iddict.holders.indexOf(this.pa.id()) === -1){
                iddict.holders.push(this.pa.id());
                this.AV_actbuffer.AF_add_update(rs, {'iddict': iddict});
            }
            // if pa.portrait is diff than rs, AF_update_value will handle
            // modify pa.portrait pointing now to rs, and handle old rs
            if(this.pa.iddict.portrait !== rs.id()){
                if(this.mode === 'M7129'){ // pa is being updated, not new
                    // pa's old portrait rs modified in this
                    this.AF_update_value({'portrait': rs.id()});
                } else {// mode is M7132/7133/7134/7135 
                    // pa, pa.portrait are new, in actbuf.saves
                    // 1.remove pa.portrait from actbuf.saves
                    // 2.let pa point to rs
                    this.AV_actbuffer.AF_remove_save(this.pa.iddict.portrait);
                    this.pa.portrait(rs);
                }
            }
        }
        AC_BinObjMgr.AF_feed_media(this.AJ_img, rs);
        this.AJ_img.appendTo(this.AJ_picframe.empty()).show();
    };// end of AC_SynopWizard.prototype.AF_handle_rs
        
    // tell if 1 of 2 check-box is checked: adp-in: +, adp-out: -, false
    AC_SynopWizard.prototype.AF_gui_adp = function(){
        if(this.AJ_adpin.prop('checked')) return '+';
        if(this.AJ_adpout.prop('checked')) return '-';
        return false;
    };// end of AC_SynopWizard.prototype.AF_gui_adp
    
    // ------------------------------------------------------------------------
    // compare a value from widget with that contained in pa/td/.If changed
    // make an update-entry in actbuffer
    // ------------------------------------------------------------------------
    AC_SynopWizard.prototype.AF_update_value = function(dic){
        if (this.mode !== 'M7129') return; // only for M7129/update-pa
        
        // get a reference of update-dict
        var dict = this.AV_actbuffer.AF_get_update_dic(this.pa);
        for(var k in dic){
            if(k === 'nutshell'){ // content[lcode] of this.td changed
                if(this.pa.nutshell()){
                    var tddic = this.AV_actbuffer.AF_get_update_dic(this.td);
                    if(!(this.AV_lcode in this.td.content()) ||
                        this.td.AF_content_face(this.AV_lcode) !== dic[k]){
                        // clone content-dict
                        var d = $.extend(true,{}, this.td.content());
                        d[this.AV_lcode] = Base64.encode(dic[k]);
                        tddic['content'] = d;
                    } else {
                        delete tddic['content'];
                    }
                } else { // td has been newly created: no nutshell to update
                    // dic[k] be b64 encoded inside AF_content_face
                    this.td.AF_content_face(this.AV_lcode, dic[k]); 
                }
            } else if(k === 'portrait'){    // pa has a different portrait rs
                var old = this.pa.portrait();
                if(old.holders().length === 1){ //old rs only ref to pa
                    // now pa ging to have diff rs, delete the old rs
                    this.AV_actbuffer.AF_add_delete(old);
                } else { // old rs has more than pa.id in its holders
                    var riddict = $.extend(true, {}, old.iddict);
                    var ind = riddict.holdres.indexOf(this.pa.id());
                    riddict.splice(ind, 1);
                    this.AV_actbuffer.AF_add_update(old, {'iddict': riddict});
                }
                if(!('iddict' in dict))
                    dict.iddict = {};
                dict['iddict']['portrait'] = dic[k];
            } else if(k === 'newtd'){
                if(dic['k'] !== this.pa['iddict']['nutshell']){
                    if(!('iddict' in dict)) 
                        dict.iddict = {};
                    dict['iddict']['nutshell'] = dic[k];
                }
            } else if (['pob', 'pod', 'tname'].indexOf(k) > -1){
                var obj = this.pa[k]();// call pa.pob() | .pod(0 | .tname()
                if(!(this.AV_lcode in obj) || // not in returned dics as a key
                    obj[this.AV_lcode] !== Base64.encode(dic[k])){
                    var xdic = $.extend(true, {}, this.pa[k]());
                    xdic[this.AV_lcode] = Base64.encode(dic[k]);
                    dict[k] = xdic;
                } else {
                    delete dict[k];
                }
            } else if(k === 'adp'){
                // find current hosting ba
                var pav = this.AV_synop.pav,
                    _pbids = this.pa.iddict.parents.slice(),
                    pid = this.pa.id(),
                    currb = pav.phost.model,
                    bid = currb.id(),
                    _cpids = currb.iddict.children.slice(),
                    // bindex: bid-index in pa.parents-array 
                    bindex = this.pa.AF_parents_index(bid),
                    // pindex: pid in currb.children-array
                    pindex = this.pa.AF_index(currb);
                // update-dict for parents-ba hosting this.pa
                var bdict = this.AV_actbuffer.AF_get_update_dic(currb),
                    adp = this.AV_synop.pav.AF_adp();// return false|+|-
                // iddict may have previous change-reqs, clear them
                if('iddict' in dict)
                    delete dict.iddict['parents'];
                if('iddict' in bdict)
                    delete bdict.iddict['children'];
                // also clear actbuffer.AV_saves
                this.AV_actbuffer.AV_saves.length = 0;
                if( adp !== dic[k]){
                    if(!adp && // adp==false, dic[k] must be - or +
                       this.pa.iddict.parents.length === 1){
                        // pa had only 1 parents-ba, and was not adp-child.
                        // now pa becomes adp-child
                        _cpids[pindex] = pid + dic[k];
                        _pbids[bindex] = currb.id() + dic[k];
                        dict.iddict = {parents: _pbids};
                        bdict.iddict = {children: _cpids};
                        // since pa was not adp-child, make new parents-ba
                        var sign = dic[k] === '-'? '+' : '-'; // opposite sign
                        var b = new BA({_id: M.mid(pid.substr(0,7), 'BA')});
                        b.iddict.children.push(pid + sign);
                        dict.iddict.parents.push(b.id() + sign);
                        this.AV_actbuffer.AF_add_save(b);
                    } else if(!dic[k]){ // dic[k] == false
                        // pa was adp-child, become biological child of currb
                        _cpids[pindex] = pid; // no more -|+ sufix
                        bdict.iddict = {children: _cpids};
                        _pbids[bindex] = currb.id(); // no more -|+ sufix
                        dict.iddict = {parents: _pbids};
                        // delete all other pbas if they are vitual
                        // if not virtual, remove pa-id from that ba's children
                        var bs = this.pa.parents();
                        for(var i=0; i<bs.length; i++){
                            if(i === bindex) {
                                continue; // current-parents-ba: dont remove
                            } else { // other than current parents-ba
                                if(bs[i].is_child(pid)){
                                    // remove ba from pa.iddict.parents
                                    var ind = this.pa
                                               .AF_parents_index(bs[i].id());
                                    dict.iddict.parents.splice(ind,1);
                                    // if ba is virtual and only has pa: delete
                                    if(bs[i].AF_is_virtual() && 
                                       bs[i].iddict.children.length === 1){
                                       this.AV_actbuffer.AF_add_delete(bs[i]);
                                    } else {
                                        // remove pa from ba.iddict.children
                                        // ad that ba to be updated in actbuffer
                                        ind = this.pa.AF_index(bs[i]);
                                        var pbd =  this.AV_actbuffer
                                                    .AF_get_update_dic(bs[i]),
                                        // clone children array
                                        _cs = bs[i].iddict.children.slice();
                                        // remove pa-id from it
                                        _cs.splice(ind,1);
                                        // update job for bs[i]
                                        pbd.iddict.children = _cs;
                                    }
                                }
                            }
                        }
                    } else {
                        // pas was/is adp-child, only - => +, or + => -
                        // re-write bid in pa.parents with new sufix(dic[k])
                        _pbids[bindex] = bid + dic[k];
                        dict.iddict = {parents: _pbids};
                        // re-write pid in currb's children with new sufix
                        _cpids[pindex] = pid + dic[k];
                        bdict.iddict = {children: _cpids};
                    }
                } else {
                    delete dict['iddict'];
                    delete bdict['iddict'];
                }
            } else {
                // attributes(e.g. dob, dod, sex, adp) changes within pa
                if(this.pa[k]() !== dic[k]){
                    dict[k] = dic[k];
                } else {
                    delete dict[k];
                }
            }
        } // for(var k in dic)
    };// end of AC_SynopWizard.AF_update_value method
    
    AC_SynopWizard.prototype.AF_collect_inputs = function(){
        this.pa.sex(this.AJ_male.prop('checked') ? 'male' : 'female');
        this.pa.dob(U.AF_date_undashed(this.jdob.val()));
        this.pa.dod(U.AF_date_undashed(this.jdod.val()));
        this.pa.tname_face(this.AV_lcode, $.trim(this.AJ_tname.val()));
        this.pa.pob_face(this.AV_lcode, $.trim(this.jpob.val()));
        this.pa.pod_face(this.AV_lcode, $.trim(this.jpod.val()));
        this.pa.nutshell().AF_content_face(
            this.AV_lcode, this.AJ_nutshell.val());
        var adpchar = '';
        if (this.AJ_adpin.prop('checked')){
            adpchar = '+';
        } else if(this.AJ_adpout.prop('checked')){
            adpchar = '-';
        }
        if(adpchar.length > 0){
            this.pa.iddict['parents'] = [this.AV_synop.pav.model.id()+ adpchar];
            var index = this.AV_actbuffer
                            .AF_get_update_index(this.AV_synop.pav.model);
            var chs = this.AV_actbuffer.updates[index][1]['iddict']['children'];
            for(var i=0; i< chs.length; i++){
                if(chs[i].indexOf(this.pa.id()) > -1){
                    chs[i] = this.pa.id() + adpchar;
                    break;
                }
            }
        }
    };// end of AC_SynopWizard.AF_collect_inputs method
    
    AC_SynopWizard.prototype.AF_handle_success = function(data, self){
        var news = self.AV_actbuffer.AF_execute();
        if(self.mode === 'M7129'){// update pa: pa name may change: reset it
            FD.AV_landing.AF_page().AF_render();
            FD.AV_synop.AF_update_pa();
        } else {
            if(self.mode === 'M7134'|| self.mode === 'M7135'){//add_female/male
                // fill in real pa male/femal parent 
                self.AV_synop.pav.model = self.pa;
                self.AV_synop.AF_update_pa();//update synop and menu for new pa
                self.AV_synop.pav.AV_patag.AF_set_handlers();
                var AV_oldpath = FD.AF_tpath(),
                    AV_path = FD.AV_path_mgr.AF_clone_new_path(self.pa.id());
                AV_path.AF_add_expand(self.pa).AF_root(self.pa);
                FD.AV_landing.AF_page()
                  .AF_delta_render(AV_oldpath).AF_update_focus_ppane();
            } else if(self.mode === 'M7133' || self.mode === 'M7132'){
                var i, new_pa;
                for(i=0; i< news.puts.length; i++){
                    if(news.puts[i].cid() === 'PA'){
                        new_pa = news.puts[i];
                        break;
                    }
                }
                if(new_pa){
                    // add spouse-pa/child-pa
                    var AV_oldpath = FD.AF_tpath();
                    var AV_path = FD.AV_path_mgr.AF_clone_new_path(new_pa.id());
                    AV_path.AF_add_expand(self.itor.model.id())
                    FD.AV_landing.AF_page()
                    .AF_delta_render(AV_oldpath).AF_update_focus_ppane();
                }
            }    
        }
    };// end of AC_SynopWizard.AF_handle_success method
    
    /* ------------------------------------------------------------------------
    // Purpose: if anything changed in the dialog against pa, put in actbuffer
    // ---------------- */
    AC_SynopWizard.prototype.AF_save_pachanges = function(){
        if(this.mode !== 'M7129')  // M29:Update. not update -> new pa
            this.AF_collect_inputs(); // simply collect all inputs
        var AV_req_dict = this.AV_actbuffer.AF_get_reqdic(this.mode);
        FD.AV_lserver.AF_post('putdocs', AV_req_dict, this);
    };// end of AC_SynopWizard.AF_save_pachanges method
    
    // ------------------------------------------------------------------------
    // bind2dlg: popup dialog, bind it to save_pachanges callback. 
    // possible modes:
    // M7129: trigger - synop-box>edit-icon for updating synop of fucos-pav
    // M7132: trigger - anchor-pav's ctxt-menu action(add-spouse)
    // M7133: trigger - bav's ctxt-menu action(add-child/1 parent-pav logged-in)
    // M7134: trigger - synop-box>edit-icon for v-pav under fucos(add-female)
    // M7135: trigger - synop-box>edit-icon for v-pav under fucos(add-male)
    // itor: hosting bav for mode = M7134/M7135/M7133:;
    // itor: hosting pav under focus for mode = M7129/M7132: 
    // --------------
    AC_SynopWizard.prototype.AF_bind2dlg = function(mode, itor){
        this.AV_actbuffer = FD.AV_actbuffer.AF_init();
        var self = this;
        self.mode = mode; 
        self.itor = itor; // itor is the pav synop is standing for
        this.AV_lcode = FD.AF_lcode();
        // make sure dialog language is the same as global lcode
        FD.AV_lang.AF_set_all(this.AJ_layout, this.AV_lcode);
        self.dlg = self.AJ_layout.dialog({
            autoOpen: false,
            width: 570,
            height: 645,
            modal: true,
            resizable: false,
            show: true,
            hide: true,
            close: function(e){ 
                self.dlg.dialog('destroy'); 
            },
            open: function(){ 
                self.AF_prepare_pa(); 
                self.AJ_lcpicker_img.attr('src', 
                    FD.AV_lang.AF_icon_filepath(self.AV_lcode));
                $('div.ui-dialog-buttonset').prepend(self.AJ_lcpicker_img);
                self.AJ_lcpicker_img.languagePicker(self.AF_lang_pick, self);
            },
            buttons: {
                'sywz-ok-button': {
                    id: 'the-sywz-ok-button',
                    text: 'OK',
                    click: function(e){
                        if(FD.AF_se()){ // se may have expired. check it
                            self.AF_save_pachanges();
                        } 
                        self.AJ_layout.dialog('close');                             
                        return U.AF_stop_event(e);
                    },
                },
                'sywz-cancel-button': {
                    id: 'the-sywz-cancel-button',
                    text: 'Cancel',
                    click: function(e){
                        self.AJ_layout.dialog('close'); 
                        return U.AF_stop_event(e);
                    }
                }
            }
        });
        self.dlg.dialog('open');
        self.dlg.dialog('widget').attr('id', 'the-sywz-edit-popup');
        FD.AV_lang.AF_set_dlg('the-sywz-edit-popup',{
            '.ui-dialog-title': self.mode, 
            '#the-sywz-ok-button':'M0002',       // OK
            '#the-sywz-cancel-button': 'M1003'   // CANCEL
        });        
    }// end of AC_SynopWizard.prototype.AF_bind2dlg
    
    return AC_SynopWizard;
})();