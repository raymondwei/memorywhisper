/*
 * Copyright (C) 2014 Adacro Inc. - All Rights Reserved
 */
var AC_SGroup = (function(){
    function AC_SGroup(bv){
        this.AV_siblings = []; // list of pavs as ba.children rendered
        this.bv(bv); // set this.id =<ba.eid>_children
        // make round icon with +/- sign in it
        this.AJ_navel=$('<div class="sgrp-navel"></div>')
            .appendTo(U.AJ_render_pane)
            .attr('title',FD.AV_lang.face('M7518')); // 'Children'
        this.AV_pwd_dlg = new AC_PwdPopup(this);
        if(bv.AF_chpermit() === 'pwd'){
            this.AJ_navel.css('background-image',
                                'url(/pics/white-lock.png)');
        } else if(!bv.AF_chpermit()){
            this.AJ_navel.css('visibility','hidden');
        }
        this.AF_set_id().AF_state("shrunk");
        this.AF_ynavel(bv.AF_ynavel());
        this.AF_gen(bv.AF_gen().AF_next_gen());
        this.AJ_navel
            .css({top:this.AF_ynavel()-8, left: this.AF_gen().xstart() - 9});
        this.AF_set_tooltip();
        
        // vertical line connecting left of stem_line of pvs. id=this.id_line
        this.AJ_line = U.AF_drawline(
            {'x':this.AF_gen().xstart(),'y':this.AF_ynavel()},
            {'x':this.AF_gen().xstart(),'y':this.AF_ynavel()},
            this.id+'_line','marker');
        this.AF_handle_navel_events();
    };// end of AC_SGroup constructor
    AC_SGroup.prototype.cid = function(){ return 'SGP' }
    
    // states: "expanded" | "shrunk"
    AC_SGroup.prototype.AF_state = function(v){
        if(!v) return this._state;
        if(v === 'shrunk' || v === 'expanded'){ this._state = v;  }
        return this;
    };// end of AC_SGroup.prototype.AF_state
    
    AC_SGroup.prototype.AF_set_id = function(){
        this.id = this.bv().id + '_children';
        this.AJ_navel.attr('id', this.id + '_navel');
        return this;
    };// end of AC_SGroup.prototype.AF_set_id
        
    AC_SGroup.prototype.AF_set_navel_icon = function(){// 'shrunk'/'expanded
        if(this.AF_state() === 'expanded'){
            this.AJ_navel.css('background-image','url(/pics/minus.png)');
        }else { // newstate == 'shrunk': test if openable or pwd-locked
            if(this.bv().AF_chpermit() === true){ // true: openable
                this.AJ_navel.css({visibility: 'visible',
                                'background-image':'url(/pics/plus.png)'});
            } else if(this.bv().AF_chpermit() === 'pwd'){ // pwd-protected
                this.AJ_navel.css({visibility:'visible',
                               'background-image':'url(/pics/white-lock.png)'});
            } else if(this.bv().AF_chpermit() === false){
                this.AJ_navel.css('visibility','hidden');
            }
        }
        return this;
    };// end of AC_SGroup.prototype.AF_set_navel_icon

    AC_SGroup.prototype.AF_ynavel = function(v){
        if(typeof(v) === 'undefined') return this._ynavel;
        this._ynavel = v;
        return this;
    };// end of AC_SGroup.prototype.AF_ynavel
    
    AC_SGroup.prototype.AF_gen = function(g){
        if(typeof(g) === 'undefined') return this._gen;
        this._gen = g;
        return this;
    };// end of AC_SGroup.AF_gen method
    
    AC_SGroup.prototype.AF_insert_sibling = function(pv, index){
        if(this.AV_siblings.indexOf(pv) === -1){
            if(typeof(index) === 'number' && index < this.AV_siblings.length){
                this.AV_siblings.splice(index, 0, pv);
            } else {
                this.AV_siblings.push(pv);
            }
        }
        return this.AF_reset_vline();
    };// end of AC_SGroup.prototype.AF_insert_sibling
    
    AC_SGroup.prototype.AF_takeaway_sibling = function(pv){
        var index = this.AV_siblings.indexOf(pv);
        if(index > -1){
            this.AV_siblings.splice(index,1);
        }
        return this.AF_reset_vline();
    };// end of AC_SGroup.prototype.AF_takeaway_sibling
    
    AC_SGroup.prototype.AF_set_tooltip = function () {
        var msg = FD.AV_lang.face('M7518') + ': ';
        if(FD.AV_path_mgr.AF_is_exp(this.bv().model.id())){
            msg += FD.AV_lang.face('M6012');
        } else {
            msg += FD.AV_lang.face('M6011');
        }
        this.AJ_navel.attr('title', msg);
        return this;
    };// end of AC_SGroup.prototype.AF_set_tooltip

    // if it was centerpa rendering, and that center-pa is among the
    // sibling, reset that(tpath.pair.centerpa = null) when expand/shrink
    AC_SGroup.prototype.AF_reset_pathpair = function(){
        var chs = U.AF_trim_adopt_sign(this.bv().model.iddict.children),
            AV_path = FD.AF_tpath(),
            pair = AV_path.AF_centerpa_pair();
        if(pair.centerpa && chs.indexOf(pair.centerpa.id()) > -1){
            AV_path.AF_centerpa_pair({centerpa: null});
        }
        return this;
    };// end of AC_SGroup.prototype.AF_reset_pathpair

    AC_SGroup.prototype.AF_handle_navel_events = function(){
        var self = this;
        self.AJ_navel.unbind('click').bind('click', function(e){
            if(self.AF_state() === "expanded"){// expanded to shrunk
                var AV_oldpath = FD.AF_tpath();
                if(!self.bv().AF_is_virtual()){
                    var AV_oldpath = FD.AF_tpath(), 
                        AV_bvanchor = self.bv().AF_anchor_pv().model;
                    // clone a new path, set focus to anchor-pv of sgrp's 
                    // parent-bv. Prevent old focus is child under this sgroup
                    FD.AV_path_mgr.AF_clone_new_path(AV_bvanchor.id())
                    // remove bv from path, propagate all the way down.
                      .AF_remove_expand(self.bv().model, true);
                }
                self.AF_state("shrunk").AF_set_navel_icon();
                FD.AV_landing.AF_page().AF_delta_render(AV_oldpath)
                    .AF_update_focus_ppane();

            } else if(self.AF_state() === "shrunk"){// was shrunk, now expanding
                if(M.AF_fadmin() || 
                   self.bv().model.AF_children_permission() !== 'pwd'){
                    self.AF_expand_siblings();
                } else {
                    self.AV_pwd_dlg.AF_popup_dlg("M7518");//children
                } 
            }
            return U.AF_stop_event(e);
        })
    };// end of AC_SGroup.AF_handle_navel_events method
    
    // handle expand action
    AC_SGroup.prototype.AF_handle_success = function(jdata, self){
        var AV_oldpath = FD.AF_tpath(), newtpath;
        if(!self.bv().AF_is_virtual()){
            if(AV_oldpath.AF_centerpa_pair().centerpa){
                newtpath = FD.AV_path_mgr.AF_clone_new_path();
            } else {
                // focus on parents-ba's anchor pa is in focus
                var AV_bvanchor = self.bv().AF_anchor_pv().model;
                // new path with focus-pa, add bv to exps
                newtpath = FD.AV_path_mgr.AF_clone_new_path(AV_bvanchor.id());
            }
            newtpath.AF_add_expand(self.bv().model);
        }
        self.AF_state("expanded")
            .AF_set_navel_icon()
            .AF_reset_pathpair();
        FD.AV_landing.AF_page()
          .AF_delta_render(AV_oldpath).AF_update_focus_ppane();
    };// end of AC_SGroup.prototype.AF_handle_success
    
    AC_SGroup.prototype.AF_expand_siblings = function(){
        FD.AV_lserver.AF_children_exp([this.bv().model], this);
    };// end of AC_SGroup.prototype.AF_expand_siblings
    
    AC_SGroup.prototype.bv = function(v){
        if(typeof(v) === 'undefined') return this._bv;
        this._bv = v;
        return this;
    };// end of AC_SGroup.bv method
    
    AC_SGroup.prototype.AF_removeme = function(d){
        this.AJ_navel.remove();
        this.AJ_line.remove();
        for(var i=0; i< this.AV_siblings.length; i++){
            this.AV_siblings[i].AF_removeme(); // pav
        }
    };// end of AC_SGroup.AF_removeme method
    
    // find all pvs below pv (younger siblings)
    AC_SGroup.prototype.AF_find_youngers = function(pv){
        var ind = this.AV_siblings.indexOf(pv);
        var res = [];
        if(ind > -1){
            for(var i=ind+1; i<this.AV_siblings.length; i++){
                res.push(this.AV_siblings[i]);
            }
        }
        return res;
    };// end of AC_SGroup.AF_find_youngers method
    
    // pv is one of the siblings in this sgrp. ymove it down for D, and then
    // move all younger siblings below pv down by D too.
    AC_SGroup.prototype.AF_stretch_down = function(pv, D){
        var ind = this.AV_siblings.indexOf(pv);
        if(ind > -1){
            pv.AF_ymove(D);
            var AV_youngsters = this.AF_find_youngers(pv);
            for(var i=0; i< AV_youngsters.length; i++){
                AV_youngsters[i].AF_ymove(D);
            }
        }
    };// end of AC_SGroup.prototype.AF_stretch_down method
    
    // make sure stem-line of all siblings are vertically centered around
    AC_SGroup.prototype.AF_centralize = function () {
        this.AF_ynavel(this.bv().AF_ynavel());
        var n = this.AV_siblings.length;
        if(n < 2) return this;
        var y = 0.5*(this.AV_siblings[n-1].AF_ynavel() - this.AV_siblings[0].AF_ynavel());
        this.AF_ymove(-y); // reset ynavel
        return this;
    };// end of AC_SGroup.AF_centralize
    
    AC_SGroup.prototype.AF_upper = function () {
        if(this.AV_siblings.length === 0 || !FD.AV_path_mgr.AF_is_exp(this.bv().model)) 
            return this.AF_ynavel() - U.AV_PAV_YMIND;// PAV_YMIND=28(min y-dist)
        return this.AV_siblings[0].AF_upper(true);
    };// end of AC_SGroup.AF_upper
    
    AC_SGroup.prototype.AF_lower = function(){
        if (this.AV_siblings.length === 0 || !FD.AV_path_mgr.AF_is_exp(this.bv().model))
            return this.AF_ynavel() + U.AV_PAV_YMIND;// PAV_YMIND=28(min y-dist)
        return this.AV_siblings[this.AV_siblings.length-1].AF_lower(true);
    };// end of AC_SGroup.AF_lower
    
    AC_SGroup.prototype.AF_reset_vline = function(){
        var n = this.AV_siblings.length;
        var newy1 = this.AF_ynavel(), newy2 = this.AF_ynavel(), x =this.AF_gen().xstart();
        if(n > 0 && FD.AV_path_mgr.AF_is_exp(this.bv().model)){
            newy1 = this.AV_siblings[0].AF_ynavel();
            newy2 = this.AV_siblings[n-1].AF_ynavel();
        } 
        this.AJ_line.attr({y1: newy1, y2: newy2, x1:x, x2:x});
        this.AJ_navel.css('top',this.AF_ynavel()-7);
        
        if($('#'+this.id+'_line').length === 0) 
            this.AJ_line.appendTo(U.AJ_render_svg);
        return this;
    };// end of AC_SGroup.AF_reset_vline method
    
    AC_SGroup.prototype.AF_ymove = function(ydelta){
        for(var i=0; i<this.AV_siblings.length; i++){
            this.AV_siblings[i].AF_ymove(ydelta);
        }
        // move the vertical line
        this.AF_reset_vline();
        return this;
    };// end of AC_SGroup.AF_ymove method
    
    return AC_SGroup;
})();