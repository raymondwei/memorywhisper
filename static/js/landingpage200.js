var _a200en = 'The choice of the example family trees here '+
    'are of no political, or personal interests. The reason for choosing '+
    'them, was because the persons in them are public figures and therefore '+
    'information about them are publicly available.',
  _a200zh = '这里选择的例子家谱，没有任何政治或个人偏好与倾向。选择它们'+
            '是由于其中的人物信息都可以在网上获取(并非“族记-足迹”所编)',
  prefix = 'http://' + window.location.host + '?private=',
  prefix0 = 'http://memorywhisper.com/?private='
  pid1 = 'YUS6R59-PA-oCsrgS1453631586608',  // Victoria Hanover
  pid2 = 'YUS6R59-PA-RakPJQ1453631587890',  // Henry George
  pid3 = 'TC6H8EU-PA-rKAaKS1407852727235',  // 毛泽东
  pid4 = 'TC6H8EU-PA-mWbgfc1408654049422',  // 蒋经国
  pid5 = 'TC6H8EU-PA-aMpAgE1408651675296',  // 俞大维
  _c200en = '<h2 class="example-ft">Example family tree homepages</h2>'+
   //----------------------------------------------------------
   '<a target="_blank" href="'+prefix+pid1+'"><div class="links">'+
   '<span class="lp200-title">Victoria Hanover&#39;s family</span><br/>'+
   '<span class="lp200-url">'+prefix0+pid1+'</span></div></a>'+
   //----------------------------------------------------------
   '<a target="_blank" href="'+prefix+pid2+'"><div class="links">'+
   '<span class="lp200-title">Henry George&#39;s family</span><br/>'+
   '<span class="lp200-url">'+prefix0+pid2+'</span></div></a>'+
    //---------------------------------------------------------
    '<a target="_blank" href="'+prefix+pid3+'"><div class="links">'+
   '<span class="lp200-title">Mao Ze Dong(毛泽东)family</span><br/>'+
   '<span class="lp200-url">'+prefix0+pid3+'</span></div></a>'+
   //---------------------------------------------------------
   '<a target="_blank" href="'+prefix+pid4+'"><div class="links">'+
   '<span class="lp200-title">Jiang Jing Guo(蒋经国)family</span><br/>'+
   '<span class="lp200-url">'+prefix0+pid4+'</span></div></a>'+
   //---------------------------------------------------------
   '<a target="_blank"  href="'+prefix+pid5+'"><div class="links">'+
   '<span class="lp200-title">Yu Da Wei(俞大维)family</span><br/>'+
   '<span class="lp200-url">'+prefix0+pid5+'</span></div></a>'+
   //---------------------------------------------------------
   '<div class="lp-info">A personal family tree home page is '+
   'accessible through a long, cryptical url known only to the owner. This is '+
   'the first(basic) level of privacy control.</div>',
    //=========================================================
  _c200zh= '<h2 class="example-ft">家谱首页例子</h2>'+
  //----------------------------------------------------------
  '<a target="_blank" href="'+prefix+pid3+'"><div class="links">'+
  '<span class="lp200-title">毛泽东家</span><br/>'+
  '<span class="lp200-url">'+prefix0+pid3+'</span></div></a>'+
  //---------------------------------------------------------
  '<a target="_blank" href="'+prefix+pid4+'"><div class="links">'+
  '<span class="lp200-title">蒋经国家</span><br/>'+
  '<span class="lp200-url">'+prefix0+pid4+'</span></div></a>'+
  //---------------------------------------------------------
  '<a target="_blank"  href="'+prefix+pid5+'"><div class="links">'+
  '<span class="lp200-title">俞大维家</span><br/>'+
  '<span class="lp200-url">'+prefix0+pid5+'</span></div></a>'+
  //---------------------------------------------------------
  '<a target="_blank"  href="'+prefix+pid1+'"><div class="links">'+
  '<span class="lp200-title">维多丽亚-翰诺威(Victoria Hanover)家</span><br/>'+
  '<span class="lp200-url">'+prefix0+pid1+'</span></div></a>'+
  //---------------------------------------------------------
  '<a target="_blank"  href="'+prefix+pid2+'"><div class="links">'+
  '<span class="lp200-title">亨利-乔治(Henry George)家</span><br/>'+
  '<span class="lp200-url">'+prefix0+pid2+'</span></div></a>'+
  //---------------------------------------------------------
  '<div class="lp-info">用户个人的家谱首页的网址是一个比较长，难以记忆的字符串，'+
  '除非拥有者传告，他人不易得到。这是第一层（基本）的隐私保护。 </div>';
  //--------------------------------------------------------

var _fc200 = new FC({_id: M.mid('FTSUPER','FC'),type: 'txt',
    iddict: {rs:'', owner: ''},
    anno: { en: Base64.encode(_a200en), zh: Base64.encode(_a200zh)},
    contdict:{ en: Base64.encode(_c200en), zh: Base64.encode(_c200zh)}
});
/**/
var AC_LandingPage200 = (function(){
    function AC_LandingPage200(fd, jbox, ctls){
        this.fd = fd;
        this.AV_lcode = fd.AF_lcode();
        this.AJ_div = jbox;
        this.AJ_upper = ctls.up
            .css('background-image','url(../pics/red_curtain.jpg');
        this.AJ_lower = ctls.dn;
        ctls.lb.css('visibility','hidden');
        ctls.rb.css('visibility','hidden');
        ctls.vc.css('visibility','hidden');
        this.AF_init();
    };// end of AC_LandingPage200 constructor

    AC_LandingPage200.prototype.AF_init = function(){
        this.AJ_lower.attr('tag',_fc200.id()+'@AF_anno_face')
            .html(_fc200.AF_anno_face(this.AV_lcode));
        this.AJ_upper.attr('tag',_fc200.id()+'@contdict_face')
            .html(_fc200.contdict_face(this.AV_lcode));
    };// end of AC_LandingPage200.prototype.AF_init

    AC_LandingPage200.prototype.AF_resize = function(w, h){
        this.AJ_div.css({width: w + 'px', height: (h - 10 )+ 'px'});
        this.AJ_upper.css({width: (w - 6) + 'px', height: (h - 68) + 'px'});
        // lower(lp-msgpane) has padding 15px both left/right: take 20 off
        this.AJ_lower.css({width: (w - 6 - 30) + 'px', height: 48 + 'px'});
    };// end of AC_LandingPage200.prototype.AF_resize

    return AC_LandingPage200;
})();// end of class AC_LandingPage200
