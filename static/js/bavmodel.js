/*
 * Copyright (C) 2014 Adacro Inc. - All Rights Reserved
 */
var AC_BAVModel = (function(){
    // role: 'root-bv', parms: {ba:, gen:, top:}; this happens as root-bv
    //   'new-parents', parms: {sgrp:}; when new spouse-pa added/created, a
    //      bv is added as new pa's parents-bv, a sgrp is also created with the
    //      new spouse-pa as first child. bv is not rendered as visible though.
    // role:new-spouse, parms:[male-pv,female-pv]
    function AC_BAVModel(parms){ // { ba:, host:, male_pv:, female_pv: }
        this.AJ_div = $('<div class="bv-gold"><div>')
            .appendTo(U.AJ_render_pane);
        this.model = parms['ba'];
        this.AF_male_pv(parms['male_pv']);
        this.AF_female_pv(parms['female_pv']);
        this.AF_anchor_index(parms['aindex']);
        this.AF_create_lines();
        this.AF_gen(parms['gen'], true);
        this.AF_top(parms['top'])
        this.host = parms['host']? parms['host'] : FD.AF_tpath();
        // possible 3 lines when male / female as anchor in the bv
        this.AF_set_id();
        this.AF_sgroup(new AC_SGroup(this)); // sgroup make bv tail line
        this.AJ_acticon = $('<img class="bvact" src="pics/right1.png"/>')
            .appendTo(this.AJ_div).css('visibility','hidden');
        //new child dialog pop-up
        this.AV_synopwizard = FD.AV_synop.AV_synopwizard; 
        var self = this;
        // add child menu-action-item
        this.AV_ctxtmenu = new AC_ContextMenu(this, this.AJ_acticon,
            {items: [{ label: 'M7133', // 'Add child'
                icon: 'pics/boy.jpg',
                action: function(e){ 
                    self.AV_synopwizard.AF_bind2dlg('M7133', self);
                    return U.AF_stop_event(e);
                }}]}, true); // left-click: true (default:right-click)
        this.AF_setup_handlers();
    };// end of AC_BAVModel constructor

    AC_BAVModel.prototype.cid = function(){ return 'BAV'; }    
    AC_BAVModel.prototype.AF_ynavel = function(){ 
        return this.AF_top() + 0.5*U.AV_BAV_H; 
    }
    AC_BAVModel.prototype.AF_chpermit = function(){
        return this.model.AF_children_permission(); 
    }
    
    AC_BAVModel.prototype.AF_set_id = function(){
        if(this.host.cid() === 'PTH'){
            this.id = this.model.id() + '_virtual';
        } else {
            this.id = this.model.id() + '_h_' + this.host.model.id()
        }
        if(this.AF_sgroup()) this.AF_sgroup().AF_set_id();
        this.AJ_div.attr('id',this.id);
        this.AJ_tail.attr('id', this.id + '_tail');
        this.AV_lines['m'][0].attr('id', this.id+'_msline'); //sloped line
        this.AV_lines['m'][1].attr('id', this.id+'_mvline'); // virtical line
        this.AV_lines['m'][2].attr('id', this.id+'_mvline'); // horizontal line
        this.AV_lines['f'][0].attr('id', this.id+'_fsline'); //sloped line
        this.AV_lines['f'][1].attr('id', this.id+'_fvline'); // virtical line
        this.AV_lines['f'][2].attr('id', this.id+'_fvline'); // horizontal line
    };// end of AC_BAVModel.prototype.AF_set_id
        
    AC_BAVModel.prototype.AF_sgroup = function(v){
        if(typeof(v) === 'undefined') return this._sgroup;
        this._sgroup = v.bv(this);
        return this;
    };// end of AC_BAVModel.prototype.AF_sgroup method
    
    AC_BAVModel.prototype.AF_is_virtual = function(){
        return this.model.AF_is_virtual();
    };// end of AC_BAVModel.prototype.AF_is_virtual method
    
    AC_BAVModel.prototype.AF_male_pv = function(v){
        if(typeof(v) === 'undefined') return this._male_pv;
        this._male_pv = v;
        return this;
    };// end of AC_BAVModel.prototype.AF_male_pv method
    
    AC_BAVModel.prototype.AF_female_pv = function(v){
        if(typeof(v) === 'undefined') return this._female_pv;
        this._female_pv = v;
        return this;
    };// end of AC_BAVModel.prototype.AF_female_pv method

    // ------------------------------------------------------------------------
    // v==undefined: getting anchor-pv; v given: pv (must be one of spouses),
    // set that pv to be anchor, the other non-anchor
    // ----------------------------------------
    AC_BAVModel.prototype.AF_anchor_pv = function(v){
        if(typeof(v) === 'undefined'){  // getter
            if(this._male_pv && this._male_pv.AF_is_anchor()) 
                return this._male_pv;
            return this._female_pv;
        } else {                        // setter
            if(v === this.AF_anchor_pv()){
                this.AF_anchor_pv().AF_is_anchor(true);
                this.AF_non_anchor_pv().AF_is_anchor(false);
                return this;
            }
            if(this.AF_anchor_pv() === this._male_pv){
                this._male_pv.AF_is_anchor(false);
                this._female_pv.AF_is_anchor(true);
            } else {
                this._female_pv.AF_is_anchor(false);
                this._male_pv.AF_is_anchor(true);
            }
            return this;
        }
    };// end of AC_BAVModel.prototype.AF_anchor_pv method

    AC_BAVModel.prototype.AF_non_anchor_pv = function(v){
        if(typeof(v) === 'undefined'){
            if(this._male_pv.AF_is_anchor()) return this._female_pv;
            return this._male_pv;
        } else {
            if(v.sex() !== this.AF_anchor_pv().sex()){
                if(v.sex() === 'male')  { this._male_pv = v; }
                else                    {  this._female_pv = v; }
            }
            return this;
        }
    };// end of AC_BAVModel.prototype.AF_non_anchor_pv method
    
    AC_BAVModel.prototype.AF_other_pv = function(pv){
        if(pv.sex() === 'male') return this._female_pv;
        return this._male_pv;
    };// end of AC_BAVModel.AF_other_pv method
    
    AC_BAVModel.prototype.AF_set_position = function(
        pos,                  // {x, y} ints, new position of left/top
        AV_animated,          // true/false - to be animated?
        AV_animate_callback){ // null/<function> - animate-callback
        if(AV_animated){
            var AV_callback_done = false;
            if(typeof(pos.x) !== 'undefined' && pos.x !== this.AF_left()){
                this.AF_left(pos.x, true, AV_animate_callback);
                AV_callback_done = true;
            }
            if(typeof(pos.y) !== 'undefined' && pos.y !== this.AF_top()){
                if(AV_callback_done)
                    this.AF_top(pos.y, true, null);
                else
                    this.AF_top(pos.y, true, AV_animate_callback);
            }
        } else {
            if(typeof(pos.x) !== 'undefined' && pos.x !== this.AF_left())
                this.AF_left(pos.x);
            if(typeof(pos.y) !== 'undefined' && pos.y !== this.AF_top())
                this.AF_top(pos.y);
        }
        return this;
    };// end of AC_BAVModel.prototype.AF_set_position
    
    return AC_BAVModel;
})();