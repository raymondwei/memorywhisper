﻿/* ----------------------------------------------------------------------------
 * How AC(access control) for login, itF, children, spouses for a pa works.
 * in pa.AF_cfg:{ M1504:{<entry>:<ac-setting>,..},..}. An entry can be:
 *   login:[<pwd>,'',''], 
 *   M7518(children): [******,20161231,M7117], 
 *   M7517(spouses): M7118
 * in fid-table, there is UR record for each pa: cid: UR, 
 *   pid: TC6H8EU-PA-rKAaKS1407852727235
 *   RowKey: <pa-eid> with PA->UR. e.g. 
 *          PA-rKAaKS1407852727235 -> UR-rKAaKS1407852727235 
 *   rec:{"_id": TC6H8EU-UR-rKAaKS1407852727235,
 *        "usage": {"127.0.0.1": {}}, 
 *        "pwds": {
 *          "TC6H8EU-IT-XeUkHa1410135035476": "5f48982...81", 
 *          "TC6H8EU-IT-uBuqvi1410135071572": "964089a53..dc24", 
 *          "login": "749fb..deb7f3f", 
 *          "M7518": "5d0c519f8e51..82a73157",
 *   }}
 * A.for login, the password is saved in UR.rec{login:<b64-encrypted-string>
 *   when pa logs in, UR.rec[login] will be verified in 2 steps: 1. sends a
 *   post request url:'replogin' with pid in it to server; 
 *   2. server generated a random N(1..50) and save pid:[N, TS] and
 *   sends N back to browser; 3.a.browser b64 encode pid+pwd > b64str; b.insert
 *   two chars @N position in b64str, and two chars in front of b64str and sends
 *   the doped b64str back to server; 4. server find pid:[N,TS] in TS+15 secs,
 *   and remove two chars from front, two chars from @N and gets the b64str.
 *   5. server find UR in FIDCODE with RowKey/pid, and verify b64str(749fb..);
 *   6. server delete pid:[N,TS] and sends OK to client/browser - positive/neg
 *   When logged in pa modify pwd, fc.contdict.M1504.login=[<pwd>,'',''], server
 *   save pwd(b64 encoded) into UR(pid)[pwds][login], and M1504.login=[**,'','']
 *   client side also blur pwd in M1504.login to be ******.
 * B.for itF, it.pk can have M7117(public), M7118(private) or M1513(pwd-protect)
 *   M7117: itF is not locked - public openable; M7118: itF(folder) is visible
 *   and openable only for logged in pa-session (folder-color: red);
 *   for pk=M7117/M7118, itF.lock is not significant, or can be missing.
 *   M1513: itF.lock=[***,<d>,<pst-pk>] (d: expiry-date,post-pk: pk after d)
 *     in logged-in session, itF is visible/openable, blue-colored. In a session
 *     where logged-in pa is not owner of itF, and d is not expired, folder-icon
 *     has a lock sign and when clicked upon, pwd-dialog pops up, asking for 
 *     pwd-input. pwd/pid/it-id will be sent to server with a verifypwd/post 
 *     request. 
 *     Server will 1.find UR(pid)[pwds][it-id] (see line 10 above), and 2.verify
 *     the pwd
 *   When a logged-in itF owner-pa modify itF.pk/lock, server will find UR(pid)
 *   and save the pwd in UR[pwds][it-id], and blur it.lock[0] to ****** and save
 *   [******,<d>,<post-pk>] in it-entity
 * C.for children of pa, pa.AF_cfg().M1504.M7518 entry:
 *   children entry missing, or, M1504.M7518===M7117 both mean public access
 *   In this case the sibling navel is openable with a (+) icon if currently 
 *   shrunk. If M1504.M7518==M7118, the bv's tail-line and sibling's navel
 *   are not visible - just like there is no child under the ba. These will show
 *   when the pa (male or female of ba) is the logged-in pa, where normal (+)
 *   sign shows up for expanding. If M1504.M7518 == [******,<d>,<post-pk>]
 *   borwser will check if <d> is expired, if yes, post-pk==M7117. In this case
 *   it will behave as in public case. Otherwise, a lock will be shown on the
 *   expansion-icon - when clicked upon, a dialog pops up asking for pwd. And
 *   after server verified positively, the children will expand.
 *   if the owner-pa(logged-in) modifies pwd/d/pk in [<pwd>,<d>,<post-pk>], the
 *   server will save the new pwd in UR(pid)[pwds][M7518] and blur M1504's
 *   [0] to be ******, but update <d> / <post-pk>.
 * D.for spouses of pa, pa.AF_cfg().M1504.M7517=M7117|M7118|[<pwd>,<>,<>]
 * ----------------------------------------------------------------------------
 */
var AC_PwdPopup = (function(){
    function AC_PwdPopup(vmodel){
        this.vmodel = vmodel;
        this.AV_actbuffer = FD.AV_actbuffer.AF_init();
        this.AF_load_template();
    };// end of AC_PwdPopup constructor
    
    AC_PwdPopup.prototype.AF_load_template = function(){
        this.AJ_layout = $("<div class='pwd-popup'></div>");
        var m = "<span class='LS' id='check-pwd-label' tag='M1504'></span>";
        this.AJ_pwd_label = $(m).appendTo(this.AJ_layout);
        this.AJ_pwd_label.text("Password");
        this.AJ_pwd = $("<input type='password' id='check-the-pwd' />")
            .appendTo(this.AJ_layout);
    };// end of AC_PwdPopup.prototype.AF_load_template
    
    AC_PwdPopup.prototype.AF_verify_password = function(pwd, pwdkey){
        var req_dic = {_pwd: pwd, _key: pwdkey};
        if(this.vmodel.cid() === 'SGP'){
            var bv = this.vmodel.bv();
            if(bv.AF_male_pv().AF_is_virtual()){
                req_dic['_pid'] = bv.AF_female_pv().model.id();
            } else if(bv.AF_female_pv().AF_is_virtual()){
                req_dic['_pid'] = bv.AF_male_pv().model.id();                
            } else {
                req_dic['_pid'] = bv.AF_male_pv().model.id();
                req_dic['_pid2'] = bv.AF_female_pv().model.id();
            }
        } else if(this.vmodel.cid() === 'ITV'){
            req_dic['_pid'] = this.vmodel.model.AF_ownerpa().id();
        } else if(this.vmodel.cid() === 'PAV'){
            req_dic['_pid'] = this.vmodel.model.id();
        }
        FD.AV_lserver.AF_post('verifypwd', req_dic, this);
    };// end of AC_PwdPopup.prototype.AF_verify_password
    
    AC_PwdPopup.prototype.AF_handle_success = function(data, self){
        self.AJ_layout.dialog('close');
        if(self.vmodel.cid() === 'ITV'){ // for pwd-protected itF
            self.vmodel.AV_act = 'expand';
            FD.AV_lserver.AF_get_info(self.vmodel.model, self.vmodel);
        } else if(self.vmodel.cid() === 'PAV'){
            AC_PwdPopup.pwd_cracked = true;
            self.vmodel.AV_patag.AF_handle_success(data, self.vmodel.AV_patag);
        } else if(self.vmodel.cid() === 'SGP'){
            self.vmodel.AF_expand_siblings();
        }
        //self.AV_actbuffer.AF_execute();
    };// end of AC_PwdPopup.prototype.AF_handle_success

    AC_PwdPopup.prototype.AF_handle_err = function(data, self){
        self.AJ_pwd_label.attr('tag','M1517') //'Wrong password'
            .text(FD.AV_lang.face('M1517'));
        self.AJ_pwd.css('background-color','pink');  // clear pwd input field
        self.vmodel.AV_act = '';
    };// end of AC_PwdPopup.prototype.AF_handle_err
    
    AC_PwdPopup.prototype.AF_popup_dlg = function(pwdkey){
        var self = this;
        self.dlg = self.AJ_layout.dialog({
            autoOpen: false,
            width: 350,
            height: 300,
            modal: true,
            resizable: false,
            show: true,
            hiden: true,
            close: function(e){ self.dlg.dialog('destroy'); },
            open: function(e){
                    self.AV_actbuffer.AF_init();
                    self.AJ_pwd.css('background-color','white').val('');
                    self.AJ_pwd.unbind('focus').bind('focus', function(e){
                        self.AJ_pwd.css('background-color','white').val('');
                        self.AJ_pwd_label.attr('tag','M1504')
                                .text(FD.AV_lang.face('M1504'));
                    });
                },
            buttons: {
                'pwd-check-ok-button': {
                    id: 'the-pwd-check-ok-button',
                    text: 'OK',
                    click: function(e){
                        self.AF_verify_password($.trim(self.AJ_pwd.val()), pwdkey);
                        return U.AF_stop_event(e);
                    }
                },
                'pwd-check-cancel-button': {
                    id: 'pwd-check-cancel-button',
                    text: 'Cancel',
                    click: function(e){
                        self.AJ_layout.dialog('close'); 
                        return U.AF_stop_event(e);
                    }
                }
            }
        });
        self.dlg.dialog('open');
        self.dlg.keyup(function(e){
            if(e.keyCode === 13) // Enter/Return key pressed
                $('#the-pwd-check-ok-button').trigger('click');
        });
        self.dlg.dialog('widget').attr('id','pwd-check-popup');
        FD.AV_lang.AF_set_dlg('pwd-check-popup',{
            '#the-pwd-check-ok-button': 'M0002', // OK
            '#pwd-check-cancel-button': 'M1003'  // CANCEL
        });
    };// end of AC_PwdPopup.prototype.AF_popup_dlg
    
    return AC_PwdPopup;
})()