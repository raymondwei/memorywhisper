﻿/*
 * Copyright (C) 2014 Adacro Inc. - All Rights Reserved
 * ----------------------------------------------------------------------------
 * from function spec 2016-12-12: <How calendar works>
   ---------------------------------------------------
   When calendar tab is active, it will show the current month with last couple 
   of week-days of previous month, and a couple of days of the next month, at 
   the end of the showing grid, where There are 7 days in a row, and there is 
   usually 6 rows, resulting in 42 grid-cells (td). 
   Example: current month is December, so it will show 2016-Nov. end days; 
   all days of 2016-Dec. and a couple of beginning days of 2017-Jan.
   Each time the Calendar tab is newly activated (turning from other tab, from 
   fa-tree tab for example), or a new month is turned to. For example flipping 
   from Dec to Nov. where Nov will become current(showing month). In such 
   events, a new CalendarPane instance is created, and the constructor is 
   called, or the .calendar's events function is called, with the signatures:
   (start, end, timezone, callback)  in line-9 of calendarpane.js.
   In example of showing month being 2016-December, start is Nov.27(Sunday), 
   end is 2017-Jan-07

   What happens then: (this == CalendarPne inst)
    - this.ce_ids = this.AF_get_ce_ids(start, end)
      Returning: ["TC6H8EU-CE-11","TC6H8EU-CE-12","TC6H8EU-CE-01"]
      Here the month number is 1-based. And the CE-id's eid is that 2 char, 
      zfilled number
    - Prepare table/calendar cells (42 pieces) on the web-page, by calling
      This.eles = this.AF_cell_array(start, end)
    - Check which of the 3 ce_ids saved in this.ce_ids already exist in 
      FD.store, and save array Of ce_ids that are missing in FD.store [..] into 
      ids array, by fd.AV_store.AF_add2lst
    - Call FD.lserver.AF_getdics_helper(ids, 'get-ces' this), and this.verb set 
      to 'get-ces' Lserver.AF_getdics_helper will first check against IDB, if a 
      ce-id in ids can be found in indexedDB, that ce will be pumped into 
      FD.store. If all can be found in indexedDB, call this.handle_success
      If still there are/is ids missing (no in IDB), these ids will be requested 
      from server, with the this.verb == 'get-ces'.
    - On the server side, if a ce with the ce-id doesn't exist in DB, call
      utils.make_ce(ce_id, db), this will collect all pas of the fid, that has
      dob/dod in the month. The resulting ce will be saved in DB and returned. 
    - When returned successfully from server, the newly aquired ce-insts will 
      be put into FD.store.
    - This.handle_success with verb=='get-ces' will
      -- Get all 3 ces from FD.store, saved into this.ces array
      -- Collect all pids from all 3 ces, saved into pids array
      -- Go thru FD.store check, IDB check against pids, the same way as done 
         with ce_ids in previous steps. If still there are pids missing, send 
         request to server, with thisverb='get-pas', call 
         lserver.AF_getdics_helper(pids, 'get-pas',this)
    - This.handle_success with ver=='get-pas' will Go thru all 3 ce in 
      array this.ces:
      -- This.ces[i].vmodel = new AC_CEVModel()
         This will put all pas of the ces[i] onto the grid-cell where a pa has 
         dob/dod, under the 2 context-menu(green colored for b-days, gray 
         for d-days)
      -- Call this.ces[i].vmodel.AF_showme(this.eles) will render the 
         context-menu on page 

    How pa.dob/dod changes works for calendar-entry (ce)
    - when A. a new pa is created; B. existing pa is deleted; C. pa updated;
      A.if pa.dob/dod is 8 chars long (has month/date), client side will delete
        any ces in indexedDB and store, that have the 2 chars month#.
        Server side will, upon pa-creation, make sure that a ce exist in DB for
        the dob/dod
      B.if dob/dod is 8 chars long(has month/date), client side will delete
        any ces in indexedDB and store, that have the 2 chars month#.
        Server side will, upon pa-deletion, make sure that the ces with the
        month# of the dob/dod will remove the pa-id entry.
      C.old/new dob/dod of pa touched [<ce-id>,..]; delete ces in 
        store/indexedDB. Server side will do all updated: removal of entries
        for old dob/dod, adding new entries for new dob/dod.
      Client side Implementation:
      any dob/dod with 8 char long string (from new pa, deleted pa, or pa 
      before/after change) will be collected to ce-ids, e.G.: 
      [TC6H8EU-CE-01, TC6H8EU-CE-12], These will be looped thru, and for each 
      ce-id:
        Delete entity in store (if exist)
        Add to idb.transactions list, and do the deletion transaction, so that 
        they will be deleted from indexedDB. 
        See:
        ○ AC_ActionBuffer.AF_execute    - for ent in saves/in deletes(id) and
          updated([ent, delta-dic]) call
        ○ M.AF_collect_dirty_ces       - to collect touched ce-ids
        ○ AC_LServer.delete_local_ces) - delete ces in store and indexedDB
 */
var AC_CalendarPane = (function(){
    function AC_CalendarPane(fd, jbox){
        this.fd = fd;
        this.jbox = jbox;
    };// end of AC_CalendarPane constructor

    AC_CalendarPane.prototype.AF_init = function(){
        var self = this;
        this.calendar = this.jbox.fullCalendar({
            lang: FD.AF_lcode(),
            firstDay: 0,        // 0: Sunday start
            laztFetching: false,// events:func called every time month changes
            events: function(start, end, timezone,callback){
                self.ce_ids = self.AF_get_ce_ids(start, end),
                    self.ces = [], 
                    ids = [];
                self.eles = self.AF_cell_array(start, end);
                // ids: ce_ids - <ones from store>
                self.fd.AV_store.AF_add2lst(self.ce_ids, ids);
                // lserver first search idb, ones found there into store
                // the ones still missing will be requested from server
                // which have then been into store, if req has been succ.
                self.verb = 'get-ces';
                self.fd.AV_lserver.AF_getdics_helper(ids, 'get-ces', self);
            }
        });
    };// end of AC_CalendarPane.prototype.AF_init
    
    // get the (CE)ids of the 3 months to be shown. For months of (start, end):
    // (Nov, Jan)/3 months: ['<fid>-CE-11','<fid>-CE-12','<fid>-CE-01'] 
    // (Jun, Jul)/2 months: ['<fid>-CE-06','<fid>-CE-07']
    AC_CalendarPane.prototype.AF_get_ce_ids = function(start, end){
        var prefix = this.fd.fid() + '-CE-', 
            sn = start._d.getMonth(), // month number 0-based: [0..11] 
            en = end._d.getMonth();   // month number 0-based: [0..11]
        if(en === 0){             // end month is January (0-base N==0)
            if(sn === 11){        // start month is December: only 2 months 
                return [prefix + '12', prefix + '01'];
            } else if(sn === 10){ // start month is Nov: 3 months
                return [prefix + '11', prefix + '12', prefix + '01'];
            }
        } else if(en === 1){      // end month is Febuary (0-base N==1)
            if(sn === 11){        // start month is December
                return [prefix + '12', prefix + '01', prefix + '02'];
            } else if(sn === 0){  // start month is January 
                return [prefix + '01', prefix + '02'];
            } else {              // impossible
                return [];
            }
        } else {                  // end month is not January nor Febuary
            if(en - sn === 1){    // end month and start month defers 1
                return [prefix + U.zfill(sn+1, 2), prefix + U.zfill(en+1, 2)];
            } else {              // end month and start month defers 2
                return [prefix + U.zfill(sn + 1, 2),
                        prefix + U.zfill(sn + 2, 2), 
                        prefix + U.zfill(en + 1, 2)];
            }
        }
        return [];
    }; // end of AC_CalendarPane.prototype.AF_get_ce_ids
    
    // get an array [<cell-element>,...] for every cell, where each 
    // element: [<jQuery-object-of-td-element>,<str of format: '20140830'>]
    AC_CalendarPane.prototype.AF_cell_array = function(start, end){
        var eles = []; // [[<jtd-element>, '20140830'],[],...]
        start.add(1,'d'); // start's next day
        while(!start.isAfter(end)){
            // every <td> element has attribute data-date='2014-08-30'
            var td_id = start._d.getFullYear() + '-' +
                U.zfill(start._d.getMonth()+1, 2)+ '-' +
                U.zfill(start._d.getDate(), 2);
            var jq_sel = 'td.fc-day[data-date=' + td_id + ']';
            eles.push([jq_sel,td_id.replace(/-/g,'')]);
            start.add(1,'d'); // move to next date
        }
        return eles;
    };// end of AC_CalendarPane.prototype.AF_cell_array
    
    AC_CalendarPane.prototype.AF_handle_success = function(data, self){
        if(self.verb === 'get-ces'){
            for(var i=0; i<self.ce_ids.length; i++){
                self.ces.push(M.e(self.ce_ids[i]));
            }
            self.pa_ids = [];
            var pids = [];
            for(i=0; i< self.ces.length; i++){
                self.ces[i].get_pids(self.pa_ids);
            }
            self.fd.AV_store.AF_add2lst(self.pa_ids, pids);
            self.verb = 'get-pas';
            self.fd.AV_lserver.AF_getdics_helper(pids, 'get-pas', self);
        } else if(self.verb === 'get-pas' ){
            for(var i=0; i<self.ces.length; i++){
                self.ces[i].vmodel = new AC_CEVModel(self.ces[i]);
                self.ces[i].vmodel.AF_showme(self.eles);
            }
        }
    };// end of AC_CalendarPane.prototype.AF_handle_success
    
    AC_CalendarPane.prototype.AF_handle_err = function(data, self){
        U.log(1, data);
    };// end of AC_CalendarPane.prototype.AF_handle_err
    
    return AC_CalendarPane;
})(); // end of AC_CalendarPane class

var AC_CEVModel = (function(){
    function AC_CEVModel(model){ // model is a CE entity
        this.model = model;
        //{'1028':[[<bday-pa>,..],[<dday-pa>,..],..}
        this.item_dic = {};
        //{'1028':[<div4bday-cmenu>,<div4dday-cmenu>],...}
        this.cmenu_dic = {};
        this.div_dic = {};
        var bdic = model.B(), ddic = model.D(), i, k, pid, p;
        for(k in bdic){
            d = bdic[k];
            for(pid in d){
                p = M.e(pid);
                this.AF_prepare_item(p, 'B');
            }
        }
        for(k in ddic){
            d = ddic[k];
            for(pid in d){
                p = M.e(pid);
                this.AF_prepare_item(p, 'D');
            }
        }
    };// end of AC_CEVModel constructor
    AC_CEVModel.prototype.cid = function(){ return 'CEV'; }
    
    AC_CEVModel.prototype.AF_showme = function(cells){
        var i, ii;
        for(i=0; i<cells.length; i++){ // cell[i]: [jtd, '20151024']
            var m = cells[i][1].substr(4,4); // '1024'
            if(m in this.item_dic){
                var cell = $(cells[i][0]); // jQ calendar-cell obj.
                for(ii=0; ii<2; ii++){     // go thru B-menu and D-menu
                    if(this.cmenu_dic[m][ii]){
                        var t = (ii===0)? 'B' : 'D';
                        var msg = '<div class="cal-event-' + t + '"></div>';
                        this.div_dic[m][ii] = $(msg).appendTo(cell);
                        this.cmenu_dic[m][ii].AF_setup(this.div_dic[m][ii]);
                    }
                }
            }
        }
    };// end of AC_CEVModel.prototype.AF_showme
    
    // put ent into item_dic, make sure divs exist
    AC_CEVModel.prototype.AF_prepare_item = function(pa, type){
        var m, i,
        micon = 'pics/male-head.png', 
        ficon='pics/female-head.png';
        if(type==='B')      m = pa.dob().substr(4,4); // 4 digits b-day 1028
        else if(type==='D') m = pa.dod().substr(4,4); // 4 digits d-day 1028
        if(!(m in this.item_dic))
            this.item_dic[m]= [[],[]];//B-pas, D-pas
        if(!(m in this.cmenu_dic)){
            this.cmenu_dic[m] = [null, null];// 2x context-menus for B and D
            this.div_dic[m] = [null, null];// 2x jdiv
        }
        switch(type){
        case 'B':
            this.item_dic[m][0].push(pa);
            if(!this.cmenu_dic[m][0]){
                this.cmenu_dic[m][0] = new AC_ContextMenu( this, // this vmodel
                    null,            // DOM-element to attach - will be set
                    {title: 'M7530'},// "Birth anniversary" - menu-title  
                    true,            // left-click: true
                    null,            // (x,y) delta-adjustment
                    this.AF_adjust_cmenu);
            }
            this.cmenu_dic[m][0].AF_modify_entry({
                label: pa.id()+'@tname_face',// let LS set localized tname
                icon: pa.sex()==='male'? micon : ficon,
                func: this.AF_handle_click, 
                func_ctx: this, arg_array: [pa, 'B']});
            break;
        case 'D':
            this.item_dic[m][1].push(pa);
            if(!this.cmenu_dic[m][1]){
                this.cmenu_dic[m][1] = new AC_ContextMenu(this, 
                    null,            // DOM-element to attach - set later
                    {title:'M7531'}, // "Death anniversary" - menu-title  
                    true,            // left-click: true
                    null,            // (x,y) delta-adjustment
                    this.AF_adjust_cmenu);
            }
            this.cmenu_dic[m][1].AF_modify_entry({
                label: pa.id()+'@tname_face',// let LS set localized tname
                icon: pa.sex()==='male'? micon : ficon,
                func: this.AF_handle_click, 
                func_ctx: this, arg_array: [pa, 'D']});
            break;
        }
    };// end of AC_CEVModel.prototype.AF_prepare_item
    
    // extra render-adjustment for the context-menu shown up
    AC_CEVModel.prototype.AF_adjust_cmenu = function(cmenu){
        // cmenu has img of 16x16. set height to be 24px so icon fits better
        $('ul.contextMenuPlugin img').css('height','24px');
    };// end of AC_CEVModel.prototype.AF_adjust_cmenu
    
    AC_CEVModel.prototype.AF_handle_click = function(pa, t){
        // render a view with pa(ent) for center/focus
        this.AV_centerpa = pa;
        FD.AV_lserver.AF_pa_center(pa, this);
    };// end of AC_CEVModel.prototype.AF_handle_click
    
    AC_CEVModel.prototype.AF_handle_success = function(data, self){
        FD.AV_path_mgr.AF_chop_from_ci();
        // trigger tab-0 and set landing.active_index
        $('a#ui-id-1').trigger('click');
        FD.AF_view_faiv(self.AV_centerpa, true);
    };// end of AC_CEVModel.prototype.AF_handle_success
    
    return AC_CEVModel;
})();// end of AC_CEVModel
