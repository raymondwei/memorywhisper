/**
 * issues and discussions
 */
var AC_LandingPage400 = (function(){
    function AC_LandingPage400(fd, jbox, ctls){
        this.AJ_div = jbox;
        this.fd = fd;
        /*
        this.AV_actbuffer = this.fd.AV_actbuffer;
        this.AV_folders = []; // contains 2 top level itemvmodels
        var ids = [];
        this.fd.AV_store.AF_add2lst(
            ['FTSUPER-IT-lp500internals','FTSUPER-IT-lp500publicinfo'],
            ids);
        this.AV_act = 'get-topitfs';
        this.fd.AV_lserver.AF_getdics_helper(ids, this.AV_act, this);
        */
    };// end of AC_LandingPage400 constructor
    AC_LandingPage400.cid = function(){ return 'LP5'}

    AC_LandingPage400.prototype.AF_render = function(){
        this.AV_landing.AJ_pane.empty();
        U.AF_remove_class_pattern(this.AV_landing.AJ_pane,
                                  /(^|\s)t5-bg-\S+/g)
         .addClass('t5-bg-lp500');
        this.AJ_top_ul = $("<ul class='forum-ul'></ul>")
            .appendTo(this.AV_landing.AJ_pane);
        this.AV_forum = new AC_ForumGroup( this.fd,
            this.AV_itf,
            this.AJ_top_ul,'');// fid =='': a public forum, visitor can write
        // get all tds(docs) under this.AV_itf
        this.AV_act = 'get-tds';
        this.AV_actbuffer = FD.AV_actbuffer.AF_init();
        var ids = [];
        for(var i=0; i<this.AV_itf.iddict.docs.length; i++){
            ids.push(this.AV_itf.iddict.docs[i]);
        }
        this.fd.AV_lserver.AF_getdics_helper(ids,'get-tds', this);
    };// end of  AC_LandingPage400.prototype.AF_render

    AC_LandingPage400.prototype.AF_handle_success = function(data, self){
        if(self.AV_act === 'get-topitfs'){
            self.AV_itf = M.e('FTSUPER-IT-lp500publicinfo');
            if(!self.AV_itf){
                self.AV_act = "save-initials"; // itf2 missing
                self.AV_itf = new IT(
                    {_id: 'FTSUPER-IT-lp500publicinfo', type: 'F' });
                self.AV_itf.AF_ownerpa('FTSUPER-PA-superadmin');
                self.AV_itf.owner('FTSUPER-LP-LP500');
                self.AV_itf.name_face('en','Issues & Discussion');
                self.AV_itf.name_face('zh','议题 & 讨论');
                self.AV_itf.title_face('en','Issues & Discussion');
                self.AV_itf.title_face('zh','议题 & 讨论');
                self.AV_actbuffer.AF_add_save(self.AV_itf);
            }
            if(self.AV_act === 'save-initials'){ // if any initial is missing
                // save the created initial itf(s)
                var req_dic = self.AV_actbuffer.AF_get_reqdic(self.AV_act);
                self.fd.AV_lserver.AF_post('putdocs',req_dic, self);
            } 
        } // if(self.AV_act === 'get_topitfs')
        else if(self.AV_act === 'save-initials'){
            self.AV_actbuffer.AF_execute();
        } else if(self.AV_act === 'get-tds'){
            self.AV_actbuffer.AF_execute();
            self.AV_forum.AF_add_tdlines()
                .AF_expand_toggle(true);
        }
    };// end of  AC_LandingPage400.prototype.AF_handle_success
//U3RvcmllcyBvZiBsaWZlIGFuZCBmYW1pbHk=
    return AC_LandingPage400;
})();// end of class AC_LandingPage400
