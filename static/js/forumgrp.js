/**
 * AC_ForumGroup represents a forum-group, with a title bar where at right end
 * there is a + sign for adding a new thread, title text is on title bar.
 * data model: itF is the container its iddict.docs is a list of ids of
 * root tds. each td can have its iddict.children that are the following
 * comment-tds. If a child is RS, then it is a media file used by parent-td.
 */
var AC_ForumGroup = (function(){
    function AC_ForumGroup(AV_fd, jbox){
        /*
        this.AJ_root = jbox;
        this.AV_fd = AV_fd,
        this.AV_it = AV_itf;
        this.AJ_parent = AJ_parent;
        this.AV_actbuffer = this.AV_fd.AV_actbuffer.AF_init();
        this.AV_fid = AV_fid;
        this.AV_tdlines = [];
        /** <div class="itembox">
         *    <span class="item-title-box">
         *      <span class="LS" tag="<it-id>@name_face"></span></span>
         *    <div class="item-icon" style.background-image:url(<url>)
         *  </div>
         *  <ul class="forum-ul">
         *    <li></li>
         *    ...
         *  </ul>
         *
        var AJ_div = $('<div class="forum-folder"></div>');
        this.AJ_titlebox = $('<span class="item-title-box"></span>')
            .appendTo(AJ_div).css('top','5px');
        var m = '<span class="LS forum-title" tag="'+AV_itf.id()+
                '@name_face"></span>';
        this.AJ_title = $(m).appendTo(this.AJ_titlebox);
        this.AJ_title.text(AV_itf.name_face());
        this.AJ_foldericon = $('<div class="item-icon"></div>')
            .appendTo(AJ_div);
        this.AJ_foldericon.css('background-image','url(pics/folder-icon.png)');
        this.AJ_addbtn = $('<div class="forum-add-item-button"></div>')
            .appendTo(AJ_div);
        if(AJ_parent[0].tagName === 'UL'){
            this.AJ_li = $('<li class="forum-li"></li>').appendTo(AJ_parent);
            AJ_div.appendTo(this.AJ_li);
            this.AJ_titleline = $('<hr/>')
                .appendTo(this.AJ_li).css('visibility','hidden');
            this.AJ_ul = $('<ul class="forum-ul" id="'+AV_itf.id()+'_ul"></ul>')
                .appendTo(this.AJ_li);
            this.AJ_ul.css('display','none'); // initially schrink children
            this.AV_expand = false;
        } else {

        }
        this.AJ_dlg_layout = $('<div class="td-dlg"></div>');
        $('<span class="LS" id="td-title-label" tag="M8005"></span>')
          .text(this.AV_fd.AV_lang.face('M8005')).appendTo(this.AJ_dlg_layout);
        $('<span class="LS" tag="M7147" id="td-author-label"></span>')
          .text(this.AV_fd.AV_lang.face('M7147')).appendTo(this.AJ_dlg_layout);
        this.AJ_td_title = $('<input id="td-title-input"/>')
            .appendTo(this.AJ_dlg_layout);
        this.AJ_td_author = $('<input id="td-author-input"/>')
            .appendTo(this.AJ_dlg_layout);
        this.AJ_td_text = $('<div contentEditable="true" id="td-text"></div>')
            .appendTo(this.AJ_dlg_layout);
        this.AF_eventhandlers();
        */
    };// end of AC_ForumGroup constructor

    AC_ForumGroup.prototype.AF_eventhandlers = function(){
        var self = this;
        this.AJ_addbtn.unbind('click').bind('click', function(e){
            self.AF_popup_dlg('M8005');
        });
        this.AJ_titlebox.unbind('click').bind('click', function(e){
            self.AF_expand_toggle();
        });
    };// end of AC_ForumGroup.prototype.AF_seteventhandlers

    AC_ForumGroup.prototype.AF_wizard_handlers = function(self){
        self.AJ_td_title.unbind('input').bind('input', function(e){
            if($.trim(self.AJ_td_title.val()).length === 0){
                self.AJ_td_title.css('background-color','pink');
            } else {
                self.AJ_td_title.css('background-color','white');
            }
            self.AF_update_save_btn();
        });
        self.AJ_td_author.unbind('input').bind('input', function(e){
            if($.trim(self.AJ_td_author.val()).length === 0){
                self.AJ_td_author.css('background-color','pink');
            } else {
                self.AJ_td_author.css('background-color','white');
            }
            self.AF_update_save_btn();
        });
    };// end of AC_ForumGroup.prototype.AF_wizard_handlers

    // expand/shrink the ul-li list
    AC_ForumGroup.prototype.AF_expand_toggle = function(AV_expand){
        var ids = [];
        if(typeof(AV_expand) === 'undefined'){
            this.AF_expand_toggle(!this.AV_expand);
        } else{
            this.AV_expand = AV_expand;
            if(AV_expand){
                // show the dividing line <hr/> btwn title and <ul> list
                this.AJ_titleline.css('visibility','visible');
                // make <ul> list visible
                this.AJ_ul.css('display','inline-block');
                for(var i=0; i<this.AV_tdlines.length; i++){
                    // collect all children under every tdline into ids
                    ids = ids.concat(this.AV_tdlines[i].AV_td.iddict.children);
                }
                this.AJ_foldericon // show folder-open icon
                    .css('background-image','url(pics/folder-open-icon.png)');
                // show add-td(comment) button
                this.AJ_addbtn.css('visibility','visible');

                // request all children tds of all comment-tds
                this.AV_act = 'get-feedbacks';
                if(ids.length > 0){
                    this.AV_fd.AV_lserver
                        .AF_getdics_helper(ids, 'get-feedbacks', this);
                } else {
                    this.AF_handle_success(null, this);
                }
            } else {
                // hide the <hr/> under title text
                this.AJ_titleline.css('visibility','hidden');
                // hide <ul> list
                this.AJ_ul.css('display','none');
                // show folder-closed icon
                this.AJ_foldericon.css('background-image',
                                       'url(pics/folder-icon.png)');
                // hiden away add-td button
                this.AJ_addbtn.css('visibility','hidden');
            }
        }
    };// end of AC_ForumGroup.prototype.AF_expand_toggle

    // after new-td dialog popup appears, update SAVE button according to input
    AC_ForumGroup.prototype.AF_update_save_btn = function(){
        var AV_enable = this.AJ_td_title.val().length > 0 &&
                        this.AJ_td_author.val().length > 0;
        var AV_disable_clss = "ui-button-disabled ui-state-disabled";
        if(!AV_enable) this.AJ_save_btn.addClass(AV_disable_clss);
        else        this.AJ_save_btn.removeClass(AV_disable_clss);
    };// end of AC_ForumGroup.prototype.AF_update_save_btn

    // SAVE action of new-td dialog popup
    AC_ForumGroup.prototype.AF_save_td = function(){
        var lc = this.AV_fd.AF_lcode();
        this.td.name_face(lc, $.trim(this.AJ_td_title.val()));
        // input text line-break replaced with <br/> market-up
        var txt = this.AJ_td_text[0].innerText.replace(/\n/g,'<br/>');
        this.td.AF_content_face(lc, txt);
        this.td._content['author'] = Base64.encode(
            $.trim(this.AJ_td_author.val()));
        this.AV_actbuffer.AF_add_save(this.td);
        if(this.AV_container_tdl){// new td is child td of AV_container_tdl
            // update container_td.iddict.children
            var td = this.AV_container_tdl.AV_td,
            iddict_dic = $.extend(true, {}, td.iddict);
            iddict_dic.children.push(this.td.id());
            this.AV_actbuffer.AF_add_update(td, {iddict: iddict_dic});
        } else { // new td is a child of this.AV_it.docs
            // update AV_it.iddict.docs
            var itiddict = $.extend(true, {}, this.AV_it.iddict);
            itiddict.docs.push(this.td.id());
            this.AV_actbuffer.AF_add_update(this.AV_it, {iddict: itiddict});
        }
        var req_dic = this.AV_actbuffer.AF_get_reqdic("save-td");
        this.AV_act = 'add-td';
        this.AV_fd.AV_lserver.AF_post('putdocs', req_dic, this);
    };// end of AC_ForumGroup.prototype.AF_save_td

    AC_ForumGroup.prototype.AF_popup_dlg = function(title_locale){
        this.AV_actbuffer.AF_init();
        var self = this;
        this.AV_dlg = self.AJ_dlg_layout.dialog({
            autoOpen: false,
            width: 0.82 * parseInt($('div#body_container').css('width')),
            height: 645,
            modal: true,
            resizable: false,
            show: true,
            hide: true,
            close: function(e){
                self.AJ_dlg_layout.dialog('destroy');
            },
            open: function(e){
                self.AJ_td_title.css('background-color','pink').val('');
                self.AJ_td_author.css('background-color','pink').val('');
                self.AJ_td_text.text('');
                if(title_locale === 'M7506'){
                    $('#td-title-label').text(
                        FD.AV_lang.face(['M7506','M7505']));
                }
                self.AJ_save_btn = $('#the-tddlg-save-button');
                self.td = new TD({_id: M.mid('FTSUPER','TD')});
                self.td.AF_add_holder(self.AV_it.id());
                self.AF_wizard_handlers(self);
                self.AF_update_save_btn();
            },
            buttons: {
                'tddlg-save-button': {
                    id: 'the-tddlg-save-button',
                    text: 'OK',
                    click: function(ev){
                        self.AF_save_td();
                        self.AV_dlg.dialog('close');
                        return U.AF_stop_event(ev);
                    }
                },
                'tddlg-cancel-button':{
                    id: 'the-tddlg-cancel-button',
                    text: 'Cancel',
                    click: function(ev){
                        self.AV_dlg.dialog('close');
                        return U.AF_stop_event(ev);
                    }
                }
            }
        });
        self.AV_dlg.dialog('open');
        self.AV_dlg.dialog('widget').attr('id','the-tddlg-popup');
        self.AV_fd.AV_lang.AF_set_dlg('the-tddlg-popup', {
            '.ui-dialog-title': title_locale,   //'M7506' - Comment
            '#the-tddlg-save-button':'M1004',   // Save
            '#the-tddlg-cancel-button': 'M1003' // CANCEL
        });
    };// end of AC_ForumGroup.prototype.AF_popup_dlg

    AC_ForumGroup.prototype.AF_add_tdlines = function(tdline){
        var i, td, existed = false;
        if(!tdline){ // initial: add all tdline for all it.iddct.docs
            this.AV_tdlines.length = 0;
            for(i=0; i<this.AV_it.iddict.docs.length; i++){
                td = M.e(this.AV_it.iddict.docs[i]);
                if(td){
                    tdline = new AC_TDLine(this,
                     td, 0, i);
                    this.AF_add_tdlines(tdline);
                }
            }
        } else{
            for(i=0; i<this.AV_tdlines.length; i++){
                if(tdline.AV_td.id() === this.AV_tdlines[i].AV_td.id()){
                    existed = true;
                    break;
                }
            }
            if(!existed){
                this.AV_tdlines.push(tdline);
            }
        }
        return this;
    };// end of AC_ForumGroup.prototype.AF_add_tdlines

    AC_ForumGroup.prototype.AF_handle_success = function(data, self){
        self.AV_actbuffer.AF_execute();
        // add-td done: add a new feedback(child) td to a tdline
        if(self.AV_container_tdl){
            self.AV_container_tdl
                .AF_add_comment(self.td)
                .AF_render()
                .AF_toggle_expand(true);
            self.AV_container_tdl = null;
        } else // add-td done: add a td(top level) as comment td
          if(self.AV_act === 'add-td'){
            var ind = self.AV_tdlines.length,
            tdline = new AC_TDLine(self, self.td, 0, ind);
            self.AF_add_tdlines(tdline).AF_expand_toggle(true);
        } else if(self.AV_act === 'get-feedbacks'){
            // loop thru all tdlines - populate them with their
            // child-tdlines(feedback tds). But not expand.
            for(var i=0; i<self.AV_tdlines.length; i++){
                // add all feedback child-tdlines to this tdline
                self.AV_tdlines[i].AF_add_comment();
            }
        }
        self.AV_act = null;
    };// end of AC_ForumGroup.prototype.AF_handle_success

    return AC_ForumGroup;
})();// end of AC_ForumGroup class
