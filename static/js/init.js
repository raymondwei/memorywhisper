/*
 * Copyright (C) 2014 Adacro Inc. - All Rights Reserved
 */
var FD; // global 
// stop the process of loading of a media file
var __media_stop_load = function(va){
    var src, p, 
        m = va === 'v'? $('video') : $('audio');
    if(m.length > 0){
        src = $('source', m);
        if(src){
            src.attr('src','');
        }
        p = m.get(0);
        p.pause();
        p.load();
        if(va === 'a'){
            m.css('visibility','hidden');
        }
    }
};// end of __media_stop_load

// every 2 min, update FD._se's last_update
var __heartbeat = function(fd){
    console.log('Heartbeat: '+(new Date().getTime()));
    setInterval(function(){fd.AF_se('expire-check')}, 120000); // 2 minutes
}
// using WebRTC to get local-ip(winthin WAN), assign it to fd.AV_localip
// in case WebRTC is not supported by the browser: 'badapple' by deault.
var __get_client_localip = function(){
    window.RTCPeerConnection = window.RTCPeerConnection || 
        window.mozRTCPeerConnection ||  // compatibility 
        window.webkitRTCPeerConnection; // for firefox and chrome
    var ip = 'badapple'; // IE/Edge/Safari not offering this: bad-apple
    if(window.RTCPeerConnection){
        var pc = new RTCPeerConnection({iceServers:[]}), 
            noop = function(){};
        if(pc.createDataChannel){ // Edge/IE not offering this yet (201707)
            pc.createDataChannel("");    //create a bogus data channel
            // create offer and set local description
            pc.createOffer(pc.setLocalDescription.bind(pc), noop);
            pc.onicecandidate = function(ice){  //listen for candidate events
                if(!ice || !ice.candidate || !ice.candidate.candidate){
                    ip = 'badapple';
                    return;
                }
       ip= /([0-9]{1,3}(\.[0-9]{1,3}){3}|[a-f0-9]{1,4}(:[a-f0-9]{1,4}){7})/
                .exec(ice.candidate.candidate)[1];
                pc.onicecandidate = noop;
            };
        } else {
            ip = 'badapple';
        }
    }
    return ip;
};// end of __get_client_localip

$(document).ready(function () {
    var du = 1300;  // duration of tooltip showing in miliseconds
    $(document).tooltip({
        show: {
            effect: 'slideDown',
            delay: 500
        },
        track: true,
        open: function (event, ui) {
            $(ui.tooltip).css('z-index',100000000);
            setTimeout(function () {
                $(ui.tooltip).hide('explode'); //or use 'fadeout'
            }, du);
        }
    });
    /** make sure tooltip hide away on any mouse click(left-click) */
    $(document).bind('click', function(){
        $('.ui-tooltip').hide();
    })

    /** url may have parameter in document.location.search: 
     * ?private=TC6H8EU-PA-rKAaKS1407852727235 - go to personal homepage
     * ?docview=TC6H8EU-IT-rKAaKS1407852727235 - go to personal doc-page
     * ?lang=zh
     * get {private:'TC6H8EU-PA-r',docview:'TC6H8EU-IT-rK',lang:'en'}
     */
    FD = new AC_FDClass(U.urlparams(document.location.search));
    FD.AV_localip = __get_client_localip();
    __heartbeat(FD);

    // before leaving a browser tab/when reloading it, stop the process
    // of loading media file, which can be happening at the time
    window.onbeforeunload = function(e){
        console.log('onbeforeunload called');
        __media_stop_load('v'); // video
        __media_stop_load('a'); // audio
    };

    $(window).on('resize', function(){ 
        FD.AF_resize();
        $('body').scrollTop(0);
    })
});
