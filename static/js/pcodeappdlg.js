﻿// new dialog class for purpose of user applying for a new promocode
var AC_PCodeAppDlg = (function(){
    function AC_PCodeAppDlg(ftwz){
        var self = this;
        this.AJ_layout = $(ftwz.page_msg).filter('div#pcode-app-dlg');
        this.AJ_email = self.AJ_layout.find('#pcad-email');
        this.AJ_location = self.AJ_layout.find('#pcad-location');
        this.AJ_how = self.AJ_layout.find('#pcad-how');
        this.AJ_info = self.AJ_layout.find('#pcad-information');
    };// end of AC_PCodeAppDlg constructor
    
    AC_PCodeAppDlg.prototype.AF_clear = function(){
        this.AJ_email.val('');
        this.AJ_location.val('');
        this.AJ_how.val('');
        var txt = "You are applying for a free and new ROYAL FOLDER ";
        txt += "account. <br/><br/>After the official release of the product, ";
        txt += " a Promotional Code will be sent to you.<br/>";
        txt += "A ROYAL FOLDER account created using this code will be free of";
        txt += " charge for the first 6 months.<p/>";
        txt += "Your email address and other information you have given here "
        txt += "will be kept and handled with careful discretion. <p/>";
        txt += "Thank you for your interest."
        this.AJ_info.html(txt);
    };// end of AC_PCodeAppDlg.prototype.AF_clear
    
    AC_PCodeAppDlg.prototype.AF_popup = function(){
        var self = this;
        self.AF_clear();
        self.dlg = self.AJ_layout.dialog({
            autoOpen: false,
            title: FD.AV_lang.face('M7522'),
            modal: true,
            resizable: false,
            width: 800,
            height: 500,
            show: true,
            hide: true,
            open: function(e){},
            buttons:[
                {   id: "pcad-submit-button",
                    tabIndex: -1,   // make sure no button has focus by def.
                    text: "Submit",
                    click: function (){
                        self.AF_send_req();
                        self.dlg.dialog('close');
                    }
                },
                {   id: "pcad-cancel-button",
                    tabIndex: -1,   // make sure no button has focus by def.
                    text: "Cancel",
                    click: function () {
                        self.dlg.dialog('close');
                    }
                }
            ]
        });
        self.dlg.dialog('open');
        self.dlg.dialog('widget').attr('id','pcad-registration-dlg');
        FD.AV_lang.AF_set_dlg('pcad-registration-dlg', {
            '.ui-dialog-title': 'M7522',
            '#pcad-submit-button': 'M0002',
            '#pcad-cancel-button': 'M1003'
        });
    };// end of AC_PCodeAppDlg.prototype.AF_popup
    
    AC_PCodeAppDlg.prototype.AF_send_req = function(){
        this.reqverb = 'appacct';
        var dic = { email: this.AJ_email.val(), 
                    location: Base64.encode(this.AJ_location.val()),
                    how: Base64.encode(this.AJ_how.val()) };
        FD.AV_lserver.AF_post(this.reqverb, dic, this);
    };// end of AC_PCodeAppDlg.prototype.AF_send_req
    
    AC_PCodeAppDlg.prototype.AF_handle_success = function(data, self){
        alert("Request for applying promo-code successful.");
    };
    AC_PCodeAppDlg.prototype.AF_handle_err = function(data){
        alert("failed: "+data);
    };
    return AC_PCodeAppDlg;
})();
