/**
 * Copyright (C) 2018 Adacro Inc. - All Rights Reserved
 * ----------------------------------------------------
 * Offer API for indexedDB: which is (internally) this.db
 * return jQuery promise
 * 1. AF_get_ent(id) - get a single ent done-func(e) e: entity fetched
 * 2. AF_get_cid_ents(cid) - get all ents of cid class. from curr db only
 * 3. AF_get_cid_ids(cid) - get all ids of cid class from curr db
 * 4. AF_get_ents(ids) - e: list of entities. ids can be of max 2 DBs
 * 5. AF_put_ents(ent) - e: list of ents. ids can be of max 2 DBs
 * 6. AF_delete_ents(ids) e: count. ids can be of max 2 DBs
 * ---------------
 * Note: 2 instances of FidDB can only be for FTSUPER and fid
 */
var AC_FidDB = (function(){
    function AC_FidDB(fid, cb){
        var indexedDB = indexedDB = window.indexedDB || window.mozIndexedDB || 
                           window.webkitIndexedDB || window.msIndexedDB;
        if(!window.indexedDB){
            alert("your browser doesnt support indexedDB");
            return;
        }
        this.dbname = 'DB_'+fid.toUpperCase();
        this.fid = fid;
        var self = this,
            req = indexedDB.open(this.dbname,20);
        req.onupgradeneeded = function() {
            self.db = req.result;
            var store = self.db.createObjectStore("ents", {keyPath: "id"});
            store.createIndex("cid","cid",{unique: false});
            store.createIndex("ts", "ts", {unique: false});
        };
        req.onsuccess = function(){ // indexedDB opened
            self.db = req.result;
            FD.idbs[fid] = self;
            if(fid !== 'FTSUPER' &&           // fid is not FTSUPER
               !('FTSUPER' in FD.idbs)){      // FTSUPER-DB not yet loaded
                new AC_FidDB('FTSUPER', null);// load FTSUPER-DB, no callback
            }
            if(cb) cb();
        }
        req.onerror = function(err){
            alert(err);
        }
    };// end of AC_FidDB(fd, fid) constructor

    // if ids contains ids of two fids (FTSUPER + fid-ids), 
    // return [[id,id,..],[id,id,..]] where first sub-list is FTSUPER ids;
    // if no FTSUPER-ids exists, return [[id,id,..]] - 1 sub-array only
    // ids can also be array of ents or ent-json objects
    AC_FidDB.prototype.AF_split_list = function(ids){
        var i, id, res =[], sft = [], result = [];
        for(i=0; i<ids.length; i++){
            if(typeof(ids[i]) === 'string'){
                id = ids[i];
            } else {
                id = ids[i]._id;
            }
            if(id.substr(0,7) === 'FTSUPER'){
                sft.push(ids[i]);
            } else {
                res.push(ids[i]);
            }
        }
        if(sft.length > 0){
            result.push(sft);
        } 
        if(res.length > 0) {
            result.push(res);
        }
        return result;
    };// end of AC_FidDB.prototype.AF_split_list

    // get a single entity. given its id. entity will be put into store
    AC_FidDB.prototype.AF_get_ent = function(id, db){
        var self = this;
        var deferred = $.Deferred();
        var db = db? db : this.db;
        var tx = db.transaction("ents"); // default mode: readonly
        var store = tx.objectStore("ents");
        var ent, req = store.get(id);
        req.onsuccess = function(e){
            if(!e || !e.target || !e.target.result){
                deferred.reject("idb-get-ent failed");
            } else {
                ent = M.AF_load(e.target.result.dic);
                deferred.resolve(ent);
            }
        }
        req.onerror = function(err){
            console.log("AC_FidDB.AF_get_ent filed: "+err);
            deferred.reject(null);
        }
        return deferred.promise();
    };// end of AC_FidDB.prototype.AF_get_ent

    // get array of entities from idb. put into local-store, 
    // over-writing entities in store if already pre-existed
    AC_FidDB.prototype.AF_get_cid_ents = function(cid){
        var self = this, i, dic, res = [],
            deferred = $.Deferred(),
            tx = this.db.transaction("ents"),
            store = tx.objectStore("ents");
        var myindex = store.index("cid");
        var req = myindex.getAll(cid);
        req.onsuccess = function(e){
            if(!e || !e.target || !e.target.result){
                //deferred.reject("idb-get-cid-ents failed");
            } else {
                for(i=0; i<e.target.result.length; i++){
                    dic = e.target.result[i].dic;
                    res.push(M.AF_load(dic));
                }
            }
        };
        tx.oncomplete = function(e){
            deferred.resolve(res);
        }
        tx.onerror = function(err){
            deferred.reject(err);
            console.log("idb-get-cid-ents failed: "+err);
        }
        return deferred.promise();
    };// end of AC_FidDB.prototype.AF_get_cid_ents

    /* ids is array of ent-ids. return.done(function(res){}), where 
     * res:{ents: array of entities; miss: array of ids failed to get from idb}
     * all res.ents have been then put into localstore already.
     * ids can be from both FidDB.db and fid-db
     * ---------------------------------------------------------------- */
    AC_FidDB.prototype.AF_get_ents = function(the_ids){
        var self = this;
        var res= { ents: [],           // resulting array of ents
            miss: the_ids.slice()};    // clone of ids, to get missings
        var deferred = $.Deferred();   // jQuery: return defrd.promise()

        function _get_ents(id_lst, db, res, dfd){ // id_lst ids from one fid-db
            var ids = id_lst.slice();
                id = ids.shift();
            var p = self.AF_get_ent(id, db);
            p.done(function(e){
                if(e){
                    res.ents.push(e);
                    var ind = res.miss.indexOf(e.id());
                    if(ind > -1)
                        res.miss.splice(ind, 1);
                    if(e.cid() === 'FC' 
                       && e.iddict.rs && e.iddict.rs.length >= 29){
                        ids.push(e.iddict.rs);
                    }
                }
            });
            p.always(function(x){
                if(ids.length === 0){
                    dfd.resolve(res);
                } else {
                    _get_ents(ids, db, res, dfd);
                }
            })
            return dfd.promise();
        };// end of _get_ents

        if(!the_ids || the_ids.length === 0){
            deferred.resolve({ents:[], miss: []});
        } else {
            var lst, lst2, self = this;
            var splt = this.AF_split_list(the_ids);
            if(splt.length === 1){ // only 1 fid. can be FTSUPER or 
                lst = splt[0];     // customer fid. It is this.fid anyway
                return _get_ents(lst, this.db, res, deferred);
            } else if(splt.length === 2) {
                lst = splt[0]; // ids array for FTSUPER
                lst2 = splt[1];// ids array for FD.fid() 
                // first get ents from FTSUPER db
                _get_ents(lst, FD.idbs['FTSUPER'].db, res, deferred)
                    .done(function(res){
                    // get lst2-ids from fid-db
                    _get_ents(lst2, self.db, res, deferred).done(function(e){
                        // lst2 done. merge res and e
                        var result = {};
                        if(e){
                            result.ents = res.ents.concat(e.ents);
                            result.miss = res.miss.concat(e.miss);
                            deferred.resolve(result);
                        } else {
                            deferred.reject(result);
                        }
                    }).fail(function(r){
                        deferred.reject(r);
                    })
                }).fail(function(r){
                    deferred.reject(r);
                });
            }
        }
        return deferred.promise();
    };// end of AC_FidDB.prototype.AF_get_ents

    AC_FidDB.prototype.AF_get_cid_ids = function(cid){
        var self = this, i, dic, res = [],
            deferred = $.Deferred(),
            tx = this.db.transaction("ents"),
            store = tx.objectStore("ents");
        var myindex = store.index("cid");
        var req = myindex.getAllKeys(cid);
        req.onsuccess = function(e){
            if(e && e.target && e.target.result){
                for(i=0; i<e.target.result.length; i++){
                    res.push(e.target.result[i]);
                }
            }
        };
        tx.oncomplete = function(e){
            deferred.resolve(res);
        }
        tx.onerror = function(err){
            deferred.reject(err);
            console.log("idb-get-cid-ents failed: "+err);
        }
        return deferred.promise();
    };// end of AC_FidDB.prototype.AF_get_cid_ids

    // ents is array of 
    // 1: FTBase entities - need to be toJSON-ed, then put into idb;
    // 2: json object - put into idb; and turn it to FTBase ent/put into store
    AC_FidDB.prototype.AF_put_ents = function(ents){
        function put_ents(es, db){
            var i, id, cid, ts, dic, dic0, res = [], 
                defrd = $.Deferred(),
                tx = db.transaction("ents","readwrite"),
                store = tx.objectStore("ents");
            for(i=0; i<es.length; i++){ // each es[i] can be json or ent
                id = es[i]._id;
                cid = id.substr(8,2);// PA of TC6H8EU-PA-EFhzDY1407882558674
                if(es[i].cid){ // es[i] is a FT-entity, for cid function exists
                    ts = es[i].tx();
                    res.push(es[i]);    // dont put them into localstore
                    dic0 = es[i].toJSON(); // json object of the entity: dic0
                } else { // ents[i] is a json object, for cid == undefined: 
                    ts = es[i].tx;
                    dic0 = es[i];
                    res.push(M.AF_load(dic0)); // put into localstore
                }
                dic = JSON.parse(JSON.stringify(dic0)); // clone it into dic
                var req = store.put({'id':id, 'cid': cid,'ts': ts,'dic': dic});
            }
            tx.oncomplete = function(e){
                defrd.resolve(res);
                console.log(
                    'idb-puts successful: put '+res.length+' rows into idb.');
            }
            tx.onerror = function(err){
                defrd.reject(err);
                console.log('idb-puts failed: '+err);
            }
            return defrd.promise();
        }; // end of function put_ents

        var deferred = $.Deferred(),
            res = [];
        if(!ents || ents.length === 0){
            deferred.resolve(res)
        } else {
            var lst, lst2, self = this;
            var splt = this.AF_split_list(ents);
            if(splt.length === 1){
                lst = splt[0];
                return put_ents(lst, this.db);
            } else if(splt.length === 2){
                lst = splt[0];
                lst2 = splt[1];
                // put lst, then lst2, merge them to result, resolve(result)
                put_ents(lst, FD.idbs['FTSUPER'].db).done(function(ents){
                    put_ents(lst2, self.db).done(function(e){
                        var result;
                        if(e){
                            result = ents.concat(e);
                            deferred.resolve(result);
                        } else {
                            deferred.reject(r);
                        }
                    }).fail(function(r){
                        deferred.reject(r);
                    })

                }).fail(function(r){
                    deferred.reject(r);
                })
            }
        }
        return deferred.promise();
    };// end of AC_FidDB.prototype.AF_put_ents

    // delete entities from idb. ids is array of id-strings, which can be
    // from max 2 DBs (fid-DB or FTSUPER and fid-DB)
    AC_FidDB.prototype.AF_delete_ents = function(ids){
        var self = this, defrd = $.Deferred(); 
        function delete_ents(ids, db){
            var i, deferred = $.Deferred(), cnt = 0,
            tx = db.transaction("ents","readwrite"),
            store = tx.objectStore("ents");
            for(i=0; i<ids.length; i++){
                var req = store.delete(ids[i]);
                req.onsuccess = function(e){
                    cnt++;
                }
            }
            tx.oncomplete = function(e){
                deferred.resolve(cnt);
                console.log('idb-delete-ents done: '+cnt+' deleted.');
            }
            tx.onerror = function(err){
                deferred.reject(err);
                console.log('idb-delete-ents failed: '+err);
            }
            return deferred.promise();
        };// end of function delete_ents

        if(!ids || ids.length === 0){
            defrd.resolve(0);
        } else {
            var splt = this.AF_split_list(ids);
            if(splt.length === 1){
                return delete_ents(splt[0], this.db);
            } else {
                delete_ents(splt[0], FD.idbs['FTSUPER'].db).done(function(e){
                    if(e){
                        delete_ents(splt[1], self.db).done(function(e2){
                            if(e2){
                                defrd.resolve(e + e2);
                            } else {
                                defrd.resolve(e);
                            }
                        })
                    } else {
                        defrd.resolve(0);      
                    }
                }).fail(function(r){
                    defrd.reject(-1);
                });
            }    
        }
        return defrd.promise();
    };// end of AC_FidDB.prototype.AF_delete_ents

    AC_FidDB.prototype.AF_test = function(k){
        /**
         * test: go to url: 毛泽东 homepage:
         * localhost:5000/index.html?private=TC6H8EU-PA-rKAaKS1407852727235
         * AF_test("1") -> see 273 PAs ents
         * AF_test("2") -> see 7 ents fretched from idb
         * AF_test('3') -> deletes 7 ents from idb (they still in localstore)
         * AF_test('4') -> see 7 put into idb (copies from localstore)
         */
        var ids = ["TC6H8EU-BA-IwSYZX1407882257658",
            "TC6H8EU-FA-TC6H8EU",
            "TC6H8EU-BA-QUCxRs1407853071363",
            "TC6H8EU-IP-KvbBhU1409773206271",
            "TC6H8EU-PA-MNnFIM1407882659442",
            "TC6H8EU-TD-UFKwOZ1407852727236",
            "TC6H8EU-BA-ndRqzW1407852727237"];
        if(k ==="1"){ // get-cid-ents
            this.AF_get_cid_ents('PA').done(function(e){
                debugger;
            })
        } else if(k === '2'){ // get ents of diff classes
            this.AF_get_ents(ids).done(function(e){
                debugger;
            })
        } else if(k === '3'){ // testing deleting
            this.AF_delete_ents(ids).done(function(e){
                debugger;
            })
        } else if(k === '4'){ // testing puts - after testing '3'(deletings)
            var es = M.e(ids);// ents identified by ids must exist in store
            this.AF_put_ents(es).done(function(e){
                debugger;
            })
        }
    }

    return  AC_FidDB;
})();
