/*
 * Copyright (C) 2014 Adacro Inc. - All Rights Reserved
 * ----------------------------------------------------------------------------
 * AC_ItemVModel is the viewmodel attached to an IT-instance, of either D/F types.
 */
var AC_ItemVModel = (function(){
    // model: it-data-model, parent can be ItemPane (root ul holder)
    // or AC_ItemVModel (folder(itv). Parent always has .ulid(id of ul)
    function AC_ItemVModel(model) {
        this.model = model;
        model.vmodel = this;
        this.AV_actbuffer = FD.AV_actbuffer;
        this.AV_itempane = FD.AV_infov.AV_itempane;
        this.AV_state = 'shrunk';

        // every item is a <li> element, title shown as text of the li
        this.AJ_li = $('<li class="right-pane" id="' + model.id() + '_li"></li>');
        this.AJ_itembox = $('<div class="itembox"></div>').appendTo(this.AJ_li);
        this.jtitle=$('<span class="item-title-box"></span>')
                    .appendTo(this.AJ_itembox);
        this.AJ_name = $('<span class="LS"></span>').appendTo(this.jtitle);
        this.AV_iconv = new AC_ItemIconVModel(this);
        this.AF_update_lock();
        if (model.type()[0] === 'F') {
            this.AJ_ul = $('<ul class="right-pane" id="' + model.id() + '_ul"></ul>')
                    .appendTo(this.AJ_li).css({ 'display': 'none' });
        } else {
            this.AV_docpane = FD.AV_infov.AV_docpane.AF_init();
        }
    };// end of AC_ItemVModel constructor
    
    AC_ItemVModel.prototype.cid = function(){ return 'ITV'; }
    
    AC_ItemVModel.prototype.AF_update_lock = function(){
        if(this.model.type()[0] === 'F'){
            var old = this.isLocked;
            this.isLocked = this.model.pk() === 'M1513' && !M.OA(this.model);
            if(old !== this.isLocked)
                this.AV_iconv.AF_reset();
            if(this.isLocked){ // password required
                this.AV_pwd_dlg = new AC_PwdPopup(this);
            }
        }
        if(this.AV_ctxtmenu)
            this.AV_ctxtmenu.AF_setup(this.AJ_itembox);
        else
            this.AV_ctxtmenu = new AC_ContextMenu(this,// vmodel: this
                                this.AJ_itembox,       // dom element to hang on 
                                {title: "M7125"});     // data: M7125: Action
                                // left-click: null - right click
        return this;
    };// end of AC_ItemVModel.prototype.AF_update_lock
    
    AC_ItemVModel.prototype.AF_update_ctxtmenu = function(){
        this.AV_ctxtmenu.AV_settings.items.length = 0;
        if(this.model.type()[0] == 'F'){
            this.AV_ctxtmenu.AF_modify_entry({
                label: 'M7127',  // new folder
                icon: 'pics/add-folder-icon.png',
                func: this.AV_itempane.AV_iw.AF_popup_dialog,
                func_ctx: this.AV_itempane.AV_iw, 
                arg_array: [this.model, 'M7127', 'add']});
            this.AV_ctxtmenu.AF_modify_entry({
                label: 'M7128',  // new document
                icon: 'pics/add-document-icon.png',
                func: this.AV_itempane.AV_iw.AF_popup_dialog,
                func_ctx: this.AV_itempane.AV_iw, 
                arg_array: [this.model, 'M7128', 'add']});
            this.AV_ctxtmenu.AF_modify_entry({
                label:'M7129', // update/edit
                icon: 'pics/doc-icon.png',
                func: this.AV_itempane.AV_iw.AF_popup_dialog, 
                func_ctx: this.AV_itempane.AV_iw, 
                arg_array: [this.model, 'M7107', 'edit']}); //M7107: Folder
        } else {
            this.AV_ctxtmenu.AF_modify_entry({
                label: 'M7129', // "Update":edit doc entry
                icon: 'pics/doc-icon.png',
                func: this.AV_itempane.AV_iw.AF_popup_dialog, 
                func_ctx: this.AV_itempane.AV_iw, 
                arg_array: [this.model, 'M7108', 'edit']});// M7108: "Document"
        }
        var owner = this.model.owner(),
        index = this.model.AF_index(), 
        islast = this.model.is_last();
        while( index === 0 && 
               owner.cid() !== 'IP'){
            index = owner.AF_index();
            if(index === 0)
                owner = owner.owner();
        }
        if(index !== 0){
            this.AV_ctxtmenu.AF_modify_entry({
                label: 'M0003', // up
                icon: 'pics/upsmall.png',
                func: this.AF_move_delete, 
                func_ctx: this, arg_array: ['up']});
        }
        if(!islast){
            this.AV_ctxtmenu.AF_modify_entry({
                label: 'M0004', // down
                icon: 'pics/downsmall.png',
                func: this.AF_move_delete, 
                func_ctx: this, arg_array: ['down']});
        }
        // dont care if children exist(s) under this it(itF/itD/fc).
        // server will collect and delete the whole subtree
        this.AV_ctxtmenu.AF_modify_entry({
            label: 'M0006', // delete folder
            icon: 'pics/erase.png',
            func: this.AF_move_delete,
            func_ctx: this, arg_array: ['delete']});
    };// end of AC_ItemVModel.prototype.AF_update_ctxtmenu
    
    AC_ItemVModel.prototype.AF_update_clickables = function(){
        this.AJ_name.attr('tag',this.model.id()+'@name_face')
            .text(this.model.name_face(FD.AF_lcode()));
        var ttl_txt = this.model.title_face(this.AV_lcode);
        this.AJ_itembox.attr('title',ttl_txt);
        var self = this;
        this.AF_update_ctxtmenu();
        if(this.model.type()[0] === 'F'){
            this.AJ_itembox.unbind('click').bind('click', function (e) {
                if( self.AV_state === 'shrunk'){
                    if(self.isLocked)
                        self.AV_pwd_dlg.AF_popup_dlg(self.model.id());// id as pwdkey
                    else
                        self.AF_toggle_state();    // expan/shrink operation
                    return U.AF_stop_event(e);
                } else {
                    self.AF_toggle_state();    // expan/shrink operation
                    return U.AF_stop_event(e);
                };
            });
            // for debug purpose: see id of this it(f)
            this.jtitle.unbind('dblclick').bind('dblclick', function (e) {
                if (FD.AV_is_localhost) {
                    var dbg_msg = 'id:' + self.model.id(); alert(dbg_msg);
                }
            });            
        } else {    // this.model.type()[0] === 'D'
            this.AJ_itembox.unbind('click').bind('click', function (e) {
                self.AV_act = 'open-doc';
                FD.AV_lserver.AF_get_info(self.model, self);
                return U.AF_stop_event(e);
            });
        }
    };// end of AC_ItemVModel.prototype.AF_update_clickables
    
    // handle move-up/-down, and delete of this it
    AC_ItemVModel.prototype.AF_move_delete = function(AV_action){
        this.AV_actbuffer.AF_init();
        this.AV_act = AV_action;
        var cont = this.model.owner();
        var iddict = $.extend(true, {}, cont.iddict); // deep == true
        var msg = (this.model.type()[0]==='F')? 'folders' : 'docs';
        var peers = iddict[msg];
        var ind = this.model.AF_index(), trade_it, dest_folder;
        switch(AV_action){
        case 'up':
        case 'down':
            // find next up/down sibling under the same container, if exist
            var trade_it = AC_ItemPane.AF_next_peer(AV_action, this.model, cont);
            if(trade_it){
                peers.splice(trade_it.AF_index(), 0, peers.splice(ind, 1)[0]);
                this.AV_actbuffer.AF_add_update(cont, {'iddict': iddict});
            } else {
                // this is the last sibling in up/down direction. Find next
                // sibling(uncle) container, on next higher level
                dest_folder = AC_ItemPane.AF_dest_folder(AV_action,this.model,cont);
            }
            break;
        case 'delete':
            peers.splice(ind,1); // remove this from peers in dict
            this.AV_actbuffer.AF_add_delete(this.model)
                .AF_add_update(cont, {'iddict': iddict}); 
            break;
        }
        this.AV_actbuffer.AF_add_call(this.AV_itempane.AF_populate,
                                      this.AV_itempane,[cont]);
        if(dest_folder){ // target 'uncle-container' to move tooward exists
            // remove it from cont (cont.iddict update);
            peers.splice(ind, 1); // remove this it from its peers
            // it owner update cont->dest_folder
            var it_iddict = $.extend(true, {}, this.model.iddict);
            it_iddict['owner'] = dest_folder.id();
            
            // add it to dest_folder
            var idict = $.extend(true, {}, dest_folder.iddict);
            idict[msg].push(this.model.id());
            peers.splice(ind,1);
            this.AV_actbuffer.AF_add_update(this.model, {'iddict': it_iddict})
                .AF_add_call(cont.vmodel.AF_toggle_state,cont.vmodel,['shrunk'])
                .AF_add_update(dest_folder,{'iddict':idict})
                .AF_add_call(this.AV_itempane.AF_populate,
                             this.AV_itempane,[dest_folder])
                .AF_add_update(cont, {'iddict': iddict})
                .AF_add_call(this.AV_itempane.AF_populate,
                             this.AV_itempane,[cont])
                .AF_add_call(this.AV_itempane.AF_open_folders,null,[dest_folder]);
            // for requesting adding it to dest_folder updating dest_folder,
            // it must have been expanded. If not first make sure of that
            if(dest_folder.vmodel.populated){
                this.AF_send_req(); // already expanded, go ahead send actbuffer
            } else {
                // sent request for expanding dest_folder
                this.AV_act = 'pending-'+this.AV_act;
                FD.AV_lserver.AF_get_info(dest_folder, this);
            }
        } else { // no dest_folder
            this.AF_send_req();
        }
    };// end of AC_ItemVModel.prototype.AF_move_delete

    AC_ItemVModel.prototype.AF_send_req = function(){
        var req_dict = this.AV_actbuffer.AF_get_reqdic('putdocs');
        FD.AV_lserver.AF_post('putdocs', req_dict, this);
    };// end of AC_ItemVModel.prototype.AF_send_req
    
    // if to_state not given(null), toggle state; otherwise set state
    AC_ItemVModel.prototype.AF_toggle_state = function (to_state) {
        if (to_state) {
            if(this.AV_state === to_state) return;
            this.AV_state = to_state;
            if (to_state === 'expanded'){
                this.AV_act = 'expand';
                FD.AV_lserver.AF_get_info(this.model, this);
            } else { // 'shrunk'
                this.AV_act = 'shrink';
                this.AJ_ul.css({ 'display': 'none' });
                var folders = this.model.folders();
                for(var i=0; i<folders.length; i++){
                    if(folders[i].vmodel)
                        folders[i].vmodel.AF_toggle_state('shrunk');
                }
            }
        } else {
            if (this.AV_state === 'shrunk') { // was shrunk, now expanded
                this.AV_act = 'expand'; // state set to 'expanded' in handle_succ
                FD.AV_lserver.AF_get_info(this.model, this);
            } else {                       // was expanded, now shrunk
                this.AJ_ul.css({ 'display': 'none' });                
                this.AV_act = 'shink'; 
                this.AV_state = 'shrunk';
                var folders = this.model.folders();
                for(var i=0; i<folders.length; i++){
                    if(folders[i].vmodel)
                        folders[i].vmodel.AF_toggle_state('shrunk');
                }
            }
        }
        this.AV_iconv.AF_reset();
    };// end of AC_ItemVModel.prototype.AF_toggle_state
    
    // handle the succ response of possible request sent by expand-children
    AC_ItemVModel.prototype.AF_handle_success = function(data, self){
        if(this.AV_act === 'expand'){  // type=='F': expand folder
            this.AV_itempane.AF_populate(this.model);
            this.AV_state = 'expanded';
            this.AV_iconv.AF_reset();
            this.AJ_ul.css({ 'display': 'inline-block' });
        } else if(this.AV_act.indexOf('pending') > -1){
            // srv-req for getting docs done: now execute up/down
            this.AV_act = this.AV_act.split('-')[1]; // up/down (to dest_folder)
            this.AF_send_req();
        } else if (this.AV_act.indexOf('open-doc') > -1) {
            // type == 'D'; act == 'open-doc-noload' or 'open-doc'
            this.AV_docpane.AF_showme(this.model);
        } else {    // up/down/delete
            this.AV_actbuffer.AF_execute();
        }
        self.AV_itempane.AF_trim_names();
    };// end of AC_ItemVModel.prototype.AF_handle_success
    
    AC_ItemVModel.prototype.AF_handle_err = function(data, self){
        alert(data);
    };// end of AC_ItemVModel.prototype.AF_handle_err

    return AC_ItemVModel;
})(); // end of AC_ItemVModel
