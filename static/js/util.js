﻿/*
 * Copyright (C) 2014 Adacro Inc. - All Rights Reserved
 * ---------------------------------------------------------------------------- 
 * Utility container class
 */
var U = (function(){
    function U(){}
    U.AF_osex = function(sex){
        if(sex === 'male')  return 'female';
        return 'male';
    }
    U.INT = function(str){
        var n = parseInt(str);
        return isNaN(n)? 0 : n;
    }
    U.AV_GEN_L        = 175;  // PAV_STEM_LENG + PAV_WIDTH + PAV_HIND_LENG
    U.AV_PAV_H        = 34;   // pv div height
    U.AV_PAV_W        = 140;  // pv div width
    U.AV_PAV_YMIND    = 28;   // free space not to be invaded, up/down of a pv
    U.AV_PAV_STEM_L   = 20;   // gen-left-edge to pv div left edge
    U.AV_PAV_HIND_L   = 15;   // pv div hind-edge to end of generation column
    U.AV_BAV_H        = 28;   // bv div height
    U.AV_BAV_W        = 20;   // bv div width
    U.AV_BAV_XOFFSET  = 60;   // pv div left to bv div left
    U.AV_BAV_GOFFSET  = 80;   // g-col start to bv left
    U.AV_BAV_TAIL_LEN = 76;   // g-col start to bv left
    
    U.AV_PPANE_WIDTH = 290;
    
    U.AV_MAX_FGRAPH_HEIGHT = 1200;
    U.AV_MIN_FGRAPH_HEIGHT = 687;
    U.AV_DEF_GEN_NUMBER = 5;
    
    U.urlparams = function(s){//s: '?private=TC6H8EU-PA-rKAaKS1407&lang=zh'
        var r = {};
        if(!s || s.length == 0) return r;
        var i,p,m = s.substr(1).split('&');// cut away leading ?, split &
        for(i=0; i < m.length; i++){
            p = m[i].split('=');
            if(p.length === 2){
                r[p[0]] = p[1];
            }
        }
        var splt = location.host.split('.');// can be "zh.memorywhisper.com"
        if(!r.lang && splt.length === 3){   // lang not set, 3 pieces:get "zh"
            r.lang = splt[0];               // set lang to "zh"
        }
        return r; // {private:'TC6H8EU-PA-rKAaKS1407',lang: 'zh'}
    };// end of U.urlparams 

    U.isb64 = function(m){
        var sofar = m.length > 8 && m.length % 4 ===0 && 
            m.split().length === 1 && m.split('\n').length === 1;
        if(!sofar) return false;
        /*
        // IE doesn't support RegExp() call.
        // issue: there are some places in 
        // 1.jquery-ui-*.js; 2.lang-all.js; 3.moment.js
        // I do use those 3 libs. This means: IE will not work!
        var reg = new RegExp(/[^0-9a-zA-Z+/=]/,'g');
        var r = m.match(reg); // exists a char outside of base64 charset?
        var isbase64 = !(r && r.length > 0);
        return isbase64;
        */
        var notb64char = m.match(/[^0-9a-zA-Z+/=]/);
        var found_non_b64 = !!notb64char;
        return !found_non_b64;
    }

    U.AJ_render_pane = null;
    U.AJ_render_svg = null;

    // (positive)distance male-pv top to the bv top. ind is bv.model.AF_mindex()
    U.AF_male_anchortop2bv_top = function(ind){
        return U.AV_PAV_H+ind*(U.AV_BAV_H + U.AV_PAV_H + U.AV_PAV_YMIND);
    }
    // (positive)distance from female-pv top to bv top. ind is bv.model.AF_findex()
    U.AF_female_anchortop2bv_top = function(ind){
        return U.AV_BAV_H+ind*(U.AV_BAV_H + U.AV_PAV_H + U.AV_PAV_YMIND);
    }
    //U.AV_BUCKET_ROOTURL = window.location.host + '/blobs/';
    U.AV_BUCKET_ROOTURL = '/blobs/';
    
    U.AF_get_pav_bg_classes = function(pav){ //
        var classes;
        if(pav.sex() === 'male'){
            // existing and alive male
            if(pav.model && 
               pav.model.dod() === ''){
                classes = {'focused': 'male-bg-glow',
                           'unfocused': 'male-bg',
                           'glow-str': '0px 0px 5px 5px #32C7F4'};
            } else{
                // virtaul or dead male
                classes = {'focused': 'dead-male-bg-glow',
                           'unfocused': 'dead-male-bg',
                           'glow-str': '0px 0px 5px 5px  #1171F7'};
            }
        } else {
            // existing and alive female
            if(pav.model && 
               pav.model.dod() === ''){
                classes = {'focused': 'female-bg-glow',
                           'unfocused': 'female-bg',
                           'glow-str': '0px 0px 5px 5px #EE62D2'};
            } else {
                // virtaul or dead female
                classes = {'focused': 'dead-female-bg-glow',
                           'unfocused': 'dead-female-bg',
                           'glow-str': '0px 0px 5px 5px  #B22530'};
            }
        }
        return classes;
    }// end of U.pav_bg
    
    U.AF_trim_adopt_sign = function(pid){
        if($.type(pid) === 'array'){
            var res = [];
            for(var i=0; i< pid.length; i++){
                res.push(U.AF_trim_adopt_sign(pid[i]));
            }
            return res;
        } else if($.type(pid)==='string'){
            if(pid.slice(-1) === '-' || pid.slice(-1) === '+')
                return pid.substr(0, pid.length-1);
            return pid;
        }
    }
    
    U.AF_validateEmail = function (email) { 
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
    };// end of U.AF_validateEmail method

    /* WHY the line has been created in DOM, but doesn't show up ??
    In order to add elements to an SVG object those elements have to be created 
    in the SVG namespace. As jQuery doesn't allow you to do this at present 
    (AFAIK) you can't use jQuery to create the element.     */
    U.AF_drawline = function(p0,p1, id, clss){
        var newLine = document.createElementNS(
                'http://www.w3.org/2000/svg','line');
        if(!p0){ p0 = {x: 0, y: 0}; }
        else if(isNaN(p0.x) || isNaN(p0.y)){ p0 = {x: 0, y: 0}; };
        if(!p1){ p1 = {x: 0, y: 0}; }
        else if(isNaN(p1.x) || isNaN(p1.y)){ p1 = {x: 0, y: 0}; };
        newLine.setAttribute('id', id);
        newLine.setAttribute('x1',p0.x);
        newLine.setAttribute('y1',p0.y);
        newLine.setAttribute('x2',p1.x);
        newLine.setAttribute('y2',p1.y);
        if(clss) newLine.setAttribute('class',clss);
        U.AJ_render_svg.append(newLine);
        var res = U.AJ_render_svg.find('#'+id);
        return res;
    };// end of U.AF_drawline sttic method
    
    U.AF_deleteline = function(line_id){
        $(line_id).remove();
    };
    
    // 20130627 -> 2013-06-27 (8 chars -> 10 chars)
    U.AF_date_dashed = function(date,dash){
        // if - or . already in date, do nothing, just return it.
        if(date.indexOf('-') > -1 ||date.indexOf('.') > -1)
            return date.replace(/-/g, dash);
        if(date.length < 5) return date;
        var d = dash || '-';
        var msg = date.substr(0,4) + d;
        if(date.length < 8) return msg + date.substr(4);
        else return msg + date.substr(4,2) + d + date.substr(6);
    };
    // 2013-06-27 -> 20130627 (10 chars -> 8 chars)
    U.AF_date_undashed = function(date){
        if(date.length < 10) return date;
        return date.replace(/-/g,'');
    };
    
    // used in event handler: stop e from bubbled up
    U.AF_stop_event = function(e){
        if(e){
            e.bubbles = false;
            if(e.stopPropagation)
                e.stopPropagation();
            e.preventDefault();
        }
        FD.AF_se('update');
        return false;
    };// end of static method U.AF_stop_event
    
    U.AF_compare_pas = function(pa, pb){
        if(pa.tname_face() < pb.tname_face()) return -1;
        if(pa.tname_face() > pb.tname_face()) return 1
        return 0;
    }
    
    // find AV_path from p0 to p1: father,mother,spouses,children,siblings(fmscs)
    // if 1 of those is p1, hit:true; 
    // recursive all starting from each of fmscs) as p0 recursively, till ==p1
    // so if return true, AV_path has the list of array ['c',<pa>],from p0->p1
    U.AF_related_pas = function(p0, p1, 
                      AV_path,   // [['c|s', ent],..] c: child, s: spouse
                      exclst){// exclude-list(ids) dont search-list
        if(!exclst) 
            exclst = []; 
        if(exclst.indexOf(p0.id()) === -1) // if p0 not in exclst already
            exclst.push(p0.id());          // put p0 in,
        // search from each child from all spouses, and spouse-pa
        for(var i=0; i<p0.iddict['spouses'].length; i++){
            // if this spouse-ba has been searched before, jump over it
            if(exclst.indexOf(p0.iddict['spouses'][i]) !== -1) 
                continue;
            // mark this ba-id as searched
            exclst.push(p0.iddict['spouses'][i]);
            var b = M.e(p0.iddict['spouses'][i]); // spouse-ba
            if(b){
                // AV_path of children under this spousal realtionship
                var chs = b.AF_children(); // list of child-pa entities
                for(var j=0; j<chs.length; j++){
                    if(chs[j] && exclst.indexOf(chs[j].id()) === -1){
                        AV_path.push(chs[j].rooting('c'));
                        if(chs[j].id() === p1.id() || 
                           U.AF_related_pas(chs[j], p1, AV_path, exclst))
                            return true;
                        AV_path.pop(); // pop out child
                    }
                }
                // spouse AV_path
                var sp = b.AF_other_pa(p0); // partner-pa
                if(sp && exclst.indexOf(sp.id()) === -1){
                    AV_path.push(sp.rooting('s')); // spouses
                    if(sp.id() === p1.id() || 
                       U.AF_related_pas(sp, p1, AV_path, exclst))
                        return true;
                    AV_path.pop();
                }
            }
        }// for(var i=0; i<p0.iddict['spouses'].length; i++)
        
        // collecting bids of p0.parents
        var bids = U.AF_trim_adopt_sign(p0.iddict['parents']);
        for(var i=0; i<bids.length; i++){
            if(exclst.indexOf(bids[i]) === -1){ // i-th id not in exclst
                b = M.e(bids[i]);
                if(b){
                    // test if b.male is target p?
                    var pt = b.male();
                    if(pt && exclst.indexOf(pt.id()) === -1){ // parent-pa
                        AV_path.push(pt.rooting('p'));
                        if(pt.id() === p1.id() || 
                           U.AF_related_pas(pt, p1, AV_path, exclst))
                            return true;
                        AV_path.pop(); // pop out pt
                    }
                    // test if b.female is target p?
                    pt = b.female();
                    if(pt && exclst.indexOf(pt.id()) === -1){ // parent-pa
                        AV_path.push(pt.rooting('p'));
                        if(pt.id() === p1.id() || 
                           U.AF_related_pas(pt, p1, AV_path, exclst))
                            return true;
                        AV_path.pop(); // pop out pt
                    }
                }
                // bids[i] is checked - 2b excluded for next time
                exclst.push(bids[i]); 
            }
        }
        return false;
    };// end of U.AF_related_pas
    
    U.AF_find_path = function(p0,p1,AV_path){
        var res = U.AF_related_pas(p0, p1, AV_path, []);
        var test = 11;
        return res
    }
    
    // when name is too long to be put on name tag, do:
    // 'Ludwig von Beethoven' -> Ludwig v.B. Elizabeth Tudor->Elizabeth T.
    U.AF_shorten_name = function(lname){
        // get AB from ABcdef
        function gethead(str){
            if(str.length <=1) return str; // A => A
            var n = 1;
            // go until letter becomes lower case
            while(n < str.length && str[n] === str[n].toUpperCase()) 
                n++;
            return str.substr(0,n); // ABced => AB
        }
        
        if(lname.length < 12) return lname; // 12 is short enough
        splt = lname.split(' '); // split with space
        if(splt.length > 2){ // 3 or more pieces? "Make VII Aber" => Make VII A.
            return splt[0]+' '+gethead(splt[1])+'.'+gethead(splt[2])+'.';
        } if (splt.length > 1){ // 2 pieces
            return splt[0] + ' ' +gethead(splt[1]) + '.';
        } else {
            //thenameiscapenberger => thenameisca..
            return lname.substr(0,11)+'..'; 
        }
    };// end of U.AF_shorten_name
    
    U.AF_short_it_name = function(jspan){
        var txt = jspan.text();
        var wd = $('body').width();
        // right_edge is the coord of right-edge of the string relative to
        // body(browser-window) in pixel
        var right_edge = jspan.offset().left + jspan.width();
        //span is wrapped in > 1line, or 5px to window's right edge is violated
        while(jspan.height() > 46 || right_edge > wd){
                                   // string needs to be shortened
            txt = txt.substr(0, txt.length - 3) + '..';
            jspan.text(txt);
            right_edge = jspan.offset().left + jspan.width();
            if(txt === '..')
                break;
        }
    };// end of U.AF_short_it_name
    U.AF_mime2ext = function(mime_type){
        return '.' + U.AV_mime_dic[mime_type];
    };// end of U.AF_mime2ext
    
    U.AF_ext2mime = function(ext){
        var x = U.AV_mime_dic[ext.toLowerCase()];
        if(typeof(x) === 'string') return x;
        return x[0];
    };// end of U.AF_ext2mime
    
    // check if url exists. If yes, set rs.data
    U.AF_check_url = function(url, jimg, rs, succ, fail){
        $.get(url).done(function () {
            //alert("success");
            if(succ){
                succ(jimg, url); // feed url to jimg. synch call
                rs.data(url);    // set data/url to rs.data. synch call
            }
        }).fail(function () {
            //alert("failed.");
            if(fail){
                fail(url, rs);
            }
        });
    };// end of U.AF_check_url 

    // remove all classes matching the given pattern, return the jQ object
    U.AF_remove_class_pattern = function(jobject, pattern){
        jobject.removeClass(function(index, className){
            return (className.match(pattern) || []).join(' ');
        });
        return jobject;
    };// end of U.AF_remove_class_pattern

    U.AF_array_subtract = function(ar1, ar2){ // return: ar1 - ar2
        var diff = [];
        for(var i=0; i<ar1.length; i++){
            if(ar2.indexOf(ar1[i]) === -1){ // ar1-i not in ar2
                diff.push(ar1[i]);
            }
        }
        return diff;
    };// end of U.AF_array_subtract

    // test if 2 array have the same content. []==[],['a','b']==['a','b']
    // ['b','a']!=['a','b'] != ['b'], 
    U.AF_array_equal = function(ar1, ar2){
        if(ar1 === ar2) return true;
        if(ar1.length ===0 && ar2.length===0) return true;
        if(ar1.length === ar2.length){
            for(var i=0; i<ar1.length; i++){
                if(ar1[i] !== ar2[i]) 
                    return false;
            }
            return true;
        } else 
            return false;
    };// end of U.AF_array_equal

    // add every element in ar2 into ar1, if ar1 not having that element
    U.AF_array_add = function(ar1, ar2){
        for(var i=0; i<ar2.length; i++){
            if(ar1.indexOf(ar2[i]) === -1){
                ar1.push(ar2[i]);
            }
        }
        return ar1;
    };// end of U.AF_array_add
    
    U.AV_mime_dic = {
        /* extension(case-insensitive) => mime */
        'pdf':      'application/pdf',
        'txt':      'text/plain',
        'mp4':      'video/mp4',
        'webm':     'video/webm',
        'avi':      ['video/x-msvideo', 'video/avi',
                     'video/msvideo', 'application/x-troff-msvideo'],
        'ogg':      'audio/ogg',
        'wav':      ['audio/x-wav', 'audio/wav'],
        'mp3':      ['audio/mpeg', 'audio/mpeg3', 'audio/x-mpeg', 
                     'audio/x-mpeg-3'],
        'png':      'image/png',
        'gif':      'image/gif',
        'bmp':      ['image/bmp', 'image/x-windows-bmp'],
        'jpg':      'image/jpeg',
        'jpe':      'image/jpeg',
        'jpeg':     'image/jpeg',
        'doc':      'application/msword',
        'aif':      'audio/x-aiff',
        'aifc':     'audio/x-aiff',
        'aiff':     'audio/x-aiff',
        /* mime(case sensitive) => extension(always lower case) */
        'image/jpeg':           'jpg',
        'image/pjpeg':          'jpg',
        'image/bmp':            'bmp',
        'image/x-windows-bmp':  'bmp',
        'image/gif':            'gif',
        'image/png':            'png',
        'audio/mp3':            'mp3',
        'audio/mpeg3':          'mp3',
        'audio/x-mpeg-3':       'mp3',
        'audio/mpeg':           'mp3',
        'audio/x-mpeg':         'mp3',
        'audio/wav':            'wav',
        'audio/x-wav':          'wav',
        'audio/ogg':            'ogg',
        'application/x-troff-msvideo':  'avi',
        'video/avi':            'avi',
        'video/msvideo':        'avi',
        'video/x-msvideo':      'avi',
        'video/webm':           'webm',
        'video/mp4':            'mp4',
        'application/pdf':      'pdf',
        'application/msword':   'doc'
    };// end of U.AV_mime_dic
            
    U.AV_fc_type = function(mime_or_ext){
        if(!mime_or_ext) return null;
        if(mime_or_ext.length < 5){ // it is a file extension
            // first ext->mime, then call myself with mime
            return U.AV_fc_type(U.AV_mime_dic[mime_or_ext]);
        } else // it is mime
        switch(mime_or_ext){
            case 'image/jpeg':
            case 'image/gif':
            case 'image/png':
            case 'image/pjpeg':  return 'img';
            case 'audio/aiff':
            case 'audio/x-mpeg-3':
            case 'audio/mp3':
            case 'audio/wav':
            case 'audio/mpeg3': return 'audio';
            case 'video/mp4':
            case 'video/webm':
            case 'video/x-msvideo':
            case 'application/x-troff-msvideo':
            case 'video/avi':
            case 'video/x-msvideo':
            case 'video/msvideo': return 'video'
            case 'application/pdf': return 'pdf';
        }
    };// end of U.AV_fc_type

    U.AV_ext_support = {
        'pdf': 'pdf', 
        'txt': 'txt',
        'image': ['png', 'jpg', 'gif', 'jpe', 'jpeg', 'bmp'],
        'audio': ['mp3', 'wav', 'ogg', 'aif'],
        'video': ['webm','avi','mp4']
    };
    
    // 20170512T162920 -> 2017-05-12 16:29:20
    U.AF_format_ISOtime = function(ts){
        var m, dt = ts.split('T');
        m = dt[0].substr(0,4)+'-'+dt[0].substr(4,2)+'-'+dt[0].substr(6,2)+' ';
        m += dt[1].substr(0,2)+':'+dt[1].substr(2,2)+':'+dt[1].substr(4,2);
        return m;
    };// end of U.AF_format_ISOtime

    // msg: 12, n: 4 return -> 0012
    U.zfill = function(msg, n){
        var str = ''+msg, zs = '000000';
        if(str.length < n)
            return zs.substr(0, n-str.length) + str;
        return str;
    }
    // integer to string, seconds to time-stamp: 276 secs => '04:36'
    U.AF_time_count = function(secs){
        var m = parseInt(secs / 60), s = secs % 60;
        if(m > 60){
            var h = parseInt(m/60); 
            m = m % 60;
            return U.zfill(h,2)+':'+U.zfill(m,2)+':'+U.zfill(s,2);
        } else {
            return U.zfill(m,2)+':'+U.zfill(s, 2);
        }
    }
    U.log = function(level, msg){
        if(U.debug >= level){
            console.log(msg);
        }
    }
    U.AV_animate_duration = 700;
    U.debug = 1; //0: no log, 1: err, 2: info
    return U;
})()