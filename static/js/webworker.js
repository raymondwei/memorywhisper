﻿/*
 * sha1-min.js
 */
var hexcase=0;var b64pad="";function hex_sha1(a){return rstr2hex(rstr_sha1(str2rstr_utf8(a)))}function hex_hmac_sha1(a,b){return rstr2hex(rstr_hmac_sha1(str2rstr_utf8(a),str2rstr_utf8(b)))}function sha1_vm_test(){return hex_sha1("abc").toLowerCase()=="a9993e364706816aba3e25717850c26c9cd0d89d"}function rstr_sha1(a){return binb2rstr(binb_sha1(rstr2binb(a),a.length*8))}function rstr_hmac_sha1(c,f){var e=rstr2binb(c);if(e.length>16){e=binb_sha1(e,c.length*8)}var a=Array(16),d=Array(16);for(var b=0;b<16;b++){a[b]=e[b]^909522486;d[b]=e[b]^1549556828}var g=binb_sha1(a.concat(rstr2binb(f)),512+f.length*8);return binb2rstr(binb_sha1(d.concat(g),512+160))}function rstr2hex(c){try{hexcase}catch(g){hexcase=0}var f=hexcase?"0123456789ABCDEF":"0123456789abcdef";var b="";var a;for(var d=0;d<c.length;d++){a=c.charCodeAt(d);b+=f.charAt((a>>>4)&15)+f.charAt(a&15)}return b}function str2rstr_utf8(c){var b="";var d=-1;var a,e;while(++d<c.length){a=c.charCodeAt(d);e=d+1<c.length?c.charCodeAt(d+1):0;if(55296<=a&&a<=56319&&56320<=e&&e<=57343){a=65536+((a&1023)<<10)+(e&1023);d++}if(a<=127){b+=String.fromCharCode(a)}else{if(a<=2047){b+=String.fromCharCode(192|((a>>>6)&31),128|(a&63))}else{if(a<=65535){b+=String.fromCharCode(224|((a>>>12)&15),128|((a>>>6)&63),128|(a&63))}else{if(a<=2097151){b+=String.fromCharCode(240|((a>>>18)&7),128|((a>>>12)&63),128|((a>>>6)&63),128|(a&63))}}}}}return b}function rstr2binb(b){var a=Array(b.length>>2);for(var c=0;c<a.length;c++){a[c]=0}for(var c=0;c<b.length*8;c+=8){a[c>>5]|=(b.charCodeAt(c/8)&255)<<(24-c%32)}return a}function binb2rstr(b){var a="";for(var c=0;c<b.length*32;c+=8){a+=String.fromCharCode((b[c>>5]>>>(24-c%32))&255)}return a}function binb_sha1(v,o){v[o>>5]|=128<<(24-o%32);v[((o+64>>9)<<4)+15]=o;var y=Array(80);var u=1732584193;var s=-271733879;var r=-1732584194;var q=271733878;var p=-1009589776;for(var l=0;l<v.length;l+=16){var n=u;var m=s;var k=r;var h=q;var f=p;for(var g=0;g<80;g++){if(g<16){y[g]=v[l+g]}else{y[g]=bit_rol(y[g-3]^y[g-8]^y[g-14]^y[g-16],1)}var z=safe_add(safe_add(bit_rol(u,5),sha1_ft(g,s,r,q)),safe_add(safe_add(p,y[g]),sha1_kt(g)));p=q;q=r;r=bit_rol(s,30);s=u;u=z}u=safe_add(u,n);s=safe_add(s,m);r=safe_add(r,k);q=safe_add(q,h);p=safe_add(p,f)}return Array(u,s,r,q,p)}function sha1_ft(e,a,g,f){if(e<20){return(a&g)|((~a)&f)}if(e<40){return a^g^f}if(e<60){return(a&g)|(a&f)|(g&f)}return a^g^f}function sha1_kt(a){return(a<20)?1518500249:(a<40)?1859775393:(a<60)?-1894007588:-899497514}function safe_add(a,d){var c=(a&65535)+(d&65535);var b=(a>>16)+(d>>16)+(c>>16);return(b<<16)|(c&65535)}function bit_rol(a,b){return(a<<b)|(a>>>(32-b))};
/* 
 */
function _arrayBufferToBase64( buffer ) {
    var binary = '';
    var bytes = new Uint8Array( buffer );
    var len = bytes.byteLength;
    for (var i = 0; i < len; i++) {
        binary += String.fromCharCode( bytes[ i ] );
    }
    return btoa( binary );
}
/*
 * Web Worker:
 * cannot access DOM, cannot use jQuery.
 * can use XMLHttpRequest
 */
self.addEventListener('message', function(e){
    var data = e.data;
    if(data.cmd === 'upload-media'){
        self.AF_upload_media(data);
    } else if(data.cmd === 'update-idb'){
        self.AF_update_idb(data);
    }
}, false); // end of self.addEventListener


self.AF_upload_media = function(data){
    var reader = new FileReaderSync(),
        file = data.obj;
    var arr_buffer = reader.readAsArrayBuffer(file);
    // make a data url here
    var dat1 = "data:" +file.type+";base64," + _arrayBufferToBase64(arr_buffer);
    var size = '' + dat1.length;
    var signature = hex_sha1(dat1);
    var fpath = data.fid.toUpperCase() + '/'+size + '-' +
                signature+'.'+file.name.split('.')[1];
    var msg = "_sid=" + data.sid +"&mime=" + file.type +
                '&_req_id=##' + // server will not cache ##-resp. 
                "&filepath=" + fpath + "&data=" + dat1;
    var xmlhttp = new XMLHttpRequest(), 
        rsp, res = '__OK__';
    xmlhttp.open("POST","/req/saveblob", true);//method, url, async=true
    xmlhttp.setRequestHeader("Content-type", 
                             "application/x-www-form-urlencoded");
    xmlhttp.send(msg);
    xmlhttp.onreadystatechange = function() {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            rsp = xmlhttp.response;
            if(typeof(rsp) === 'string' && 
               rsp.indexOf('__fail') > -1){
                res = JSON.parse(rsp).result;
            }
            self.postMessage({result: res, 
                              cmd: data.cmd,
                              sig: signature, 
                              size: size,
                              type: file.type});
        } else if(xmlhttp.status !== 200){
            self.postMessage({result: '__failed__', cmd: data.cmd,
                              state: ''+xmlhttp.readyState,
                              status: ''+xmlhttp.status});
        }
    }
};// end of self.AF_upload_media

/**fa has been un-conditionally loaded from server, by FD. It then call this:
 * with feeding data: {cmd: 'update-idb',fid:<fid>,ctver:<fa.ctver>}
 *  1.get ct with id == <fid>-CT-<fid> from indexedDB, resulting in 1A or 1B:
 *    1A: if ct doesn't exist in idb: request ct from server
 *    1B: if ct from idb has older version than fa.ctver, request ct from server
 *     in case of 1A/1B, AF_require_ents is called with dopost==false go step 3
 *    1C: local (from indexedDB) ct is current: do step 2
 *  2.go thru ct.snapshot against idn-ents: comparing ent.tx with ct, collecting
 *    all ids of the ents with out-dated tx => lst
 *    then call  AF_require_ents(lst, true) - dopost == true
 *  3.when AF_require_ents gets all(lst) ents from server, save them into local
 *    indexedDB
 *    3A: if dopost==true(ents synching up), posting back to caller: success
 *    3B: if dopost == false, it is a single doc(ct). Do 2.
 */
self.AF_update_idb = function(data){
    self.cmd = data.cmd;
    self.ctver = parseInt(data.ctver); // self.ctver must be integer
    self.ct_id = data.fid + '-CT-' + data.fid;
    // open indexedDB
    var req = indexedDB.open("DB_" + data.fid.toUpperCase(), 20);
    req.onsuccess = function(){ // indexedDB opened successfully
        //debugger;// Javascript: this is the way to force debugger kicking in
        self.db = req.result;
        var tx = self.db.transaction("ents", "readwrite"),// get (wr)tx object
            store = tx.objectStore("ents");
        var getct = store.get(self.ct_id);
        getct.onsuccess = function(e){
            if(!e || !e.target || !e.target.result){ 
                // case A: ct missing in idb
                self.AF_require_ents([self.ct_id], false);// dont post OK to fd
            } else {
                var ct = e.target.result.dic;
                if(parseInt(ct.version) !== parseInt(self.ctver)){
                    // case B: idb.ct is older than ctver: get ct from server
                    self.AF_require_ents([self.ct_id], false);// dont post OK to fd
                } else {
                    // case C: ct in indexedDB is up to date
                    self.AF_process_ct(ct);
                }
            }
        };
        getct.onerror = function(er){ // getting ct error - ct may not exit
            self.AF_require_ents([self.ct_id], false);// dont post OK to fd
        }
    };// end of req.onsuccess = function
    req.onerror = function(err){
        alert(err);// open-error: indexedDB could not be opened
    }
};// end of self.AF_update_idb

// save array of objects into idb - put operations
self.AF_save_objs = function(objs){ // objs is an array of dics
    var tx = self.db.transaction("ents", "readwrite"),
        store = tx.objectStore("ents"), req;
    for(i=0; i<objs.length; i++){
        var id = objs[i]['_id'],
            ts = objs[i]['tx'];
        var row = {id: id, cid: id.substr(8,2), 
                ts: ts, dic: objs[i]}
        req = store.put(row);
        req.onsuccess = function(e){
        };
        req.onerror = function(er){
        }
    }
};// end of self.AF_save_objs

// when initial-pa page is loaded, ct will be loaded from server/DB.
// when loading a new ct where an old ct existed in idb, loop thru all the
// ents in idb, if an ent.txt is older than what is in the new ct, put that
// in fetch-list. Whne finishing looping, send request to reload all ents
// (ids) in the fetch-list from DB, so that all ents are up to date.
self.AF_process_ct = function(ct){ // lst: [<ct-dic>]
    var tx = self.db.transaction("ents", "readwrite");
    var store = tx.objectStore("ents");
    var snapshots = ct.snapshots; // complete with all ents {<id>:<ts>,...}
    //delete snapshots[self.ct_id]; // dont compare ct
    store.getAll().onsuccess = function(event) {
        var i, id, lst = [],
            rows = event.target.result; // get all ents from idb
        for(i=0; i<rows.length; i++){
            if(rows[i].cid == 'CT') // CT wont count
                continue;
            id = rows[i].id;
            if(id in snapshots){
                if(rows[i].ts !== parseInt(snapshots[id])){
                    lst.push(rows[i].id);
                }
                // this id is checked. remove it from snapshots
                delete snapshots[id];
            }
        }
        // left over ids in snapshots are ids idb doesn't have
        // get them all so the idb has all what ct has in its snapshots.
        for(id in snapshots){ // UR,VR,EP,RP,RC will not be requested
            if(['UR','VR','EP','RP','RC'].indexOf(id.substr(8,2)) === -1)
                lst.push(id);
        }
        self.AF_require_ents(lst, true)
    };
};// end of self.AF_process_ct

// request list of ids from server side. 
// dopost==true: post message back for success
// dopost=false: get back ct object, call AF_process_ct(ct)
self.AF_require_ents = function(lst, dopost){
    if(lst.length > 0){
        var xmlhttp = new XMLHttpRequest(), 
            jda, res = '__OK__';
            msg = 'ids=' + JSON.stringify(lst)+'&_req_id=##';
        xmlhttp.open("POST", "/req/getdics", true);
        xmlhttp.setRequestHeader("Content-type", 
                        "application/x-www-form-urlencoded");
        xmlhttp.send(msg);
        xmlhttp.onreadystatechange = function() {
            // prev transaction tx was finished. open it again
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                jda = JSON.parse(xmlhttp.response);
                if(jda['result'].indexOf('fail') === -1){
                    if(!jda['docs'] ||jda.docs.length === 0){
                        console.log("no server doc for webworker request");
                    } else {
                        self.AF_save_objs(jda['docs']);
                        if(dopost){
                            // lst was all ids that need to synch up from server
                            self.postMessage({
                                cmd:self.cmd,
                                ctid: self.ct_id,
                                result:'__OK__fetched'
                            });
                        } else { // dopost== false: ct requested from server
                            self.AF_process_ct(jda['docs'][0]);
                        }    
                    }
                }
            } else if(xmlhttp.status !== 200){
                self.postMessage({result: '__failed__', 
                                cmd: self.cmd,
                                state: ''+xmlhttp.readyState,
                                status: ''+xmlhttp.status});
            }
        };// xmlhttp.onreadystatechange function
    };// if lst.length > 0
};// end of self.AF_require_ents

/*
State (xmlhttp.readyState) Description
--------------------------------------
0      The request is not initialized
1      The request has been set up
2      The request has been sent
3      The request is in process
4      The request is complete
*/
