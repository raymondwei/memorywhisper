/*
 * Copyright (C) 2014 Adacro Inc. - All Rights Reserved
 */
var AC_FDClass = (function(){
    AC_FDClass = function(opt){
        this.AV_store = new AC_Store(this);
        this.idbs = {}; // {<fid>: <idb>,..} created by lserver
        // set lang.AV_lcode, no server-req sent asking 4 FTSUPER-LS-*
        this.AV_lang = new AC_Lang(this, 'en');
        this.AF_lcode = function () { return this.AV_lang.AV_lcode; }
        this.opt = opt;
        this.AV_is_localhost = location.host.indexOf('localhost') > -1;
        if(opt.private){
            this._fid = opt.private.substr(0,7);
            this.AV_action = 'initial-pa-focus';
        } else if(opt.docview){
            this._fid = opt.docview.substr(0,7);
            this.AV_action = 'docview';
        } else {
            this._fid = 'FTSUPER';
            this.AV_action = 'landing';
        }
        this.fid = function(){ return this._fid; };
        // basic graphics
        this.AJ_frame = $('div#mw-frame');
        this.AJ_person_pane = $('div#person_pane');
        this.AJ_doc_pane = $('div#doc_pane').css('visibility', 'hidden');
        this.AJ_shift_btn_bg = $('div#arrow-bg');
        
        // for handling person-pane resize setting/clicking-buttons
        this.AV_ppane_width = 0;
        this.AF_se('logout');
        this.AV_acticon_pic = "pics/login2.png";
        this.AV_actbav = null;
        this.AV_tbar = null;
        this.AV_gateip = "";// Gateway/public ip to be filled from response
        this.AV_loginpv = null;
    
        // in case specific lcode wanted: prepare.
        if(opt.lang) 
            this.AV_lang.AV_lcode = opt.lang;

        //this.AF_init();
        this.AV_path_mgr = new AC_PathMgr(this);
        this.AV_bom = new AC_BinObjMgr(this); // for binary image/pdf/audio
        this.AV_cfg = new AC_CFG(this);
        this.AV_actbuffer = new AC_ActionBuffer(this.AV_store);
        this.AV_landing = new AC_Landing(this);
        this.AF_centerpa(null);
        this.AV_lserver = new AC_LServer(this);
    }; // end of AC_FDClass constructor

    AC_FDClass.prototype.cid = function(){ return 'FD'; }
    
    AC_FDClass.prototype.AF_tpath = function(){
        return this.AV_path_mgr.AF_curr_tpath();
    };// end of AC_FDClass.prototype.AF_tpath

    // docid & lcode are for docview request only
    //AC_FDClass.prototype.AF_init = function (pid, docid, lcode) {
    AC_FDClass.prototype.AF_init = function () {
        this.req_dic = {};
        if('private' in this.opt){
            this.AV_lserver.AF_initialpa_focus(this.opt.private, this);
        } else if('docview' in this.opt){
            this.req_dic['docid'] = this.opt.docview;
            this.req_dic['lcode'] = this.opt.lang;
            this.AV_lserver.AF_post(this.AV_action, this.req_dic, this);
        } else {
            this.AV_lserver.AF_landing();
        }
    };// end of AC_FDClass.AF_init method

    AC_FDClass.prototype.AF_init_ids = function(){
        var ids = [],
            ids0 = ['FTSUPER-FA-FTSUPER','FTSUPER-LS-icons','FTSUPER-LS-en'];
        this.AV_store.AF_add2lst(ids0, ids);
        if(this.AV_lang.AV_lcode !== 'en')
            this.AV_store.AF_add2lst('FTSUPER-LS-'+this.AV_lang.AV_lcode, ids);
        if(this.fid() !== 'FTSUPER')
            this.AV_store.AF_add2lst(this.fid()+'-FA-'+this.fid(), ids);
        return ids;
    };// end of AC_FDClass.prototype.AF_init_ids

    AC_FDClass.prototype.AF_init_components = function(act){
        if(act === 'docview' || !this.AV_tbar){
            this.AV_tbar = new AC_TitleBarVModel(this);
            this.AV_infov = new AC_InfosVModel(this.AJ_person_pane);
            this.AV_synop = new AC_Synop();
        } else {
            this.AV_synop.AF_init();// synop.pav=null, hide synop
        }
        // show tbar-nav when pid exists and no docid 
        this.AV_tbar.AF_show_hide_nav(act !== 'docview');
    };// end of AC_FDClass.prototype.AF_init_components
    
    AC_FDClass.prototype.AF_centerpa = function(v){
        if(typeof(v) === 'undefined') return this._centerpa;
        this._centerpa = v;
        return this;
    };// end of AC_FDClass.prototype.AF_centerpa
    
    AC_FDClass.prototype.AF_view_faiv = function (pa, no_centerpa) {
        if(!no_centerpa){
            this.AF_centerpa(pa);
            this.AF_pa_theme(pa);
        }
        // set pa to focus, and start new tpath with pa-parents as root
        this.AV_path_mgr.AF_new_path(pa)//create a new path with pa as focus-pa
            .AF_center_pa(pa);
        this.AV_landing.AF_page().AF_render()
            .AF_update_focus_ppane();
        this.AV_tbar.AF_show_hide_nav(true);
        // cut away the Q-string: www.memorywhisper.com/<Q-string>
        window.history.replaceState("page2","title","/");
    };// end of of AC_FDClass.AF_view_faiv method

    AC_FDClass.prototype.AF_pa_theme = function(pa){
        if(pa){
            var lc = 'en', theme = 'default', title;
            var conf = pa.AF_cfg();
            if(conf){
                lc = conf['M1508'];
                theme = conf['M7519'];
            }
            this.AV_lang.AF_switch_lang(lc);
            
            title = this.AF_centerpa().tname_face()+' '+
                    this.AV_lang.face('M7119');
            this.AF_switch_theme(theme);
            this.AV_tbar.AJ_logoword.removeClass('LS').text(title);
        }
    };// end of AC_FDClass.prototype.AF_pa_theme
    
    AC_FDClass.prototype.AF_personal_theme = function (pa) {
        var tkey = 'default';
        if(pa){
            var conf = pa.AF_cfg();
            if ('M7519' in conf) tkey =  conf['M7519'];
            this.AV_lang.AF_switch_lang(conf['M1508']);
        } else {
            this.AV_lang.AF_switch_lang('en');
        }
        this.AF_switch_theme(tkey);
    };// AC_FDClass.prototype.AF_personal_theme
    
    AC_FDClass.prototype.AF_switch_theme = function(AV_theme_name){
        if (AV_theme_name in AC_BigButton.AV_themes) {
            var AV_theme_dict = AC_BigButton.AV_themes[AV_theme_name];
            var root = "url(http://" + window.location.host + "/";
            var src = root + AV_theme_dict['fpane-bg'] + ")";
            $('div#tab1').css('background-image', src);
            src = root + AV_theme_dict['cat-bg'] + ")";
            $('div.left').css('background-image', src);
            src = root + AV_theme_dict['cont-bg'] + ")";
            $('div.right').css('background-image', src);
        }        
    };// end of AC_FDClass.prototype.AF_switch_theme
    
    AC_FDClass.prototype.AF_handle_err = function(data, self){
        if(self.AV_action === 'login'){
            if(data === "lserver busy"){
                var debug = 1;
            } else {
                // login failed. Keep dialog open. color pwd field pink
                self.AV_tbar.AJ_loginpwd.css('background-color','pink')
                    .unbind('focus').bind('focus', function(e){
                        $('#login-submit-button').button("enable");
                    })
                self.AV_tbar.login_dlg.dialog( // login failed
                        'option', 'title', 
                        self.AV_lang.face('M1503')); // 'Failed to log in'
            }
        } else if(self.AV_action === 'initial-pa-focus'){
            alert("Wrong personal homepage url.")
            self.AF_init();
        }
    };// end of AC_FDClass.prototype.AF_handle_err
    
    // callback for webworker: ct is now in idb. load it into store
    // and call webworker again for updating FTSUPER-
    AC_FDClass.prototype.AF_ct2store = function(data, ctx){
        var ctid = data.ctid, self = this,
            ftsuper_fa = M.e('FTSUPER-FA-FTSUPER'),
            fid = ctid.substr(0,7);
        ctx.AV_lserver.AF_get_idb(fid, function(){
            ctx.idbs[fid].AF_get_ent(ctid).done(function(ct){
                M.ae(ct);
                if(fid !== 'FTSUPER'){
                    ctx.AV_wwp.AF_send_req({
                        cmd: 'update-idb',
                        fid: 'FTSUPER',
                        ctver: ftsuper_fa.AF_ctver()
                    }, 
                    {
                        func: ctx.AF_ct2store,
                        ctx: ctx
                    });
                }
            })
        });
    };// end of AC_FDClass.prototype.AF_ct2store

    /**
     * When act="login", FD.AF_authenticate will create a FD.temp_se = se, and
     * if login succeeds, FD._se will be assigned from FD.temp_se; 
     * when AF_se('logout'), setting FD._se to null.
     * when constructing req_dic (actbuffer), act (format of "C:<id>,D:<id>,..")
     * will be put in FD._se.actlog[tx]=act, where FD._se.tx=tx is updated
     * @param: act: 'logout' | 'update' | 'expire-check' | none
     * return FD._se - an SE entity (if logged in); or null (logged out)
     */
    AC_FDClass.prototype.AF_se = function(act){
        var nowts = Math.round((new Date()).getTime()/1000);// 10 digits secs
            nowts_str = '' + nowts;
        if(typeof(act) === 'string'){
            if(act === 'logout') {
                this._se = null;
            } else if(act === 'update'){
                if(this.AF_se()){
                    // update se.ltx with 10 digits now-ts in string format
                    this._se.tx(nowts_str);
                }
            } else if(act === 'expire-check'){
                if(this._se){ // if it is logged in
                    console.log("expire-check:"+nowts_str);
                    var last_update = parseInt(this._se.tx());
                    if(last_update + 1800 < nowts){
                        console.log('Expired');
                        this.AV_tbar.reqverb = 'logout';
                        this.AV_lserver.AF_post('logout',
                            {_sid: this.AF_sid()}, this.AV_tbar)
                    } else {
                        console.log("session age:"+ (nowts - last_update));
                    }
                } else {
                    console.log('not logged in');
                }
            }
        }
        return this._se; // also for act is missing
    };// end of AC_FDClass.prototype.AF_se

    AC_FDClass.prototype.AF_sid = function(){
        if(this.AF_se()) return this.AF_se().id();
        return '';
    }

    AC_FDClass.prototype.AF_synch_idb = function(){
        this.AV_wwp.AF_send_req({
            cmd: 'update-idb',
            fid: this.fid(),
            ctver: this.AV_fa.AF_ctver()
        }, 
        // callback post idb synched-up for fid: load ct into store
        // and synch up FTSUPER-*
        {
            func: this.AF_ct2store,
            ctx: this
        }); // no calling-ctx - no callback needed
    };// end of AC_FDClass.prototype.AF_synch_idb

    AC_FDClass.prototype.AF_handle_success = function(data, self){
        self.AV_lang.AF_populate_choices();
        if(!self.AV_wwp)
            self.AV_wwp = new AC_WebWorkerPortal();
        if(['docview','initial-pa-focus'].indexOf(self.AV_action) > -1){
            self.AF_init_components(self.AV_action);
            self.AV_landing.AF_init(false);
            if(self.AV_action === 'initial-pa-focus'){
                // load fid/fa, then what fa.ctver check triggers to load in idb
                self.AV_fa = M.e(self.fid() + '-FA-' + self.fid());
                self.AF_synch_idb();
                var pa = M.e(self.opt.private);
                if(pa){
                    self.AV_action = 'pa-center';
                    self.AV_lserver.AF_pa_center(pa, self);
                } else{
                    alert("Wrong private url.")
                    self.AF_init(); // go to landing page (About tab)
                }
            } else { // docview
                // docview req already fetched ownerpa in data. get prefer-lang
                var ownerpa = M.e(self.req_dic['docid']).AF_ownerpa();
                self.AV_lang.AF_switch_lang(ownerpa.AF_cfg()['M1508']);
                self.AV_infov.AV_docpane.AF_showme(
                    M.e(self.req_dic['docid']));
                $('button#doc-close-btn').css('visibility','hidden');
            }
        } else if(self.AV_action === 'pa-center'){
            var pa = M.e(self.opt.private);
            self.AF_view_faiv(pa);
        } else if(self.AV_action === 'login'){
            self.AV_acticon_pic = "pics/right1.png";
            self.AV_loginpv = self.AF_tpath().AF_focus();
            if(self.AV_tbar.login_dlg.dialog('isOpen'))
                self.AV_tbar.login_dlg.dialog('close');
            self.AV_tbar.AF_loginout(true);
            self.AF_pa_theme(self.AV_loginpv.model);
            self.AV_loginpv.AF_update_lclick_items()
                .AV_patag.AF_setlove_icon(true)
                .AF_set_focus_bg(true);
            self._se = self.temp_se;
            self.temp_se = null;
            self.AV_synop.AF_init(self.AV_loginpv);
            self.AV_infov.AF_show_state('hidden');
        } else if(this.AV_action === 'landing'){
            self.AV_fa = M.e('FTSUPER-FA-FTSUPER');
            self.AF_synch_idb();
            self.AF_init_components(self.AV_action);
            self.AV_landing.AF_init(true);
            if(this.opt.lang){
                self.AV_lang.AF_switch_lang(self.opt.lang);
            } else if(window.location.host.split('.')[0] === 'zh'){
                self.AV_lang.AF_switch_lang('zh');
            }
        }
        self.AF_resize();
    };// end of AC_FDClass.prototype.AF_handle_success
    
    // login request doesn't use actbuffer to contain se entity. but use
    // se entity directly
    AC_FDClass.prototype.AF_authenticate = function(pav, AV_password){
        this.AV_action = 'login';
        this.AV_actbuffer.AF_init();
        var thepid = pav.model.id();
        this.temp_se = new SE({pid: thepid, _id: M.mid('FTSUPER','SE')});
        var dic = {_pid:  thepid,
                   _pwd: Base64.encode(thepid + AV_password),
                   se: JSON.stringify(this.temp_se) };
        this.AV_lserver.AF_post('login', dic, this);
    };// end of AC_FDClass.AF_authenticate method
        
    AC_FDClass.prototype.AF_resize = function(){
        var body = $('body');
        var body_width = U.INT(body.css('width'));
        var body_height = U.INT(body.css('height'));
        $('div#mw-frame').css({'width': body_width, 'height': body_height});
        var tbar_height = 48;
        $('div#body_container').css(
                {'height': body_height - tbar_height,'width':  body_width});
        this.AJ_person_pane.css('height', body_height - tbar_height);
        var tabs = $('#tabs').css({ width: body_width - this.AV_ppane_width, 
                                    height: body_height - tbar_height});
        this.AJ_doc_pane.css({width: body_width - this.AV_ppane_width,
                         height: body_height - tbar_height});
        this.AV_lang.AJ_container.css('height', '759px');
        if(this.AV_infov && this.AV_ppane_width > 0)
            this.AV_infov.AV_docpane.AF_setup_dimension();
        this.AV_landing.AF_resize(tabs);
    };// end of AC_FDClass.prototype.AF_resize

    return AC_FDClass;
})();
