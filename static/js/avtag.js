﻿/*
 * Copyright (C) 2014 Adacro Inc. - All Rights Reserved
 * ----------------------------------------------------
 * Audio/Video tag class
 */
var AC_AVTag = (function(){
    function AC_AVTag(host,  // the hosting AudioPlayer object
                   pair){ // [<timeoffset-in-string>,<b64-comment>]
        this.host = host; // Audio/Video-Player as host of AC_AVTag
        // ref to pair in host.AV_curr_contdict['tags'][<pair>,<pair>,..]
        this.src_pair = pair;//[<time-offset-string>,<b64-comment>];
        this.comment = Base64.decode(pair[1]);
        this.time_offset =  parseFloat(pair[0]);
        this.AV_highlighted = false;
        
        this.AJ_stick = $('<div class="tag-stick"></div>')
            .appendTo(this.host.AJ_container).css('visibility','hidden');
        var msg = '<div class="audio-tag" id="'+ this.id +'"></div>'
        this.div = $(msg);
        //this.tag = $(msg).appendTo(host.tag_container);
        this.AJ_name = $('<span class="tag-name"></span>').appendTo(this.div);
        this.AF_setup_handlers();
        if(this.host.AV_bigshow.AV_docpane.AV_allow_write){
            this.AV_ctxtmenu = new AC_ContextMenu(this, this.div, {});
            this.AV_ctxtmenu.AF_modify_entry({label: 'M6023',
                icon: 'pics/play-button.png',
                func: this.start_play, 
                func_ctx: this, 
                arg_array: []});
            this.AV_ctxtmenu.AF_modify_entry({label: 'M6022',
                icon: 'pics/adjust-vol.png',
                func: this.AF_adjust_offset,
                func_ctx: this,
                arg_array: []
            });
            this.AV_ctxtmenu.AF_modify_entry({label: 'M7129',
                icon: 'pics/pencil.jpg',
                func: this.edit_comment,
                func_ctx: this, 
                arg_array: []});
            this.AV_ctxtmenu.AF_modify_entry({label: 'M0006',
                icon: 'pics/erase.png',
                func: this.AF_deleteme, 
                func_ctx: this, arg_array: []});
        }
    }; // end of AC_AVTag constructor
    AC_AVTag.prototype.cid = function(){ return 'AVT' ; }
    
    AC_AVTag.prototype.name = function(v){
        if(typeof(v) === 'undefined'){
            return this.name;
        } else {
            this.AJ_name.text(v);
            return this;
        }
    };// end of AC_AVTag.prototype.name
    
    AC_AVTag.prototype.AF_adjust_offset = function(){
        var self = this;
        this.spinner= $('<input type="text" id="spinner" size="80"/>')
            .appendTo(this.host.AJ_tag_descr).val(this.time_offset);
            /* I probably didn't pick spinner when downloading jquery ui
               so now I just use input. Later I will re-download, with spinner
            .spinner({
                    step: 0.01,
                    numberFormat: "n",
                    value: self.time_offset,
                    change: function(e, ui){
                        var x = ui.value;
                    }
            });
            */
        this.spinner_ok = $('<button>OK</button>')
            .appendTo(this.host.AJ_tag_descr)
            .bind('click', function(e){
                var r = parseFloat(self.spinner.val());
                if(r > self.total_time) r = self.total_time;
                if(r <= 0) r = 0;
                self.src_pair[0] = r + '';
                self.time_offset = r;
                self.slider_offset = 200+600*(r / self.total_time);
                self.AJ_stick.css('left', self.slider_offset+'px');
                self.spinner.remove();
                self.spinner_ok.remove();
                self.AF_reinsert();
            });
    };// end of AC_AVTag.prototype.AF_adjust_offset
    
    // when time-offset changed, the position of this tag in
    // host.AV_curr_contdict and .tags may change. extract this tag and
    // re-insert will make sure its position is right so that
    // host.tags/.AV_curr_contdict['tags'] array remains sorted correctly
    AC_AVTag.prototype.AF_reinsert = function(){
        this.AF_deleteme();
        var ind = this.host.AF_insert_newtag(this, true);
        this.name(''+(ind+1));
        this.AJ_stick.appendTo(this.host.AJ_container);
        this.AF_setup_handlers();
        this.AF_set_highlight(true);
    };// end of AC_AVTag.prototype.AF_reinsert
    
    AC_AVTag.prototype.AF_deleteme = function(){
        var index = this.host.tags.indexOf(this);
        this.AF_destroy(index);
        this.host.AF_update_save_btn();
    };// end of AC_AVTag.prototype.AF_deleteme
    
    AC_AVTag.prototype.AF_destroy = function(index){
        this.host.AV_curr_contdict['tags'].splice(index, 1);
        this.host.tags.splice(index, 1);
        this.AJ_stick.remove();
        this.div.remove();
    };// end of AC_AVTag.prototype.AF_destroy
    
    AC_AVTag.prototype.AF_set_highlight = function(hl){
        if(hl){
            this.total_time = this.host.player[0].duration;
            if(this.host.htag !== this){
                if(this.host.htag) 
                    this.host.htag.AF_set_highlight(false);
                this.host.htag = this;
                this.AV_highlighted = true;
                if(this.host.AJ_tag_descr.css('visibility') === 'hidden')
                    this.host.AJ_tag_descr.css('visibility','visible');
                this.host.AJ_tag_descr.empty()
                    .html(Base64.decode(this.src_pair[1]));
                if(this.host.AV_bigshow.AV_docpane.AV_allow_write)
                    this.host.AJ_tag_descr.prop("contentEditable", true).focus();
                this.div.addClass("tag-highlight");
                // progress bar: offset: 200px, length: 600px (see css file)
               this.slider_offset=200+600*(this.time_offset / this.total_time);
                this.AJ_stick.css({visibility: 'visible',
                                left: this.slider_offset+'px'});
            }
        } else {
            this.div.removeClass("tag-highlight");
            this.AJ_stick.css('visibility','hidden');
            this.AV_highlighted = false;
        }
        return this;
    };// end of AC_AVTag.prototype.AF_set_highlight
    
    AC_AVTag.prototype.AF_setup_handlers = function(){
        var self = this;
        this.div.unbind('click').bind('click', function(e){
            self.AF_set_highlight(true);
        });
        this.div.dblclick(function(e){
            self.AF_set_highlight(true).AF_play_at();
        });
    };// end of AC_AVTag.prototype.AF_setup_handlers
    
    AC_AVTag.prototype.AF_play_at = function(){
        var self = this;
        this.host.player[0].addEventListener('playing', self.AF_set_play_time);
        this.host.player[0].data = // save offset, and callback for removal
            {thetime: self.time_offset, thecall: self.AF_set_play_time};
        this.host.player[0].pause(); // pause/play to trigger 'playing' event
        this.host.player[0].play();
    };// end of AC_AVTag.prototype.AF_play_at

    AC_AVTag.prototype.AF_set_play_time = function(){
        // 'this ' is the <audio> element
        this.currentTime = this.data['thetime'];
        this.removeEventListener('playing', this.data['thecall']);
        this.play();
    };// end of AC_AVTag.prototype.AF_set_play_time
    return AC_AVTag;
})();// end of AC_AVTag
