
var saveAs = saveAs
  // IE 10+ (native saveAs)
  || (typeof navigator !== "undefined" &&
      navigator.msSaveOrOpenBlob && navigator.msSaveOrOpenBlob.bind(navigator))
  // Everyone else
  || (function() {
    //"use strict";
    // IE <10 is explicitly unsupported
    if (typeof navigator !== "undefined" &&
        /MSIE [1-9]\./.test(navigator.userAgent)) {
        return;
    }
    var get_URL = function() {
        if(URL) return URL;
        //if(webkitURL) return webkitURL;
        if(window) return window;
        //return URL || webkitURL || window;
    };
    var URL = get_URL(); //URL || webkitURL || window;
    //var URL = window;
    var save_link = document.createElementNS("http://www.w3.org/1999/xhtml", "a");
    var can_use_save_link = !window.externalHost && "download" in save_link;
    var click = function(node) {
        var event = document.createEvent("MouseEvents");
        event.initMouseEvent(
            "click", true, false, window, 0, 0, 0, 0, 0
            , false, false, false, false, 0, null
        );
        node.dispatchEvent(event);
    };
    var webkit_req_fs = window.webkitRequestFileSystem;
    var req_fs = window.requestFileSystem || webkit_req_fs || window.mozRequestFileSystem;
    var throw_outside = function(ex) {
        (window.setImmediate || window.setTimeout)(function() {
            throw ex;
        }, 0);
    };
    var force_saveable_type = "application/octet-stream";
    var fs_min_size = 0;
    var deletion_queue = [];
    var process_deletion_queue = function() {
        var i = deletion_queue.length;
        while (i--) {
            var file = deletion_queue[i];
            if (typeof file === "string") { // file is an object URL
                URL.revokeObjectURL(file);
            } else { // file is a File
                file.remove();
            }
        }
        deletion_queue.length = 0; // clear queue
    };
    var dispatch = function(filesaver, event_types, event) {
        var event_types = [].concat(event_types);
        var i = event_types.length;
        while (i--) {
            var listener = filesaver["on" + event_types[i]];
            if (typeof listener === "function") {
                try {
                    listener.call(filesaver, event || filesaver);
                } catch (ex) {
                    throw_outside(ex);
                }
            }
        }
    };
    var FileSaver = function(blob, name) {
        // First try a.download, then web filesystem, then object URLs
        var filesaver = this;
        var type = blob.type;
        var blob_changed = false;
        var object_url;
        var target_window;
        var get_object_url = function() {
            var object_url = get_URL().createObjectURL(blob);
            deletion_queue.push(object_url);
            return object_url;
        };
        var dispatch_all = function() {
            dispatch(filesaver, "writestart progress write writeend".split(" "));
        };
        // on any filesys errors revert to saving with object URLs
        var fs_error = function() {
            // don't create more object URLs than needed
            if (blob_changed || !object_url) {
                object_url = get_object_url(blob);
            }
            if (target_window) {
                target_window.location.href = object_url;
            } else {
                window.open(object_url, "_blank");
            }
            filesaver.readyState = filesaver.DONE;
            dispatch_all();
        };
        var abortable = function(func) {
            return function() {
                if (filesaver.readyState !== filesaver.DONE) {
                    return func.apply(this, arguments);
                }
            };
        };
        var create_if_not_found = {create: true, exclusive: false};
        var slice;

        filesaver.readyState = filesaver.INIT;
        if (!name) { name = "download"; }
        if (can_use_save_link) {
            object_url = get_object_url(blob);
            save_link = document.createElementNS("http://www.w3.org/1999/xhtml", "a");
            save_link.href = object_url;
            save_link.download = name;
            var event = document.createEvent("MouseEvents");
            event.initMouseEvent(
                "click", true, false, window, 0, 0, 0, 0, 0
                , false, false, false, false, 0, null
            );
            save_link.dispatchEvent(event);
            filesaver.readyState = filesaver.DONE;
            dispatch_all();
            return;
        }
        // Object and web filesystem URLs have a problem saving in Google Chrome when
        // windowed in a tab, so I force save with application/octet-stream
        // http://code.google.com/p/chromium/issues/detail?id=91158
        if (window.chrome && type && type !== force_saveable_type) {
            slice = blob.slice || blob.webkitSlice;
            blob = slice.call(blob, 0, blob.size, force_saveable_type);
            blob_changed = true;
        }
        // Since I can't be sure that the guessed media type will trigger a download
        // in WebKit, I append .download to the filename.
        // https://bugs.webkit.org/show_bug.cgi?id=65440
        if (webkit_req_fs && name !== "download") {
            name += ".download";
        }
        if (type === force_saveable_type || webkit_req_fs) {
            target_window = window;
        }
        if (!req_fs) {
            fs_error();
            return;
        }
        fs_min_size += blob.size;
        req_fs(window.TEMPORARY, fs_min_size, abortable(function(fs) {
            fs.root.getDirectory("saved", create_if_not_found, abortable(function(dir) {
                var save = function() {
                    dir.getFile(name, create_if_not_found, abortable(function(file) {
                        file.createWriter(abortable(function(writer) {
                            writer.onwriteend = function(event) {
                                target_window.location.href = file.toURL();
                                deletion_queue.push(file);
                                filesaver.readyState = filesaver.DONE;
                                dispatch(filesaver, "writeend", event);
                            };
                            writer.onerror = function() {
                                var error = writer.error;
                                if (error.code !== error.ABORT_ERR) {
                                    fs_error();
                                }
                            };
                            "writestart progress write abort".split(" ").forEach(function(event) {
                                writer["on" + event] = filesaver["on" + event];
                            });
                            writer.write(blob);
                            filesaver.abort = function() {
                                writer.abort();
                                filesaver.readyState = filesaver.DONE;
                            };
                            filesaver.readyState = filesaver.WRITING;
                        }), fs_error);
                    }), fs_error);
                };
                dir.getFile(name, {create: false}, abortable(function(file) {
                    // delete file if it already exists
                    file.remove();
                    save();
                }), abortable(function(ex) {
                    if (ex.code === ex.NOT_FOUND_ERR) {
                        save();
                    } else {
                        fs_error();
                    }
                }));
            }), fs_error);
        }), fs_error);
    };
    var FS_proto = FileSaver.prototype;
    saveAs = function(blob, name) {
        return new FileSaver(blob, name);
    };

    FS_proto.abort = function() {
        var filesaver = this;
        filesaver.readyState = filesaver.DONE;
        dispatch(filesaver, "abort");
    };
    FS_proto.readyState = FS_proto.INIT = 0;
    FS_proto.WRITING = 1;
    FS_proto.DONE = 2;

    FS_proto.error =
    FS_proto.onwritestart =
    FS_proto.onprogress =
    FS_proto.onwrite =
    FS_proto.onabort =
    FS_proto.onerror =
    FS_proto.onwriteend =
        null;

    window.addEventListener("unload", process_deletion_queue, false);
    saveAs.unload = function() {
        process_deletion_queue();
        window.removeEventListener("unload", process_deletion_queue, false);
    };
    return saveAs;
}(
       typeof self !== "undefined" && self
    || typeof window !== "undefined" && window
    || this.content
));

if (typeof module !== "undefined" && module !== null) {
  module.exports = saveAs;
} else if ((typeof define !== "undefined" && define !== null) && (define.amd != null)) {
  define([], function() {
    return saveAs;
  });
}
