/*
 * Copyright (C) 2014 Adacro Inc. - All Rights Reserved
 */
// TimeStamp - 19771231THHMMSS
// t=<non-zero int number>: time-number - a time-stamp of milisecs given
// t=true: get now-string in 20160528 format(8 digits)
// t=false: get now-string in 20160528T133859 format
function TS(t){
    var ts = new Date();
    if(t && typeof(t) === 'number') // set-time int-number given
        ts.setTime(t);
    var msg = U.zfill(ts.getFullYear(),4) + U.zfill(ts.getMonth()+1,2);
    msg += U.zfill(ts.getDate(),2);
    if(t && typeof(t) === 'boolean') // short == true
        return msg;
    msg += 'T' + U.zfill(ts.getHours(),2);
    msg += U.zfill(ts.getMinutes(),2 )+ U.zfill(ts.getSeconds(),2);
    return msg; // 20160528T133859 format
}// end of function TS(t)

// 13 digits UTC miliseconds since Epoch. 
// the same as (new Date()).getTime(), only it is UTC
function __UTC(){
    var now = new Date();
    var t = Date.UTC(now.getUTCFullYear(),now.getUTCMonth(),now.getUTCDate(),
             now.getUTCHours(),now.getUTCMinutes(),now.getUTCSeconds(),
             now.getUTCMilliseconds());
    return t;
}

var DEFAULT_LANG = 'en';
function __update_iddict(self, new_iddict) {
    var old_iddict = self.iddict;
    for(var k in new_iddict){
        if($.isArray(new_iddict[k])){    // it is an array
            if(k in old_iddict)
                old_iddict[k].length = 0;   // purge all array-eles in old
            else
                old_iddict[k] = [];
            for(var i=0; i<new_iddict[k].length; i++){  // push all from new
                old_iddict[k].push(new_iddict[k][i]);   // to old array
            }
        } else if(typeof(new_iddict[k]) === 'object'){  // it is an object
            for(var kk in old_iddict[k]){   // purge all from old
                delete old_iddict[k][kk];
            }
            for(var kk in new_iddict[k]){   // assign new properties to old
                old_iddict[k][kk] = new_iddict[k][kk];
            }
        } else {    // string 
            old_iddict[k] = new_iddict[k];  // over-write old with new string
        }
        self['_' + k] = null; // force refresh data of self.[k]() call
    }// for k in new_iddict
    return old_iddict;
};// end of __update_iddict

function __getset(self, name, value){
    var _name = '_'+name;
    if(typeof(value) === 'undefined'){
        return self[_name];
    } else {
        self[_name] = value;
        return self;
    }
};// end of __getset

function __ref_getset(self, name, value){
    var _name = '_'+name;
    if(typeof(value) === 'undefined'){      // getter
        if(self[_name]) return self[_name]; // available - return it
        // self._name is null
        if (self.iddict[name]) {   // iddict does have the id of name
            self[_name] = M.e(self.iddict[name]);
        } else
            self[_name] = null;
        return self[_name]; // return, regardless null or real ent
    } else { // value exists: setter. value is string(id) or entity-object
        if(typeof(value) === 'string'){ // id-string
            self.iddict[name] = value;
        } else if(typeof(value) === 'object'){
            // value maybe null or ''
            self.iddict[name] = value? value.id() : value;
            self[_name] = value;
        }
        return self;
    }
};// end of __ref_getset

function __reflst_getter(self,name){ // get the self.<name> array
    var _name = '_'+name;
    if(!self[_name] || self[_name].length === 0){
        self[_name] = [];
        if (self.iddict[name]) {
            for (var i = 0; i < self.iddict[name].length; i++) {
                var ent = M.e(U.AF_trim_adopt_sign(self.iddict[name][i]));
                if (ent) self[_name].push(ent);
            }
        } else self.iddict[name] = [];
    }
    if(self[_name].length !== self.iddict[name].length){
        self[_name] = [];
        for(var i=0; i<self.iddict[name].length; i++){
            var ent = M.e(self.iddict[name][i]);
            if(ent) self[_name].push(ent);
        }
    }
    return self[_name];
};// end of __reflst_getter

function __reflst_adder(host,       // hosting object/entity
                        addee,      // containee object/entity
                        container_name, // array-name under host
                        host_name){     // back-ref host-name under containee
    var id = addee.id();
    var _cname = '_'+container_name;
    if(!host[_cname]) host[_cname] = [];
    if(!host.iddict[container_name]) host.iddict[container_name] = [];
    var ind = host.iddict[container_name].indexOf(id);
    if(ind === -1){
        if(host_name)
            addee[host_name](host);
        ind = host[_cname].length;
        host.iddict[container_name].push(id);
        host[_cname].push(addee);
    }
    return ind;
};// end of __reflst_adder

function __index(self, host_name, array_name){
    var host = self[host_name]();
    if(host && host.iddict)
        return host.iddict[array_name].indexOf(self.id());
    return -1;
}

function __last_index(self, host_name, array_name){
    var ind = __index(self, host_name, array_name);
    if(ind === -1) return true;
    var host = self[host_name]();
    return ind +1 === host.iddict[array_name].length;
}

// for custom-localized string: {'en':'',..}
function __lcstr(self, dname, lc, str){
    var name = '_' + dname;
    var lcode = lc || FD.AF_lcode();
    if (typeof (str) === 'undefined') { // getter
        return LS.face(self[name], lcode);
    } else { // str has new value:         setter
        if(!self[name]) self[name] = {};
        self[name][lcode] = Base64.encode(str);
        return self;
    }
};// end of __lcstr

var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};

var FTBase = (function(){
    function FTBase(d){
        this._id = d['_id'] || 'FTSUPER-XX-asdfgh1364194589525';
        var m = d['tx'] || '' + __UTC();
        this._tx = m.substr(0,10);
    }

    FTBase.prototype.tx = function (v) { return __getset(this, 'tx', v); }
    FTBase.prototype.cid = function(){
        return this._id.substr(8,2);
    }
    FTBase.prototype.fid = function(v){
        if(typeof v !== 'undefined'){
            if (v.length == 7){ // v must be 7 long
                 this._id = v + this._id.substr(7); 
            }
            return this;
        }
        return this._id.substr(0,7);
    }
    // get dDaGGf1408743052017 out of TC6H8EU-PA-dDaGGf1408743052017
    FTBase.prototype.eid = function(){
        return this._id.substr(11);
    }
    // get dDaGGf out of TC6H8EU-PA-dDaGGf1408743052017
    FTBase.prototype.eidname = function(){
        return this._id.substr(11,6);
    }
    FTBase.prototype.id = function(v){ return __getset(this, 'id', v); }
    FTBase.prototype.crtimeRAW = function(){ 
        return this._id.substr(17,13);
    }
    FTBase.prototype.crtime = function(){
        return (new Date()).setTime(Number(this.crtimeRAW()));
    }
    // read out TS from id last 13 digits di()[-13:]: time ent was created.
    FTBase.prototype.crtimeISO = function(id){
        if(id){
            if(id.length === 13){
                return TS(Number(id));
            } else {
                return TS(Number(id.substr(17,13)));
            }
        } else {
            return TS(this.crtime());
        }
    }
    FTBase.prototype.force_refresh = function(key){
        if('iddict' in this){
            var keys = key? [key] : Object.keys(this.iddict);
            for(var i=0; i<keys.length; i++){
                var _name = '_' + keys[i];
                this[_name] = null;
            }
        }
        return this;
    }
    FTBase.prototype.base2json = function(){
        return { '_id': this._id, 'tx':this._tx };
    }
    FTBase.prototype.baseupdate = function(d){
        for(var k in d){
            switch(k){
            case 'tx':  this.tx(d[k]); break;
            case '_id': this.id(d[k]); break;
            case 'fid': this.fid(d[k]); break;
            }
        }
        return this;
    }
    return FTBase;
})();

var FA = (function(_super){
    __extends(FA, _super);
    function FA(d){
        _super.call(this, d);
        if('iddict' in d){
            this.iddict = d['iddict'];
        } else 
            this.iddict = {
                'fadmins': d['fadmins'] || [],
                'bulletins': d['bulletins'] || []
            };
        this._history = d['history'] || [];
        this._ctver = d['ctver'] || '1'; // current ct-version number
    }
    FA.prototype.AF_ctver = function(v){ return __getset(this, 'ctver', v); }
    FA.prototype.AF_history = function(v){ return __getset(this, 'history',v);}
    
    FA.prototype.bulletins=function(){return __reflst_getter(this,'bulletins');}
    FA.prototype.fadmins=function(){return __reflst_getter(this,'fadmins');}
    FA.prototype.add_fadmin = function(pid){
        if(this.iddict.fadmins.indexOf(pid) === -1)
            this.iddict.fadmins.push(pid);
        return this;
    }
    FA.prototype.update = function(d){
        this.baseupdate(d); // covers _id, tx, fid and vid update
        for(var k in d){
            switch(k){
            case 'iddict': this.iddict = __update_iddict(this, d[k]); break;
            case 'ctver':  this.AF_ctver(d[k]); break;
            case 'history':this.AF_history(d[k]); break;
            }
        }
        return this;
    }
    FA.prototype.toJSON = function(){
        var msg = this.base2json();
        msg['iddict'] = this.iddict;
        msg['ctver'] = this.AF_ctver();
        msg['history'] = this.AF_history();
        return msg;
    }
    return FA;
})(FTBase);

// Bulletin-board
var BB = (function(_super){
    __extends(BB, _super);
    function BB(d){
        _super.call(this, d);
        if('iddict' in d){
            this.iddict = $.extend(true,this.iddict, d['iddict']);
        } else {
            this.iddict = {
                'stories':  d['stories'] || [], // TDs as entries
                'events':   d['events'] || [],  // TDs as entries
                'forums':d['forums'] || []      // TDs as entries
            };
        }
        this._name = d['name'] || 'M4001'; // Family-Bulletin
    }
    BB.prototype.name = function(v){ return __getset(this, 'name', v);  }
    BB.prototype.stories = function(){ return __reflst_getter(this,'stories');}
    BB.prototype.events = function(){ return __reflst_getter(this,'events');}
    BB.prototype.forums = function(){return __reflst_getter(this,'forums');}
    BB.prototype.update = function(d){
        this.baseupdate(d); // covers _id, tx, fid and vid update
        for(var k in d){
            switch(k){
            case 'iddict':  this.iddict = __update_iddict(this, d[k]); break;
            case 'name':    this.name(d[k]);    break;
            }
        }
        return this;
    }
    BB.prototype.toJSON = function(){
        var msg = this.base2json();
        msg['iddict'] = this.iddict;
        msg['name'] = this.name();
        return msg;
    }
    return BB;
})(FTBase);

var PA = (function(_super){ // person class
    __extends(PA, _super);
    function PA(d){
        _super.call(this, d);
        if('iddict' in d){
            this.iddict = $.extend(true,this.iddict, d['iddict']);
        } else {
            this.iddict = {
              'ips': d['ips'] || [],         // list of ids of ips
              'spouses': d['spouses'] || [], // list of ids of spouse-bas
              'parents': d['parents'] || [], // list of ids of parents-ba
              'nutshell': d['nutshell'] || '',  // id of TD object 
              'portrait': d['portrait'] || '',  // id of RS object
            };
        }
        this._cfg = {};
        if(d['cfg']){ 
            $.extend(this._cfg, d['cfg']);
        }
        this._tname = {'en':''};
        if(d['tname']){ $.extend(this._tname, d['tname']); }
        this._pob = {'en':''};
        if(d['pob']){ $.extend(this._pob, d['pob']); }
        this._pod = {'en':''};
        if(d['pod']){ $.extend(this._pod, d['pod']); } 
        this._dob = d['dob'] || ''; // day of birth 8 digits format: 20141031
        this._dod = d['dod'] || ''; // day of death in 8 digits format
        this._sex = d['sex'] || 'male';     // or female
        // role is int(4 bytes / 32 bit) bitmask: 
        // last/rightest byte:   00000001 :(login: last bit)
        // 2nd last byte:        00000001 :(fadmin)
        // first 2 bytes not yet used.
        // E.G.: initial pa: 257 (fadmin + login)
        this._role = d['role'] || '1'; // default is login
        // used in search-relation/wizard: s|c|p 
        this._rooting = d['rooting'] || ''; 
    };// end of PA constructor
    PA.prototype.AF_cfg = function(v){ return __getset(this, 'cfg', v);  }
    PA.prototype.tname = function(v){ return __getset(this, 'tname', v);  }
    PA.prototype.tname_face = function(lc, str){
        return __lcstr(this,'tname', lc, str);
    }
    PA.prototype.pob = function(v){ return __getset(this, 'pob', v);  }
    PA.prototype.pob_face = function(lc, str){
        return __lcstr(this,'pob', lc, str);
    }
    PA.prototype.pod = function(v){ return __getset(this, 'pod', v);  }
    PA.prototype.pod_face = function(lc, str){
        return __lcstr(this,'pod', lc, str);
    }
    
    PA.prototype.dob = function(v){ return __getset(this, 'dob', v);  }
    PA.prototype.dod = function(v){ return __getset(this, 'dod', v);  }
    PA.prototype.sex = function(v){ return __getset(this, 'sex', v);  }
    PA.prototype.role = function(v){ return __getset(this, 'role', v);  }
    PA.prototype.rooting = function(v){ return __getset(this, 'rooting', v); }

    PA.prototype.parents = function(){return __reflst_getter(this,'parents');}
    PA.prototype.portrait = function(v){return __ref_getset(this,'portrait',v);}
    PA.prototype.nutshell = function(v){return __ref_getset(this,'nutshell',v);}
    PA.prototype.ips = function(){ return __reflst_getter(this,'ips'); }
    PA.prototype.spouses = function(){ return __reflst_getter(this,'spouses'); }

    // return indexes of parents-bid in iddict['parents'] where bid in it
    // mat have -|+ sufix. input bid has no sufix
    PA.prototype.AF_parents_index = function(bid){
        var inds = [], i;
        for(i=0; i<this.iddict['parents'].length; i++){
            if(this.iddict.parents[i].indexOf(bid)> -1) // [i] may have sufix
                return i;  // but if [i]-bid contains bid, it is a hit
        }
        return -1;
    };// end of PA.prototype.AF_parents_index
    
    // posible returns: true, F, M, S, C, null:
    // this(pa) is p-self: return true
    // this(pa) is father/mother of p: return F/M, 
    // this(pa) is spouse of p: return S
    // this(pa) is of p child: return C. 
    // else return null (p is not related to this(pa) at all)
    PA.prototype.kin = function(p){
        if(this.id() === p.id()) return true;   // self
        var bas = this.parents();
        for(var i=0; i<bas.length; i++){
            if(bas[i].male() &&
               bas[i].male().id()   === p.id()) return 'F'; // p is father
            if(bas[i].female() &&
               bas[i].female().id() === p.id()) return 'M'; // p is mother
        }
        bas = this.spouses();
        for(i=0; i<bas.length; i++){
            if(bas[i].AF_other_pa(this) === p) return 'S'; // p is a spouse-bas
            if(bas[i].is_child(p)) return 'C';          // p is a child
        }
        return null;    // p is not a kin of this PA
    };// end of PA.kin
    PA.prototype.add_ip = function(ip){ // ip must be an ip entity
        return __reflst_adder(this, ip, 'ips', 'owner'); 
    }
    PA.prototype.AF_add_spouse = function(ba){ // ba must be a ba entity
        return __reflst_adder(this, ba, 'spouses', this.sex());
    }
    PA.prototype.trust = function(pa){
        return true;
    }
    // 0-based children-index under parents() /ba
    PA.prototype.AF_index = function (ba) {
        var ids = U.AF_trim_adopt_sign(ba.iddict['children'])
        for(var i=0; i<ids.length; i++){
            if(ids[i].indexOf(this.id()) > -1)
                return i;
        }
        return -1;
    };// end of PA.prototype.AF_index
    PA.prototype.update = function (d) {
        this.baseupdate(d); // covers _id, tx, fid and vid update
        for(var k in d){
            switch(k){
            case 'iddict':  __update_iddict(this, d[k]); break;
            case 'dob':     this.dob(d[k]); break;
            case 'dod':     this.dod(d[k]); break;
            case 'sex':     this.sex(d[k]); break;
            case 'cfg':     $.extend(true, this._cfg, d[k]);
                            break;
            case 'tname':       this.tname(d[k]); break;
            case 'portrait':    this.portrait(d[k]); break;
            case 'parents':     this._parents = d[k]; break;
            case 'pob':         this.pob(d[k]); break;
            case 'pod':         this.pod(d[k]); break;
            case 'nutshell':    this.nutshell(d[k]); break;
            case 'role':        this.role(d[k]); break;
            case 'ips':         this.iddict['ips'] = d[k];
                this._ips = null; break;    // make sure of next re-assign
            case 'spouses':     this.iddict['spouses'] = d[k];
                this._spouses = null; break;
            }
        }
        return this;
    };// end of PA.prototype.update
    
    PA.prototype.toJSON = function(){
        var msg = this.base2json();
        msg['iddict'] = this.iddict;
        msg['cfg'] = this.AF_cfg();
        msg['tname'] = this.tname();
        msg['dob'] = this.dob();
        msg['dod'] = this.dod();
        msg['pob'] = this.pob();
        msg['pod'] = this.pod();
        msg['sex'] = this.sex();
        msg['role'] = this.role();
        return msg;
    }
    return PA;
})(FTBase);// end of PA

var IP = (function(_super){ // info pnae, for a category of items
    __extends(IP, _super);
    function IP(d){
        _super.call(this, d);
        if('iddict' in d){
            this.iddict = $.extend(true,this.iddict, d['iddict']);
        } else {
            this.iddict = {
              'owner': d['owner'] || '', // PA-id owning this under its ips
              'ownerpa': d['owner'] || '',// the same as owner
              'folders': d['folders']|| [],// list of ids of IT(F) ents
              'docs': d['docs'] || []      // list of ids of IT(D) ents
            };
        };
        this._name = d['name'] || '';           // system localized string
        this._title = d['title'] || {'en':''};  // custom localized str
        this._pk = d['pk'] || 'public';
    }
    // name is system localized
    IP.prototype.name = function(v){ return __getset(this, 'name', v);  }
    // title is custom localized, for hover-over custom hints/description
    IP.prototype.title = function(v){ return __getset(this, 'title', v); }
    IP.prototype.title_face = function(lc,str){
        return __lcstr(this, 'title', lc, str);
    }
    IP.prototype.pk = function(v){ return __getset(this, 'pk', v);  }
    IP.prototype.owner = function(v) { return __ref_getset(this,'owner',v);}
    IP.prototype.ownerpa = function(v) { return __ref_getset(this,'ownerpa',v);}
    IP.prototype.folders = function(){ return __reflst_getter(this,'folders');}
    IP.prototype.docs = function(){ return __reflst_getter(this,'docs');}
    IP.prototype.AF_add_folder = function(it){
        if(it.type()[0] !== 'F') return -1;
        return __reflst_adder(this, it, 'folders', 'owner'); 
    }
    IP.prototype.AF_add_doc = function(it){
        if(it.type()[0] !== 'D') return -1;    // only for doc-it
        return __reflst_adder(this, it, 'docs', 'owner'); 
    }
    IP.prototype.AF_index = function(){ 
        return __index(this,'owner','ips'); 
    };// end of IP.prototype.AF_index
    IP.prototype.update = function(d){
        this.baseupdate(d); // covers _id, tx, fid and vid update
        for(var k in d){
            switch(k){
            case 'iddict':  __update_iddict(this, d[k]); break;
            case 'owner':   this.owner(d[k]);   break;
            case 'pk':      this.pk(d[k]);      break;
            case 'name':    this.name(d[k]);    break;
            case 'title':   this.title(d[k]);   break;
            case 'folders': 
                this.iddict['folders'] = d[k]; 
                this._folders = null; break;
            case 'docs':    
                this.iddict['docs'] = d[k]; 
                this._docs = null; break;
            }
        }
        return this;
    }
    IP.prototype.toJSON = function(){
        var msg = this.base2json();
        msg['iddict'] = this.iddict;
        msg['name'] = this.name();
        msg['title'] = this.title();
        msg['pk'] = this.pk();
        return msg;
    }
    return IP;
})(FTBase);// end of IP

var IT = (function(_super){ // item class. item can be doc or folder
    __extends(IT, _super);
    function IT(d){
        _super.call(this, d);
        if('iddict' in d){
            this.iddict = $.extend(true,this.iddict, d['iddict']);
        } else {
            this.iddict = {
                'owner': d['owner'] || '', // id of container ent
                'facets': d['facets'] || [],
                'folders': d['folders'] || [],
                'docs': d['docs'] || [],
                'ownerpa' : d['ownerpa'] || '' // pid of onwer-pa
            };
        };
        this._type = d['type'] || 'D'; // D | F
        this._name = d['name'] || {};  // {en:'..',zh: '..', ..}
        this._title = d['title'] || {};// {en:'..',zh: '..', ..}
        this._pk = d['pk'] || 'M7117'; // M7117, M7118, M1513
        // lock used only when pk==M1513, [<pwd>,<exp-date>,<post-pk>]
        this._lock = d['lock'] = d['lock'] || [];
    }
    IT.prototype.lock = function(v){ return __getset(this, 'lock', v); }
    IT.prototype.name = function(v){ return __getset(this,'name',v); }
    IT.prototype.name_face = function(lc, str){
        return __lcstr(this, 'name', lc, str);
    }
    IT.prototype.title = function(v){ return __getset(this,'title',v); }
    IT.prototype.title_face = function(lc, str){
        return __lcstr(this, 'title', lc, str);
    }
    IT.prototype.type = function(v){ return __getset(this, 'type', v);  }
    IT.prototype.pk = function(v){ return __getset(this, 'pk', v);  }
    IT.prototype.owner = function(v) { return __ref_getset(this,'owner',v);}
    IT.prototype.AF_ownerpa = function(v) { return __ref_getset(this,'ownerpa',v);}
    IT.prototype.folders = function(){ return __reflst_getter(this,'folders');}
    IT.prototype.docs = function(){ return __reflst_getter(this,'docs');}
    IT.prototype.facets = function(){ return __reflst_getter(this,'facets');}
    IT.prototype.AF_add_folder = function(it){
        if(it.type()[0] !== 'F') 
            return -1;
        return __reflst_adder(this, it, 'folders', 'owner'); 
    }
    IT.prototype.AF_add_doc = function(it){
        if(it.type()[0] !== 'D') return -1;    // only for doc-it
        return __reflst_adder(this, it, 'docs', 'owner'); 
    }
    IT.prototype.AF_add_facet = function(fc){
        return __reflst_adder(this, fc, 'facets', 'owner'); 
    }
    // ad a td ent into docs. td.holders.push(this.id())
    IT.prototype.AF_add_td = function(td){
        if(it.type()[0] !== 'F') return -1;    // only for folder-it
        var tid = td.id();
        if(this.iddict.docs.indexOf(tid) === -1){
            this.iddict.docs.push(tid);
            td.AF_add_holder(this.id());
        }
    }
    // from this it up, all containers till ip, return a list of ents
    // first is ip
    IT.prototype.AF_get_containers = function(){
        var x = this, lst = [];
        do {
            x = x.owner();
            lst.push(x);
        } while(x.cid() !== 'IP');
        return lst.reverse();
    };// end of IT.prototype.AF_get_containers
    // get the very fist leaf that is a itD
    IT.prototype.AF_get_firstdoc = function(){
        var i, r, itfs;
        if(this.type()[0]=='D')     return this;
        if(this.docs().length > 0)  return this.docs()[0];
        itfs = this.folders();
        for(i=0; i<itfs.length; i++){
            r = itfs[i].AF_get_firstdoc();
            if(r) return r;
        }
        return false;
    };// end of IT.prototype.AF_get_firstdoc
    // get index of this it in its container.folders or .docs
    IT.prototype.AF_index = function(consider_pk){// pk can make this it invisible
        var cntname = this.type()[0]==='F'? 'folders':'docs';
        var ind = __index(this,'owner', cntname);
        if(!consider_pk) 
            return ind;
        else {
            var container_array = this.owner().iddict[cntname];
                index = 0, NOA = !M.OA();
            for(var i=0; i<ind; i++){
                var m = M.e(container_array[i]);
                if( NOA && m.pk() === 'M7118')  continue;
                else                            index++;
            }
            return index;
        }
    };// end of IT.prototype.AF_index
    IT.prototype.is_last = function(){ 
        return __last_index(this,'owner',this.type()[0]==='F'? 'folders':'docs');
    }
    IT.prototype.level_depth = function(){
        var dp = 0, oid = this.iddict['owner'];
        while(oid.substr(8,2) === 'IT'){
            dp++;
            oid = M.e(oid).iddict['owner'];
        }
        return dp;
    }
    // sharable is a itF, with at least one itD under its containees
    // to call this, all the nodes in its subtree have to be fetched into store.
    // (fcs contained in itD not necessary)
    IT.prototype.AF_is_sharable = function(){
        var i,e,res = false;
        if(this.type()[0] == 'F'){
            if(this.iddict.docs.length > 0)
                return true;
            for(i=0; i<this.iddict.folders.length; i++){
                e = M.e(this.iddict.folders[i]);
                if(e && e.AF_is_sharable())
                    return true;
            }
        }
        return res;
    };// end of IT.prototype.AF_is_sharable

    IT.prototype.update = function(d){
        this.baseupdate(d); // covers _id, tx, fid and vid update
        for(var k in d){
            switch(k){
            case 'iddict':  __update_iddict(this, d[k]); break;
            case 'owner':   this.owner(d[k]);   break;
            case 'name':    M.AF_update_dict(this.name(), d[k]);  break;
            case 'title':   M.AF_update_dict(this.title(), d[k]); break;
            case 'pk':      this.pk(d[k]);      break;
            case 'lock':    this.lock(d[k]);    break;
            case 'type':    this.type(d[k]);    break;
            case 'folders': this.iddict['folders'] = d[k];
                this._folders = null; break;
            case 'docs':    this.iddict['docs'] = d[k];
                this._docs = null; break;
            case 'facets':  this.iddict['facets'] = d[k];
                this._facets = null; break;
            }
        }
        return this;
    }
    IT.prototype.toJSON = function(){
        var msg = this.base2json();
        msg['iddict'] = this.iddict;
        msg['name'] = this.name();
        msg['title'] = this.title();
        msg['pk'] = this.pk();
        msg['type'] = this.type();
        if(this.lock() && this.lock().length > 0)
            msg['lock'] = this.lock();
        return msg;
    }
    return IT;
})(FTBase); // end of IT class

var FC = (function(_super){ // facet class. a doc-item can contain Nx fcs
    __extends(FC, _super);
    function FC(d){
        _super.call(this, d);
        if('iddict' in d){
            this.iddict = $.extend(true,this.iddict, d['iddict']);
        } else {
            this.iddict = {
                'owner': d['owner'] || '',
                'rs': d['rs'] || ''
            };
        };
        this._type = d['type'] || '';       // audio,video,img,pdf,txt
        this._name = d['name'] || {'en':''};
        this._anno = d['anno'] || {'en':''};
        this._contdict = d['contdict'] || {};
        if((this._type === 'audio' || this._type === 'video') &&
            !('tags' in this._contdict)){
            this._contdict['tags'] = [];
        }
    }
    FC.prototype.name = function(v){ return __getset(this, 'name', v); }
    FC.prototype.name_face = function(lc, str){ 
        return __lcstr(this, 'name', lc, str);  
    }
    FC.prototype.type = function(v){ return __getset(this, 'type', v);  }
    FC.prototype.anno = function(v){ return __getset(this, 'anno', v);  }
    FC.prototype.AF_anno_face = function(lc, str){ 
        return __lcstr(this,'anno', lc, str);
    }
    FC.prototype.owner = function(v) { return __ref_getset(this,'owner',v); }
    FC.prototype.rs = function(v){ return __ref_getset(this, 'rs', v); }
    FC.prototype.AF_contdict = function(v){ return __getset(this, 'contdict', v); }
    FC.prototype.contdict_face = function (lc, str) {
        return __lcstr(this, 'contdict', lc, str);
    }
    FC.prototype.private_url = function(v){
        if(typeof(v) === 'undefined')
            return this._contdict['private'];
        this._contdict['private'] = v;
        return this;
    }
    FC.prototype.AF_index = function(){ 
        return __index(this, 'owner','facets'); 
    };// end of FC.prototype.AF_index
    FC.prototype.is_last = function(){ 
        return __last_index(this,'owner','facets');
    }
    FC.prototype.update = function (d, lc) {
        var lcode = lc || FD.AF_lcode();
        this.baseupdate(d); // covers _id, tx, fid and vid update
        for(var k in d){
            switch(k){
            case 'iddict':   __update_iddict(this, d[k]); break;
            case 'owner':    this.owner(d[k]);      break;
            case 'rs':       this.rs(d[k]);         break;
            case 'name':     M.AF_update_dict(this.name(), d[k]);     break;
            case 'anno':     M.AF_update_dict(this.anno(), d[k]);     break;
            case 'contdict': M.AF_update_dict(this.AF_contdict(),d[k]); break;
            case 'type':     this.type(d[k]);       break;
            }
        }
        return this;
    }
    FC.prototype.toJSON = function(){
        var msg = this.base2json();
        msg['iddict'] = this.iddict;
        msg['type'] = this.type(); //audio,img,pdf,txt,form,(video)
        anno_d = {};
        for (var k in this.anno()) {
            anno_d[k] = this.anno()[k];
        }
        msg['anno'] = anno_d;
        msg['name'] = this.name();
        // make sure all values in contdict are eol-encoded
        msg['contdict'] = this.AF_contdict()
        return msg;
    }
    return FC;
})(FTBase);// end of FC class

var TD = (function(_super){ // text-doc class
    __extends(TD, _super);
    function TD(d){
        _super.call(this, d);
        if('iddict' in d){
            this.iddict = $.extend(true,this.iddict,d['iddict']);
        } else {
            this.iddict = { 
                'holders': d['holders'] || [], // list of ids(pa/containing-td)
                'children': d['children'] || [] // ids of children-tds
            };
        }
        this._content = { 'en': '', // thumbup/down count for forum td
                          thumbup: [], thumbdown: [] };
        if (d['content']) {
            for (var k in d['content']) {
                if(k.indexOf('thumb') > -1 && 
                   typeof(d['content'][k]) === 'number'){
                    this._content[k] = [];
                } else {
                    this._content[k] = d['content'][k];                
                }
            }
        }
        if('name' in d){
            if(typeof(d['name'])==='string')
                this._name = {'en':''};
            else
                this._name = d['name'];
        } else {
            this._name = {'en':''};
        }
        this._name = d['name'] || {'en':''};
    }
    TD.prototype.AF_children = function(){
        return __reflst_getter(this,'children');}
    TD.prototype.holders = function(v){ return __reflst_getter(this,'holders');}
    TD.prototype.AF_add_holder = function(hid){
        if(this.iddict['holders'].indexOf(hid) === -1)
            this.iddict['holders'].push(hid);
        return this;
    }
    TD.prototype.AF_add_child = function(tdid){
        if(this.iddict.children.indexOf(tdid) === -1){
            this.iddict.children.push(tdid);
        }
    }
    TD.prototype.content = function(v){ return __getset(this, 'content', v); }
    TD.prototype.AF_content_face = function (lc, str) {
        return __lcstr(this, 'content', lc, str);
    }
    TD.prototype.name = function(v){ return __getset(this, 'name', v);  }
    TD.prototype.name_face = function(lc, str){ 
        return __lcstr(this, 'name', lc, str);  
    }
    TD.prototype.update = function (d, lc) {
        this.baseupdate(d); // covers _id, tx, fid and vid update
        var lcode = lc || FD.AF_lcode();
        for(var k in d){
            switch(k){
            case 'iddict':  __update_iddict(this, d[k]); break;
            case 'name':    this.name(d[k]);    break;
            case 'holders': this.holders(d[k]); break;
            case 'content': M.AF_update_dict(this.content(), d[k]); break;
            }
        }
        return this;
    };
    TD.prototype.toJSON = function(){
        var msg = this.base2json();
        msg['iddict'] = this.iddict;
        msg['content'] = this.content();
        msg['name'] = this.name();
        return msg;
    }
    return TD;
})(FTBase);// end of TD class

var RS = (function(_super){ // resource class. used for containing media file
    __extends(RS, _super);
    function RS(d){
        _super.call(this, d);
        if('iddict' in d){
            this.iddict = $.extend(true,this.iddict, d['iddict']);
        } else {
            this.iddict = {
                'holders': d['holders'] || [] // list of pa-id or fc-id
            };
        };
        this._name = d['name'] || ''; // url end-piece: file-name
        this._mime = d['mime'] || 'mime';
        this._data = d['data'] || '';
        this._size = d['size'] || 'size in byte';
        this._title = d['title'] || ''; // mouse over text
        this._signature = d['signature'] || '';
    }
    RS.prototype.name = function(v){ return __getset(this, 'name', v);  }
    RS.prototype.holders = function(v){ return __reflst_getter(this,'holders');}
    RS.prototype.AF_add_holder = function(hid){
        if(this.iddict['holders'].indexOf(hid) === -1){
            this.iddict['holders'].push(hid);
        }
        return this;
    }
    RS.prototype.title = function(v){ return __getset(this, 'title', v);  }
    RS.prototype.mime = function(v){ return __getset(this, 'mime', v);  }
    RS.prototype.data = function(v){ return __getset(this, 'data', v);  }
    RS.prototype.size = function(v){ return __getset(this, 'size', v);  }
    RS.prototype.signature = function(v){ return __getset(this,'signature',v);}
    
    RS.prototype.file_name = function(){ 
        if(this._name.substr(7,4) !== "-FC-"){
            if(this.fid() === 'FTSUPER'){
                return 'blobs/' + this.fid() + '/' + this.name();
            } else {
                // '<fid>/<size>-<signature>.<ext>' ext is always lower-case
                var msg = this.fid()+'/'+this.size()+'-'+this.signature();
                return msg + U.AF_mime2ext(this.mime());
            }
        } else { // "TC6H8EU-FC-UNiIjZ1436297839251".substr(7,4)==='-FC-'
            // ref to txt blob: name is FC-id where its contdict ref to this
            return this.fid() + "/"+this._name + ".txt"
        }
    }
    // get a copy of RS, without iddict and _id
    RS.prototype.extract_dict = function(){
        return {name: this.name(), 
                title: this.title(),  
                mime: this.mime(), 
                data: this.data(), 
                size:this.size(), 
                signature:this.signature()};
    };
    RS.prototype.update = function(d){
        this.baseupdate(d);
        for(var k in d){
            switch(k){
            case 'iddict':  __update_iddict(this, d[k]); break;
            case 'name':    this.name(d[k]); break;
            case 'title':   this.title(d[k]); break;
            case 'holders': this.holders(d[k]); break;
            case 'mime':    this.mime(d[k]); break;
            case 'data':    this.data(d[k]); break;
            case 'size':    this.size(d[k]); break;
            case 'signature': this.signature(d[k]); break;
            }
        }
        return this;
    }
    RS.prototype.toJSON = function(v){
        var msg = this.base2json();
        msg['iddict'] = this.iddict;
        msg['name'] = this.name();
        msg['title'] = this.name();
        msg['mime'] = this.mime();
        msg['size'] = this.size();
        //msg['data'] = this.data();
        msg['signature'] = this.signature();
        return msg;
    }
    return RS;
})(FTBase);// end of RS class

var BA = (function(_super){ // bond class. for spousal bond(male/female PAs)
    __extends(BA, _super);
    function BA(d){
        _super.call(this, d);
        if('iddict' in d){
            this.iddict = $.extend(true,this.iddict, d['iddict']);
        } else {
            this.iddict = {
                'male': d['male'] || '',   // id of PA ent for male
                'female': d['female'] || '', // id of PA ent for female
                'children': d['children'] || [] // list of ids of children-pas
            };
        };
        this._type = d['type'] || 'married';
    }
    BA.prototype.type = function(v){ return __getset(this, 'type', v);  }
    BA.prototype.AF_children = function(){
        return __reflst_getter(this,'children');
    }
    // if p/pid is a child of this BA? 
    // posible returns: 
    // false: No, p is NOT ba's child' 
    // true: Yes, p is bio-child; 
    // -: Yes, p is a child, but has been adopted out
    // +: Yes,p is an adopted(i) child
    BA.prototype.is_child = function(p){ 
        var x, pid = typeof(p) === "string"? p : p.id();
        for(var i=0; i<this.iddict['children'].length; i++){
            x = this.iddict['children'][i];
            if(pid === x) return true; // pid among children
            // pid if part of a child-id with +/- at end: return +/-
            if(x.indexOf(pid) === 0) return x.slice(-1); // cut-out -/+ @end
        }
        return false;
    }// end of BA.prototype.is_child
    // index of pid in iddict.children. regardless if array-ele has sufix(-|+)
    BA.prototype.AF_child_index = function(pid){
        for(var i=0; i<this.iddict.children.length; i++){
            if(this.iddict.children[i].indexOf(pid) > -1)
                return i;
        }
        return -1;
    };// end of BA.prototype.AF_child_index
    BA.prototype.male = function(v){ return __ref_getset(this,'male',v); }
    BA.prototype.female = function(v){return __ref_getset(this,'female',v); }

    // if male spouse exists, find position-index of this(BA) 
    // under that males spouse-list.
    // if male is virtual(no pa-model), return -1
    BA.prototype.AF_mindex = function(){
        if(this.male()) 
            return this.male().spouses().indexOf(this);
        return -1;
    }

    // if female spouse exists, find position-index of this(BA) 
    // under that females spouse-list.
    // if female is virtual(no pa-model), return -1    
    BA.prototype.AF_findex = function(){
        if(this.female()) 
            return this.female().spouses().indexOf(this);
        return -1;
    }
    BA.prototype.AF_other_pid = function(pid){
        if(this.iddict.male === pid) return this.iddict.female;
        if(this.iddict.female === pid) return this.iddict.male;
        return null;
    }
    BA.prototype.AF_other_pa = function(pa){
        if(pa.sex() === 'male' && this.iddict['female']) return this.female();
        if(pa.sex() === 'female' && this.iddict['male']) return this.male();
        return null;
    }
    BA.prototype.AF_is_virtual = function(){
        var male_missing = typeof(this.iddict.male)==='string' &&
                           this.iddict.male.length === 0
                           || this.iddict.male===null;
            female_missing = typeof(this.iddict.female)==='string' &&
                           this.iddict.female.length === 0
                           || this.iddict.female===null;
        return male_missing && female_missing;
    }
    // ------------------------------------------------------------------------
    // AF_children_permission: see if children are allowed to be shown.
    // -----------------------------------------------------------------
    // pa.cfg.M1504(pwd-protection) has {<key><v>,...}. 
    // 1.If M7518(children) is in neither cfg.M1504 from male/female: 
    //   return true(show);
    // 2.One of male/female has M7518 in his/her cfg.M1504 and v==M7118(private)
    //   return false (no show)
    // 3.One of male/female has M7518 in his/her cfg.M1504 and v is an array
    //   ['******',<expire-ts>,<post-exp-act>]
    // 3.1 now-string shows that ts is expired
    //    3.1.1 post-expire-act is M7118(prvate): return false (no show)
    //    3.1.2 post-expire-act is M7117(public): return true (show)
    // 3.2 if ts not expired, return 'pwd' (pwd needed for showing)
    // -------------------------------------------------------------
    BA.prototype.AF_children_permission = function(){
        var nowstr = TS(true);
        function judge_M1504(M1504){
            if(M1504 && 'M7518' in M1504){  // M7518(children) is in
                if(M1504.M7518 === 'M7118') // private
                    return false;           // no show(hidden)
                // M1504.M7518: ['******',<exp-ts>,<post-exp-act>]
                if(M1504.M7518[1] <  nowstr){      // expired 
                    if(M1504.M7518[2] === 'M7118'){// post-exp: is private
                        return false;              // no show
                    } else {          // post-expire-act: M7117(public)
                        return true;  // show
                    } 
                } else // not expired
                    return 'pwd'; // pwd needed for showing
            }
            return true;
        };// end of function judge_M1504(M1504)

        // if there is no child under this ba, return false(no-show)
        if(this.iddict['children'].length === 0) return false;
        // virtual ba never block children to be accessed (show children)
        if(M.AF_fadmin() || this.AF_is_virtual()) 
            return true;
        // if logged-in pa is male/female or one of the child of this ba 
        // then show-children
        if(FD.AV_loginpv && 
           (this.iddict.male == FD.AV_loginpv.model.id() ||
            this.iddict.female == FD.AV_loginpv.model.id() ||
            this.is_child(FD.AV_loginpv.model)))
            return true;
        var M1504, 
            mlock, flock;      // male-lock, female-lock
        if(this.male()){
            mlock = judge_M1504(this.male().AF_cfg().M1504);
            if(mlock !== true) // 'pwd' or false
                return mlock;
        }
        if(this.female()){
            return judge_M1504(this.female().AF_cfg().M1504);
        }
        return true;
    };// end of BA.prototype.AF_children_permission
    BA.prototype.update = function(d){
        this.baseupdate(d); // covers _id, tx, fid and vid update
        for(var k in d){
            switch(k){
            case 'iddict':this.iddict = __update_iddict(this, d[k]); break;
            case 'type':  this.type(d[k]); break;
            case 'children': this._children = d[k]; break;
            case 'male':  this.male(d[k]);   break;
            case 'female':this.female(d[k]); break;
            }
        }
        return this;
    };// end of BA.prototype.update
    BA.prototype.toJSON = function(){
        var msg = this.base2json();
        msg['iddict'] = this.iddict;
        msg['type'] = this.type();
        return msg;
    }
    return BA;
})(FTBase);// end of BA class

// SE only exists on server side

// LG id = pa.id().replace('-PA-','-LG-')

// Calendar-Entity has all entries for a month. 
// id: <fid>-CE-<2 char month-number>
var CE = (function(_super){
    // _id: <fid>-CE-<2 char month, as 02 for Feb>
    __extends(CE, _super);
    function CE(d){
        _super.call(this, d);
        // B(irth) and D(eath) of a person (pid). repeat for each year
        // all entries has 2 char date(e.g. D12): repeat every year. Format:
        // D12:{<pid>:<msg>,<pid>:<msg>}. msg def:'', can have pid-spec message
        this._B = d['B'] || { // 
            D01:{},D02:{},D03:{},D04:{},D05:{},D06:{},D07:{},D08:{},
            D09:{},D10:{},D11:{},D12:{},D13:{},D14:{},D15:{},D16:{},
            D17:{},D18:{},D19:{},D20:{},D21:{},D22:{},D23:{},D24:{},
            D25:{},D26:{},D27:{},D28:{},D29:{},D30:{},D31:{}
        };
        this._D = d['D'] || { // D01:{<pid>:<msg>}
            D01:{},D02:{},D03:{},D04:{},D05:{},D06:{},D07:{},D08:{},
            D09:{},D10:{},D11:{},D12:{},D13:{},D14:{},D15:{},D16:{},
            D17:{},D18:{},D19:{},D20:{},D21:{},D22:{},D23:{},D24:{},
            D25:{},D26:{},D27:{},D28:{},D29:{},D30:{},D31:{}
        };
        // E(vent) for a person, msg is a b64 string
        // if 2 char date(e.g.D21): repeat-events: shown on every year
        // if 8 char(D20121231): only shown on that day/year. no-repeat per y
        this._E = d['E'] || {};
    };
    CE.prototype.B = function(v){ return __getset(this, 'B', v); }
    CE.prototype.D = function(v){ return __getset(this, 'D', v); }
    CE.prototype.E = function(v){ return __getset(this, 'E', v); }
    // add B|D|E event for pid in date-entry-dic, keyed by date_str
    CE.prototype.add = function(
            type,     // B|D|E
            date_str, // D23, for type==E, can be 9 digits: D20121231
            pid,      // pa-id
            msg){     // def: null=>M7102(birthday)/M7103(deathday)/Event-msg
        var _T = '_' + type;
        if(!(date_str in this[_T]))
            this[_T][date_str] = {};
        this[_T][date_str][pid] = msg? msg : '';
    };// end of CE.prototype.add

    // get array of pid for given event-type in this month
    CE.prototype.event_pids = function(type){
        if(['B','D','E'].indexOf(type) === -1) return [];
        var lst = [], i, k,
            d = this['_'+type];//{D01:{<pid>:<msg>,...},D02:..}
        for(k in d){ //K: D01,D02,...D31
            ar = Object.keys(d[k]);//ar: {<pid>:<msg>,<pid>:<msg>,..}
            U.AF_array_add(lst, ar);
        }
        return lst;
    };// end of CE.prototype.event_pids
    
    CE.prototype.get_pids = function(lst){
        var lst = lst? lst : [];
        lst = U.AF_array_add(lst, this.event_pids('B'));
        lst = U.AF_array_add(lst, this.event_pids('D'));
        lst = U.AF_array_add(lst, this.event_pids('E'));

        return lst;
    };// end of CE.prototype.get_pids
    
    CE.prototype.update = function(d){
        this.baseupdate(d); // covers _id, tx, fid
        for(var k in d){
            this['_'+k] = $.extend(true, this['_'+k], d[k]);
        }
        return this;
    };// end of CE.prototype.update
    
    CE.prototype.toJSON = function(){
        var msg = this.base2json();
        msg['B'] = this.B();
        msg['D'] = this.D();
        msg['E'] = this.E();
        return msg;
    };// end of CE.prototype.toJSON
    
    return CE;
})(FTBase);

// CT: content table. for DB snapshots and update management
// CT is a singleton ent, id-ed: <fid>-CT-<fid>
var CT = (function(_super){
    __extends(CT, _super);
    function CT(d){
        _super.call(this, d);
        this._versid = d['versid'] || {}; //map: {<version>:<SE-id>,...}
        this._snapshots = d['snapshots'] || {};
        // every time content changed (new le added, and snapshot updated)
        // version increases.
        this._version = d['version'] || '1';
    };// end of CT constructor
    CT.prototype.AF_versid = function(v){return __getset(this,'versid',v); }
    CT.prototype.AF_snapshots = function(v){ 
        return __getset(this, 'snapshots',v); 
    }
    CT.prototype.AF_version = function(v){ return __getset(this, 'version',v);}
    // getter/seter of a single snapshot: K: id of ent, v: current tx of ent
    // if v absent: return tx, if v given, put it in(insert/over write)
    CT.prototype.AF_snapshot = function(k, v){
        if(typeof(v) === 'undefined'){
            return this._snapshots[k];
        } else {
            this._snapshots[k] = v;
            return this;
        }
    };// end of CT.prototype.AF_snapshot

    CT.prototype.update = function(d){
        this.baseupdate(d); // covers _id, tx, fid
        for(var k in d){
            switch(k){
            case 'snapshots': this.AF_snapshots();   break;
            case 'version':   this.AF_version(d[k]); break;
            case 'versid':    this.AF_versid(d[k]);  break;
            }
        }
        return this;
    };// end of CT.prototype.update

    CT.prototype.toJSON = function(){
        var msg = this.base2json();
        msg['snapshots'] = this.AF_snapshots();
        msg['version'] = this.AF_version();
        msg['versid'] = this.AF_versid();
        return msg;
    }
    return CT;
})(FTBase);

var LS = (function(_super){ // language switch class in FTSUPER
    __extends(LS, _super);
    function LS(d){
        _super.call(this, d);
        this._icon = d['icon'] || '';
        this._words = d['words'] || {};
    }
    LS.prototype.toJSON = function(){
        var msg = this.base2json();
        msg['icon'] = this.icon();
        msg['words'] = this._words;
        return msg;
    }
    LS.prototype.icon_dic = function(){
        var dic = {};
        if(this.eid() === 'icons'){
            dic = this._words;
        }
        return dic;
    }
    LS.prototype.icon = function(v){ 
        return __getset(this, 'icon', v); 
    }
    LS.prototype.face = function(key, default_face){
        if($.isArray(key)){
            var i,m = '';
            for(i=0; i<key.length; i++){
                m += this.face(key[i], default_face) + ' ';
            }
            return m.trim();
        } else {
            if(key in this._words){
                var msg = this._words[key];
                return Base64.decode(msg);
            } else if(key){
                var ent, lc = this.eid(), 
                    splt = key.split('@');
                if(splt.length > 1 && splt[0].split('-').length === 3 ){
                    // key like "TC6H8EU-PA-oXqkPo1402369187321@pob_face"
                    ent = M.e(splt[0]);
                    if(ent && ent[splt[1]])
                        return ent[splt[1]](lc); // call pa.pob_face(lc)
                } 
            }    
        }
        return default_face || 'unknown';
    }
    // get v[lcode] in base64 decoded format
    // if v is a string, return base64 decoded of its
    LS.face = function(v, lcode){
        if(!v) return '';
        // if v is string, just return the b64 decode of v
        if(typeof(v) === 'string'){
            return Base64.decode(v);
        } else if(typeof(v)==='object') {
            // v is dict: every lc string is base64 encoded. decode it 
            var lc = lcode || FD.AF_lcode(); // take lcode of FD.AF_lcode
            if(lc in v && v[lc]){ // if v has lc, get that string
                var m = Base64.decode(v[lc]);
                var isb64 = U.isb64(m);
                while(isb64){
                    m = Base64.decode(m);
                    isb64 = U.isb64(m);
                }
                return m;
            }  else if(DEFAULT_LANG in v && v[DEFAULT_LANG]){
                // v doesnt have it, get default(v should have default key?)
                return  Base64.decode(v[DEFAULT_LANG]);
            }
            // v not having key/default-key: get first entry
            for(var k in v){
                // k.split(-)[0] has to be 2chars(like en, or zh-tw)
                // this prevent k=='thumbdown' to be processed here.
                // I abused v to have non-lcode keys, used for other purposes.
                // but remember: non-lcode key may not be 2 chars long!
                if(k.split('-')[0].length === 2){
                    if(v[k]) return Base64.decode(v[k]);
                }
            }
            return ''; // getting here: nothing available
        }
    };// end of LS.face
    return LS;
})(FTBase);

/**
 * Session: client creates an instance se with fid: FTSUPER when logging in
 * when logged out, which will set se.tx too,
 * (a)server will clone se and modify se.fid to be client fid
 * and save it into fid-table for record keeping. and delete se from FTSUPER
 * when client side sees that se expired, client will logout. 
 * Server side: no need to update se.tx. Server only does purge/logout process
 * where server calls.se.die(): (a)
 * From se.crtime and se.tx, one can find out the life-span of a se
 * with .astack se serves as action-log for the session.
 * every putdocs request must have a logged in session; and every putdocs' act
 * (it is a dic) will be appended at the end of astack-array
 */
var SE =  (function(_super){
    __extends(SE, _super);
    function SE(d){
        _super.call(this,d);
        //this._lu = d['lu'] || '';// last update - use tx
        this._pid = d['pid'] || ''; // person-id who initiated this session
        // acts-stack: array of acts-dic: [{<id>:'C',<id>:'D'},{}]
        this._actlog = d['actlog'] || {}; //{<tx>:'C:<id>,D:<id>,..',<tx>:..s}
    }
    SE.prototype.pid = function(v){ return __getset(this, 'pid', v); }
    SE.prototype.actlog = function(v){ return __getset(this, 'actlog', v); }
    SE.prototype.add_log = function(log, tx){ // log: 'C:<id>,D:<id>,U:<id>,..'
        this.tx(tx);
        this._actlog[tx] = log;
        return this;
    }
    
    SE.prototype.toJSON = function(){
        var msg = this.base2json();
        msg['pid'] = this.pid();
        msg['actlog'] = this.actlog();
        return msg;
    }
    return SE;
})(FTBase);
