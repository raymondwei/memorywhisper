var AC_Sadmin = (function() {
  function AC_Sadmin() {
    this.AV_fid_cids = [
      "PA",
      "BA",
      "IP",
      "IT",
      "FC",
      "RS",
      "TD",
      "CE",
      "BB",
      "UR",
      "SE",
      "CT",
      "FA"
    ];
    this.AV_ftsuper_cids = [
      "AL",
      "SE",
      "TD",
      "RS",
      "FC",
      "IT",
      "VR",
      "EP",
      "RC",
      "LS",
      "RP",
      "CT",
      "FA"
    ];
    this.AJ_cmds = $("#sadmin-cmds");
    this.AJ_fids = $("#sadmin-fids");
    this.AJ_cids = $("#sadmin-cids");
    this.AJ_eids = $("#sadmin-eids");
    this.AJ_fpath = $("#sadmin-filepath");
    this.AJ_doit = $("#sadmin-button");
    this.AJ_delbutton = $("#sadmin-del-button");
    this.AJ_status_info = $("#sadmin-status-info");
    this.AJ_chkbox = $("#sadmin-status-chkbox");
    this.AJ_showbox = $("#sadmin-show-box");
    this.AV_dels = [];
    this.AF_setups();
  } // end of

  AC_Sadmin.prototype.AF_setups = function() {
    var i, jda, v,
      self = this;
    this.AJ_cmds.unbind("change").bind("change", function(e) {
      self.AF_clear();
      self.AJ_chkbox.css("visibility", "hidden");
      self.AJ_fids.css("visibility", "hidden");
      self.AJ_cids.css("visibility", "hidden");
      self.AJ_eids.css("visibility", "hidden");
      self.AJ_fpath.css("visibility", "hidden");
      if (this.value === "0") { // no cmd chosen
      } else if (this.value === "1") { // show DB
        self.AJ_fids.css("visibility", "visible");
        self.AJ_cids.css("visibility", "visible");
        self.AF_get_fids(); // offer all DBs to show
      } else if (this.value === "2") { // verify DB
        self.AJ_fids.css("visibility", "visible");
        self.AF_get_fids(); // offer all DBs to verify
      } else if (this.value === "3") { // back up DB
        self.AJ_fids.css("visibility", "visible");
        self.AF_get_fids(); // offer all DBs to backup
      } else if (this.value === "4" || this.value === "5") { 
        // recover DB(data) / recover media files(zip)
        self.AJ_fids.css('visibility','visible');
        self.AF_get_fids(); // offer all fids to recover
      } else if (this.value === "6") { // generate pcodes
      } else if (this.value === "7") { // load lang dict
      } else if (this.value === "8") { // reset ct
        self.AJ_fids.css("visibility", "visible");
        self.AF_get_fids(); // offer all DBs to reset ct
      }
    });
    this.AJ_fids.unbind("change").bind("change", function(e) {
      self.AF_clear();
      self.fid = self.AJ_fids.val();
      if(self.AJ_cmds.val() === '4' || self.AJ_cmds.val() === '5'){ // f-list
        if(self.AJ_cmds.val() === '4'){
          self.AF_get_files('getdfiles');
        } else {
          self.AF_get_files('getzfiles');
        }
      } else if(self.AJ_cmds.val() === '1') { // show DB
        self.AJ_cids.empty();
        $("<option>", { value: "-", text: "-" }).appendTo(self.AJ_cids);
        self.fid = this.value;
        if (this.value === "FTSUPER") {
          for (i = 0; i < self.AV_ftsuper_cids.length; i++) {
            v = self.AV_ftsuper_cids[i];
            $("<option>", { value: v, text: v }).appendTo(self.AJ_cids);
          }
        } else if (this.value !== "-") {
          for (i = 0; i < self.AV_fid_cids.length; i++) {
            v = self.AV_fid_cids[i];
            $("<option>", { value: v, text: v }).appendTo(self.AJ_cids);
          }
        } else {
          self.fid = null;
        }
      }
    });
    this.AJ_cids.unbind("change").bind("change", function(e) {
      self.AF_clear();
      if (this.value !== "-") {
        self.cid = this.value;
        self.AJ_eids.css("visibility", "visible");
      } else {
        self.cid = null;
        self.AJ_eids.css("visibility", "hidden");
      }
    });
    this.AJ_doit.unbind("click").bind("click", function(e) {
      if (self.AJ_cmds.val() === "1") { // show-DB
        self.AF_get_ents();
      } else if(self.AJ_cmds.val() === "2"){ // verify DB
        if(self.fid !== '-' && self.fid !== 'FTSUPER'){
          $.get('/sadmin/verifydb',{fid: self.fid}, function(data){
            jda = JSON.parse(data);
            self.AJ_showbox.empty();
            if(jda['result'] === '__OK__'){
              self.AJ_status_info.text(self.fid +" DB verification done.");
              $('<p></p>').text("SUM: " + jda.sum).appendTo(self.AJ_showbox);
              $('<p></p>').text("PAS: " + jda.pas).appendTo(self.AJ_showbox);
              $('<p></p>').text("BAS: " + jda.bas).appendTo(self.AJ_showbox);
              $('<p></p>').text("IPS: " + jda.ips).appendTo(self.AJ_showbox);
              $('<p></p>').text("ITS: " + jda.its).appendTo(self.AJ_showbox);
              $('<p></p>').text("FCS: " + jda.fcs).appendTo(self.AJ_showbox);
              $('<p></p>').text("TDS: " + jda.tds).appendTo(self.AJ_showbox);
              $('<p></p>').text("RSS: " + jda.rss).appendTo(self.AJ_showbox);
            } else {
              self.AJ_status_info.text(self.fid +" DB verification failed.")
            }
          })

        }
      } else if(self.AJ_cmds.val() === "3"){
        if(self.fid !== '-'){
          $.get("/sadmin/backupdb",{fid: self.fid}, function(data){
            self.AJ_showbox.empty();
            jda = JSON.parse(data);
            if(jda.result === '__OK__'){
              self.AJ_showbox.html(jda.info);
              self.AJ_status_info.text('Backed up.');
            } else {
              self.AJ_status_info.text(self.fid + ' backup failed');
            }
          })
        }
      } else if(self.AJ_cmds.val() === "4" || self.AJ_cmds.val() === "5"){
        self.AF_recoverdb(self.AJ_fpath.val());
      } else if(self.AJ_cmds.val() === "6"){ //generate pcodes
        $.get("/sadmin/genpcode",{}, function(data){

        })
      } else if(self.AJ_cmds.val() === "7"){ // load lang dict
        $.get("/sadmin/loadldict",{}, function(data){

        })
      } else if(self.AJ_cmds.val() === "8"){ // reset ct
        if(self.fid){
          $.get("/sadmin/resetct",{fid: self.fid}, function(data){
            jda = JSON.parse(data);
            self.AJ_showbox.empty();
            if(jda.result === '__OK__'){
              self.AJ_status_info.text('ct reset OK')
            } else {
              self.AJ_status_info.text('ct reset failed')
            }
          })
        }
      }
    });
    self.AJ_delbutton.css("visibility", "hidden");
  }; // end of AC_Sadmin.prototype.AF_setups

  AC_Sadmin.prototype.AF_setup_chkbox = function() {
    var self = this;
    this.AJ_chkbox.unbind("change").bind("change", function(e) {
      if (this.checked) {
        $(".sadmin-li-chkbox").each(function(index, ele) {
          $(this).prop("checked", true);
          var theid = $(this)
            .parent()
            .attr("id");
          if (self.AV_dels.indexOf(theid) === -1) {
            self.AV_dels.push(theid);
          }
        });
      } else {
        self.AV_dels.length = 0;
        $(".sadmin-li-chkbox").each(function(index, ele) {
          $(this).prop("checked", false);
        });
      }
      if (self.AV_dels.length > 0) {
        self.AJ_delbutton.css("visibility", "visible");
        self.AJ_delbutton.text("delete " + self.AV_dels.length);
        self.AF_setup_deletion();
      } else {
        self.AJ_delbutton.css("visibility", "hidden");
      }
    });
  }; // end of AC_Sadmin.prototype.AF_setup_chkbox

  AC_Sadmin.prototype.AF_get_files = function(verb){
    var self = this, jda, i, v;
    self.AJ_fpath.css("visibility", "visible");
    if(this.fid !== '-'){
      $.get("/sadmin/"+verb,{fid: self.fid}, function(data){
        jda = JSON.parse(data);
        self.AJ_showbox.empty();
        if(jda.result === '__OK__'){
          v = $('<pre></pre>').text(jda.log);
          self.AJ_showbox.append(v);
          self.AJ_status_info.text('select a DB-data file');
          self.AJ_fpath.empty().append($('<option',{value:'-',text:'-'}));
          for(i=0; i<jda.lst.length; i++){
            $('<option>',{value:jda.lst[i], text: jda.lst[i]})
              .appendTo(self.AJ_fpath);
          }
        } else {
          self.AJ_status_info.text();
        }
      });
    }
  };// end of AC_Sadmin.prototype.AF_get_files

  AC_Sadmin.prototype.AF_recoverdb = function(fname){
    var self = this, v, jda;
    var verb = fname.split('.').pop() === 'data'? 
                'recoverdfile' : 'recoverzfile';
    $.get("/sadmin/"+verb, {fid: self.fid, fpath: fname}, function(data){
      jda = JSON.parse(data);
      self.AJ_status_info.text('');
      self.AJ_status_info.text(fname+' recovery '+jda.info);
      if(jda.result === '__OK__'){
        // put log.txt into showbox
        self.AJ_showbox.empty();
        v = $('<pre></pre>').text(jda.log);
        v.appendTo(self.AJ_showbox);
      }
    })
  };// end of AC_Sadmin.prototype.AF_recoverdb

  AC_Sadmin.prototype.AF_get_fids = function() {
    var i,
      self = this;
    this.AF_clear();
    $.get("/sadmin/getfids").done(function(data) {
      var v,
        op,
        jda = JSON.parse(data);
      if (jda["result"] === "__OK__") {
        self.AJ_fids.empty().append($("<option>", { value: '-', text: '-' }));
        for (i = 0; i < jda["docs"].length; i++) {
          v = jda["docs"][i];
          op = $("<option>", { value: v, text: v });
          self.AJ_fids.append(op);
        }
      }
      self.AJ_status_info.text(jda.info);
    });
  }; // end of AC_Sadmin.prototype.AF_get_fids

  // when any of the check-boxes is checked/unchecked, do this
  AC_Sadmin.prototype.AF_ent_chkbox_setup = function() {
    var self = this;
    $(".sadmin-li-chkbox")
      .unbind("change")
      .bind("change", function(e) {
        var theid = $(this)
            .parent()
            .attr("id"),
          ind = -1;
        if (this.checked) {
          ind = self.AV_dels.indexOf(theid);
          if (ind === -1) {
            // not in dels array?
            self.AV_dels.push(theid); // add theid to the dels array
          }
        } else {
          // check-box being un-checked
          ind = self.AV_dels.indexOf(theid);
          if (ind !== -1) {
            // in the dels array?
            self.AV_dels.splice(ind, 1); // remove it
          }
        }
        if (self.AV_dels.length > 0) {
          self.AJ_delbutton.css("visibility", "visible");
          self.AJ_delbutton.text("delete " + self.AV_dels.length);
          self.AJ_chkbox.prop("checked", true);
          self.AF_setup_deletion();
        } else {
          self.AJ_delbutton.css("visibility", "hidden");
          self.AJ_chkbox.prop("checked", false);
        }
      });
  }; // end of AC_Sadmin.prototype.AF_ent_chkbox_setup

  AC_Sadmin.prototype.AF_get_ents = function() {
    var i,
      jda,
      docs,
      self = this,
      parm = {
        fid: this.fid,
        cid: this.cid,
        eid: this.AJ_eids.val().trim()
      };
    $.get("/sadmin/getdics", parm, function(data) {
      jda = JSON.parse(data);
      if (jda["result"] === "__OK__") {
        docs = jda["docs"];
        self.AF_clear(docs.length > 0);
        for (i = 0; i < docs.length; i++) {
          self.AF_make_box(docs[i]).appendTo(self.AJ_showbox);
        }
        if (docs.length > 0) {
          self.AF_setup_chkbox();
          self.AF_ent_chkbox_setup();
        }
        self.AJ_chkbox.prop("checked", false);
      }
      self.AJ_status_info.text(jda["info"]);
    });
  }; // end of AC_Sadmin.prototype.AF_get_ents

  AC_Sadmin.prototype.AF_setup_deletion = function() {
    var self = this;
    self.AJ_delbutton.unbind("click").bind("click", function(e) {
      if (self.AV_dels.length > 0) {
        $.post("sadmin/dels", { ids: JSON.stringify(self.AV_dels) }, function(
          data
        ) {
          jda = JSON.parse(data);
          if (jda["result"] === "__OK__") {
            $("div.sadmin-box").each(function(e) {
              if (self.AV_dels.indexOf($(this).attr("id")) > -1) {
                $(this).remove();
              }
            });
            self.AV_dels.length = 0;
          }
          self.AF_clear(self.AV_dels.length > 0); //show chkbox
          self.AJ_chkbox.prop("checked", false);
          self.AJ_status_info.text(jda["info"]);
        });
      }
    });
  }; // end of AC_Sadmin.prototype.AF_setup_deletion

  AC_Sadmin.prototype.AF_make_box = function(dic) {
    var ent_dic = JSON.parse(dic),
      libox = $('<div class="sadmin-box"></div>').appendTo(this.AJ_showbox),
      entbox = $('<div class="sadmin-rec-box"></div>').appendTo(libox),
      chkbox = $('<input type="checkbox" class="sadmin-li-chkbox"/>').appendTo(
        libox
      );
    libox.attr("id", ent_dic._id);
    entbox.text(dic);
    return libox;
  }; // end of AC_Sadmin.prototype.AF_make_libox

  AC_Sadmin.prototype.AF_clear = function(show_chkbox) {
    if (!show_chkbox) this.AJ_chkbox.css("visibility", "hidden");
    else this.AJ_chkbox.css("visibility", "visible");
    this.AJ_eids.val("");
    this.AJ_delbutton.css("visibility", "hidden");
    this.AJ_showbox.empty();
    this.AJ_status_info.text("");
  }; // end of AC_Sadmin.prototype.AF_clear

  return AC_Sadmin;
})();
