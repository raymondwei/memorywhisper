/*
 * Copyright (C) 2014 Adacro Inc. - All Rights Reserved
 */
/**----------------------------------------------------------------------------
 * pa is data model for a real pa, can be null if this is a virtual pv
 * ----------------------------------------------------------------------------
 */
var AC_PAVModel = (function AC_PAVModel(){
    function AC_PAVModel(parms){
        this.AV_loves = [];
        this.model = parms['pa'];  // PA instance
        if(!this.model){
            this.vsex = parms['sex'] || 'male';
        }
        this.phost = parms['phost']; // the hosting parent bv
        this._is_anchor = parms['is_anchor'];
        this._anc_state = parms['ast'] || 'shrunk';
        this.AV_patag = new AC_PaTag(this);
        this.AF_gen(parms['gen']);
        if(!this.phost){ // no parent-hosting bv
            // marriage host(bv), only for non-anchor pv. 
            this.mhost = parms['mhost'];
        }
        this.AF_top(parms['top']);
        this.AJ_stem = U.AF_drawline( // set up stem-line
            {x:this.AF_left() - U.AV_PAV_STEM_L, y:this.AF_ynavel()},
            {x:this.AF_left(), y:this.AF_ynavel()}, 
            'stem_tmp_id',
            'marker').css('visibility', 'hidden');
        this.AF_anc_state(parms['ast'] || 'shrunk');
        this.AF_set_id();
        this.AV_synopwizard = FD.AV_synop.AV_synopwizard; 
    };// end of AC_PAVModel constructor
    AC_PAVModel.prototype.cid = function(){ return 'PAV'; }
    
    AC_PAVModel.prototype.AF_set_id = function(new_anchor){
        if(new_anchor || (!this.phost && !this.mhost)){
            this.id = this.model.id() + '_c_' + this.phost.model.id();
        } else {
            if(this.phost){
                if(this.phost.cid() === 'PTH')
                    this.id = this.model.id() + '_c_PTH';
                else 
                    // has a parent-ba, id = <pid>_<parent-ba-id>
                    this.id = this.model.id() + '_c_' + this.phost.model.id();
            } else if(this.mhost){
                if(this.AF_is_virtual()){
                    this.id = 'virtual_'+this.sex()+'_' + this.mhost.model.id();
                } else {
                    // has a marriage-host-ba, model exists,
                    // id = <pid>_<marriage-host-ba-id>
                    this.id = this.model.id() + '_s_' + this.mhost.model.id();
                }
            }
        }
        this.AV_patag.AJ_div.attr('id',this.id);
        this.AJ_stem.attr('id', this.id + '_stem');
        return this;
    };// end of AC_PAVModel.prototype.AF_set_id
    
    AC_PAVModel.prototype.AF_gen = function(g){
        if(typeof(g) === 'undefined') return this._gen;
        this._gen = g;
        this.AF_left(this._gen.AF_index()*U.AV_GEN_L+U.AV_PAV_STEM_L);
        return this;
    };// end of AC_PAVModel.AF_gen method
    
    AC_PAVModel.prototype.AF_anc_state = function(state){
        if(typeof(state) === 'undefined') return this._anc_state;
        this._anc_state = state;
        if(this.AF_is_anchor()){
            if(state === 'expanded'){
                this.AJ_stem.css('visibility','visible');
                this.AV_patag.AJ_anchor.removeClass().addClass('anchor-minus');
            } else {    // shrunk
                this.AJ_stem.css('visibility','hidden');
                this.AV_patag.AJ_anchor.removeClass().addClass('anchor-plus');
            }
        }
        return this;
    };// end of AC_PAVModel.prototype.AF_anc_state method

    AC_PAVModel.prototype.AF_removeme = function(){ 
        this.AV_patag.AJ_div.remove();
        if(this.AJ_stem) this.AJ_stem.remove();
        for(var j=0; j<this.AV_loves.length; j++){
            this.AV_loves[j].AF_removeme(); // bv will detach non-anchor pv
        }
        return this;
    };// end of  AC_PAVModel.AF_removeme method
    
    // ------------------------------------------------------------------------
    // After pv block lined-up, when resolving conflict in next gen-column, it
    // can result that sgrp-root-bv has to be moved up/down in that pv-block:
    // This is done here inside the anchor pv: bv is the 1 2b moved bu y.
    // --------------------------------------------
    AC_PAVModel.prototype.AF_stretch_bvs = function(bv, y){
        if(!this.AF_is_anchor() || y === 0) return this.AF_lower();
        var ind = this.AV_loves.indexOf(bv);
        if (this.sex() === 'female') {
            // anchor is female: move up bv and other bvs above it
            for (var i = ind; i < this.AV_loves.length; i++) {
                this.AV_loves[i].AF_ymove(-y);// incl. lines
                this.AV_loves[i].AF_non_anchor_pv().AF_ymove(-y);
            }
            //this.AF_top(this.AF_top() - y);
        } else {
            // anchor is male: move down bv and other bvs below it
            for (var i = ind; i < this.AV_loves.length; i++) {
                // move down y of all bvs from ind-th down
                this.AV_loves[i].AF_ymove(y);// incl. lines
                this.AV_loves[i].AF_non_anchor_pv().AF_ymove(y);// move partner
            }
        }
        return this.AF_lower();
    };// end of AC_PAVModel.AF_stretch_bvs method
    
    // if pa's AC allows spouses(M7517) to be opened (is owner, or AC=M7117)
    // possible returns:
    // false:   pa!=login-pa && M1504.M7517==M7118
    // true:    1.login-pa is me; 2.M1504 has no 'M7517'; 3.M1504.M7517==M7117
    // 'locked': access locked (requiring pwd)
    AC_PAVModel.prototype.AF_spouse_expandable = function(){
        if(FD.AV_loginpv && 
           (M.AF_fadmin() ||
            FD.AV_loginpv.model.id() === this.model.id()))
            return true;
        var M1504 = this.model.AF_cfg().M1504;
        if(!('M7517' in M1504) || M1504.M7517==='M7117') // public 
            return true;
        if(M1504.M7517 === 'M7118') // private access(M7118) 
            return false;
        // M1504.M7517 == [<pwd>,<date>,<post-date-pk>]
        if(AC_PwdPopup.pwd_cracked) // <pwd> has been verified
            return true;
        if(M1504.M7517[1].length !== 8){
            U.log(1,"exp-date format of spouse-ac wrong:"+M1504.M7517[1]);
            return true;
        }
        var nowstr = TS(true);          // TS of now-string in 20160528 format
        if(nowstr > M1504.M7517[1] &&   // expired and
           M1504.M7517[2] === 'M7117'){ // post-exp-pk: public
            return true;
        }
        // not expired, or expired and post-exppk is private
        return "locked";
    };// end of AC_PAVModel.prototype.AF_spouse_expandable
    
    AC_PAVModel.prototype.AF_is_virtual = function(){ return !this.model; }
        
    AC_PAVModel.prototype.sex = function(){
        if(this.model) return this.model.sex();
        return this.vsex;
    };// end of AC_PAVModel.sex method

    AC_PAVModel.prototype.AF_left = function(v){
        if(typeof(v) === 'undefined') // getter
            return this.AV_patag.AF_left();
        // setter
        this.AV_patag.AF_left(v);
        var newx1 = U.INT(v)-20;
        if(this.AJ_stem) 
            this.AJ_stem.attr({'x1':newx1,'x2':v});
        return this;
    };// end of AC_PAVModel.AF_left method
    
    AC_PAVModel.prototype.AF_top = function(v){
        if(typeof(v) === 'undefined') 
            return this.AV_patag.AF_top();
        this.AV_patag.AF_top(v);
        var newy = U.INT(v)+17;
        if(this.AJ_stem) this.AJ_stem.attr({'y1':newy,'y2':newy});
        return this;
    };// end of AC_PAVModel.AF_top method
    
    AC_PAVModel.prototype.AF_set_position = function(
            pos,        // pos:{x,y} is the new left/top of patag.AJ_div(left/top)
            animated,   // true/false: animted?
            animate_callback){ // null/<function>
        this.AV_patag.AF_set_position(pos, animated, animate_callback);
        // setting new stem not animated - it shows up at beginning
        if(this.AJ_stem){
            var x1 = pos.x? pos.x - 20 : this.AF_left() - 20, 
                x2 = pos.x? pos.x : this.AF_left(), 
                y1 = pos.y? pos.y + 17 : this.AF_top() + 17, 
                y2 = y1;
            this.AJ_stem.attr({'x1': x1, 'x2': x2,'y1': y1,'y2': y2});
        }
        return this;
    };// end of AC_PAVModel.prototype.AF_set_position
    
    // ------------------------------------------------------------------------
    // if not extended,return this pv's (top-AC_PAVModel.YMINDIST); if extended,
    // return highst bv's upper
    // --------------------------------------------
    AC_PAVModel.prototype.AF_upper = function(extended){
        if(extended && this.AF_is_anchor()){
            var y = this.AF_upper();
            for(var i=0; i< this.AV_loves.length; i++){
                if(this.AV_loves[i].AF_upper() < y)
                    y = this.AV_loves[i].AF_upper();
            }
            return y;
        } else
            return this.AV_patag.AF_top() - U.AV_PAV_YMIND
    };// end of AC_PAVModel.AF_upper method

    // ------------------------------------------------------------------------
    // if not extended,return this pv's lower edge + AC_PAVModel.YMINDIST; 
    // if extended, return lowest bv's lower
    // --------------------------------------------
    AC_PAVModel.prototype.AF_lower = function(extended){
        if(extended && this.AF_is_anchor()){
            var y = this.AF_lower();
            for(var i=0; i<this.AV_loves.length; i++){
                if(this.AV_loves[i].AF_lower() > y)
                    y = this.AV_loves[i].AF_lower();
            }
            return y;
        } else
            return this.AV_patag.AF_top() + U.AV_PAV_H + U.AV_PAV_YMIND;
    };// end of AC_PAVModel.AF_lower method

    AC_PAVModel.prototype.AF_ynavel = function(){ 
        return this.AF_top() + 0.5*U.AV_PAV_H; 
    }
    
    AC_PAVModel.prototype.AF_is_anchor = function(v){//v: undefined | true|false
        if(typeof(v) !== 'undefined'){ // setter
            if(!this.AF_is_virtual()) 
                this.AV_patag.AF_set_anchor_visuals(v);
            this._is_anchor = v;
            if(v){
                if(this.AJ_stem && this.AF_anc_state() === 'expanded') 
                    this.AJ_stem.css('visibility','visible');
            } else {
                if(this.AJ_stem) this.AJ_stem.css('visibility','hidden');
            }
            return this;
        } else { // getter
            return this._is_anchor;
        }
    };// end of AC_PAVModel.AF_is_anchor method
    
    AC_PAVModel.prototype.AF_ymove = function( delta ){
        this.AF_top(this.AF_top() + delta);   // move this pv
        // anchor pv also move all his/her spouses
        if(this.AF_is_anchor()){
            for(var i=0; i< this.AV_loves.length; i++){
                this.AV_loves[i].AF_other_pv(this).AF_ymove(delta);
                this.AV_loves[i].AF_ymove(delta);
            }
        }
    };// end of AC_PAVModel.AF_ymove method

    // returns: 
    // false: not-adopted
    // + :
    // - : adopted-out, adopted-in
    AC_PAVModel.prototype.AF_adp = function(){
        if(this.phost && this.phost.cid() === 'BAV'){
            var a = this.phost.model.is_child(this.model.id());
            if(a === '-' || a === '+') return a;
            if(!a) U.log(1, "no parent?");
        }
        return false;
    };// end of AC_PAVModel.prototype.AF_adp
    
    AC_PAVModel.prototype.cfg_updater = function(d){
        
    };// end of AC_PAVModel.prototype.cfg_updater
    
    return AC_PAVModel;
})(); // AC_PAVModel
