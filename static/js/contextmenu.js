/*
 * Copyright (C) 2014 Adacro Inc. - All Rights Reserved
 */
var AC_ContextMenu = (function(){
    function AC_ContextMenu(vmodel, //ent,
                element,    // jquery object of the containing element
                menu_data,  // array of menu-item definitions
                left_click, // true: left-click; false: right-click
                pos_delta,  // {x:x-delta, y:y-delta} - extra (x,y) adjustment
                adjust_call){ // extra call after menu shows up
        this.vmodel = vmodel
        this.element = element;
        this.click_event = left_click ? 'click' : 'contextmenu';
        this.pos_delta = pos_delta;
        this.adjust_call = adjust_call;
        this.AV_settings = {
            contextMenuClass: 'contextMenuPlugin',
            gutterLineClass: 'gutterLine',
            headerClass: 'header',
            seperatorClass: 'divider',
            title: menu_data.title? menu_data.title : '',
            items: []
        };
        $.extend(this.AV_settings, menu_data);
        this.AF_setup();
    };// end of AC_ContextMenu constructor
    
    AC_ContextMenu.prototype.AF_ = function(){
        this.AV_settings.items.length = 0;
    };// end of AC_ContextMenu.prototype.AF_reset_items

    AC_ContextMenu.prototype.AF_setup = function(ele){
        var self = this;
        if(ele) self.element = ele;
        if(self.element)
        this.element.unbind(self.click_event).bind(self.click_event, 
            function(e) {
                // if pav not focused upon, or no menu-item exists: no menu
                if(self.vmodel && self.vmodel.cid() === 'PAV' &&
                    !self.vmodel.AF_focused() &&
                    self.AV_settings.items.length > 0)
                    return false;
                if(self.vmodel && self.vmodel.cid() === 'TBV'){
                    if(!FD.AV_loginpv)
                        return false;
                }
                var top, left;
                if (self.vmodel){
                    // for some cases the menu shouldn't be active'
                    if( self.vmodel.cid()[0] === 'I' && // vmodel is 'ITV'
                        !M.OA(self.vmodel)){    // no access permission
                        return;                 // no action
                    } else if(self.vmodel.cid()[0] === 'B'){ // 'BAV'
                        if(!M.OA(self.vmodel))  // no access permission
                            return;             // no action
                    } else if(self.vmodel.cid() === 'AVT'){ // Audio/Video-Tag
                        if(!self.vmodel.AV_highlighted)// only for highlighted tag 
                            return false;
                    }
                    left = e.pageX + 5, top = e.pageY;
                } else {
                    // ent == null: context-menu for logged in user: 
                    //  logout/settings: left/top not by mouse-cursor, but 
                    // lined up with self.element
                    left = self.element[0].offsetLeft;
                    top = self.element[0].offsetTop + 24;
                }
                var menu = self.AF_create_menu(e);
                if(!menu) return;
                menu.show();
                /* nudge to the right, so the pointer is covering the title */
                
                if (top + menu.height() >= $(window).height()) {
                    var ydelta = top + menu.height() - $(window).height() + 8;
                    top -= ydelta;
                    if (top < 50)
                        top = 50;
                }
                if (left + menu.width() >= $(window).width()) {
                    left -= menu.width();
                }
                if(self.pos_delta){
                    if(self.pos_delta.x) left = left + self.pos_delta.x;
                    if(self.pos_delta.y) top = top + self.pos_delta.y;
                }
                // Create and show menu
                menu.css({zIndex:1000001, left:left, top:top})
                    .bind('contextmenu', function() { 
                        return false; 
                    });

                // Cover rest of page with invisible div (mask) so that only
                // menu is active. When the mask clicked, the popup-menu 
                // disappears, and the mask also disappears
                var bg = $('<div class="contextMenuPluginShield"></div>')
                    .css({left:0, top:0, width:'100%', height:'100%', 
                        position:'absolute', zIndex:1000000})
                    .appendTo(document.body)
                    .bind('contextmenu click', function() {
                        // If click or right click anywhere else on page: remove
                        bg.remove();
                        $('ul.contextMenuPlugin').remove();
                        $('div.contextMenuPluginShield').remove();
                        menu.remove();
                        return false;
                    });
                // When clicking on a link in menu: clean up 
                // (in addition to handlers on link already)
                menu.find('a').click(function() {
                    bg.remove();
                    menu.remove();
                });
                if(self.adjust_call)
                    self.adjust_call(self);
                // Cancel event, so real browser popup doesn't appear.
                return false;
            }
        );// end of this.element.bind(function(e){})
    };// end of AC_ContextMenu.prototype.AF_setup
    
    AC_ContextMenu.prototype.AF_title = function(v){
        if(typeof(v) === 'undefined')
            return this.AV_settings.title;
        this.AV_settings.title = v;
        return this;
    };// end of AC_ContextMenu.prototype.AF_title

    AC_ContextMenu.prototype.AF_modify_entry = function(entry, op){
        /*  entr.lable      - lcode code(e.G. M7534)
            entry.icon      - icon shown in ctxtmenu left col
            entry.func      - bacllback func ref
            entry.func_ctx  - callback func context
            entry.arg_array - func arg-list
            --------------------------------
            op==missing/add: add entry if no already existed
            op==remove: remove the item identified by entry.label + entry.icon
        */
        var i;
        if(!op || op === 'add'){
            // label is unique, if exists in items, don't add, return 
            for(i=0; i<this.AV_settings.items.length; i++){
                if(this.AV_settings.items[i]['label'] === entry.label)
                    return this;
            }
            var item = { 
                label: entry.label,
                icon:  entry.icon,
                action: function(e){
                    entry.func.apply(entry.func_ctx, entry.arg_array);
                    return U.AF_stop_event(e);
                }
            };
            this.AV_settings.items.push(item);
        } else if(op === 'remove'){
            i = 0;
            while(i<this.AV_settings.items.length){
                if(this.AV_settings.items[i].label === entry.label &&
                   this.AV_settings.items[i].icon === entry.icon){
                    this.AV_settings.items.splice(i,1);
                } else 
                    i++;
            }
        }
        return this;
    };// end ofContextMenu.prototype.AF_modify_entry
    
    AC_ContextMenu.prototype.AF_create_menu = function(e){
        if(this.AV_settings.items.length === 0) return;
        var menu = $('<ul class="' + this.AV_settings.contextMenuClass + 
            '"><div class="' + this.AV_settings.gutterLineClass + '"></div></ul>')
            .appendTo(document.body);
        if (this.AV_settings.title) {
            $('<li class="' + this.AV_settings.headerClass + ' LS"></li>')
                .attr('tag', this.AV_settings.title).appendTo(menu);
        }
        this.AV_settings.items.forEach(function(item) {
            if (item) {
                var row_li='<li><a href="#"><span class="LS"></span></a></li>';
                var row = $(row_li).appendTo(menu);
                if(item.icon){
                  $('<img src="'+item.icon+'">').insertBefore(row.find('span'));
                }
                row.find('span').attr('tag', item.label);
                if (item.action) {
                    row.find('a').click(function(){ 
                        $('ul.contextMenuPlugin').remove();
                        $('div.contextMenuPluginShield').remove();
                        item.action(e); 
                    });
                }
            } else {
                $('<li class="' + settings.seperatorClass + '"></li>')
                    .appendTo(menu);
            }
        });
        menu.find('.' + this.AV_settings.headerClass )
            .text(this.AV_settings.title);
        FD.AV_lang.AF_set_all(menu);
        return menu;
    };// end of AC_ContextMenu.prototype.AF_create_menu
    return AC_ContextMenu;
})();