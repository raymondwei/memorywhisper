
var AC_LandingPage100 = (function(){
    function AC_LandingPage100(fd, jbox, ctls){
        this.fd = fd;
        this.AV_lcode = fd.AF_lcode();
        this.AJ_div = jbox; // <div id=lp100 class=lp-tab/>
        this.AJ_upper = ctls.up; //<div class=lp-showpane/> child of jbox
        this.AJ_lower = ctls.dn; //<div class=lp-msgpane/> child of jbox
        this.AJ_lbtn = ctls.lb;  //<img/>left page-turn button. child of jox
        this.AJ_rbtn = ctls.rb;  //<img/>right page-turn button. child of jbox
        this.AJ_vtr = ctls.vc.css('visibility','hidden');
        // displayed page. child of AJ_upper(div.lp-showpane)
        this.AJ_curr = $('<div class="show-middle"></div>')
            .appendTo(this.AJ_upper);
        this.AV_fc_cursor = -1;
        this.AV_fcs = [];
        this.AF_setup_handlers();
        this.AV_vplayer = new AC_MediaPlayer(this,'video');
        this.AV_aplayer = new AC_MediaPlayer(this,'audio');
        this.AJ_highted_item = null;
        this.AJ_item_actimg = $('<img class="lpitem"/>')
            .attr('src','pics/right.png');
        var ids = this.fd.AV_store.AF_add2lst([
            'FTSUPER-FC-landingpage_fc090',
            'FTSUPER-FC-landingpage_fc100',
            'FTSUPER-FC-landingpage_fc101',
            'FTSUPER-RS-landingpage_rs101en',
            'FTSUPER-RS-landingpage_rs101zh',
            'FTSUPER-FC-landingpage_fc102',
            'FTSUPER-IT-landingpage100'
        ]);
        this.fd.AV_lserver.AF_getdics_helper(ids, 'get-lp100', this);
    };// end of AC_LandingPage100 constructor

    AC_LandingPage100.prototype.AF_setup_navbox = function(){
        this.spans = $('span.lp100-nav');
        if(this.spans.length === 0) return;
        var self = this;
        this.spans.unbind('click').bind('click', function(e){
            self.AF_show_pane($(this).attr('id'));
        });
        return this;
    };// AC_LandingPage100.prototype.AF_setup_navbox

    AC_LandingPage100.prototype.AF_resize = function(w, h){
        this.AJ_div.css({width: w + 'px', height: h + 'px'});
        this.AJ_upper.css({width: (w - 6) + 'px', height: (h - 55) + 'px'});
        // lower(lp-msgpane) has padding 15px both left/right: take 20 off
        this.AJ_lower.css({width: (w - 6 - 20) + 'px', height: 43 + 'px'});    
    };// end of AC_LandingPage100.prototype.AF_resize

    AC_LandingPage100.prototype.AF_init = function(){
        if(this.AV_fcs.length > 0){
            this.AV_fc_cursor = 1;
            this.AF_show_pane();
        }
    };// end of AC_LandingPage100.prototype.AF_init

    AC_LandingPage100.prototype.AF_handle_success = function(data, self){
        self.it = M.e("FTSUPER-IT-landingpage100");
        self.AV_fcs = M.e(self.it.iddict.facets); // array of fc ents
        self.AF_init();
    };// end of AC_LandingPage100.prototype.AF_handle_success

    // depending on fc-cursor position, make left/right arrow hidden/visible
    AC_LandingPage100.prototype.AF_update_arrows = function(){
        var l_vsbl = this.AV_fc_cursor > 0? 'visible' : 'hidden',
        r_vsbl=this.AV_fc_cursor < (this.AV_fcs.length-1)? 'visible' : 'hidden';
        this.AJ_lbtn.css('visibility',l_vsbl); // left-button visibility
        this.AJ_rbtn.css('visibility',r_vsbl); // right-button visibility
        return this;
    };// end of AC_LandingPage100.prototype.AF_update_arrows

    // setup eventhandlers for lbtn,rbtn and pbtn, sbtn
    AC_LandingPage100.prototype.AF_setup_handlers = function(){
        var self = this, curr_fc,
        spans = $('span.lp100-nav');     
        this.AJ_lbtn.unbind('click').bind('click', function(e){
            curr_fc = self.AV_fcs[self.AV_fc_cursor];
            // before leaving a video page, stop/cut streaming maybe going
            if(curr_fc && curr_fc.type() === 'video'){
                self.AV_vplayer.AF_set_media_src();
                __media_stop_load('v');
            }
            self.AF_show_pane('-');
            U.AF_stop_event(e);
        });
        this.AJ_rbtn.unbind('click').bind('click', function(e){
            curr_fc = self.AV_fcs[self.AV_fc_cursor];
            // before leaving a video page, stop/cut streaming maybe going
            if(curr_fc && curr_fc.type() === 'video'){
                self.AV_vplayer.AF_set_media_src();
                __media_stop_load('v');
            }
            self.AF_show_pane('+');
            U.AF_stop_event(e);            
        });
    };// end of AC_LandingPage100.prototype.AF_setup_handlers

    AC_LandingPage100.prototype.AF_show_fc = function(jdiv){
        var rs, vp, self = this,
        lc = FD.AF_lcode() === 'zh'? 'zh' : 'en',
            fc = this.AV_fcs[this.AV_fc_cursor];
        this.AJ_lower.attr('tag', fc.id() + '@AF_anno_face')
            .html(fc.AF_anno_face(this.AV_lcode));
        __media_stop_load('a');

        if(fc.type() === 'txt'){
            var div = $('div.lp-text');
            if(div.length === 0){
                div = $('<div class="lp-text LS"></divs>')
                .attr('tag', fc.id()+'@contdict_face');
            }  
            div.html(fc.contdict_face(this.AV_lcode)).appendTo(jdiv);
            if(this.AV_fc_cursor === 1){
                this.AF_setup_navbox();
                vp = $('audio');
                if(vp.length === 0 || vp.css('visibility') === 'hidden'){
                    this.AV_aplayer.AF_set_media_src(
                       // 'OnceUponATimeInAmerica.mp3', div);
                       'river-flows-in-you.mp3', div);
                    }
            }
        } else if(fc.type() === 'video'){
            rs = M.e(fc.AF_contdict()[this.AV_lcode]);
            this.AV_vplayer.AF_set_media_src(rs.name(), jdiv);
        } 
        if(this.AV_fc_cursor === 3){
            this.AF_set_flist_handlers();
        }
        return jdiv;
    };// end of AC_LandingPage100.prototype.AF_show_fc

    AC_LandingPage100.prototype.AF_show_gif = function(id){
        alert(id);
    }

    // set event handlers on feature-list (fc102) page
    AC_LandingPage100.prototype.AF_set_flist_handlers = function(){
        var self = this, findex,
            fc = this.AV_fcs[this.AV_fc_cursor],
            tips = fc.AF_anno_face().split('||');
        self.AJ_lower.html('');
        $('div.lpitem').unbind('click').bind('click', function(e){
            var splt = $(this).attr('id').split('-');
            findex = parseInt(splt[splt.length - 1]) - 1;            
            self.AJ_highted_item
                .removeClass('lpitem-highlight')
                .addClass('lpitem');
            self.AJ_item_actimg.detach();
            $(this).removeClass('lpitem').addClass('lpitem-highlight');
            $(this).append(
                self.AJ_item_actimg.css({width: '26px',height:'30px'}));
            self.AF_set_flist_handlers();
            self.AJ_lower.html(tips[findex]);
        });
        this.AJ_highted_item = $('div.lpitem-highlight');
        this.AJ_highted_item.unbind('click').bind('click',function(e){
            self.AF_show_gif($(this).attr('id').split('-')[2]);
        })
    };// end of AC_LandingPage100.prototype.AF_set_flist_handlers

    AC_LandingPage100.prototype.AF_show_fitem = function(fitem_id){

    };// end of AC_LandingPage100.prototype.AF_show_fitem

    // dir === null: show current fc, no animation
    // dir === '-' : show prev fc animated
    // dir === '+' : show next fc animated
    // dir = "nav-item-N" : show this.AV_fcs[int(N)]
    AC_LandingPage100.prototype.AF_show_pane = function(dir){// null | - | +
        var self = this;
        if(!dir){
            // div.show-middle contained by div.lp-showpane
            this.AF_show_fc(this.AJ_curr);
        } else { // dir == - | +
            var div = $('<div></div>');
            self.AJ_curr.fadeTo(500, 0.0, function(e){
                $(this).remove();
            });
            if(dir === '-' || dir === 'nav-item-0'){
                self.AV_fc_cursor--;
                div.addClass('show-left').empty().appendTo(self.AJ_upper);
                self.AF_show_fc(div).animate({width: '100%'}, 400, function(e){
                    $(this).removeClass('show-left').addClass('show-middle');
                    self.AJ_curr = $(this);
                });
            } else if(dir === '+' || 
                ['nav-item-2','nav-item-3'].indexOf(dir) > -1){
                if(dir === '+'){
                    self.AV_fc_cursor++;                    
                } else if(dir === 'nav-item-2'){
                    self.AV_fc_cursor = 2;
                } else if(dir === 'nav-item-3'){
                    self.AV_fc_cursor = 3;
                }
                div.addClass('show-right').empty().appendTo(self.AJ_upper);
                self.AF_show_fc(div).animate({width: '100%'}, 400, function(e){
                    $(this).removeClass('show-right').addClass('show-middle');
                    self.AJ_curr = $(this);
                });
            }    
        } 
        return this.AF_update_arrows();
    };// end of AC_LandingPage100.prototype.AF_show_pane

    return AC_LandingPage100;
})();// end of AC_LandingPage100 class

var AC_MediaPlayer = (function(){
    function AC_MediaPlayer(lp100, va){ // 'v' for video, 'a' for audio
        this.lp100 = lp100;
        this.AV_va = va;
        var self = this;
        if(va === 'video'){
            this.AJ_vplay_btn = $(
                '<img class="lp-media-button" src="pics/play-button.png"/>')
                .appendTo(lp100.AJ_div).css('visibility','hidden');
            this.AJ_vpause_btn = $(
                '<img class="lp-media-button" src="pics/pause-button.png"/>')
                .appendTo(lp100.AJ_div).css('visibility','hidden');
            this.AJ_vtr = lp100.AJ_vtr;
                this.AJ_player = $('<video id="mw-video-player"></video>');
            this.AJ_source = $('<source type="video/mp4">')
                .appendTo(this.AJ_player);
        } else if(va === 'audio'){
            this.AJ_aframe = $('<div class="lp-audio-contrl"></div>');
            this.AJ_aplay_btn = $('<div id="lp-audio-play">')
                .appendTo(this.AJ_aframe)
                .attr('src','pics/play-button.png');
            this.AJ_plus = $('<div id="lp-audio-plus"></div>')
                .appendTo(this.AJ_aframe).css('visibility','hidden');
            this.AJ_minus = $('<div id="lp-audio-minus"></div>')
                .appendTo(this.AJ_aframe).css('visibility','hidden');
            this.AJ_vol = $('<span id="lp-audio-vol"></span>')
                .appendTo(this.AJ_aframe).css('visibility','hidden');
                //.css('visibility', 'hidden');
            this.AJ_player = $('<audio autoplay="false" loop></audio>');
            this.AJ_source = $('<source type="audio/mpeg">')
                .appendTo(this.AJ_player);
        }
    };// end of AC_MediaPlayer constructor
    
    AC_MediaPlayer.prototype.AF_play = function(){
        this.AJ_player.get(0).play();
        var self = this,
            apause_btn_url = 'url(../pics/pause-button.png)';
        if(this.AV_va === 'video'){
            self.AJ_vpause_btn.css('visibility','visible');
            self.AJ_vplay_btn.css('visibility','hidden');
            this.AJ_vpause_btn.unbind('click').bind('click', function(e){
                self.AF_pause();
            })
        } else {
            self.AJ_aplay_btn.css('background-image',apause_btn_url);
            self.AJ_aplay_btn.unbind('click').bind('click',function(e){
                self.AF_pause();
            });
        }
    };// end of AC_MediaPlayer.prototype.AF_play

    // pause video
    AC_MediaPlayer.prototype.AF_pause = function(){
        this.AJ_player.get(0).pause();
        var self = this,
            aplay_btn_url = 'url(../pics/play-button.png)';
        if(this.AV_va === 'video'){
            self.AJ_vpause_btn.css('visibility','hidden');
            self.AJ_vplay_btn.css('visibility','visible');
            this.AJ_vplay_btn.unbind('click').bind('click', function(e){
                self.AF_play();
            })        
        } else {
            self.AJ_aplay_btn.css('background-image',aplay_btn_url);
            self.AJ_aplay_btn.unbind('click').bind('click',function(e){
                self.AF_play();
            });
        }
    };// end of AC_MediaPlayer.prototype.AF_pause

    // jdiv is the div containing play(<video or <audio). It is
    // this.AV_va==video: div.show-middle; 
    // this.AV_va==audio:
    AC_MediaPlayer.prototype.AF_set_media_src = function(name, jdiv){
        if(!FD.AV_landing.AV_is_public)  // only when it is public page
            return;                      // set audio player
        var self = this, msg, vtr_set = false,
        p = this.AJ_player.get(0);
        p.pause();
        p.remove();
        // test if $('video'|'audio') already exists under jdiv
        if(jdiv && $(this.AV_va, jdiv).length === 0)// jdiv given has no player 
            this.AJ_player.appendTo(jdiv);          // append player to jdiv
        this.AJ_source.attr('src', "blobs/FTSUPER/"+name);
        p.load();// load src if name given; !name: tops previous load(streaming)
        if(!name){
            this.AJ_player.detach();
            if(this.AV_va === 'video'){
                this.AJ_vpause_btn.css('visibility', 'hidden');
                this.AJ_vplay_btn.css('visibility', 'hidden');
            }     
            return; 
        }
        if(this.AV_va === 'audio'){
            this.AJ_player.css('visibility','visible');
            p.volume = 0.2;
            this.AF_pause();
            this.AJ_aframe.appendTo(jdiv)
                .hover(function(e){
                    self.AJ_plus.css('visibility','visible');
                    self.AJ_minus.css('visibility','visible');
                    self.AJ_vol.css('visibility','visible');
                }, function(e){
                    self.AJ_plus.css('visibility','hidden');
                    self.AJ_minus.css('visibility','hidden');
                    self.AJ_vol.css('visibility','hidden');
                }
            );
            this.AJ_plus.unbind('click').bind('click', function(e){
                if(p.volume < 1) p.volume += 0.1;
                self.AJ_vol.text(Math.round(100*p.volume)+'%');
            });
            this.AJ_minus.unbind('click').bind('click', function(e){
                if(p.volume > 0) p.volume -= 0.1;
                self.AJ_vol.text(Math.round(100*p.volume)+'%');
            });
        } else if(this.AV_va === 'video'){
            this.AF_pause();
            this.AJ_player.unbind('timeupdate').bind('timeupdate',function(e){
                if(Math.round(this.currentTime) > 0){
                    if(!vtr_set){
                        self.AJ_vtr.css('visibility','visible');
                        vtr_set = true;    
                    }
                    msg = U.AF_time_count(Math.round(this.currentTime)) + '/' +
                    U.AF_time_count(Math.round(this.duration));
                    self.AJ_vtr.text(msg);
                } else {
                    self.AJ_vtr.css('visibility','hidden');
                    vtr_set = false;    
                }
            })
            $('img.lp-media-button').css('border','0px');
            this.AJ_vtr.css('visibility','hidden');            
        }
    };// end of AC_MediaPlayer.prototype.AF_set_media_src

    return AC_MediaPlayer;
})();
