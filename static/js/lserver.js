var AC_LServer = (function(){
    function AC_LServer(AV_fd){
        this.AV_fd = AV_fd;
        this.counter = 0;         // req-id: this.clientname_<counter>
        // within 5 sec, the same req-verb will be blocked
        this.req_exp_time = 5000; // 5 seconds (5000 milisecs)
        // random 12char for this session
        this.clientname = M.AF_random_string(6);
        this.pending = {}; // {<req-id>: <act>}
        this.idbs = AV_fd.idbs; // fid-keyed idb-dict
        this.AF_get_idb(AV_fd.fid(), AV_fd.AF_init.bind(AV_fd));
    };// end of AC_LServer constructor

    AC_LServer.prototype.AF_get_idb = function(fid, cb){
        if(!(fid in this.idbs)){
            this.idbs[fid] = new AC_FidDB(fid, cb);
        } else {
            if(cb)
                cb();
        }
        //return this.idbs[fid];
    };// end of AC_LServer.prototype.AF_get_idb
    
    // get a req-id, if a req with the same verb exists that is younger
    // than 5 secs: return null; if a req with the same verb exists that
    // is older than 5 secs: delete that pending entry, and return a new id.
    // if no pending req exists with the same verb, return a new id, which is
    // <this.clientname>_<counter++>
    AC_LServer.prototype.AF_get_req_id = function(act){
        var allow = true, 
            req_id, i,
            now = __UTC(),  // now-time-stamp in milisecs
            ks = Object.keys(this.pending);// keys of all pending reqs
        for(i=0; i<ks.length; i++){
            if(this.pending[ks[i]].act === act){
                // if a pending req_id with the same req-verb(act) exists, 
                // check if over 5 secs old: if yes, delete that entry
                if((now - this.pending[ks[i]].ts) > this.req_exp_time){
                    delete this.pending[ks[i]];
                } else {
                    // the same act within last 5 secs: block the new req
                    allow = false;
                }
            }
        }
        if(allow){ // not blocked
            this.counter++;
            req_id = this.AV_fd.AV_localip + '@' + 
                    this.clientname + '_' + this.counter;
            if(this.AV_fd.AV_gateip.length > 0)
                req_id = this.AV_fd.AV_gateip + '#' + req_id;
            this.pending[req_id] = {ts: now, act: act};
            return req_id;
        } else 
            return null;
    };// end of AC_LServer.prototype.AF_get_req_id
    
    // standard success event handling
    AC_LServer.prototype.AF_process_success = function(
                eventobj, // from event-source {news:[ent,..], dels:[id,..]}
                actverb,  // user calling verb 
                ctx,      // [op] given context  
                deferred){// [op] for resolve 
        U.log(2, actverb + ' succeeded.');
        if(ctx && ctx.AF_handle_success)
            ctx.AF_handle_success(eventobj, ctx);
        if(deferred)
            deferred.resolve(eventobj);
    };// end of AC_LServer.prototype.AF_process_success
    
    AC_LServer.prototype.AF_process_fail = function(err, actverb, ctx, deferred){
        U.log(2, actverb + ' failed: ' + err);
        if(ctx && ctx.AF_handle_err)
            ctx.AF_handle_err(err, ctx);
        if(deferred)
            deferred.reject(err);
    };// end of AC_LServer.prototype.AF_process_fail
    
    /**
     * send ajax-post request with reqverb. dic is the post content, where
     * it will be extended with {_req_id:, _fid:}
     * On getting resp, handle docs(save in store/idb), and dels / blobs
     */
    AC_LServer.prototype.AF_post_req = function(reqverb, act, dic){
        var self = this, data = {}, 
            deferred = $.Deferred(),
        // generate a client-req-id for preventing repeated sending from
        // client to server, or from server responding to client.
        req_id = this.AF_get_req_id(act);// if act is already pending:no id
        if(!req_id){
            U.log(1, "AC_LServer busy with prev-"+act);
            deferred.reject("lserver busy");
        }
        $.extend(dic, {_req_id: req_id, _fid: FD.fid()});
        if(!('_pid' in dic ))
            dic._pid = FD.AV_loginpv? FD.AV_loginpv.model.id() :
                            FD.AF_centerpa()? FD.AF_centerpa().id() : '';
        
        // showing to client the waiting clock 
        $('div.wait-icon').css('visibility','visible');
        var ajax_promise = $.post('/req/' + reqverb, dic);
        ajax_promise.done(function(e){
            //hide hour-glass: waiting is done.
            $('div.wait-icon').css('visibility','hidden');
            U.log(2, '/req/' + reqverb + ' ' + act + " done.");
            var jda = JSON.parse(e);
            if(jda['result'].indexOf('__OK__') === -1){// no __OK__:failed
                U.log(2, 'result not OK:' + jda['result']); 
                delete self.pending[req_id];
                deferred.reject(reqverb + " fail on server:" + jda['result']);
            } else // __OK__ found: successful
            if(req_id === jda['_req_id'] && req_id in self.pending){
                // req-id is found in pending-verbs
                U.log(0,
                "Done with "+req_id+":"+JSON.stringify(self.pending[req_id]));
                if('docs' in jda){ // docs in response
                    // save docs in store and idb
                    U.log(2, act + " post_req results:" + jda);
                    data.ents =  M.AF_load(jda['docs']);// put into store
                    // into indexedDB: dont care about done-callback
                    self.AF_get_idb(FD.fid(), function(){
                        self.idbs[FD.fid()].AF_put_ents(data.ents);
                        U.log(0, "store count increase:"+data.ents.length);
                    });
                }
                if('dels' in jda){
                    M.de(jda.dels);
                    data.dels = jda['dels'];
                }
                if(jda.ctver){// putdocs-> ctver increase: current DB.ct.ctver
                    FD.AV_fa.AF_ctver(jda.ctver);
                    FD.AF_synch_idb();
                }
                if('blob' in jda) 
                    data.blob = jda['blob'];
                if('key' in jda)
                    data.key = jda['key'];
                if('gateip' in jda)
                    self.AV_fd.AV_gateip = jda['gateip'];
                // delete verb from pendings
                delete self.pending[req_id];
                deferred.resolve(data);
            } else { // req_id is not inpending acts
                U.log(0, reqverb+ " req-id not in pending-list:" + req_id)
                deferred.reject(reqverb+ " req-id mismatch:" + req_id);
            }
        });
        ajax_promise.fail(function(e){
            $('div.wait-icon').css('visibility','hidden');//hide hour-glass
            U.log(0, "Fail with " + req_id + ":" + self.pending[req_id]);
            delete self.pending[req_id];
            U.log(2, '/req/' + reqverb +' '+ act+ " failed");
            deferred.reject(reqverb + " failed on ajax.");
        });    
        return deferred.promise();
    };// end of AC_LServer.prototype.AF_post_req
    
    // non-get request: call for action on server side
    AC_LServer.prototype.AF_post = function(verb, dic, ctx){
        var self = this;
        this.AF_post_req(verb, verb, dic).done(function(e){
            // no deferred: null, look-ahead: true
            self.AF_process_success(e,    // source eventobj 
                                    verb, // verb
                                    ctx,  // calling context
                                    null, // deferred: null 
                                    true);
        }).fail(function(err){
            self.AF_process_fail(err, verb, ctx);
        })
    };// end of AC_LServer.prototype.AF_post
        
    // ids - array of ids from both FidDB.db and fid-db. get from indexedDB
    // missing ones send req to server asking for them
    AC_LServer.prototype.AF_getdics_helper = function(ids, act, ctx){
        var deferred = $.Deferred(), po, 
            self = this;
        FD.AF_se('update'); // in case session exists, update se.tx

        // missing: array of ids that arent in idb. Get them from server/DB
        function AF_process_missing(missing){
            if(missing.length === 0){
                self.AF_process_success(null,        // source eventobj 
                                        act+' req',  // verb 
                                        ctx,         // calling context
                                        deferred);   //
            } else {
                // some from idb mssing: send getdics req > server asking that
                po = self.AF_post_req('getdics', act, 
                                    {ids: JSON.stringify(missing)});
                po.done(function(e){
                    // look-ahead: false
                    self.AF_process_success(e, act, ctx, deferred);
                });
                po.fail(function(err){
                    self.AF_process_fail(err, act, ctx, deferred);
                }); 
            }
        };// end of function AF_process_missing(missing)
        
        // get ents(ids) from idb. If missing>0, call AF_process_missing
        // ids must be of the same fid
        if(ids.length > 0){
            var fid = ids[0].substr(0,7);
            this.AF_get_idb(fid, function(){
                self.idbs[fid].AF_get_ents(ids).done(function(e){
                    if(e.miss.length > 0){
                        AF_process_missing(e.miss);
                    } else {
                        // all ents fetched from idb: nothing missing
                        self.AF_process_success(null, act, ctx, deferred);
                    }
                })
            });
        } else { // ids.length == 0: nothing to get, call the success-callback
            self.AF_process_success(null, act, ctx, deferred);
        }
        return deferred.promise();
    };// end of AC_LServer.prototype.AF_getdics_helper
    
    /* APP REQUEST */
    // ent is the containing IP/IT entity.
    // get all containees of entity
    AC_LServer.prototype.AF_get_info = function(ent, ctx){
        var deferred = $.Deferred(), 
            promise, self = this,
            ids = [];
            AC_ActionBuffer.AF_nextlevel_ids(ent, ids);
        if(ids.length === 0){
            self.AF_process_success(null, 'get-info', ctx, deferred);
        } else {
            promise = this.AF_getdics_helper(ids, 'get-info');
            promise.done(function(e){
                self.AF_process_success(e, 'get-info', ctx, deferred);
            });
            promise.fail(function(err){
                self.AF_process_fail(err, 'get-info', ctx, deferred);
            })
        }
        return deferred.promise();
    };// end of AC_LServer.prototype.AF_get_info
    
    AC_LServer.prototype.AF_landing = function(){
        var deferred = $.Deferred(), 
            prom, ids = FD.AF_init_ids();
        // getting all these and callback points to FD.AF_handle_success
        prom = this.AF_getdics_helper(ids, 'landing', this.AV_fd);
        prom.done(function(e){
            deferred.resolve(e);
        });
        prom.fail(function(err){
            deferred.reject(err);
        })
        return deferred.promise();
    };// end of AC_LServer.prototype.AF_landing

    // using pid(not pa), to get pa_focus ents
    AC_LServer.prototype.AF_initialpa_focus = function(pid, ctx){
        var self = this, 
        fid = pid.substr(0,7),
        ct_id = fid + '-CT-' + fid,
        deferred = $.Deferred(), 
        pro, pro1, ids = FD.AF_init_ids();
        FD.AV_store.AF_add2lst(pid, ids);
        
        if(!FD.AV_store.AF_id_is_in(pid)){
            pro = this.AF_getdics_helper(ids, 'initial-pa-focus');
            pro.done(function(jda){
                var pa = M.e(pid);
                if(!pa){
                    self.AF_process_fail('', 'initial-pa-focus', ctx, deferred);
                } else {
                    pro1 = self.AF_pa_focus(pa, ctx);
                    pro1.done(function(e){
                        deferred.resolve(e);
                    })
                    pro1.fail(function(e){
                        if(ctx && ctx.AF_handle_err)
                            ctx.AF_handle_err(pid, ctx);
                        deferred.reject(e);
                    })
                }
            });
            pro.fail(function(e){
                deferred.reject(e);
            });
            return deferred.promise();
        } else {
            return this.AF_pa_focus(M.e(pid), ctx);
        }
    };// end of AC_LServer.prototype.AF_initialpa_focus
    
    // loads all ents for pa to be focused
    AC_LServer.prototype.AF_pa_focus = function(pa, ctx){
        // pa containees and p-ba and N x s-bas
        var ids = [];
        AC_ActionBuffer.AF_focus_ids(pa, ids);
        return this.AF_getdics_helper(ids, 'pa-focus', ctx);
    };// end of AC_LServer.prototype.AF_pa_focus
    
    // collecting all missing spousal pas, and parents pas(father/mother)
    AC_LServer.prototype.AF_pa_love_exp = function(pa, ctx){
        var i, ids = [],
            pbas = M.e(U.AF_trim_adopt_sign(pa.iddict.parents)), 
            spouses = pa.spouses();
        for(i=0; i<spouses.length; i++){
            // if pa not in store/idb, into ids-array.
            FD.AV_store.AF_add2lst(spouses[i].AF_other_pid(pa.id()), ids);
        }
        // get also father/mother
        for(i=0; i<pbas.length; i++){
            FD.AV_store.AF_add2lst(
                [pbas[i].iddict.male, pbas[i].iddict.female], ids);
        }
        // get ents of ids
        return this.AF_getdics_helper(ids, 'pa-love-exp', ctx);
    };// end of AC_LServer.prototype.AF_pa_love_exp
    
    // get all children pas of ent(ent can be pa or bas-array)
    //  ent is a pa: expand all pa's children(from every spouse-bas)
    //  ent is an array [ba,ba,..]: expand children from these bas
    AC_LServer.prototype.AF_children_exp = function(ent, ctx){
        var ids = [];
        if($.isArray(ent)){ // ent is array of bas (all spousal-bas of pa)
            for(var i=0; i<ent.length; i++){
                // ba-children: array of possibly -|+ prefixed id list
                // trim off the possible -|+, put child-id(pid)s into ids
                // if the child-is(pid) is not in FD.AV_store already
                FD.AV_store.AF_add2lst(
                     U.AF_trim_adopt_sign(ent[i].iddict.children), ids);
            }
            return this.AF_getdics_helper(ids, 'pa-children-exp', ctx);
        } else if(ent.cid() === 'PA'){ 
            // ent is a pa
            return this.AF_children_exp(ent.spouses());
        }
    };// end of AC_LServer.prototype.AF_children_exp
    
    // pa-center: show all spousal pas, and all children pas. for focus pa
    AC_LServer.prototype.AF_pa_center = function(pa, ctx){
        var promise = this.AF_pa_focus(pa), // get all ents for focusing pa
            self = this, p2, p3, deferred = $.Deferred();
        promise.done(function(jad){
            p2 = self.AF_pa_love_exp(pa);  // get all missing spousal pas
            p2.done(function(e){
                p3 = self.AF_children_exp(pa); // get all missing children pas
                p3.done(function(e){
                    self.AF_process_success(e,'child-exp', ctx, deferred);
                }).fail(function(err){
                    self.AF_process_fail(err, 'child-exp', ctx, deferred);
                })
            });
            p2.fail(function(err){
                self.AF_process_fail(err, 'love-exp', ctx, deferred);
            })
        });
        promise.fail(function(err){
            self.AF_process_fail(err, 'pa-focus', ctx, deferred);
        })
        return deferred.promise();
    };// end of AC_LServer.prototype.AF_pa_center
    
    // condition: 
    // 1.pa' 1st parent-ba must not be virtual
    // 2.pa is in focus
    // purpose: 
    // 1.get focus-ents for 1st parent-ba's male(if non-existing then female)
    // 2.get ents for that male-parent-pa love-exp 
    // 3.get all children of male-parent-pa
    AC_LServer.prototype.AF_pa_ancester_exp = function(pa, ctx){
        var self = this, deferred = $.Deferred(), p1, p2, p3,
            ba = M.e(U.AF_trim_adopt_sign(pa.iddict.parents[0])),
            pid = ba.iddict.male? ba.iddict.male : ba.iddict.female; 
        p1 = this.AF_initialpa_focus(pid);
        p1.done(function(e){
            var p = M.e(pid);
            p2 = self.AF_pa_love_exp(p);
            p2.done(function(e){
                p3 = self.AF_children_exp(p);
                p3.done(function(e){
                    if(ctx && ctx.AF_handle_success){
                        ctx.AF_handle_success(e, ctx);
                    }
                    deferred.resolve(e);
                });
                p3.fail(function(e){
                    if(ctx && ctx.AF_handle_err)
                        ctx.AF_handle_err(e, ctx);
                    deferred.reject(e);
                });
            });
            p2.fail(function(e){
                if(ctx && ctx.AF_handle_err)
                    ctx.AF_handle_err(e, ctx);
                deferred.reject(e);
            })
        });
        p1.fail(function(e){
            if(ctx && ctx.AF_handle_err)
                ctx.AF_handle_err(e, ctx);
            deferred.reject(e);
        });
        return deferred.promise();
    };// end of AC_LServer.prototype.AF_pa_ancester_exp

    // delete-local-ces will not send req to server side, it quietly deletes
    // some ces in indexedDB, so that they will be fetched from DB next time.
    // -----------------------------------------------------------------------
    // use-case: 
    // when a new pa created, or when dpb/dod of existing pa is modified
    // all ce(s) touched, will be deleted from store/idb. This way
    // ce will be fetched from sverver-DB, which is updated by the pa-actions
    // months is array of ce_ids to be deleted.
    AC_LServer.prototype.AF_delete_local_ces = function(ces_ids){
        if(!ces_ids.length || ces_ids.length === 0 )
            return;
        var self = this,
            fid = ces_ids[0].substr(0,7);
        this.AF_get_idb(fid, function(){
            self.idbs[fid].AF_delete_ents(ces_ids);
        });
    };// end of AC_LServer.prototype.AF_delete_local_ces
    
    return AC_LServer;
})()