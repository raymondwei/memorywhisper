/*
 * Copyright (C) 2014 Adacro Inc. - All Rights Reserved
 * ----------------------------------------------------------------------------
 * AC_PaTag is a person's name-tag on the family-tree graph on the left side
 */// ---------------------
var AC_PaTag = (function(){
    function AC_PaTag(pav){
        this.pav = pav;
        this.AF_init_visuals(); // take sex/alive from pav.model
        this.AV_ctxtmenu = new AC_ContextMenu(this.pav,
                            this.AJ_acticon, // act-icon 
                            {},    // menu will be fed later
                            true); // left-click=true
        this.AF_set_handlers();
        this.AV_pwd_dlg = new AC_PwdPopup(pav);
    };// end of AC_PaTag constructor

    AC_PaTag.prototype.AF_update_adpmenu = function () {
        // only when pav is anchor, show adp, if it has adp.
        if(this.pav.AF_is_anchor() && this.AJ_adp){
            this.AV_goto_bindexes = this.AF_adp_goto_bas();
            // M6010: bio-parents / M6009: adopting-parents
            var msg = this.pav.AF_adp() === '+'? 'M6010' : 'M6009'; 
            if(this.AV_goto_bindexes.length > 0){
                this.AV_adp_ctxtmenu = new AC_ContextMenu(this.pav,
                                this.AJ_adp, // element for click
                                {},          // menu will be fed later
                                true,        // left-click=true
                                {x: 130});   // position offset
                for(var i = 0; i < this.AV_goto_bindexes.length; i++){
                    this.AV_adp_ctxtmenu.AF_modify_entry({label: msg,
                        icon: 'pics/greenright-arrow.png',
                        func: this.AF_go_parents, 
                        func_ctx: this, arg_array: [this.AV_goto_bindexes[i]]});
                }
            } else {
                if(M.OA(this.pav)){
                    this.AV_adp_ctxtmenu = new AC_ContextMenu(this.pav,
                        this.AJ_adp,
                        {},
                        true,
                        {x: 130});
                    this.AV_adp_ctxtmenu.AF_modify_entry({label: msg,
                        icon: 'pics/square-plus.jpg',
                        func: this.AF_add_parents, 
                        func_ctx: this, arg_array: [msg]});
                } else if(this.AV_adp_ctxtmenu){
                    this.AV_adp_ctxtmenu.AF_modify_entry(
                        {label: msg, icon: 'pics/square-plus.jpg'},
                        "remove"
                    );
                }
            }
        }
        return this.AF_set_tooltip();
    };// end of AC_PaTag.prototype.AF_update_adpmenu
    
    AC_PaTag.prototype.cid = function(){ return 'PTG'; }
    
    AC_PaTag.prototype.AF_add_parents = function(msg){
        var x = 1;
    };// end of AC_PaTag.prototype.AF_add_parents

    // fill goto_bids array with ids of bas, that are not the host ba
    AC_PaTag.prototype.AF_adp_goto_bas = function(){
        var AV_bindexes = [],
            bids = this.pav.model.iddict['parents'];
            hostbid = this.pav.phost.model.id();
        for(var i=0; i<bids.length; i++){
            if(bids[i].indexOf(hostbid)==-1){ // not hosting-ba
                AV_bindexes.push(i);
            }
        }
        return AV_bindexes;
    };// end of AC_PaTag.prototype.AF_adp_goto_bas
    
    AC_PaTag.prototype.AF_go_parents = function(index){
        this.AV_parent_index = index;
        this.reqverb = 'go-adp-parent';
        FD.AV_lserver.AF_pa_center(this.pav.model, this);
    }; //end of AC_PaTag.prototype.AF_go_parents
    
    AC_PaTag.prototype.AF_left = function(v, AV_animated, AV_animate_callback){
        // setter
        if(typeof(v) !== 'undefined'){ 
            if(AV_animated){  // move to left==v position with animation
                this.AJ_div.animate({left: v}, 
                                    U.AV_animate_duration, 
                                    AV_animate_callback);
            } else // move to left==v position without animation
                this.AJ_div.css('left',v); 
            return this; 
        } 
        // getter: return left-position in pixel count(integer)
        return U.INT(this.AJ_div.css('left'));
    };// end of AC_PaTag.AF_left method
    
    AC_PaTag.prototype.AF_top = function(v, AV_animated, AV_animate_callback){
        // setter: set top, return this
        if(typeof(v) !== 'undefined'){ 
            if(AV_animated) { // move to top==v position with animation
                this.AJ_div.animate({top: v}, 
                                    U.AV_animate_duration, 
                                    AV_animate_callback);
            } else  // move to top==v position without animation
                this.AJ_div.css('top',v); 
            return this; 
        }
        // getter: return top position in pixel count(integer)
        return U.INT(this.AJ_div.css('top'));
    };// end of AC_PaTag.AF_top method
    
    AC_PaTag.prototype.AF_set_position = function(
                            pos, 
                            AV_animated, 
                            AV_animate_callback){
        if(AV_animated){
            var AV_callback_done = false;
            if(pos.x !== this.AF_left()){
                this.AF_left(pos.x, true, AV_animate_callback);
                AV_callback_done = true;
            }
            if(pos.y !== this.AF_top()){
                if(AV_callback_done)
                    this.AF_top(pos.y, true, null);
                else
                    this.AF_top(pos.y, true, AV_animate_callback);
            }
        } else {
            if(pos.x !== this.AF_left()) 
                this.AF_left(pos.x);
            if(pos.y !== this.AF_top())
                this.AF_top(pos.y);
        }
        return this;
    };// end of AC_PaTag.prototype.AF_set_position
    
    AC_PaTag.prototype.AF_set_tooltip = function(){
        var msg = FD.AV_lang.face('M6013');
        if(this.pav.AF_is_anchor()){
            if(this.pav.AF_anc_state() === 'expanded')
                msg = FD.AV_lang.face('M6012'); // click to fold
            else
                msg = FD.AV_lang.face('M6011'); // click to expand
        }
        if (this.AJ_anchor) 
            this.AJ_anchor.attr('title', FD.AV_lang.face('M7516')+': '+msg);
        if (this.AJ_love){
            msg = FD.AV_lang.face('M7517') + ': '; // spouses :
            if(this.AJ_love.attr('src') === '/pics/minus1.png')
                msg += FD.AV_lang.face('M6012'); // click to fold
            else
                msg += FD.AV_lang.face('M6011');
            this.AJ_love.attr('title', msg);    // click to expand
        }
        // mouse-over (tooltip/title) 'Adopted child'
        if (this.AJ_adp){
            if(this.AJ_adp.attr('class') === 'adopt-in')
                this.AJ_adp.attr('title', FD.AV_lang.face('M0018'));
            else
                this.AJ_adp.attr('title', FD.AV_lang.face('M6007'));
        }
        return this;
    };// end of AC_PaTag.prototype.AF_set_tooltip
    
    AC_PaTag.prototype.AF_tname = function(){
        if(this.pav.model){
            return U.AF_shorten_name(this.pav.model.tname_face(FD.AF_lcode()));
        } else {
            return FD.AV_lang.face('M7137'); //"Unknown"
        }
    };// end of AC_PaTag.prototype.AF_tname
    
    // ------------------------------------------------------------------------
    // 1.setup icons for sex/alive/anchor/acticon; 2.arrow icon; 3.anchor-icon
    // ----------
    AC_PaTag.prototype.AF_init_visuals = function(){
        var pa = this.pav.model, 
            AV_love_top, 
            AV_divclass, 
            AV_ls_tag, 
            AV_imgsrc;
        // pa not existing or pa is a dead person
        var dead = !pa || (pa.dod() && pa.dod() !== ''); 
        if(this.pav.sex() === 'male'){
            AV_imgsrc = "pics/male-head.png";
            AV_love_top = U.AV_PAV_H - 6;
            if(dead)AV_divclass = "name-tag-bg dead-male-bg";
            else    AV_divclass = "name-tag-bg male-bg";
        } else {
            AV_imgsrc = "pics/female-head.png";
            AV_love_top = -21;
            if(dead)AV_divclass = "name-tag-bg dead-female-bg";
            else    AV_divclass = "name-tag-bg female-bg";
        }
        var spanclss = 'fullname LS';
        var AV_ls_tag = pa? pa.id() + '@tname_face' : 'M7137'; 
        var msg = '<div class="' +AV_divclass+'"><span class="' +spanclss+'" ';
        msg+='tag="'+AV_ls_tag+'">'+this.AF_tname()+'</span><img class="head" ';
        msg += 'src="' +AV_imgsrc+'"/></div>';
        
        this.AJ_div = $(msg).appendTo(U.AJ_render_pane)
            .attr('title', // mouse-over tooltip - Person: click to read about
                  FD.AV_lang.face('M1521') +      // M1521: person
                  ': '+ FD.AV_lang.face('M6014'));// M6014: click to read about
        this.AJ_love = $('<img />')
             .appendTo(this.AJ_div)
             .css({'top':AV_love_top, 'visibility':'hidden','z-index':'1'});

        var self = this;
        // debug-help on localhost: dbl-click showing pid
        this.AJ_div.dblclick(function (e) {
            if(FD.AV_is_localhost){
                var dbg_msg = 'id:'+self.pav.model.id()+', '+
                    self.pav.id+', top:'+self.AF_top();
                alert(dbg_msg);
            }
        });
        // acticon(the right-arrow): set it to be initially hidden.
        msg = '<img class="pvact" src="pics/right1.png" />';
        this.AJ_acticon=$(msg).css('visibility','hidden').appendTo(this.AJ_div);
        
        // anchor img
        this.AJ_anchor = $('<div class="anchor"></div>').appendTo(this.AJ_div);
        this.AJ_anchor.css('visibility','hidden');

        // adoption img
        var adp_inout = this.pav.AF_adp(), // +/-/false
            clss;
        if(adp_inout){
            clss = (adp_inout === '+')? "adopt-in" : "adopt-out";
            this.AJ_adp=$('<div class="'+clss+'"></div>').appendTo(this.AJ_div);
        }
        return this;
    };// end of AC_PaTag.AF_init_visuals method
    
    AC_PaTag.prototype.AF_set_handlers = function(){
        var self = this;
        // click whole patag body causing focus
        this.AJ_div.unbind('click').bind('click', function(e){ 
            FD.AJ_person_pane.css('box-shadow', 'none');
            self.pav.AF_handle_focus_click().AF_focused(true);
            return U.AF_stop_event(e);
        });
        this.AJ_div.hover(
            function(e){
                if(self.pav.AF_focused() && !self.pav.AF_is_virtual()){
                    self.AJ_div.attr('title','')
                }
            },
            function(e){
                FD.AJ_person_pane.css('box-shadow','none');
                self.AJ_div.attr('title', 
                    FD.AV_lang.face('M1521') +': '+ FD.AV_lang.face('M6014'));
            }
        );
        // click on anchor icon causing setting of becoming anchor
        this.AJ_anchor.unbind('click').bind('click', function(e){
            if(!self.pav.AF_is_anchor()){
                self.pav.AF_become_anchor();
            } else {
                // when pav is anchor, toggle -/+ and exp/shr ancester-root
                if(self.pav.AF_anc_state() === 'expanded'){
                    self.pav.AF_anc_shrink();
                } else {
                    self.pav.AF_anc_expand();
                }
            }
            return U.AF_stop_event(e);
        });
        this.AJ_love.unbind('click').bind('click', function(e){
            if(!self.pav.AF_all_love_expanded()){
                if(self.AJ_love.attr('src').indexOf('locked') > -1){
                    self.AV_pwd_dlg.AF_popup_dlg('M7517'); //spouses
                } else {
                    self.pav.AF_love_expand();
                }
            } else {
                self.pav.AF_love_shrink();
            }
            return U.AF_stop_event(e);
        });
        return this;
    };// end of AC_PaTag.AF_set_handlers method
    
    // act: string of: up/down/delete
    AC_PaTag.prototype.AF_handle_action = function (act) {
        this.AV_act = act;
        this.AV_actbuffer = FD.AV_actbuffer.AF_init();   
        var host, msg, arr, pa = this.pav.model;
        var ind = this.pav.AF_index();//child-index or spouse-index
        if(this.pav.AF_is_anchor()){
            host = this.pav.phost.model;// host is the containing parent-ba
            msg = 'children';
        } else {
            // host is the anchor-pa/ the hosting spouse of this pav
            host = this.pav.mhost.AF_other_pv(this.pav).model;
            //host = this.pav.mhost.model;
            msg = 'spouses';
        }
        iddict = $.extend(true,{}, host.iddict);
        arr = iddict[msg];
        this.AV_new_focus_pid = pa.id();
        switch(act){
        case 'up':   arr.splice(ind-1, 0, arr.splice(ind,1)[0]);   break;
        case 'down': arr.splice(ind+1, 0, arr.splice(ind,1)[0]);   break;
        case 'delete-spouse': // this pav is non-anchor pav, 2b deleted
            //1. remove host(ba)-id from spouses of anchor-pa(host)
            if(ind > -1){
                arr.splice(ind, 1);
            }
            //2. delete the only spouse-ba. if pa does have more than 1
            // spouses-ba existed, no deleting was offered: impossible here
            this.AV_actbuffer.AF_add_delete(pa.iddict.spouses[0]); 

            //3. put pa and his/her parents-ba, into actbuffer.deletes
            this.AV_actbuffer.AF_add_delete(pa);//containees done on serverside
            this.AV_actbuffer.AF_add_delete(pa.iddict.parents[0]);
            this.AV_new_focus_pid = host.id();
            break;
        case 'delete-child':
            var bas = pa.parents();
            if(bas.length == 1){ // pa has only 1 parents-ba: delete pa
                this.AV_actbuffer.AF_add_delete(pa); // delete pa
            } else {// pa has multiple parents-bas(pa is adopted):not delete pa
                // step 1. remove host-ba from pa.parents-bas
                // step 2. remove pa from host.children
                var piddict = $.extend(true, {}, pa.iddict);
                var bindex = // find host's index in pa.parents-list
                    U.AF_trim_adopt_sign(piddict['parents'])
                     .indexOf(host.id());
                if(bindex > -1){ // step 1: cut host-child(pa) bond
                    piddict['parents'].splice(bindex, 1);
                    this.AV_actbuffer.AF_add_update(pa, {'iddict': piddict });
                }
            }
            arr.splice(ind,1); // step 2: remove pa from host.children
            if(host.AF_is_virtual())    // arr must have 1 child remaining
                this.AV_new_focus_pid = arr[0];
            else
                // use parents-ba's anchor-pa for new focus.
                this.AV_new_focus_pid =this.pav.phost.AF_anchor_pv().model.id();
            break;
        default: U.log(1, 'unknown act:'+act); break;
        }// switch
        //update iddict of host(which can be ba(parents-ba)/pa(anchor).
        this.AV_actbuffer.AF_add_update(host, {'iddict': iddict });
        var req_dict = this.AV_actbuffer.AF_get_reqdic(msg+'-'+act);
        this.reqverb = 'putdocs';
        FD.AV_lserver.AF_post('putdocs', req_dict, this);
    };// end of AC_PaTag.AF_handle_action method
    
    AC_PaTag.prototype.AF_handle_err = function(data){
        alert(data);
    };// end of AC_PaTag.AF_handle_err method
    
    AC_PaTag.prototype.AF_handle_success = function(data, self){
        var focus_pid;
        if(self.reqverb === 'go-adp-parent'){
            /*
            var ba = M.e(U.AF_trim_adopt_sign(this.AV_goto_bids[index]));
            // ba:adopting(in or out) parents-ba, cannot be virtual: rootpa exists.
            var AV_rootpa = ba.male()? ba.male() : ba.female();
            */
            FD.AV_path_mgr.AF_new_path(this.pav.model)
                .AF_center_pa(this.pav.model, self.AV_parent_index)
                FD.AV_landing.AF_page().AF_render().AF_update_focus_ppane();
        } else if(self.reqverb === 'putdocs'){
            self.AV_actbuffer.AF_execute();
            var AV_oldpath = FD.AF_tpath();
            FD.AV_path_mgr.AF_clone_new_path(self.AV_new_focus_pid);
            FD.AV_landing.AF_page()
              .AF_delta_render(AV_oldpath).AF_update_focus_ppane();
        } else { // 'verifypwd' done by pwd_dlg.AF_popup_dlg>OK
            self.pav.AF_love_expand();
        }
    };// end of AC_PaTag.AF_handle_success method
    
    // v not given: setup lov-icon, but set it to be hidden
    // v==true: try to make love-icon visible, if !virtual && pa has it
    // v==false: make love-icon hidden
    AC_PaTag.prototype.AF_setlove_icon = function(v){
        // love-icon will be hidden, if
        if(this.pav.AF_is_virtual() || // pav is virtual, or
           !v                       || // v == false, or 
           !this.pav.AF_is_anchor() || // pav is NOT anchor, or
           this.pav.model.iddict['spouses'].length === 0){ // no spouse
             if(this.AJ_love)
                 this.AJ_love.css('visibility','hidden');
             return this;
        }
        var clss = this.pav.sex() + "-love",
            lv_top = this.pav.sex() === 'male'? 18 : -18,
            src, msg = FD.AV_lang.face('M7517') + ': ',   // "spouses:"
            // spouse(s)-access: (true, false, 'locked')
            spouse_acc = this.pav.AF_spouse_expandable(); 
        if(!spouse_acc){ // spouse_ac == false
            this.AJ_love.css('visibility','hidden');
        } else if(spouse_acc === "locked"){
            src = (this.pav.sex() === 'male') ?
                '/pics/locked-mspouse.png' : '/pics/locked-fspouse.png';
            this.AJ_love.attr("src", src);
            this.AJ_love.css('visibility','visible');
            msg += FD.AV_lang.face('M6011'); // click to expand
            this.AJ_love.attr('title', msg); // spouses: click to expand
        } else if(spouse_acc){               // spouse_acc == true
            if(this.pav.AF_all_love_expanded()){
                this.AJ_love.attr("src","/pics/minus1.png")
                    .removeClass(clss).addClass("love-narrow")
                    .css({top: lv_top});
                // reset love-bavs's tail-line
                for(var i=0; i<this.pav.AV_loves.length; i++){
                    var bv = this.pav.AV_loves[i];
                    bv.AF_reset_tailline();
                    bv.AF_sgroup().AF_set_navel_icon(); // reset sgrp-navel-icon
                }
                msg += FD.AV_lang.face('M6012'); // click to fold
                this.AJ_love.attr('title', msg);// spouses: click to fold
            } else { // shrunk, but openable
                src = this.pav.sex()=='male'? 
                    '/pics/openable-mspouse.png' : 
                    '/pics/openable-fspouse.png';
                this.AJ_love.attr("src",src)
                    .removeClass("love-narrow").addClass(clss);
                msg += FD.AV_lang.face('M6011'); // click to expand
                this.AJ_love.attr('title', msg); // spouses: click to expand
            }
            this.AJ_love.css('visibility','visible');
        }
        return this;
    };// end of AC_PaTag.prototype.AF_setlove_icon
    
    AC_PaTag.prototype.AF_set_anchor_visuals = function(anc){
        if(anc){
            if(this.pav.AF_anc_state() === 'expanded'){
                this.AJ_anchor.removeClass().addClass('anchor-minus');
            } else {    // shrunk
                this.AJ_anchor.removeClass().addClass('anchor-plus');
            }
        } else
            this.AJ_anchor.removeClass().addClass('anchor');
    };// end of AC_PaTag.AF_set_anchor_visuals method
    
    AC_PaTag.prototype.AF_set_focus_bg = function(onoff){
        var classes = U.AF_get_pav_bg_classes(this.pav),
           vpav_act = !this.pav.AF_is_virtual() || FD.AV_loginpv;
        if(onoff){ // turn on
            this.AJ_acticon.attr('src',FD.AV_acticon_pic)
                .css('visibility', vpav_act? 'visible':'hidden');
            if(FD.AV_actbav){
                FD.AV_actbav.AJ_acticon.css('visibility','hidden');
                FD.AV_actbav = null;
            }
            this.AJ_div.removeClass(classes['unfocused']);
            this.AJ_div.addClass(classes['focused']);
        } else {  // turn off
            this.AJ_acticon.css('visibility','hidden');
            this.AJ_div.removeClass(classes['focused']);
            this.AJ_div.addClass(classes['unfocused']);
        }
    };// end of AC_PaTag.prototype.AF_set_focus_bg
    
    // ------------------------------------------------------------------------
    // refresh/update the tname, also the alive/dead, from input keys
    // --------------------------------------
    AC_PaTag.prototype.AF_refresh = function(keys){
        if(!keys) keys=['tname','dod','sex'];
        for(var i=0; i< keys.length; i++){
            switch(keys[i]){
            case 'tname':  
                this.AJ_div.find('span.fullname')
                    .text(this.pav.model.tname_face(FD.AF_lcode()));
                break;
            case 'dod':
                var alive_class = (this.pav.model.sex() === 'male')? 
                    "name-tag-bg male-bg": "name-tag-bg female-bg";
                var dead_class = (this.pav.model.sex() === 'male')? 
                    "name-tag-bg dead-male-bg": "name-tag-bg dead-female-bg";
                var alive = this.pav.model.dod() ? false: true;
                this.AJ_div.toggleClass(alive_class, alive)
                         .toggleClass(dead_class, !alive);
                break;
            case 'sex': var img = this.AJ_div.find('img.head');
                if(this.pav.model.sex() === 'male'){
                    img.attr('src','pics/male-head.png');
                    if(this.AJ_div.hasClass("dead-female-bg")){
                        this.AJ_div.removeClass("dead-female-bg");
                        this.AJ_div.addClass("dead-male-bg");
                    } else if(this.AJ_div.hasClass("female-bg")){
                        this.AJ_div.removeClass("female-bg");
                        this.AJ_div.addClass("male-bg");                    
                    }
                } else {
                    img.attr('src','pics/female-head.png');
                    if(this.AJ_div.hasClass("dead-male-bg")){
                        this.AJ_div.removeClass("dead-male-bg");
                        this.AJ_div.addClass("dead-female-bg");
                    } else if(this.AJ_div.hasClass("male-bg")){
                        this.AJ_div.removeClass("male-bg");
                        this.AJ_div.addClass("female-bg");                    
                    }
                }
            case 'dob': break;
            case 'tx': break;
            default: U.log(1, 'patag.AF_refresh key unknown:'+keys[i]);
            }
        }
    };// end of AC_PaTag.prototype.AF_refresh method
    return AC_PaTag;
})();
