﻿/*
 * Copyright (C) 2014 Adacro Inc. - All Rights Reserved
 */
// pavmovel1.js: methods dealing with pav issued requests:
// 1. focus
// 2. love_expansion
// 3. anchor_expansion
// --------------------------
// ------------------------------------------------------------------------
// when name tag clicked, this tag is focused: collect bas/rest ids from pa
// and send server-request if
// person-pane, with
// synop and infos.left/right panes updated. and update FD.AF_focus.
// v: true/false
// --------------------------------------
AC_PAVModel.prototype.AF_handle_focus_click = function(){
    // writing access
    if(this.model){
        this.AV_action = 'focus';
        FD.AV_lserver.AF_pa_focus(this.model, this);
    } else {
        // this pv is virtual
        FD.AV_synop.AF_init(this);
        FD.AF_tpath().AF_focus(this);
    }
    return this; 
};// end of AC_PAVModel.AF_handle_focus_click method

AC_PAVModel.prototype.AF_focused = function(v){
    if(typeof v === 'undefined') // getter
        return this._focused;
    this.AV_patag.AF_setlove_icon(v);
    this._focused = v;
    this.AV_patag.AF_set_focus_bg(v);
    this.AV_patag.AJ_anchor.css('visibility',
            (!this.AF_is_virtual() && v)? 
                'visible' : 
                'hidden');
    if(v){
        FD.AV_synop.AJ_div
          .css('box-shadow', U.AF_get_pav_bg_classes(this)['glow-str']);
        FD.AV_infov.AF_show_state('hidden');
    }
};// end of AC_PAVModel.prototype.AF_focused

// find list-index(position) of this pa among siblings/spouses
AC_PAVModel.prototype.AF_index = function () {
    // for anchor-pa: return pa's child-index
    if (this.AF_is_anchor()){ // is anchor: find sibling-index
        if(!this.phost){
            alert('non-anchor pa has no bv!');
            return -1;
        }
        return this.model.AF_index(this.phost.model);
    } else { // is not anchor:
        if (this.mhost) {
            if (this.sex() === 'male') // this is a male, find the
                // index of my marriage among female-partner's spouse-list
                return this.mhost.model.AF_findex();
            else // this is a female: find the
                // index of my marriage among male-partner's spouse-list
                return this.mhost.model.AF_mindex();
        } else {
            alert('non-anchor pa has no bv!');
            return -1;
        }
    }
};// end of  AC_PAVModel.prototype.AF_index 

AC_PAVModel.prototype.AF_handle_success = function(data, self){
    var ba = M.e(U.AF_trim_adopt_sign(self.model.iddict['parents'][0]));
    if(self.AV_action === 'focus'){
        FD.AV_synop.AF_init(self);
        FD.AF_tpath().AF_focus(self);
    } else if(self.AV_action === 'expand-love' || 
              self.AV_action === 'shrink-love'){
        var AV_oldpath = FD.AF_tpath();
        // clone path with this being the focus pav
        var AV_path = FD.AV_path_mgr.AF_clone_new_path(self.model.id());
        // make sure center-pa-single-parent-expand is set off, if that
        // single parent-ba is amoung this.model.spouses
        var pair = AV_path.AF_centerpa_pair();
        if( pair.parent &&
            this.model.iddict.spouses.indexOf(pair.parent.id()) > -1){
            AV_path.AF_centerpa_pair({parent: null});
        }
        if(self.AV_action === 'expand-love'){
            AV_path.AF_add_expand(self.model); // put pa in expands
        } else {
            AV_path.AF_remove_expand(self.model, true); // propagate: true
            AC_PwdPopup.pwd_cracked = false;
        }
        FD.AV_landing.AF_page()
          .AF_delta_render(AV_oldpath).AF_update_focus_ppane();
        self.AV_patag.AF_set_tooltip();
    } else if(self.AV_action === 'become-anchor'){
        var AV_oldpath = FD.AF_tpath();
        FD.AV_path_mgr.AF_new_path(self.model) // focus = this.model
            .AF_center_pa(this.model);
        FD.AV_landing.AF_page().AF_delta_render(AV_oldpath)
          .AF_update_focus_ppane(); 
    } else if(self.AV_action === 'expand-ancester'){
        var AV_oldpath = FD.AF_tpath();
        var AV_newpath = FD.AV_path_mgr.AF_clone_new_path(this.model.id())
                         .AF_add_expand(ba); // parents-ba added to expands
        if(ba.AF_is_virtual()){
            AV_newpath.AF_root(ba);
        } else {
            ba.male()?  AV_newpath.AF_root(ba.male()) : 
                        AV_newpath.AF_root(ba.female());
        }
        FD.AV_landing.AF_page().AF_delta_render(AV_oldpath)
          .AF_update_focus_ppane();
    } else if(self.AV_action === 'shrink-ancester'){
        // after shrink this.model is the root.
        var AV_oldpath = FD.AF_tpath();
        FD.AV_path_mgr.AF_clone_new_path(this.model.id()) // this in focus
          .AF_root(this.model)
            // remove all ent befor(older than) this gen, or
            // in same gen, but not this.model
          .AF_trim_expands(this, AV_oldpath.AF_vroot());
        FD.AV_landing.AF_page().AF_delta_render(AV_oldpath)
          .AF_update_focus_ppane();
    }
};// end of AC_PAVModel.prototype.AF_handle_success

AC_PAVModel.prototype.AF_anc_expand = function(){
    this.AV_action = "expand-ancester";
    var bs = this.model.parents(); 
    if(bs.length === 1 && bs[0].AF_is_virtual()){
        // the only parents-ba is virtual: no server req to send
        this.AF_handle_success(null, this);
    } else 
        FD.AV_lserver.AF_pa_ancester_exp(this.model, this);
};// end of AC_PAVModel.AF_anc_expand method

AC_PAVModel.prototype.AF_anc_shrink = function(){
    this.AV_action = 'shrink-ancester';
    this.AF_handle_success(null, this)
};// end of AC_PAVModel.AF_anc_shrink method

AC_PAVModel.prototype.AF_become_anchor = function(){
    if(this.AF_is_anchor()) return;
    this.AV_action = 'become-anchor';
    FD.AV_lserver.AF_pa_center(this.model, this);
};// end of AC_PAVModel.prototype.AF_become_anchor

// love-icon was expanded, when love-icon of this pv clicked: 
// shrink all his/her spouses
AC_PAVModel.prototype.AF_love_shrink = function(){
    //if(!this.model || !FD.AV_path_mgr.AF_is_exp(this.model))
    if(!this.model || !this.AF_all_love_expanded())
        return this;
    this.AV_action = 'shrink-love';
    this.AF_handle_success(null, this);
};// end of AC_PAVModel.prototype.AF_love_shrink

AC_PAVModel.prototype.AF_all_love_expanded = function(){
    if(this.AF_is_virtual()) return false;
    return this.model.iddict.spouses.length === this.AV_loves.length;
};// end of AC_PAVModel.prototype.AF_all_love_expanded

// love-icon was not-expanded, and now love-icon clicked: 
// expand all his/her spouses: load all bas, and spouses+children,
// where children, if exist, not expanded (after loaded in store)
AC_PAVModel.prototype.AF_love_expand = function(){
    if(!this.model || this.AF_all_love_expanded()) 
        return this;
    this.AV_action = 'expand-love';
    FD.AV_lserver.AF_pa_love_exp(this.model, this);
};// end of AC_PAVModel.prototype.AF_love_expand

AC_PAVModel.prototype.AF_handle_err = function(data){
    alert(data);
};// end of AC_PAVModel.prototype.AF_handle_err

AC_PAVModel.prototype.AF_update_lclick_items = function(){
    var AV_ctxtmenu = this.AV_patag.AV_ctxtmenu;
    if(!AV_ctxtmenu)
        var debug = 1;
    AV_ctxtmenu.AV_settings.items.length = 0;
    this.AV_patag.AF_update_adpmenu();
    if(!FD.AV_loginpv){
        // if not logged in, offer login cmd
        AV_ctxtmenu.AF_modify_entry({
            label: 'M1001',                 // login
            icon: 'pics/loginicon.png',     // login icon
            func: FD.AV_tbar.AF_login_popup,// login call 
            func_ctx: FD.AV_tbar,           // call context
            arg_array: [this]});
        return this;
    } else { // login pav exists
        // virtual pav offers add-male/female
        if(this.AF_is_virtual()){
            // add(fill-in) male/female to become non-virtual
            var pic = 'pics/girl.jpg', m = 'M7134';
            if(this.sex() === 'male'){
                pic = 'pics/boy.jpg';
                m = 'M7135';
            }
            AV_ctxtmenu.AF_modify_entry({label: m, icon: pic,
                func: this.AV_synopwizard.AF_bind2dlg, 
                func_ctx: this.AV_synopwizard, 
                arg_array: [m, this]});
            return this;
        }

        var spouses = this.model.spouses();
        // child operations: up|down|delete
        if( this.phost &&       // hosting parent-bav exist, and bav is
           (M.OA(this) ||       // this is logged-in, or loginpv is trustee
            M.OA(this.phost))){ // non-virtual: M.OA(father or mother) == true
            // if bav virtual, M.OA(one of child) == true
            
            // add-spouse 
            var pic = this.sex() === 'male' ?         // spouse is of the
                    'pics/girl.jpg' : 'pics/boy.jpg'; // opposite sex
            AV_ctxtmenu.AF_modify_entry({
                label: 'M7132', icon: pic,    // M7132: add-spouse 
                func: this.AV_synopwizard.AF_bind2dlg, 
                func_ctx: this.AV_synopwizard, 
                arg_array: ['M7132', this]});
            // edit personal info
            AV_ctxtmenu.AF_modify_entry({
                label: 'M1500',// M1500: edit-personal info
                icon: 'pics/pencil.jpg',
                func: FD.AV_synop.AV_synopwizard.AF_bind2dlg,
                func_ctx: FD.AV_synop.AV_synopwizard,
                arg_array: ["M7129", FD.AV_synop.pav]}); // M7129: Update
            
            if(this.phost &&                // this is anchor (child pav) 
               this.phost.cid() === 'BAV'){ // and not root pav
                var i, ind=-1, 
                    pavs = this.phost.AF_sgroup().AV_siblings;
                for(i=0; i<pavs.length; i++){
                    if(pavs[i].id === this.id)
                        ind = i;
                }
                if(pavs.length > 1){ // this(pav) is not a single child
                    // up/down button added here
                    if(ind > 0){ // allow up arrow
                        AV_ctxtmenu.AF_modify_entry({
                            label: 'M0003', // M0003: up
                            icon: 'pics/upsmall.png',
                            func: this.AV_patag.AF_handle_action, 
                            func_ctx: this.AV_patag, 
                            arg_array: ['up']});
                    }
                    if(ind > -1 && ind < (pavs.length-1)){ 
                        // pav not last child: allow down arrow
                        AV_ctxtmenu.AF_modify_entry({
                            label: 'M0004', // M0004: down
                            icon: 'pics/downsmall.png',
                            func: this.AV_patag.AF_handle_action, 
                            func_ctx: this.AV_patag, 
                            arg_array: ['down']});
                    }
                }
                // pav has no spouses and no-self-deleting: allow delete
                if(spouses.length === 0 && // no spouse
                FD.AV_loginpv.model.id() !== this.model.id()){ // no self-deleting
                    AV_ctxtmenu.AF_modify_entry({
                        label: 'M0006',  // M0006: Delete
                        icon: 'pics/erase.png',     
                        func: this.AV_patag.AF_handle_action, 
                        func_ctx: this.AV_patag, 
                        arg_array: ['delete-child']});
                }
            }
        }// child operations: up|down|delete
        
        var parents = this.model.parents();
        // non-anchor-spouse operations: up|down|delete
        if(this.mhost &&      // pav is non-anchor pv, hosted by anchor-spouse
           M.OA(this.mhost)){ // mhost is bav thru which this pav is conected
            var AV_anchor_pav = this.mhost.AF_other_pv(this);
            // get the array of ba-ids of anc-pa's spouses
            var bids = AV_anchor_pav.model.iddict.spouses; 
            if(bids.length > 1){ // not the single spouse
                if(bids.indexOf(this.mhost.model.id()) > 0){//up allowed
                    AV_ctxtmenu.AF_modify_entry({
                        label: 'M0003', // M003: up (as spouse)
                        icon: 'pics/upsmall.png',
                        func: this.AV_patag.AF_handle_action, 
                        func_ctx: this.AV_patag, 
                        arg_array: ['up']});
                }
                if(bids.indexOf(this.mhost.model.id()) < (bids.length - 1)){
                    AV_ctxtmenu.AF_modify_entry({
                        label: 'M0004', // M0004: down (as spouse)
                        icon: 'pics/downsmall.png',
                        func: this.AV_patag.AF_handle_action, 
                        func_ctx: this.AV_patag, 
                        arg_array: ['down']});
                }
            }
            /**   delete this as a spouse, only when
             * 1. this the spousal-relationship has no child
             * 2. its (only)parent-ba is virtual and it is the only child there
             * 3. this spousal-relation is the only one this pa has, and
             * 4. this is not the logged-in person (no self-deletion allowed)
             */
            if(this.mhost.model.iddict.children.length === 0 && // condition 1
               parents.length === 1 &&                    // condition 2
               parents[0].iddict.children.length === 1 && // condition 2
               parents[0].AF_is_virtual() &&              // condition 2
               this.model.iddict.spouses.length === 1 &&  // condition 3
               FD.AV_loginpv.model.id() !== this.model.id()){ // condition 4
                AV_ctxtmenu.AF_modify_entry({
                    label: 'M0006', // delete allowed
                    icon: 'pics/erase.png',     
                    func: this.AV_patag.AF_handle_action, 
                    func_ctx: this.AV_patag, 
                    arg_array: ['delete-spouse']});
            }
        }// non-anchor-spouse operations: up|down|delete
        // if this isn't loggedin pav, offer invite
        if(FD.AV_loginpv.model.id() !== this.model.id()){ // pav is not loginpv
            AV_ctxtmenu.AF_modify_entry({label: 'M0007', // invite 
                icon: 'pics/mail.png',  
                func: FD.AV_tbar.AF_invite_popup, 
                func_ctx: FD.AV_tbar, arg_array: [this]});
        } 
    }// end of login pav exists
    return this;
};// end of AC_PAVModel.prototype.AF_update_lclick_items

AC_PAVModel.prototype.AF_filter_rclick_items = function(){
    var AV_noshows = [];
    if(this.AF_is_anchor()){
        // M7529: 'Show blood lineage'
        AV_noshows.push('M7529');// is already anchor: no switch anchor
        if(this.model.iddict['spouses'].length === 0){
            AV_noshows.push('M7525',   // 'Expand spouses'
                         'M7526');  // 'Fold spouses'
        } else if(this.AV_loves.length === 0){ // no love shown
            // spouses already expanded: no expand-spouse action to offer
            AV_noshows.push('M7525');  // 'Expand spouses'
        } else {
            AV_noshows.push('M7526')     // 'Fold spouses'
        }
        if(this.AF_anc_state() === 'expanded'){
            AV_noshows.push('M7528'); // no expand-ancestery action to offer
        } else {
            AV_noshows.push('M7527'); // no shrink-ancestery action to offer
        }
    } else {
        // no-anchor: no expand/shrink-spouse, no exp/shr-ancestery
        AV_noshows.push('M7525','M7526','M7527','M7528');
    }
    return AV_noshows;
};// end of AC_PAVModel.prototype.AF_filter_rclick_items
