/*
 * Copyright (C) 2014 Adacro Inc. - All Rights Reserved
 */
var AC_BigButton = (function(){
    // represent the big buttons in info-pane: families/categories
    // ent:pa family-choices; ent: ip categories
    function AC_BigButton(infov, 
            usage, // 'fa title'|'ip name'|'private'
            ent){  // ent == pa | ip      | null
        this.ent = ent;
        this.AV_infov = infov;
        this.usage = usage || 'fa title';
        this.AJ_div=$('<div class="LS"></div>');
        var self = this;
        var face;
        if(usage === 'ip name'){
            this.AJ_div.attr('id',ent.id()+'_cat');
            face = FD.AV_lang.face(ent.name());
            this.AJ_div.attr({'title':ent.title_face(FD.AF_lcode()),
                            'tag':ent.name()});
        } else if(usage === 'new ip'){
            face = FD.AV_lang.face('M7141'); // 'Add Category'
            this.AJ_div.addClass("add-new-entry");
            this.AJ_div.attr('tag','M7141');
            this.newcat = new AC_CatWizard(this.AV_infov);
        } 
        this.AJ_div.button({ label: face}).bind('click', function(e){
            self.AV_actbuffer = FD.AV_actbuffer.AF_init();
            if(usage === 'new ip'){
                // user clicked "new category" button
                self.newcat.AF_popup('new-ip');
            } else if(self.usage === 'ip name'){
                // switch to item-pane with 500 misec animation speed
                //if(self.AJ_div.attr('disable') !== 'true'){
                self.AV_actbuffer.AF_add_call(
                    self.AV_infov.AF_show_rightpane,
                    self.AV_infov, [self.ent]);
                FD.AV_lserver.AF_get_info(self.ent, self);
            }
            return U.AF_stop_event(e);
        });
    };// end of AC_BigButton constructor

    AC_BigButton.prototype.cid = function(){ return 'BBT'; }
    
    AC_BigButton.prototype.AF_handle_success = function(jdata, self){
        self.AV_actbuffer.AF_execute();
    };// end of AC_BigButton.prototype.AF_handle_success
    
    AC_BigButton.prototype.AF_append2div = function(div){
        this.AJ_div.appendTo(div);
    };// end of AC_BigButton.prototype.AF_append2div
    
    AC_BigButton.prototype.AF_destroy = function(){
        this.AJ_div.remove();
    };// end of AC_BigButton.prototype.AF_destroy
    
    AC_BigButton.AV_cat_catalog = [
        'M7001',        // life chronicle
        'M7002',        // photo albums
        'M7003',        // places to remember
        'M7004',        // collections
        'M7005',        // hobbies
        'M7006',        // readings
        'M7007',        // writings
        'M7008',        // importance people
        'M7009',        // correspondences
        'M7011',        // pets
        'M7012',        // Career 
        'M7013',        // goals and plans
        'M7014',        // accounts
        'M7015',        // names and titles
        'M7016',        // poetry
        'M7017',        // masterpieces
        'M7018',        // body and health
        'M7019',        // thoughts and ideas
        'M7020',        // favorites
        'M7021',        // prides
        'M7143',        // expertise
        'M1516'         // comments from others
    ];
    AC_BigButton.AV_themes = {
        'default': {
            'fpane-bg': 'pics/bg1.jpg',         // leftside graph bg
            'cat-bg': 'pics/redwood-rimmed.jpg',// info-pane bg
            'cont-bg': 'pics/cont-map1.jpg'     // info-pane bg2
        },
        'th1': {
            'fpane-bg': 'pics/bg3a.jpg',
            'cat-bg': 'pics/redwood-rimmed.jpg',
            'cont-bg': 'pics/contbg1.jpg'
        },
        'th2': {
            'fpane-bg': 'pics/th2bg1.jpg',
            'cat-bg': 'pics/th2bg2.jpg',
            'cont-bg': 'pics/th2bg3.jpg'
        },
        'th3': {
            'fpane-bg': 'pics/chn-grid1.jpg',
            //'cat-bg': 'pics/cat-bg1.jpg',
            'cat-bg': 'pics/Louis-Vuitton.jpg',
            'cont-bg': 'pics/cont-bg2.jpg'
        },
        'th4': {
            'fpane-bg': 'pics/french-tile1.jpg',
            'cat-bg': 'pics/cat-bg3.jpg',
            'cont-bg': 'pics/cat-bg2.jpg'
        }
    }
    return AC_BigButton;
})(); // end of class AC_BigButton

