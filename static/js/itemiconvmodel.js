﻿
// this class manages the icon of the item-vmodel.
// itv icon:
var AC_ItemIconVModel = (function(){
    function AC_ItemIconVModel(itvmodel){
        this.itv = itvmodel;
        this.AJ_container = itvmodel.AJ_itembox;
        this.AJ_div = $("<div class='item-icon'></div>")
                    .appendTo(this.AJ_container);
        if(itvmodel.model.type() === 'D'){
            this.AJ_div.css('background-image',
                'url(/pics/doc-icon.png)');
        } else {
            this.jlockicon = $("<img class='keylock' />").appendTo(this.AJ_div);
            this.AF_reset();
        }
    };
    
    AC_ItemIconVModel.prototype.AF_reset = function(){
        if(this.itv.model.type() === 'D') return;
        var iconurl, lockicon;
        if(this.itv.model.pk() === 'M7117'){ // M7117: public
            iconurl = this.itv.AV_state==="shrunk"? 
                'url(/pics/folder-icon.png)' : 
                'url(/pics/folder-open-icon.png)';
        } else if(this.itv.model.pk() === 'M7118'){ // M7118: private
            iconurl = this.itv.AV_state==="shrunk"? 
                'url(/pics/folder-picon.png)' : 
                'url(/pics/folder-open-picon.png)';
        } else if(this.itv.model.pk() === 'M1513'){//
            if(M.OA(this.itv)){ // owner: PP-lock ->blue icons
                iconurl = this.itv.AV_state==="shrunk"? 
                    'url(/pics/folder-ficon.png)' : 
                    'url(/pics/folder-open-ficon.png)';
            } else {  // not owner -> show lock/key-icons on the folder
                iconurl = this.itv.AV_state==="shrunk"? 
                    'url(/pics/folder-icon.png)' : 
                    'url(/pics/folder-open-icon.png)';
            }
        }
        this.AJ_div.css('background-image', iconurl);
        if(this.itv.isLocked){
            if(this.itv.AV_state === "shrunk"){
                lockicon = "/pics/lockIcon.png";
            } else {
                lockicon = "/pics/key.png";
            }
            this.jlockicon.css('visibility','visible');
            this.jlockicon.attr('src', lockicon);
        } else {
            this.jlockicon.css('visibility','hidden');
        }
        return this;
    };// end of AC_ItemIconVModel.prototype.AF_reset
   
    return AC_ItemIconVModel;
})();// end of AC_ItemIconVModel
