﻿/*
 * Copyright (C) 2014 Adacro Inc. - All Rights Reserved
 */
// initial: 2012-12-09 Waterloo, last update: 2014-06-17 Kitchener
// ---------------------------------------------------------------
// store holds all data model instances
// -----------------------------------------------------------------

// store is the model repository on client side
var AC_Store = (function(){
    function AC_Store(fd){
        this.fd = fd;
        this.AV_repos = {};
    };// end of AC_Store constructor

    AC_Store.prototype.AF_repo = function(fid){
        var thefid = fid? fid : this.fd.fid();
        if(!(thefid in this.AV_repos)) 
            this.AV_repos[thefid] = {pool: {}, entdict: {}};
        return this.AV_repos[thefid];
    };// end of AC_Store.prototype.AF_repo
    
    // serialize all ents from entdict (the highst version of every eid)
    AC_Store.prototype.AF_snapshot = function(){
        var ents = [];  // array of ents: a snapshot of current DB
        for(var id in this.pool){ // keys from entdict are ids
            ents.push(this.pool[id]);
        }
        return ents;
    };// end of AC_Store.prototype.AF_snapshot

    // ids can be id(string) or array of ids.
    // if an id in not in store, add it to lst
    // when calling LServer.AF(ids, <act>, ctx), server side will
    // get ents identified by ids
    AC_Store.prototype.AF_add2lst = function(ids, lst){
        if(!lst)lst = [];
        if(!ids) return lst;
        if($.isArray(ids)){
            for(var i=0; i<ids.length; i++){
                this.AF_add2lst(ids[i], lst);
            }
        } else { // ids is a single id-string
            if(!this.AF_id_is_in(ids)){
                if(!$.isArray(lst)){
                    var x = 1;
                    debugger;
                } else 
                if(lst.indexOf(ids) === -1){
                    lst.push(ids);
                }
            } else{
                var ent = M.e(ids);
                if(ent.cid() === 'FC'){
                    this.AF_add2lst(ent.iddict.rs, lst);
                }
            } 
        }
        return lst;
    };// end of AC_Store.prototype.AF_add2lst

    AC_Store.prototype.AF_id_is_in = function(id){
        var repo = this.AF_repo(id.substr(0,7));
        return id in repo.pool;
    };// end of AC_Store.prototype.AF_id_is_in

    // put the entity into store (repo.pool and repo.entdict), 
    // possibly overwriting pre-existing one
    // return: the entity. e can be:
    // 1. js object:will be constructed to be an entity then put into store; 
    // 2. FBase entity (with cid as a function - from FBase): put into store
    AC_Store.prototype.AF_put_ent = function(e){
        if($.isArray(e)){
            var lst = [];
            for(var i=0; i<e.length; i++){
                lst.push(this.AF_put_ent(e[i]));
            }
            return lst;
        } else {
            var AV_ent = e, 
                AV_repo = this.AF_repo(e._id.substr(0,7));
            if(!e.cid){ // e isn't FTBase entity, construct/assign to AV_ent
                AV_ent = eval('new ' + e._id.substr(8,2) + '(e)');
            }
            // save (possibly overwriting) in store
            AV_repo.pool[AV_ent.id()] = AV_ent;
            if(!(AV_ent.cid() in AV_repo.entdict))
                AV_repo.entdict[AV_ent.cid()] = {};
            AV_repo.entdict[AV_ent.cid()][AV_ent.id()] = AV_ent;
            return AV_ent;
        }
    };// end of AC_Store.prototype.AF_put_ent
    
    AC_Store.prototype.AF_load_dbfile = function(datalines){
        for(var i=0; i<datalines.length; i++){
            var line = datalines[i];
            if(line[0] !== '#' && line.trim().length > 0){
                var ent = JSON.parse(line); // json object
                this.AF_put_ent(ent);
            }
        }
    };// end of AC_Store.prototype.AF_load_dbfile
    
    // get a list(array) of ids of all entities of the same cid
    AC_Store.prototype.AF_getEidsByCid = function(cid, fid){
        var _keys = [], repo = this.AF_repo(fid);
        if(repo.entdict[cid])
            _keys = Object.keys(repo.entdict[cid]);
        return _keys;
    };// end of AC_Store.prototype.AF_getEidsByCid method
    
    // get a list(array) of entities of the given cid. Unsorted
    AC_Store.prototype.AF_getEntsByCid = function(cid){
        return this.AF_repo().entdict[cid];
    };// end of AC_Store.prototype.AF_getEntsByCid method
    
    // remove all objects of fid
    AC_Store.prototype.AF_dump_fid = function(fid){
        var repo = this.AF_repo(fid);
        for(var ek in repo.pool){
            if(ek.substr(0,8) === fid){
                delete repo.pool[ek];
            }
        }
        for(var cid in repo.entdict){
            var d = repo.entdict[cid];
            for(var ek in d){
                if(ek.substr(0,7) === fid){
                    delete d[ek];
                }
            }
        }   
    };// end of AAC_Store.prototype.AF_dump_fid
    
    // possible paras: (eid,vid),(_id),(eid)
    // if no vid, get highst vid entity, otherwise, get entity with vid
    AC_Store.prototype.AF_getEntity = function(id){
        var repo = this.AF_repo(M.fid(id));
        if (id in repo.pool){
            return repo.pool[id];
        }
        return null;
    }; // end of AC_Store.prototype.AF_getEntity method
    
    AC_Store.prototype.AF_delEntity = function(id){
        U.log(2, 'deleting '+id);
        var repo = this.AF_repo(id.substr(0,7));
        if(!(id in repo.pool)){
            U.log(2, "delEntity:"+id+" doesnt exist in store.");
        } else {
            delete repo.pool[id];
            var cid = id.substr(8,2);
            delete repo.entdict[cid][id];
        }
        return this;
    };// end of AC_Store.prototype.AF_delEntity method

    // find/return the rs entity with the same sig, null if not found
    // ids is a list of ids from store having been checked
    AC_Store.prototype.AF_find_rs_bysignature = function(sig, ids){
        var repo = this.AF_repo(),
            rslst = repo.AF_getEntsByCid('RS');
        if(ids) ids.length = 0;
        for(var id in rslst){
            if(ids)
                ids.push(id);
            if(rslst[id].signature() === sig) 
                return rslst[id];
        }
        return null;
    };// end of AC_Store.AF_find_rs_bysignature
    
    return AC_Store;
})(); // AC_Store
