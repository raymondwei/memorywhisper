
/**
 * a td is rendered as a td-line, which has title prefixed by expand -|+
 * and a hidden textarea + comment-button
 */
var AC_TDLine = (function(){
    function AC_TDLine(AV_parent,   // hosting obj with .AJ_ul
                       AV_td,       // td entity
                       AV_indlevel, // indentation level index
                       AV_index){   // 0-based index under container
        this.AV_parent = AV_parent;
        this.AV_td = AV_td;
        this.AV_indlevel = AV_indlevel;
        this.AJ_li = $('<li class="forum-li"></li>')
            .appendTo(AV_parent.AJ_ul.css('width','100%'));
        // tdline = titleline + content
        var m = $('<div class="tdline"></div>').appendTo(this.AJ_li);
        this.AJ_tdline = m;
        // titleline: icon + title + followup-box + author
        this.AJ_titleline = $('<div class="tdline-box"></div>').appendTo(m);
        this.AJ_index_number = $('<div class="tdline-index"></div>');
        this.AJ_index_number.text(''+(AV_index + 1))
                .appendTo(this.AJ_titleline);
        this.AJ_title = $('<div class="tdline-title"></div>')
                .appendTo(this.AJ_titleline);
        this.AJ_author = $('<div class="tdline-author"></div>')
                .appendTo(this.AJ_titleline);
        // followup-box
        m = $('<div class="followup-box"></div>');
        this.AJ_feedback = m;
        this.AJ_thup = $('<div class="thumbup"></div>').appendTo(m);
        this.AJ_thupct = $('<span class="thumbup-count"></span>').appendTo(m);
        this.AJ_thdn = $('<div class="thumbdown"></div>').appendTo(m);
        this.AJ_thdnct = $('<span class="thumbdown-count"></span>').appendTo(m);
        this.AJ_fup = $('<div class="followup"></div>').appendTo(m);
        this.AJ_fupct = $('<span class="followup-count"></span>').appendTo(m);
        this.AJ_tdts = $('<span class="td-ISO-ts"></span>').appendTo(m);
        // td text content
        this.AJ_tdtxt = $('<div class="td-content"></div>')
                .appendTo(this.AJ_tdline);

        if(this.AV_indlevel === 0){ // top level tds have followup children
            this.AJ_fblh = $('<div class="feedback-list-head"></div>')
                .appendTo(this.AJ_tdline).css('display','none');
            this.AJ_icon = $('<div class="tdline-icon"></div>')
                .appendTo(this.AJ_fblh);
            $('<span class="LS fblh-text" tag="M7506"></span>')
                .text(FD.AV_lang.face('M7506')).appendTo(this.AJ_fblh);
            // construct ul for containing followup-li's
            this.AJ_ul = $('<ul class="forum-ul"></ul>')
                .appendTo(this.AJ_tdline);
            // initially: followups not expanded
            this.AJ_ul.css('display','none');
            this.AV_childlines = [];
        } else { // this is a followup td
            this.AJ_title.css('left','36px');
            this.AJ_index_number.css('left','12px');
            // td text content
            this.AJ_tdtxt.removeClass('td-content')
                .addClass("td-feedback-text");
            // index number's bg a bit darker
            this.AJ_index_number.css(
                {'background-color':'#333','border': '1px darkred solid',
                  color: "white"});
            // no followup/expand-icon for followup tds
            this.AJ_fup.css('visibility','hidden');
            this.AJ_fupct.css('visibility','hidden');
        }
        this.AJ_tdtxt.css('display','none'); // hide it now
        this.AV_showtxt = false;             // it is no-show
        this.AF_render(); // render title-line info: title
        this.AV_expand = false;// AJ_ul display = none
        this.AV_act = '';
    };// end of AC_TDLine constructor

    AC_TDLine.prototype.AF_render = function(){
        //this.AJ_li.appendTo(this.AV_parent.AJ_ul);
        this.AJ_title.text(this.AV_td.name_face());
        this.AJ_author.text(Base64.decode(this.AV_td.content().author));
        this.AJ_thupct.text(''+this.AV_td.content().thumbup.length);
        this.AJ_thdnct.text(''+this.AV_td.content().thumbdown.length);
        var self = this;
        // level-0 td can have feedback(followups) list
        if(this.AV_indlevel === 0){
            var fupct = this.AV_td.iddict.children.length;
            this.AJ_fupct.text(''+fupct);
            if(fupct > 0){
                this.AJ_fblh.css('display','block');
                this.AJ_icon.css('background-image',
                                 'url(../pics/square-plus.jpg)');
                this.AJ_fblh.unbind('click').bind('click', function(e){
                    self.AF_toggle_expand();
                })
            }
        }
        this.AJ_title.unbind('click').bind('click', function(e){
            self.AF_toggle_tdtxt();
        })
        return this;
    };// end of AC_TDLine.prototype.AF_render

    AC_TDLine.prototype.AF_toggle_tdtxt = function(AV_showtxt){
        if(typeof(AV_showtxt) === 'undefined'){
            this.AF_toggle_tdtxt(!this.AV_showtxt);
        } else {
            if(AV_showtxt){
                this.AJ_tdtxt.html(this.AV_td.AF_content_face());
                this.AJ_tdtxt.css('display','block');
                this.AJ_tdts.text(U.AF_format_ISOtime(this.AV_td.crtimeISO()));
                this.AJ_feedback.appendTo(this.AJ_tdtxt);
                this.AV_actbuffer = FD.AV_actbuffer.AF_init();
                this.AF_feedback_handlers();
            } else {
                this.AJ_tdtxt.css('display','none');
                this.AJ_feedback.detach();
            }
            this.AV_showtxt = AV_showtxt;
        }
    };// end of AC_TDLine.prototype.AF_toggle_tdtxt

    AC_TDLine.prototype.AF_update_thumb = function(upordown, cnt_dic){
        var changed = false,
        clientid = FD.AV_gateip + '#' + FD.AV_localip,
        key = upordown? 'thumbup' : 'thumbdown',    // key of up/down
        okey =  upordown? 'thumbdown' : 'thumbup';  // opposite key
        var ind = cnt_dic[key].indexOf(clientid),   // index of clientid in key
        oind = cnt_dic[okey].indexOf(clientid);     // index of clientid in okey
        if(ind === -1){ // not voted in thumbup/thumbdown
            if(oind !== -1){
                cnt_dic[okey].splice(oind, 1);
            }
            cnt_dic[key].push(clientid);
            changed = true;
            this.AV_act = 'thumb';
        }
        return changed;
    };// end of AC_TDLine.prototype.AF_update_thumb

    AC_TDLine.prototype.AF_save_update = function(content){
        this.AV_actbuffer.AF_add_update(this.AV_td, {content: content});
        var dic = this.AV_actbuffer.AF_get_reqdic('putdocs');
        FD.AV_lserver.AF_post('putdocs', dic, this);
    };// end of AC_TDLine.prototype.AF_save_update

    AC_TDLine.prototype.AF_feedback_handlers = function(){
        var self = this,
        fbdic = $.extend(true, {}, this.AV_td.content()); 
        this.AJ_thup.unbind('click').bind('click', function(e){
            if(self.AF_update_thumb(true, fbdic)){
                self.AF_save_update(fbdic);
            }
        });
        this.AJ_thdn.unbind('click').bind('click', function(e){
            if(self.AF_update_thumb(false, fbdic)){
                self.AF_save_update(fbdic);
            }
        });
        this.AJ_fup.unbind('click').bind('click', function(e){
            self.AV_parent.AV_container_tdl = self;
            self.AV_parent.AF_popup_dlg('M7506'); // "comments"
        });
    };// end of AC_TDLine.prototype.AF_feedback_handlers

    // toggle the feedback child-td list
    AC_TDLine.prototype.AF_toggle_expand = function(AV_expand){
        if(typeof(AV_expand) === 'undefined'){
            this.AF_toggle_expand(!this.AV_expand);
        } else {
            if(AV_expand){
                this.AJ_ul.css('display','inline-block');
                this.AJ_icon.css('background-image',
                                 'url(../pics/square-minus.jpg)');
            } else {
                this.AJ_ul.css('display','none');
                this.AJ_icon.css('background-image',
                                 'url(../pics/square-plus.jpg)');
            }
            this.AV_expand = AV_expand;
        }
    };// end of AC_TDLine.prototype.AF_toggle_expand

    // AV_td given: make a new TDLine(AV_td), push it in AV_childlines
    //   if it is not there already
    // AV_td not given: add all this.AV_td's child-td
    AC_TDLine.prototype.AF_add_comment = function(AV_td){
        var exist = false, i;
        if(AV_td){
            for(i=0; i<this.AV_childlines.length; i++){
                if(this.AV_childlines[i].AV_td.id() === AV_td.id()){
                    exist = true;
                    break;
                }
            }
            if(!exist){
                var ind = this.AV_childlines.length;
                tdl = new AC_TDLine(this, AV_td, 1, ind);
                this.AV_childlines.push(tdl);
            }
        } else {
            this.AV_childlines.length = 0;
            this.AJ_ul.empty();
            for(i=0; i<this.AV_td.iddict.children.length; i++){
                var td = M.e(this.AV_td.iddict.children[i]);
                this.AF_add_comment(td);
            }
        }
        return this;
    };// end of AC_TDLine.prototype.AF_add_comment

    AC_TDLine.prototype.AF_handle_success = function(data, self){
        self.AV_actbuffer.AF_execute();
        if(self.AV_act === 'thumb'){
            self.AJ_thupct.text(''+self.AV_td.content().thumbup.length);
            self.AJ_thdnct.text(''+self.AV_td.content().thumbdown.length);
        } else if(self.AV_act === 'comment'){
            self.AJ_fupct.text(''+self.AV_td.iddict.children.length);
        }

    };// end of AC_TDLine.prototype.AF_handle_success

    return AC_TDLine;
})();