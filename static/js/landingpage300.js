var AC_LandingPage300 = (function(){
    function AC_LandingPage300(fd, jbox, ctls){
        this.fd = fd;
        this.AV_lcode = fd.AF_lcode();
        this.AJ_div = jbox;
        this.AJ_upper = ctls.up // div.lp-showpane
            .css('background-image','url(../pics/giphy1.gif');
        this.AJ_lower = ctls.dn;
        this.AJ_lbtn = ctls.lb.css('visibility','hidden');;
        this.AJ_rbtn = ctls.rb.css('visibility','hidden');;
        this.AJ_vtr = ctls.vc.css('visibility','hidden');
        this.AV_vplayer = new AC_MediaPlayer(this,'video');
        
        // displayed page. child of AJ_upper(div.lp-showpane)
        this.AJ_curr = $('<div class="show-middle"></div>')
            .appendTo(this.AJ_upper); // contained by <div.lp-showpane>
        this.AV_items = [];
        this.AJ_hlighted_item = null;
        this.AJ_item_actimg = $('<img class="lpitem"/>')
            .attr('src','pics/right.png');
    };// end of function AC_LandingPage300 constructor

    AC_LandingPage300.prototype.AF_resize = function(w, h){
        this.AJ_div.css({width: w + 'px', height: h + 'px'});
        this.AJ_upper.css({width: (w - 6) + 'px', height: (h - 60) + 'px'});
        // lower(lp-msgpane) has padding 15px both left/right: take 20 off
        this.AJ_lower.css({width: (w - 6 - 30) + 'px', height: 48 + 'px'});
    };// end of AC_LandingPage300.prototype.AF_resize


    AC_LandingPage300.prototype.AF_show_item = function(ind,ttl_txt){
        // ind is string
        var fcid = 'FTSUPER-FC-landingpagefc300_' + U.zfill(ind,2),
        i=0, self = this, fc = M.e(fcid);
        if(!fc){
            alert('not yet ready');
            return;
        }
        var html = fc.contdict_face(),
            fnames = fc.AF_anno_face().split('||');

        var page = $(html),
            page_title = $('<div class="lp300-steps-pagetitle"></div>')
                .text(ind + ': ' +ttl_txt);
        $('.lp300-steps', page).each(function(ind, jdiv){
            $(this).css('background-image',
                     'url(blobs/FTSUPER/'+fnames[ind]+');')
                .scrollTop(0) ;// scrol to top of the page
        });
        var div = $('<div class="show-right"></div>').appendTo(this.AJ_upper);
        div.append(page_title);        
        div.append(page);
        div.animate({width: '100%'}, 400, function(e){
            self.AJ_curr.detach();
            $(this).removeClass('show-right').addClass('show-middle');
            self.AJ_curr = $(this);
        });
        this.AJ_lbtn.css('visibility','visible').unbind('click').bind('click',
            function(e){
            self.AF_init();
        })
    };// end of AC_LandingPage300.prototype.AF_show_item

    AC_LandingPage300.prototype.AF_set_list_handler = function(){
        var self = this, splt, index,
        tips = this.AV_fc.AF_anno_face().split('||');
        self.AJ_lower.html('');
        $('div.lpitem').unbind('click').bind('click', function(e){
            splt = $(this).attr('id').split('-');
            index = parseInt(splt[splt.length - 1]) - 1;
            self.AJ_hlighted_item.removeClass('lpitem-highlight')
                .addClass('lpitem');
            self.AJ_item_actimg.detach();
            $(this).removeClass('lpitem').addClass('lpitem-highlight');
            $(this).append(
                self.AJ_item_actimg.css({width: '26px',height:'30px'}));
            self.AF_set_list_handler();
            self.AJ_lower.html(tips[index]);
        });
        this.AJ_hlighted_item = $('div.lpitem-highlight');
        this.AJ_hlighted_item.unbind('click').bind('click',function(e){
            self.AF_show_item($(this).attr('id').split('-')[2],
                              self.AJ_hlighted_item.text());
        })

    };// end of AC_LandingPage300.prototype.AF_set_listhandler

    AC_LandingPage300.prototype.AF_init = function(){
        this.AJ_lbtn.css('visibility','hidden');
        var ids = this.fd.AV_store.AF_add2lst([
            'FTSUPER-FC-landingpage_fc300',
            'FTSUPER-FC-landingpagefc300_01',
            'FTSUPER-FC-landingpagefc300_02',
            'FTSUPER-FC-landingpagefc300_03',
            'FTSUPER-FC-landingpagefc300_04',
            'FTSUPER-FC-landingpagefc300_05',
            'FTSUPER-FC-landingpagefc300_06'
        ]);
        this.fd.AV_lserver.AF_getdics_helper(ids, 'get-lp300', this);
    };// end of AC_LandingPage300.prototype.AF_init

    AC_LandingPage300.prototype.AF_handle_success = function(data, self){
        self.AV_fc = M.e('FTSUPER-FC-landingpage_fc300');
        self.AJ_curr.empty();
        if(self.AV_fc){
            var div = $('div.lp-text');
            if(div.length === 0){
                div = $('<div class="lp-text LS"></div>')
                    .attr('tag',self.AV_fc.id() + '@contdict_face');
            }
            div.html(self.AV_fc.contdict_face(self.AV_lcode))
               .appendTo(self.AJ_curr);
               self.AF_set_list_handler();
        }
    };// end of AC_LandingPage300.prototype.AF_handle_success

    return AC_LandingPage300;
})();// end of class AC_LandingPage300

