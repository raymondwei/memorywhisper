/*
 * Copyright (C) 2014 Adacro Inc. - All Rights Reserved
 */
// bavmodel1.js for hosting bavmodel visuals
// ------------------------------------------------------------------------
// setting line-set for lines['m'] and lines['f'] based on current location
// -----------------------------------------
AC_BAVModel.prototype.AF_reset_lines = function(){
    if(this.AF_is_virtual()) return this;
    var anc = this.AF_anchor_pv(),
        AV_mlines = this.AV_lines['m'],
        AV_flines = this.AV_lines['f'];
    if(anc.sex() === 'male' && this.AF_anchor_index() > 0){
        var sl = {x1:isNaN(anc.AF_left())? 0 : anc.AF_left(), 
                  y1:isNaN(anc.AF_top())?  0 : anc.AF_top()+U.AV_PAV_H,
                  x2:isNaN(anc.AF_left())? 0 : anc.AF_left()-11, 
                  y2:anc.AF_top()+U.AV_PAV_H+0.5*U.AV_BAV_H};
        var vl = { x1:isNaN(anc.AF_left())? 0 : anc.AF_left()-11,
                   y1:isNaN(anc.AF_top())?  0 : anc.AF_top()+U.AV_PAV_H+0.5*U.AV_BAV_H,
                   x2:isNaN(anc.AF_left())? 0 : anc.AF_left()-11,
                   y2:isNaN(this.AF_top())? 0 : this.AF_top()};
        var hl = {x1:isNaN(anc.AF_left())? 0 : anc.AF_left()-11, 
                  y1:isNaN(this.AF_top())? 0 : this.AF_top(),
                  x2: isNaN(this.AF_left())? 0 : this.AF_left(), 
                  y2: isNaN(this.AF_top())?  0 : this.AF_top() };
        AV_mlines[0].attr({'x1':sl.x1,'y1':sl.y1,'x2':sl.x2,'y2':sl.y2});
        AV_mlines[1].attr({'x1':vl.x1,'y1':vl.y1,'x2':vl.x2,'y2':vl.y2});
        AV_mlines[2].attr({'x1':hl.x1,'y1':hl.y1,'x2':hl.x2,'y2':hl.y2});
    } else if(anc.sex() === 'female' && this.AF_anchor_index() > 0){
        var sl = { x1:anc.AF_left(), y1:anc.AF_top(),
                   x2:anc.AF_left()-11, y2: anc.AF_top()-0.5*U.AV_BAV_H};
        var vl = { x1:anc.AF_left()-11, y1: anc.AF_top()-0.5*U.AV_BAV_H,
                   x2:anc.AF_left()-11, y2: this.AF_top()+U.AV_BAV_H};
        var hl = { x1:anc.AF_left()-11, y1: this.AF_top()+U.AV_BAV_H,
                   x2: this.AF_left(),  y2: this.AF_top()+U.AV_BAV_H};
        AV_flines[0].attr({'x1':sl.x1,'y1':sl.y1,'x2':sl.x2,'y2':sl.y2});
        AV_flines[1].attr({'x1':vl.x1,'y1':vl.y1,'x2':vl.x2,'y2':vl.y2});
        AV_flines[2].attr({'x1':hl.x1,'y1':hl.y1,'x2':hl.x2,'y2':hl.y2});
    } else {
        AV_mlines[0].attr({'x1':0,'y1':0,'x2':0,'y2':0});
        AV_mlines[1].attr({'x1':0,'y1':0,'x2':0,'y2':0});
        AV_mlines[2].attr({'x1':0,'y1':0,'x2':0,'y2':0});        
        AV_flines[0].attr({'x1':0,'y1':0,'x2':0,'y2':0});
        AV_flines[1].attr({'x1':0,'y1':0,'x2':0,'y2':0});
        AV_flines[2].attr({'x1':0,'y1':0,'x2':0,'y2':0});        
    }
    return this;
};// end of AC_BAVModel.AF_reset_lines method

// lines' positions all take the positions of the lines of bav
AC_BAVModel.prototype.AF_copy_line_positions = function(bav){
    var i;
    for(i=0; i<bav.AV_lines['m'].length; i++){
        this.AV_lines['m'][i].attr({
          'x1':bav.AV_lines['m'][i].attr('x1'),'y1':bav.AV_lines['m'][i].attr('y1'), 
          'x2':bav.AV_lines['m'][i].attr('x2'),'y2':bav.AV_lines['m'][i].attr('y2')});
    }
    for(i=0; i<bav.AV_lines['f'].length; i++){
        this.AV_lines['f'][i].attr({
          'x1':bav.AV_lines['f'][i].attr('x1'),'y1':bav.AV_lines['f'][i].attr('y1'), 
          'x2':bav.AV_lines['f'][i].attr('x2'),'y2':bav.AV_lines['f'][i].attr('y2')});
    }
    if(!bav.AJ_tail)
        this.remove();
    else {
        this.AJ_tail.attr({
            'x1': bav.AJ_tail.attr('x1'), 'y1': bav.AJ_tail.attr('y1'),
            'x2': bav.AJ_tail.attr('x2'), 'y2': bav.AJ_tail.attr('y2')});
        var sg = this.AF_sgroup(), sg1 = bav.AF_sgroup();
        sg.AF_ynavel(sg1.AF_ynavel());
        sg.AJ_navel.css({'top': sg1.AJ_navel.css('top'),
                       'left': sg1.AJ_navel.css('left')});
        sg.AJ_line.attr({
            'x1':sg1.AJ_line.attr('x1'), 'y1': sg1.AJ_line.attr('y1'),
            'x2':sg1.AJ_line.attr('x2'), 'y2': sg1.AJ_line.attr('y2')
        });
    }
    return this;
};// end of AC_BAVModel.prototype.AF_copy_line_positions

AC_BAVModel.prototype.AF_create_lines = function(init){
    this.AV_lines = {m:[],f:[]};   // in array: 0:slant-line, 1:vline, 2:hline
    var lines = this.AV_lines['m'];
    lines[0] = U.AF_drawline({x:0,y:0},{x:0,y:0},this.id+'_msline','bmarker');
    lines[1] = U.AF_drawline({x:0,y:0},{x:0,y:0}, this.id+'_mvline','bmarker');
    lines[2] = U.AF_drawline({x:0,y:0},{x:0,y:0}, this.id+'_mhline','bmarker');
    lines = this.AV_lines['f'];
    lines[0] = U.AF_drawline({x:0,y:0},{x:0,y:0},this.id+'_fsline','bmarker');
    lines[1] = U.AF_drawline({x:0,y:0},{x:0,y:0}, this.id+'_fvline','bmarker');
    lines[2] = U.AF_drawline({x:0,y:0},{x:0,y:0}, this.id+'_fhline','bmarker');
    this.AJ_tail = U.AF_drawline( {x: this.AF_left()+U.AV_BAV_W, y:this.AF_ynavel()},
    {x: this.AF_left()+U.AV_BAV_W, y:this.AF_ynavel()},'tail_tmp_id','marker');
    return this.AF_reset_tailline();
};// end of AC_BAVModel.AF_create_lines method

AC_BAVModel.prototype.AF_showhide_lines = function(shows,hides){ //e.g. ['m'],['f']
    function get_ids(m_or_f,the_id){
        var a=[];
        a[0] = the_id+'_'+m_or_f+'sline'; // this.id cannot be used, for
        a[1] = the_id+'_'+m_or_f+'vline'; // this here isn't bavmodel
        a[2] = the_id+'_'+m_or_f+'hline'; // use the_id for inputting id
        return a;
    };// end of function get_ids
    // showing the lines of m/f (shows[i])
    for(var i=0; i<shows.length; i++){
        var ids = get_ids(shows[i],this.id);
        for(var k=0; k<3; k++){
            if($('#'+ids[k]).length == 0){  // if non-exist on fgpane
                this.AV_lines[shows[i]][k].appendTo(FD.AJ_svg);
            }
        }
    }
    // hiding the lines of m/f (hides[i])
    for(var i=0; i<hides.length; i++){
        for(var k=0; k<3; k++){
            this.AV_lines[hides[i]][k].remove();
        }
    }
};// end of AC_BAVModel.AF_showhide_lines method

AC_BAVModel.prototype.AF_gen = function(g, init){
    if(typeof(g) === 'undefined') return this._gen;
    this._gen = g;
    this.AF_left(this._gen
        .AF_index()*U.AV_GEN_L+U.AV_PAV_STEM_L+U.AV_BAV_XOFFSET);
    return this;
};// end of AC_BAVModel.prototype.AF_gen method

// ------------------------------------------------------------------------
// upper-y of this bv: pv's top - U.AV_PAV_YMIND, 
// namely top pv's upper rim
// ----------------------------------
AC_BAVModel.prototype.AF_upper = function(){
    if(this.AF_anchor_pv().sex() === 'male'){
        return this.AF_top() - this.AF_dist2anchor() - U.AV_PAV_YMIND;
    } else {
        return this.AF_top() - U.AV_PAV_H - U.AV_PAV_YMIND;
    }
};// end of AC_BAVModel.AF_upper method

// ------------------------------------------------------------------------
// lower-y of this bv: lower pv's lower edge + U.AV_PAV_YMIND, namely
// lower pv's lower rim
// -----------------------------------
AC_BAVModel.prototype.AF_lower = function(){
    if(this.AF_anchor_pv().sex() === 'male'){
        return this.AF_top()+U.AV_BAV_H+U.AV_PAV_H+U.AV_PAV_YMIND;
    } else {
        return this.AF_top()+this.AF_dist2anchor()+U.AV_PAV_H+U.AV_PAV_YMIND;
    }
};// end of AC_BAVModel.AF_lower method

AC_BAVModel.prototype.AF_top = function(y, AV_animated, AF_animate_callback){
    // reading access: getter
    if(typeof(y) === 'undefined') 
        return U.INT(this.AJ_div.css('top'));
    // setter
    if(AV_animated){
        this.AJ_div.animate({top: y}, U.AV_animate_duration, AF_animate_callback);
    } else {
        this.AJ_div.css('top', y);    // set top of the ring-picture symbol
    }
    return this.AF_reset_tailline();
};// end of AC_BAVModel.AF_top method

AC_BAVModel.prototype.AF_reset_tailline = function(){
    if(!this.AJ_tail) return this;
    // y-coordinate for jtail
    var liney = isNaN(this.AF_top())? 0 : this.AF_top()+0.5*U.AV_BAV_H,
        ba = this.model,
        length = (ba.iddict.children.length > 0 && this.AF_chpermit())? 
                U.AV_BAV_TAIL_LEN : 0;
    if(isNaN(this.AF_left()))
        this.AJ_tail.attr({x1: 0, y1:0, x2:0, y2: 0});
    else
        this.AJ_tail.attr({x1:this.AF_left() + U.AV_BAV_W, y1: liney,
                    x2:this.AF_left() + U.AV_BAV_W + length, y2: liney});
    if(this.AF_sgroup())
        this.AF_sgroup().AF_ynavel(liney);
    return this;
};// end of AC_BAVModel.AF_reset_tailline method

AC_BAVModel.prototype.AF_anchor_index = function(AV_index){
    if(typeof(AV_index) === 'undefined')
        return this._aindex;
    this._aindex = AV_index;
    return this;
};//end of AC_BAVModel.prototype.AF_anchor_index

// ymove visuals(div+lines), if sgrp exists, move that too
AC_BAVModel.prototype.AF_ymove = function( delta, meonly ){
    this.AF_top(this.AF_top() + delta);
    if(!meonly){
        this.AF_sgroup().AF_ymove(delta);
    }
    this.AF_reset_lines();
};// end of AC_BAVModel.AF_ymove method

AC_BAVModel.prototype.AF_left = function(x, AV_animated, AF_animate_callback){
    // getter
    if(typeof(x) === 'undefined') 
        return U.INT(this.AJ_div.css('left'));
    // setter
    if(AV_animated){
        this.AJ_div.animate({left: x}, U.AV_animate_duration, AF_animate_callback);
    } else {
        this.AJ_div.css('left', x);
    }
    return this.AF_reset_lines().AF_reset_tailline();
};// end of AC_BAVModel.AF_left method
    
AC_BAVModel.prototype.AF_removeme = function(){
    this.AJ_div.remove();
    this.AJ_tail.remove();
    this.AF_non_anchor_pv().AF_removeme();
    this.AF_showhide_lines([],         // shows: none;
                        ['m','f']); // hides: remove lines for m and f
    this.AF_sgroup().AF_removeme();
    return this;
};// end of AC_BAVModel.AF_removeme method

// ------------------------------------------------------------------------
// calc distance(in pixels) from anchpr pv.AF_top() tp bv.AF_top()
// --------------------------------------------------
AC_BAVModel.prototype.AF_dist2anchor = function(){
    return U.AF_male_anchortop2bv_top(this.AF_anchor_index());
};// end of AC_BAVModel.AF_dist2anchor method

    
AC_BAVModel.prototype.AF_setup_handlers = function(){
    var pv, self = this;
    this.AJ_div.unbind('dblclick').bind('dblclick', function (e) {
        if (FD.AV_is_localhost) {
            var dbg_msg = 'id:' + self.model.id() + ', ' +
                    self.id + ', top:' + self.AF_top();
            alert(dbg_msg);
        }
    });
    this.AJ_div.unbind('click').bind('click', function(e){
        pv = FD.AF_tpath().AF_focus(); // current focus pav
        if(M.OA(self) && pv === self.AF_non_anchor_pv()){
            // take focus away from prev-focus-pav
            pv.AV_patag.AJ_acticon.css('visibility','hidden');
            FD.AV_actbav = self;
            self.AJ_acticon.css('visibility','visible');
        }
    });
};// end of AC_BAVModel.AF_setup_handlers method

// --------------------------------------------------------------------
// test if bv is below this(bv), below here means y-coord bigger value
// if this/bv are not under the same anchor(married to the same pa),
// goes back(gen-index --), till: a) 2 bv-anchors are siblings, or b)
// 2 bvs are married to the same anchor-pa. In case of a), return 
// index1<index2; in case of b), if anchor pa is male: return index1<index2
// if anchor-pa is female, return index1>index2
// ------------------------------------------
AC_BAVModel.prototype.AF_below_me = function(bv){
    if(this.AF_is_virtual() || bv.AF_is_virtual()){
        alert('judge blowme for virtual bv');
        return false;
    }
    var pv = bv.AF_anchor_pv();
    if(pv === this.AF_anchor_pv()){
        // bv and this are the marriages of the same anchor-pv
        var ind0 = this.AF_anchor_index();
        var ind = bv.AF_anchor_index();
        return pv.sex()==='male'? ind0 < ind : ind0 > ind;
    } else {
        var pv0 = this.AF_anchor_pv();
        var pv = bv.AF_anchor_pv();
        
        var bv0 = pv0.phost;
        var bv1 = pv.phost;
        if(bv0 === bv1){ // share the same parents:
            var ind = bv1.model.iddict['children'].indexOf(pv0.model.id());
            var index = bv1.model.iddict['children'].indexOf(pv.model.id());
            return ind < index;
        } else 
            return bv0.AF_below_me(bv1);
    }
};// end of AC_BAVModel.prototype.AF_below_me
