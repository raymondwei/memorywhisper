﻿var AC_MergeWizard = (function(){
    function AC_MergeWizard(fd){
        this.pv = fd.AV_loginpv;
        this.pa = this.pv.model;
        this.fd = fd;
        this.AF_load_template();
        this.AF_init();
    };// end of AC_MergeWizard constructor
    
    AC_MergeWizard.prototype.AF_load_template = function(){
        this.AJ_layout = $('<div id="merge-dlg"></div>');
        this.AJ_head = $('<div id="head-section"></div>')
            .appendTo(this.AJ_layout);
        var rgroup = $('<div class="head-radio-group"></div>')
            .appendTo(this.AJ_head);
        this.jp0r1 = 
          $('<input type="radio" class="radio1" name="reason" value="same" />')
            .appendTo(rgroup);
        this.jp0r2 =
          $('<input type="radio" class="radio2" name="reason" value="out" />')
            .appendTo(rgroup);
        this.jp0r3 =
          $('<input type="radio" class="radio3" name="reason" value="in" />')
            .appendTo(rgroup);
        $('<span class="LS no-adoption-label" tag="M6006">No adoption</span>')
            .appendTo(rgroup);
        $('<span class="LS adopted-out-label" tag="M6007">Adopted out</span>')
            .appendTo(rgroup);
        $('<span class="LS adopted-in-label" tag="M6008">Adopted in</span>')
            .appendTo(rgroup);
        this.jp0div = $('<div id="head-pa"></div>').appendTo(this.AJ_head);
        this.jsrch_name = $('<input type="text" id="merge-search-input"/>')
            .appendTo(this.AJ_head);
        this.srch_btn = 
            $('<img id="merge-search-button" src="pics/magnifier0.png"/>')
            .appendTo(this.AJ_head);
        var moffering = $('<div id="merge-offerings"></div')
            .appendTo(this.AJ_layout);
        this.AJ_ul = $('<ul id="merge-pa-list"></ul>').appendTo(moffering);
        this.AJ_info = $('<div id="merge-pa-info"></div>');
        $('<span id="merge-pa-tname" />').appendTo(this.AJ_info);
        $('<span id="merge-pa-dates" />').appendTo(this.AJ_info);
        $('<span id="merge-pa-pob" />').appendTo(this.AJ_info);
    };// end of AC_MergeWizard.prototype.AF_load_template
    
    AC_MergeWizard.prototype.AF_init = function(){
        this.highli = null;
        // set visuals of head-person
        var pamsg = "<span class='fullname'>"+this.pa.tname_face()+"</span>";
        $(pamsg).appendTo(this.jp0div);
        var src = this.pa.sex() === 'male'? 
            "pics/male-head.png" : "pics/female-head.png";
        $("<img class='head' src='"+src+"'/>").appendTo(this.jp0div);
        var self = this;
        // search button click event hadler
        this.srch_btn.unbind('click').bind('click', function(e){
            var thename = $.trim(self.jsrch_name.val());
            if(thename.length > 0){
                var locals = self.AF_find_local_candidates(thename);
                var ar = [];
                for(var k in locals)
                    ar.push(locals[k])
                if(ar.length > 0)
                    self.AF_populate(ar);
                self.reqverb = 'search';
                var req_dict = {name: thename, lcode: FD.AF_lcode()};
                req_dict['excls'] = JSON.stringify(FD.AV_store.AF_getEidsByCid(
                                        'PA',self.pa.fid()));
                FD.AV_lserver.AF_post(self.reqverb, req_dict, self);
                
            }
        });
    };// end of AC_MergeWizard.prototype.AF_init
    
    // find pas in local store: the same sex pas
    AC_MergeWizard.prototype.AF_find_local_candidates = function(tname){
        var pas = this.fd.AV_store.AF_getEntsByCid('PA');
        var res = {};
        for(var id in pas){
            if( pas[id].fid()===this.pa.fid()&&pas[id].sex()===this.pa.sex()&&
                pas[id] !== this.pa && !(id in res)){
                if(tname){
                    if(JSON.stringify(pas[id].tname()).indexOf(tname) > -1)
                        res[id] = pas[id];
                } else {
                    res[id] = pas[id];
                }
            }
        }
        return res;
    };// end of AC_MergeWizard.prototype.AF_find_local_candidates
    
    // data: {news:[<ent>,<ent>,..]} is a list of pa entities
    AC_MergeWizard.prototype.AF_populate = function(data, concat_list){
        if(!concat_list){
            this.pavs = [];
            this.AJ_ul.empty();
        }
        if(!data.ents){
            var locals = this.AF_find_local_candidates();
            for(var id in locals){
                this.pavs.push(new AC_MergeCandidate(this, locals[id], this.AJ_ul));
            }
        } else {
            var i, lst = data.ents? data.ents : [];
            for(i=0; i<lst.length; i++){
                this.pavs.push(new AC_MergeCandidate(this, lst[i], this.AJ_ul));
            }
        }
    }; // end of AC_MergeWizard.prototype.AF_populate
    
    // merge this.pa with p. this.pa is dominent side.
    AC_MergeWizard.prototype.AF_merge = function(highlight){
        //this.op = "same"; // in, out
        this.op = $('input[name="reason"]:checked').val();
        this.reqverb = 'merge';
        var hi_op = $('input[name="reason1"]:checked').val();
        var req_dict = {p0id: this.pa.id(), p1id: highlight.pa.id(), 
                        op0: this.op, op1: hi_op};
        this.fd.AV_lserver.AF_post(this.reqverb, req_dict, this);
        U.log(2, "merging "+this.pa.tname_face()+" with "+
              highlight.pa.tname_face());
    };// end of AC_MergeWizard.prototype.AF_merge
    
    AC_MergeWizard.prototype.AF_popup = function(){
        var self = this;
        FD.AV_lang.AF_set_all(this.AJ_layout, this.fd.AF_lcode());
        self.dlg = self.AJ_layout.dialog({
            title: self.fd.AV_lang.face('M1519'), // merge
            position: {my: "top", at:"right"},
            autoOpen: false,
            width: 380,
            height: 720,
            modal: false, // bg inactive; inter-action with bg.
            resizable: false,
            show: true,
            hide: true,
            close: function(e){
            },
            open: function(e){
                var adp = self.pv.AF_adp();
                if(!adp){
                    self.jp0r1.prop('checked', true);
                } else if(adp === '-'){
                    self.jp0r2.prop('checked', true);
                } else {
                    self.jp0r3.prop('checked', true);
                }
                self.AF_populate();
            },
        });
        self.dlg.dialog('open');
    };// end of AC_MergeWizard.prototype.AF_popup
    
    AC_MergeWizard.prototype.AF_handle_err = function(data, self){
        if(self.reqverb === 'merge'){
            U.log(1, "merge err:" + data);
        } else if(self.reqverb === 'getdics'){
            U.log(1, "getdics failed.");
        }
    };// end of AC_MergeWizard.prototype.AF_handle_err
    
    AC_MergeWizard.prototype.AF_handle_success = function(data, self){
        if(self.reqverb === 'getdics'){ // high-lighting a pa in ul-list
            var AV_oldpath = FD.AF_tpath();
            self.fd.AV_path_mgr.AF_chop_from_ci()
            self.fd.AV_path_mgr.AF_new_path(self.highli.pa)
                .AF_center_pa(self.highli.pa);
            self.fd.AV_landing.AF_page().AF_delta_render(AV_oldpath);
        } else if(self.reqverb === 'search'){ // found pas with name-input
            self.AF_populate(data, true);
        } else if(self.reqverb === 'merge'){
            FD.AV_cfg.mergewizard.dlg.dialog('close');
            window.location.reload();
        }
    };// end of AC_MergeWizard.prototype.AF_handle_success
    
    return AC_MergeWizard;
})();

var AC_MergeCandidate = (function(){
    function AC_MergeCandidate(host, pa, jul){
        this.host = host;
        this.pa = pa;
        this.AJ_ul = jul;
        this.AJ_li = $("<li class='offer-pa'></li>").appendTo(jul);
        this.AV_state = "shrunk";
        this.jpa = $("<div class='pa-tag'></div>").appendTo(this.AJ_li);
        var pamsg = "<span class='fullname'>"+pa.tname_face()+"</span>";
        $(pamsg).appendTo(this.jpa);
        var src = pa.sex() === 'male'? 
            "pics/male-head.png" : "pics/female-head.png";
        $("<img class='head' src='"+src+"'/>").appendTo(this.jpa);
        var self = this;
        self.AJ_li.unbind('click').bind('click', function(e){
            if(self.AV_state === 'shrunk'){
                self.AF_highlight(true) // set high-light of this.pa, and rerender
                    .AF_render_tree();  // family tree with this.pa as centerpa
            }
        });
    };// end of AC_MergeCandidate constructor
    
    // on the fgraph-pane, render a family tree centered with this.pa
    AC_MergeCandidate.prototype.AF_render_tree = function(){
        var ids = [];
        AC_ActionBuffer.AF_focus_ids(this.pa, ids);
        this.host.reqverb = 'getdics';
        this.fd.AV_lserver.AF_getdics_helper(ids, 'getdics', this);
        return this;
    };// end of AC_MergeCandidate.prototype.AF_render_tree
    
    AC_MergeCandidate.prototype.AF_highlight = function(onoff){
        if(onoff){
            if(this.host.highli)
                this.host.highli.AF_highlight(false);
            this.AJ_li.removeClass('offer-pa').addClass("highlighted-pa");
            this.host.highli = this;
            this.AV_state = "expanded";
            this.AF_attach_actors();
        } else {
            this.AJ_li.removeClass("highlighted-pa").addClass('offer-pa');
            this.AV_state = "shrunk";
            this.AF_detach_actors();
        }
        return this;
    };// end of AC_MergeCandidate.prototype.AF_highlight
    
    AC_MergeCandidate.prototype.AF_attach_actors = function(){
        var self = this;
        if(!this.jgrp){
            var msg = "<div class='head-radio-group'></div>";
            this.jgrp = $(msg).appendTo(this.AJ_li).css('left','185px');
            
            var m = "<input type='radio' class='CL' name='reason1'/>";
            this.r1 = $(m.replace('CL','radio1')).appendTo(this.jgrp)
                .attr({'value':'same','checked':false});
            this.r2 = $(m.replace('CL','radio2')).appendTo(this.jgrp)
                .attr({'value':'out','checked':false});
            this.r3 = $(m.replace('CL','radio3')).appendTo(this.jgrp)
                .attr({'value':'in','checked':false});
            var opchar = self.pa.iddict['parents'][0].slice(-1);
            if(opchar === '+'){
                this.r3.prop('checked',true);
            } else if(opchar === '-'){
                this.r2.prop('checked',true);
            } else {
                this.r1.prop('checked',true);
            }

            m = "<span class='LS CL' tag='MX'>MY</span>";
            var s = m.replace('CL','no-adoption-label').replace('MX','M6006')
                     .replace('MY',FD.AV_lang.face('M6006'));
            $(s).appendTo(this.jgrp).css('color','white');
            
            s = m.replace('CL','adopted-out-label').replace('MX','M6007')
                 .replace('MY',FD.AV_lang.face('M6007'));
            $(s).appendTo(this.jgrp).css('color','white');
            
            s = m.replace('CL','adopted-in-label').replace('MX','M6008')
                 .replace('MY',FD.AV_lang.face('M6008'));
            $(s).appendTo(this.jgrp).css('color','white');
            
            this.merge_btn = $("<img class='merge-actor' />")
                .appendTo(this.jpa).attr('src','pics/settings.png');
            this.merge_btn.unbind('click').bind('click', function(e){
                self.host.AF_merge(self);
            });
        } else {
            this.jgrp.appendTo(this.AJ_li).css('left','185px');
            this.merge_btn.appendTo(this.jpa);
        }
    };// end of AC_MergeCandidate.prototype.AF_attach_actors
    
    AC_MergeCandidate.prototype.AF_detach_actors = function(){
        if(this.jgrp){
            this.jgrp.detach();
            this.merge_btn.detach();
        }
    };// end of AC_MergeCandidate.prototype.AF_detach_actors
    
    return AC_MergeCandidate;
})();
