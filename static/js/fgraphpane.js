/*
 * Copyright (C) 2014 Adacro Inc. - All Rights Reserved
 */
var AC_FGraphPane = (function AC_FGraphPane(){
    function AC_FGraphPane(fd, jftgraph){    // id of div which is the container
        this.AJ_div = jftgraph;
        var m = '<svg xmlns="http://www.w3.org/2000/svg" id="the-svg" ' +
                'version="1.1"></svg>';
        this.AJ_svg = $(m).appendTo(this.AJ_div);
        U.AJ_render_pane = jftgraph;
        U.AJ_render_svg = this.AJ_svg;
        this.AV_curr_height = 0;
        this.AV_curr_width = 0;
        this.fd = fd;
        this.AV_render_engine = new AC_RenderEngine(this);
        this.AF_init_gens();
    }; // end of AC_FGraphPane constructor
    
    // used in AC_FGraphPane.prototype.AF_delta_render:
    // 1.step, act='copy', clone jftgraph to jrender_pane, starting delta-render
    // 2.last step in delta-render/AC_FGraphPane.animated_creates act='flip'
    AC_FGraphPane.prototype.AF_swap_render_pane = function(act){
        // clear the slate, for both start/end
        AC_FGraphPane.AV_callbacks.length = 0; 
        if(act === 'copy'){
            U.AJ_render_pane = this.AJ_div.clone();
            U.AJ_render_svg = this.AJ_svg.clone();
            U.AJ_render_pane.empty(); // just take over css
            U.AJ_render_svg.empty(); // just take over css
            U.AJ_render_svg.appendTo(U.AJ_render_pane);// set relation
            // set pane enough height to contain all stuff drawn on it.
            // at the end of rendering, the height will be set to fitting
            U.AJ_render_pane.css('height', U.AV_MAX_FGRAPH_HEIGHT + 'px');
        } else if(act === 'flip') {
            var AJ_tab1 = $('#tab1');
            this.AJ_div.remove();
            this.AJ_div = U.AJ_render_pane;
            U.AJ_render_pane.appendTo(AJ_tab1);
            this.AJ_div.css('height', this.AV_curr_height);
            this.AJ_div.css('width', this.AV_curr_width);
        }

    };// end of AC_FGraphPane.prototype.AF_swap_render_pane

    AC_FGraphPane.prototype.AF_init_gens = function(){
        this.AV_gens = []; // a gen is a col on fgraphpane: a generation
        for(var i=0; i<5; i++){ //by default, 5 gens
            this.AV_gens[i] = new AC_GenColumn(i, this);
        }
    };// end of AC_FGraphPane.AF_init_gens method
    
    // last(highst index) gen that's not empty
    AC_FGraphPane.prototype.AF_lastactive_gen = function(){
        var i;
        for(i=0; i<this.AV_gens.length; i++){
            if(this.AV_gens[i].AV_blocks.length === 0) break;
        }
        return this.AV_gens[i-1];
    };// end of AC_FGraphPane.AF_lastactive_gen method
    
    // set pa in focus, render fd.AF_tpath - tpath has now root, which is
    // pa's father or mother; if parents-ba(pba) is virtual, root == pba
    AC_FGraphPane.prototype.AF_render = function (keep){
        if(!keep){
            this.AF_clear_slate();
        }
        this.AF_init_gens();    // clear/recreate 5 gen-cols
        this.AV_render_engine.AF_expand();
        this.AV_render_engine.AF_arrange_position();
        
        // extend fgraph div's dimension if necessary
        this.AV_render_engine.AF_adjust_panesize();
        return this;
    };// end of AC_FGraphPane.AF_render method
    
    AC_FGraphPane.prototype.AF_update_focus_ppane = function(){
        var pav = FD.AF_tpath().AF_focus();
        FD.AV_synop.AF_init(pav);
        return this;
    };// end of AC_FGraphPane.prototype.AF_update_focus_ppane
    
    AC_FGraphPane.prototype.AF_clear_slate = function(){
        for(var i=0; i<this.AV_gens.length; i++){
            this.AV_gens[i].AF_removeme();
        }
        return this;
    };// end of AC_FGraphPane.AF_clear_slate method
    
    //-------------------------------------------------------------------------
    // About animated delta-rendering
    //-------------------------------------------------------------------------
    // each array-ele: [<call-function>, <call-context>, [<param-array>]]
    AC_FGraphPane.AV_callbacks = [];
    
    AC_FGraphPane.AF_fire_callbacks = function(){
        for(var i=0; i<AC_FGraphPane.AV_callbacks.length; i++){
            AC_FGraphPane.AV_callbacks[i][0]                   // callback func
                      .apply(AC_FGraphPane.AV_callbacks[i][1], // func-context
                             AC_FGraphPane.AV_callbacks[i][2]);// call params
        }
        // first thing the callback should do:
        // AC_FGraphPane.AV_callbacks.length = 0;
    };// end of AC_FGraphPane.AF_fire_callbacks
    
    // do animated  - 1.fading out. 2.removed from DOM(callback-1),
    // 3.call AC_FGraphPane.AF_animated_moves(AV_moves, created)(callback-2)
    AC_FGraphPane.AF_animated_deletes = function(AV_deletes, AV_moves, AV_creates){
        var ar =[], i, j;
        for(i=0; i< AV_deletes.length; i++){
            if(AV_deletes[i].cid() === 'PAV'){
                ar.push(AV_deletes[i].AV_patag.AJ_div);
                ar.push(AV_deletes[i].AJ_stem);
            } else if(AV_deletes[i].cid() === 'BAV'){
                ar.push(AV_deletes[i].AJ_div);
                for(j=0; j<AV_deletes[i].AV_lines['m'].length; j++)
                    ar.push(AV_deletes[i].AV_lines['m'][j]);
                for(j=0; j<AV_deletes[i].AV_lines['f'].length; j++)
                    ar.push(AV_deletes[i].AV_lines['f'][j]);
            }
        }
        if(ar.length > 0){
            AC_FGraphPane.AV_callbacks.push(
                [AC_FGraphPane.AF_animated_moves,AC_FGraphPane,
                 [AV_moves, AV_creates]]);
            for(i=0; i<ar.length; i++){
                ar[i].fadeOut(U.AV_animate_duration, function(e){
                    $(this).remove(); // remove the element from DOM
                    AC_FGraphPane.AF_fire_callbacks();
                });
            }
        } else {
            AC_FGraphPane.AF_animated_moves(AV_moves, AV_creates);
        }
    };// end of AC_FGraphPane.AF_animated_deletes
    
    // append all elements to U.AJ_render_pane, with opacity = 0
    // animate opacity to 1 with duration. as Callback: 
    // AC_FGraphPane.AF_animated_creates(AV_creates)
    AC_FGraphPane.AF_animated_moves = function(AV_moves, AV_creates){
        AC_FGraphPane.AV_callbacks.length = 0;
        AC_FGraphPane.AV_callbacks.push(
                [AC_FGraphPane.AF_animated_creates,AC_FGraphPane,[AV_creates]]);
        if(AV_moves.length > 0){
            for(var i=0; i< AV_moves.length; i++){
                var vm = AV_moves[i][0];
                vm.AF_set_position(AV_moves[i][1], true, null );
                if (vm.cid() === 'BAV') {
                    // BAV reset-lines called synchronously
                    vm.AF_copy_line_positions(AV_moves[i][2]);
                }
            }
        } 
        AC_FGraphPane.AF_fire_callbacks()
    };// end of AC_FGraphPane.AF_animated_moves 

    AC_FGraphPane.AF_animated_creates = function(AV_creates){
        var i, e, ele, ar = [];
        AC_FGraphPane.AV_callbacks.length = 0;
        for(i=0; i< AV_creates.length; i++){
            e = AV_creates[i];
            if(e.cid() === 'PAV'){
                //ele = e.AV_patag.AJ_div.clone(); clone would lose handlers
                ele = e.AV_patag.AJ_div;
                ele.css('display','none');
                ar.push(ele);
                ele.appendTo(U.AJ_render_pane);
            } else if(e.cid() === 'BAV'){
                //ele = e.AJ_div.clone(); clone would lose handlers
                ele = e.AJ_div;
                ele.css('display','none');
                ar.push(ele);
                ele.appendTo(U.AJ_render_pane);
            }
        }
        if(ar.length > 0){
            AC_FGraphPane.AV_callbacks.push(
                [FD.AV_landing.AF_page().AF_swap_render_pane, 
                    FD.AV_landing.AF_page(), ["flip"]]);
            for(i=0; i<ar.length; i++){
                if(i < (ar.length-1))
                    ar[i].fadeIn(U.AV_animate_duration);
                else
                    // when last, put animation-finish-callback: 
                    ar[i].fadeIn(U.AV_animate_duration, function(){
                        AC_FGraphPane.AF_fire_callbacks();
                    });
            }
        } else {
            FD.AV_landing.AF_page().AF_swap_render_pane('flip');
        }
    };// end of AC_FGraphPane.AF_animated_creates 
    
    // render graph on f-pane. If AV_base_path given, do animation:
    // transition from old path to new path.
    AC_FGraphPane.prototype.AF_delta_render = function(AV_base_path){
        if(!AV_base_path){   // no old path: render from scratch
            this.AF_render();// clear f-pane(keep != true)
        } else {
            // flip will be done as last call back in animated_creates
            this.AF_swap_render_pane('copy');
            this.AF_render(true); // keep visuals not removed from DOM
            var AV_deltas = AC_TPath.AF_find_deltas(AV_base_path, FD.AF_tpath());
            // start the head 1.AV_deletes. 2.AV_moves and
            // 3.AV_creates are chained behind
            AC_FGraphPane.AF_animated_deletes(
                AV_deltas.AV_deletes, AV_deltas.AV_moves, AV_deltas.AV_creates);
        }
        return this;
    };// end of AC_FGraphPane.prototype.AF_delta_render
    
    return AC_FGraphPane;
})();
