/*
 * Copyright (C) 2014 Adacro Inc. - All Rights Reserved
 */
var AC_FCVModel = (function () {
    function AC_FCVModel(docpane, facet, newfc){
        this.AV_docpane = docpane;
        this.AV_lcode = docpane.AV_lcode;
        this.fext = facet.type();
        this.AV_actbuffer = this.AV_docpane.AV_actbuffer;
        this.rsupdate = false;
        this.model = facet;
        facet.vmodel = this;
        // text-area to be attached/detached to docpane.AJ_lower
        this.janno = $('div#doc-lower-text-area');
        this.newfc = newfc;
        this.AV_bigshow = docpane.AV_bigshow;
        this.AF_init_listbox();
        if(!this.newfc){ // if not new fc, setup a change dict in 
            this.AF_init(); // actbuffer.updates, referred by this.dict
        } // any changes saved in this.dict in actbuffer.updates automatically
    };// end of AC_FCVModel constructor
    
    AC_FCVModel.prototype.AF_init = function(){
        this.dict = this.AV_actbuffer.AF_get_update_dic(this.model);
        this.AF_set_actmenu();
        return this;
    };// end of AC_FCVModel.prototype.AF_init
    
    /* -----------------------------------------------------------------------
     <li#(fc-id)>                    =>this.AJ_li
        <div.libox#(fc-id_div)>        =>this.AJ_div
          <div.outer>                    =>this.jouterdiv
             <div.inner/>                  =>this.AJ_inner
          </div.outer>
          <img.[action upicon]/>         =>this.jimgup   //arrow  @right-rim
          <img.[action deleteicon]/>     =>this.jimgdel  //X-icon @right-rim
          <img.[action downicon]/>       =>this.jimgdown //arrow  @right-rim
        </div.libox>
     </li>
    */
    AC_FCVModel.prototype.AF_init_listbox = function(){
        this.AJ_li = $('<li id="'+this.model.id()+'"></li>');
        // box containing thumb-img(left-down) and 3 act-icons(right rim)
        this.AJ_div = $('<div class="libox" id="'+this.model.id()+'_div"></div>');
        var jouterdiv = $('<div class="outer"></div>')
                        .appendTo(this.AJ_div);
        this.AJ_inner = $('<div class="inner"></div>').appendTo(jouterdiv);
        
        if (this.fext === 'img') {
            // for img type, fill in (thumb)picture into AJ_inner
            AC_BinObjMgr.AF_feed_media(this.AJ_inner, this.model.rs());
        } else {
            // for types other than img
            // setup doc icon(<img.doc/>)left-down in the box and
            // put text into AJ_inner
            var iconimg = $('<img class="doc" />');
            var imgsrc = "";
            switch(this.fext){
            case "pdf":     imgsrc += "pics/pdficon.png";   break;
            case "audio":   imgsrc += "pics/audioicon.png"; break;
            case "video":   imgsrc += "pics/videoicon.png"; break;
            case "txt":     imgsrc += "pics/txticon.png";   break;
            default: U.log(1, "type unknown: "+this.fext); break;
            }
            iconimg.attr('src', imgsrc).appendTo(this.AJ_div);
            this.AJ_inner.html('<p/>'+
                             this.model.name_face(this.AV_docpane.AV_lcode));
        }

        // set 3 act-icons (up/down/delete) on the right rim
        msg = '<img class="acticon upicon" src="pics/upsmall.png"/>';
        this.jimgup = $(msg).appendTo(this.AJ_div);
        msg = '<img class="acticon deleteicon" src="pics/erase.png"/>';
        this.jimgdel = $(msg).appendTo(this.AJ_div);
        msg='<img class="acticon downicon" src="pics/downsmall.png"/>';
        this.jimgdown = $(msg).appendTo(this.AJ_div);
        this.AJ_li.append(this.AJ_div); // put div box into li
    };// end of AC_FCVModel.prototype.AF_init_listbox
    
    AC_FCVModel.prototype.AF_title = function(v){
        if (typeof (v) === 'undefined'){ // no v: getter
            if(this.newfc)
                return this.model.name_face(this.AV_docpane.AV_lcode);
            else
                if('name' in this.dict) // modified before: return new value
                    return this.dict['name'][this.AV_lcode];
                else    // not modified: return original value
                    return this.model.name_face(this.AV_lcode);
        }
        // v !== undefined - it has a value: setter
        // update list-icon text with v
        if(this.fext === 'img'){
            this.AJ_inner.attr('title',Base64.decode(v));
        } else {
            this.AJ_inner.attr('title',Base64.decode(v)).text(Base64.decode(v));
        }
        if(this.newfc){
            this.model.name()[this.AV_lcode] = v;
        } else {
            if(this.model.name_face(this.AV_lcode) !== v){
                var name_dic = $.extend(true, {},this.model.name());
                name_dic[this.AV_lcode] = v;
                this.dict['name'] = name_dic;
            } else {
                delete this.dict['name'];
            }
        }
        this.AV_docpane.AF_update_save_btn(); // TBD
    };// end of AC_FCVModel.prototype.AF_title

    AC_FCVModel.prototype.AF_anno = function(v){ // v has value: setter
        if(this.newfc){
            this.model.AF_anno_face(this.AV_lcode, v);
        } else {
            if(this.model.AF_anno_face(this.AV_lcode) !== v){
                var anno_dic = $.extend(true, {}, this.model.anno());
                anno_dic[this.AV_lcode] = v;
                this.dict['anno'] = anno_dic;
            } else {
                delete this.dict['anno'];
            }
        }
        this.AV_docpane.AF_update_save_btn();
    };// end of AC_FCVModel.prototype.AF_anno
    // 
    AC_FCVModel.prototype.AF_contdict = function (d) {
        // d has value: setter
        if(this.newfc){
            this.model.AF_contdict(d);
        } else {
            if(Object.keys(d).length > 0){
                this.dict['contdict'] = d;
            } else {
                delete this.dict['contdict'];
            }
        }
        this.AV_docpane.AF_update_save_btn();
    };// end of AC_FCVModel.prototype.AF_contdict
        
    // ------------------------------------------------------------------------
    // attachme: attach=true: attach jli to docpane.AJ_ul; false: detach jli 
    // -------------------------------------------
    AC_FCVModel.prototype.AF_attachme = function(attach){
        if (attach) {  // true
            var tx = this.model.name_face(this.AV_lcode);
            if(this.fext !== 'img'){
                this.AJ_inner.attr('title',tx).html('<p/>'+tx);
            } else {
                this.AJ_inner.attr('title',tx);
            }
            //  not-highlighted when put in
            this.AJ_li.find('div.libox').removeClass('highlight');
            this.AJ_li.appendTo(this.AV_docpane.AJ_ul);
            var self = this;
            this.AJ_li.unbind('click').bind('click', function (e) {
                self.AF_set_highlight();
                return U.AF_stop_event(e);
            });
            //this.AV_docpane.AV_bigshow.set_anno_toggle(this.fext);
        } else {
            this.AJ_li.detach();
        }
        return this;
    };// end of AC_FCVModel.AF_attachme method
    
    // index in docpane.fcvs array. if -1, something is wrong
    AC_FCVModel.prototype.AF_index = function(){
        for(var i=0; i<this.AV_docpane.fcvs.length; i++){
            if(this.AV_docpane.fcvs[i] === this){
                return i;
            }
        }
        return -1;
    };// end of AC_FCVModel.prototype.AF_index
    
    // move this fcv in fcv-list on the right side, up by 1
    AC_FCVModel.prototype.AF_moveup = function(e){
        var elder = this.AV_docpane.fcvs[this.AF_index() - 1];
        var t = this.AJ_li.detach();  // gui/li detch from ul
        elder.AJ_li.before(this.AJ_li); // attach in front of elder li
        // swap positions in docpane.fcvs array
        this.AV_docpane.fcvs.splice(this.AF_index()-1,0,
                this.AV_docpane.fcvs.splice(this.AF_index(), 1)[0]);
        this.AF_set_actmenu(true); // set my action-menu, set-highlight:true
        this.AV_docpane.AF_update_save_btn();
        return U.AF_stop_event(e);
    };// end of AC_FCVModel.AF_moveup method
    
    // move this fcv in fcv-list on the right side, down by 1
    AC_FCVModel.prototype.AF_movedown = function(e){
        var younger = this.AV_docpane.fcvs[this.AF_index() + 1];
        this.AJ_li.detach(); // detach li from ul (gui)
        younger.AJ_li.after(this.AJ_li); // attach under younger li
        // swap positions in docpane.fcvs array
        this.AV_docpane.fcvs.splice(this.AF_index()+1,0,
                this.AV_docpane.fcvs.splice(this.AF_index(), 1)[0]);
        this.AF_set_actmenu(true);// set my action-menu, set-highlight:true
        this.AV_docpane.AF_update_save_btn();
        return U.AF_stop_event(e);
    };// end of AC_FCVModel.AF_movedown method
    
    AC_FCVModel.prototype.AF_deleteme = function(e){
        this.AJ_li.detach();  // remove li from ul
        this.AV_docpane.fcvs
            .splice(this.AF_index(),1); // off from docpane.fcvs array
        this.AF_set_highlight(false);  // no highlight on me - I am gone
        
        if(this.newfc){
            this.AV_docpane.AV_actbuffer.AF_remove_save(this.model.id());
            var rs = this.model.rs();
            if(this.model.type() === 'txt' && rs){
                // if txt type fc has a rs, this rs is not shared. remove it
                this.AV_docpane.AV_actbuffer.AF_remove_save(rs);
            } else {
                // non-txt fc's rs could have been shared. check that
                if(rs.iddict.holders.length === 1){
                    this.AV_docpane.AV_actbuffer.AF_remove_save(rs);
                } else {
                    this.AV_docpane.AV_actbuffer.AF_remove_update(rs);
                }
            }
        } else {
            this.AV_docpane.AV_actbuffer.AF_add_delete(this.model.id());
            var rs = this.model.rs();
            if(this.model.type() === 'txt' && rs){
                // if txt type fc has a rs, this rs is not shared. remove it
                this.AV_docpane.AV_actbuffer.AF_add_delete(rs);
            } else if(rs){
                // non-txt fc's rs could have been shared. check that
                if(rs.iddict['holders'].length === 1){
                    this.AV_docpane.AV_actbuffer.AF_add_delete(rs);
                } else {
                    var iddict = $.extend(true, {}, rs.iddict);
                    var ind = iddict['holders'].indexOf(this.model.id());
                    if( ind > -1){
                        iddict['holders'].splice(ind, 1);
                        this.AV_docpane.AV_actbuffer
                            .AF_add_update(rs,{'iddict':iddict});
                    }
                }
            }
        }
        this.AV_docpane.AF_update_save_btn();
        return U.AF_stop_event(e);
    };// end of AC_FCVModel.AF_deleteme method
    
    // ------------------------------------------------------------------------
    // if allow-write, and this fcv in highlght, show act-icons, and
    // bind handlers to them
    // ---------------------------------------------------
    AC_FCVModel.prototype.AF_set_actmenu = function (highlight) {
        var self = this;
        // showing up arrow if this is NOT the first facet
        if (highlight && self.AF_index() > 0 && self.AV_docpane.AV_allow_write){
            this.jimgup.css('visibility', 'visible');
            this.jimgup.unbind('click').bind('click', function (e) {
                self.AF_moveup(e);
            });
        } else {    // hide up arrow for this IS the first facet
            this.jimgup.css('visibility', 'hidden');
        }
        if (highlight && self.AV_docpane.AV_allow_write){ // show delete icon
            this.jimgdel.css('visibility', 'visible');
            this.jimgdel.unbind('click').bind('click', function (e) {
                self.AF_deleteme(e);
            });
        } else {    // not in highlight: hide delete icon
            this.jimgdel.css('visibility', 'hidden');
        }
        // show down arrow if NOT the last facet
        if (highlight && this.AF_index() < this.AV_docpane.fcvs.length - 1
            && self.AV_docpane.AV_allow_write) {
            this.jimgdown.css('visibility', 'visible');
            this.jimgdown.unbind('click').bind('click', function (e) {
                self.AF_movedown(e);
            });
        } else {    // hide down arow for this IS the last facet
            this.jimgdown.css('visibility', 'hidden');
        }
    };// end of AC_FCVModel.AF_set_actmenu method
    
    // ------------------------------------------------------------------------
    // turnon: true/undefined: set this fcv to be in highlight;
    // turnon: false, make sure this is not highlighted.
    // -------------------------------------------------
    AC_FCVModel.prototype.AF_set_highlight = function(turnon){
        if(typeof(turnon) === 'undefined' || turnon){ // true or not given
            if(this.AV_docpane.AV_highlight_fcv){
                this.AV_docpane.AV_highlight_fcv.AJ_li
                    .find('div.libox').removeClass('highlight');
                this.AV_docpane.AV_highlight_fcv.AF_set_actmenu(false);
            }
            this.AJ_li.find('div.libox').addClass('highlight');
            this.AV_docpane.AV_highlight_fcv = this;
            this.AF_set_actmenu(true);
            this.AV_docpane.AV_bigshow.AF_show(this.AF_show_anno());
        } else { // before docpane closes, it call this on its highlight_fcv
            if(this.AV_docpane.AV_highlight_fcv === this){
                this.AV_docpane.AV_highlight_fcv = null;
                this.AJ_li.find('div.libox').removeClass('highlight');
                this.AF_set_actmenu(false);
            }
        }
        return this;
    };// end of AC_FCVModel.AF_set_highlight method
    
    // if showanno==T, anno is attached to lower div
    AC_FCVModel.prototype.AF_show_anno = function(){
        var self = this;
        var msg = this.model.AF_anno_face(this.AV_lcode);
        this.janno.html(msg);
        if(!this.AV_docpane.AV_allow_write){
            this.janno.prop('contentEditable', false);
        } else {
            this.janno.prop("contentEditable", true);
            this.janno.unbind('input').bind('input',
                function(e){
                    self.AF_anno(Base64.encode(self.janno.html()));
                    self.AV_docpane.AF_update_save_btn();
                    return U.AF_stop_event(e);
            });
        }
        return this;
    };// end of AC_FCVModel.AF_show_anno method    

    return AC_FCVModel;
})();