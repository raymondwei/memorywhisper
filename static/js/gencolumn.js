﻿/*
 * Copyright (C) 2014 Adacro Inc. - All Rights Reserved
 * ------------------------------------------------------
 * A AC_GenColumn represents a generation in f-tree,displayed on a column on the
 * fgraphpane. each gen has blocks. each block is a sgroup or a virtual bv that
 * only happend on root gen.
 *///--------------------------------------------------------------------------
var AC_GenColumn = (function(){
    function AC_GenColumn(index, AV_fpane){
        this.AF_index(index);
        this.AV_fpane = AV_fpane;// the fgraph-div
        // sgrp or root(virtual) bv, or root (non-virtual)pv
        this.AV_blocks = []; 
        this.AV_lasty = 0; // y-coord is down-ward. keep trace of lowest y on gen
        this.AV_state = 'attached';
    };// end of AC_GenColumn constructor

    AC_GenColumn.prototype.AF_index = function(v){
        if(typeof(v) === 'undefined') return this._index;
        this._index = v;
        return this;
    };// end of AC_GenColumn.AF_index method
    
    // x-coordinate of left edge of this gen-column. Read-only
    AC_GenColumn.prototype.xstart =function(){
        return this.AF_index()*U.AV_GEN_L;
    };// end of AC_GenColumn.prototype.xstart

    AC_GenColumn.prototype.AF_next_gen = function(){
        if(this.AF_index() + 1 >= this.AV_fpane.AV_gens.length){
            this.AV_fpane.AV_gens.push(
                new AC_GenColumn(this.AF_index()+1, this.AV_fpane));
        }
        return this.AV_fpane.AV_gens[this.AF_index()+1];
    };// end of AC_GenColumn.AF_next_gen method
    
    AC_GenColumn.prototype.AF_prev_gen = function(){
        if(this.AF_index() === 0) return null;
        return this.AV_fpane.AV_gens[this.AF_index() - 1];
    };// end of AC_GenColumn.AF_prev_gen method
    
    // ------------------------------------------------------------------------
    // from block-0 downwards, find the first 2 blocks overlapping
    // return: [sgrp1, sgrp2, D] where s1/s2 (block-index ascending) are the
    // overlapping parties; D is the overlapped amount; null if no overlap.
    // --------------------------------------------------------------------
    AC_GenColumn.prototype.AF_next_overlap = function(){
        var i = 0;
        while(i+1 < this.AV_blocks.length){
            var L1 = this.AV_blocks[i].AF_lower(), 
                U2 = this.AV_blocks[i+1].AF_upper();
            if( L1 > U2 + U.AV_PAV_YMIND + 1){
                return [this.AV_blocks[i],this.AV_blocks[i+1],
                        L1-(U2+U.AV_PAV_YMIND+1)+1];
            } else{
                i++;
            }
        }
        return null;
    };// end of AC_GenColumn.AF_next_overlap method
    
    // ------------------------------------------------------------------------
    // after having found overlap between sgrp1 and sgrp2, find the bv1/bv2 of
    // sgrp1 and sgrp2, trace bv1/bv2 so many gen-cols back until they are in
    // the same sgrp, then stretch this sgrp(lower party ymove for D, and all
    // bvs/pvs below it. conf is the result of AC_GenColumn.AF_resolve_conflict
    // ----------------------------------------------------------
    AC_GenColumn.prototype.AF_resolve_conflict = function(conf){
        if(!conf) return null;
        var bv1 = conf[0].bv(), bv2 = conf[1].bv();
        var pv1 = bv1.AF_anchor_pv(), pv2 = bv2.AF_anchor_pv();
        if(pv1 === pv2){ // bv1 and bv2 are hosted by the same anchor pv
            pv1.AF_stretch_bvs(pv1.sex()==='male'? bv2: bv1, conf[2]);
            if(pv1.AF_gen().AF_index() > 0){
                var sgrp = pv1.phost.AF_sgroup();
                var AV_youngers = sgrp.AF_find_youngers(pv1);
                for(var i=0; i<AV_youngers.length; i++){
                    AV_youngers[i].AF_ymove(conf[2]);
                }
            }
        } else if(pv1.AF_gen().AF_index() === 0){
            alert("gen0 has 2 anchors");
        } else {// pv1 != pv2
            // tracing bv1 bv2 up till their anc_pas are in the same sgroup
            while(pv1.AF_gen().AF_index() > 0 && 
                  pv1 !== pv2 &&
                  pv1.phost != pv2.phost){
                bv1 = pv1.phost;
                bv2 = pv2.phost;
                pv1 = bv1.AF_anchor_pv();
                pv2 = bv2.AF_anchor_pv();
            }
            if(bv1.AF_gen().AF_index() > 0 || 
               FD.AV_path_mgr.AF_root().indexOf('-PA-') > -1){
                if (pv1 === pv2) { 
                    // bv1 bv2 are spouses of the same person
                    pv1.AF_stretch_bvs(pv1.sex() === 'male'? bv2:bv1, conf[2]);
                } else {
                    var sgrp = pv1.phost.AF_sgroup();
                    // move down pv1 and pvs below pv1
                    sgrp.AF_stretch_down(pv2, conf[2]);
                }
            }
        }
        return this.AF_next_overlap();
    };// end of AC_GenColumn.AF_resolve_conflict method
    
    AC_GenColumn.prototype.AF_removeme = function(){
        for(var i=0; i< this.AV_blocks.length; i++){
            if(this.AV_blocks[i].cid() === 'BAV'){
                this.AV_blocks[i].AF_removeme();
                this.AV_blocks[i].AF_male_pv().AF_removeme(true);// excl spouses
                this.AV_blocks[i].AF_female_pv().AF_removeme(true); // only pv
            } else {
                // sgrp or root (non-virtual) pv
                this.AV_blocks[i].AF_removeme();
            }
        }
        this.AV_blocks = [];
        this.AV_state = 'detached';
    };// end of AC_GenColumn.AF_removeme method
    
    // sort blocks in this gen according to indexes(of children index in this
    // generation, or, in case the 2 neighbouring bv.anchor-pas are not siblings
    // go back to last gen, till 2 anchor-pas are siblings, or spouses of the 
    // same host. Not care about y-coord.
    AC_GenColumn.prototype.AF_sort_blocks = function(ind){
        var start = ind || 0;
        var bv0 = this.AV_blocks[start].bv();
        if(start < this.AV_blocks.length && 
           this.AV_blocks[start].cid() === 'SGP'){
            var i = start+1, goon = true;
            while(i < this.AV_blocks.length && goon){
                var bv = this.AV_blocks[i].bv();
                if(bv0.AF_below_me(bv)){
                    i++;
                } else { // bv is above bv0
                    // trade place:bv0 <-> bv
                    var tmp = this.AV_blocks[i];
                    this.AV_blocks[i] = this.AV_blocks[start];
                    this.AV_blocks[start] = tmp;
                    this.AF_sort_blocks(start);
                    return this;
                }
            }
            if(start < this.AV_blocks.length-1)
                this.AF_sort_blocks(start+1);
        }
        return this;
    };// end of AC_GenColumn.prototype.AF_sort_blocks
    
    // adding to blocks array, make sure sorted by ynavel: ascending
    // a block can be bav, sgrp, pav
    AC_GenColumn.prototype.AF_add_block = function(AV_block){
        if (this.AV_blocks.indexOf(AV_block) === -1) {
            var y = AV_block.AF_ynavel();
            var AV_inser_b4_index = this.AV_blocks.length;
            for (var i = 0; i < this.AV_blocks.length; i++) {
                if (this.AV_blocks[i].AF_ynavel() > y) {
                    AV_inser_b4_index = i;
                    break;
                }
            }
            this.AV_blocks.splice(AV_inser_b4_index, 0, AV_block);
        }            
        this.AV_lasty = AV_block.AF_lower(true);
        return this;
    };// end of AC_GenColumn.AF_add_block method
    
    AC_GenColumn.prototype.AF_del_block = function(AV_block){
        var ind = this.AV_blocks.indexOf(AV_block);
        if(ind > -1){
            this.AV_blocks.splice(ind,1);
        }
        return this;
    };// end of AC_GenColumn.prototype.AF_del_block method
    
    return AC_GenColumn;
})();