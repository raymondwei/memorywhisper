﻿/*
 * Copyright (C) 2014 Adacro Inc. - All Rights Reserved
 * ------------------------------------------------------
 * under div#infos there are 2 divs: div.left: cats-buttons and
 * div.right: this class manage it
 * --------------------------------------------------------------- */
// cat content pane class
var AC_ItemPane = (function(){
    function AC_ItemPane(infov){
        this.AV_infov = infov;
        // get container div, assign it to this.AJ_contdiv
        this.AJ_contdiv = infov.AJ_rigth;
        this.AF_clear();   // clear folders
        this.AJ_addfolder_btn_span = $('<span class="add-folder"></span>')
            .append($('<img src="pics/add-folder-icon.png"/>'));
        this.AJ_adddoc_btn_span = $('<span class="add-doc"></span>')
            .append($('<img src="pics/add-doc-icon.png"/>'));
        this.AV_iw = new AC_ItemWizard(FD, this);
    };// end of AC_ItemPane constructor
    
    AC_ItemPane.prototype.cid = function(){ return 'CCP'; }// CatCont-Pane
    
    AC_ItemPane.prototype.AF_clear = function(){
        if(this.ip && this.ip.vmodel){
            this.ip.vmodel.AJ_ul.remove();
            this.ip.vmodel.AV_state = "shrunk";
        }
        this.ip = null;
    };// end of AC_ItemPane.AF_clear. method
    
    // update_ipv set ip's containees(folders and docs)
    AC_ItemPane.prototype.AF_update_ipv = function(ip){
        this.AF_clear();
        this.ip = ip; 
        if(!ip.vmodel) 
            ip.vmodel = new AC_IPVModel(ip, this.AV_infov);
        this.AF_populate(ip);
        ip.vmodel.AF_attachme().AV_state = "expanded";
        return this.AF_add_action_buttons(ip);
    };// end of AC_ItemPane.prototype.AF_update_ipv method

    // for logged-in user, add 2 big-buttons as last li in ul: 
    // [add-folder] and [add-doc]
    AC_ItemPane.prototype.AF_add_action_buttons = function(ip){
        var self = this;
        if(FD.AV_loginpv && 
           (M.AF_fadmin() ||
            ip.owner().id() === FD.AV_loginpv.model.id())){
            if(this.AJ_actli){
                var box = ip.vmodel.AJ_ul.find('div#'+ip.id()+'_additem');
                if(box.length === 0)
                    this.AJ_actli.appendTo(ip.vmodel.AJ_ul);
            } else {
                this.AJ_actli = $('<li></li>').appendTo(ip.vmodel.AJ_ul);
                var msg='<div class="new-entry-box" id="'+ip.id()+'_additem"/>';
                this.AJ_actionbox = $(msg).appendTo(this.AJ_actli);
                this.AJ_addfolder_btn_span.appendTo(this.AJ_actionbox);
                this.AJ_adddoc_btn_span.appendTo(this.AJ_actionbox);
            }
        } else if(this.AJ_actli){
            this.AJ_actli.detach();
        }
        return this;
    };// end of AC_ItemPane.AF_add_action_buttons method

    // ent is the container it(f) or ip. repopulate ent's containees(it-F/D)
    AC_ItemPane.prototype.AF_populate = function(ent){
        if(ent.cid() === 'IT' && ent.type()[0] === 'D') 
            return ent;
        ent.force_refresh('folders').force_refresh('docs');
        var folders = ent.folders(), docs = ent.docs();
        ent.vmodel.AJ_ul.empty();
        var concat = folders.concat(docs);
        for(var i=0; i<concat.length; i++){
            if(!concat[i].vmodel) 
                concat[i].vmodel = new AC_ItemVModel(concat[i]);
            if(concat[i].type()[0] === 'D'){
                concat[i].vmodel.AF_update_lock()
                         .AJ_li.appendTo(ent.vmodel.AJ_ul);
            } else if(concat[i].pk() !== 'M7118' || M.OA(concat[i])){ 
                concat[i].vmodel.AF_update_lock()
                         .AJ_li.appendTo(ent.vmodel.AJ_ul);
            }
        }
        for(i=0; i<concat.length; i++){
            if(concat[i].vmodel)
                concat[i].vmodel.AF_update_clickables();
        }
        return ent;
    };// end of AC_ItemPane.prototype.AF_populate
    
    // given it under its container (cont), return next sibling of it 
    // in dir(up/down) direction. If it is on the edge, return null
    AC_ItemPane.AF_next_peer = function(dir, it, cont){
        var i, np = null,
            name = it.type()[0]==='F'? 'folders' : 'docs';
        var peers = cont[name]();
        if(dir === 'up'){
            i = it.AF_index() - 1;
            if(i >= 0) 
                np = peers[i];
        } else {
            i = it.AF_index() + 1;
            if(i < peers.length)
                np = peers[i];
        }
        return np;
    };// end of AC_ItemPane.AF_next_peer 
    
    // if it is highest among its siblings, but move up, or, it is last and 
    // move down, it then needs a new folder to host it; 
    AC_ItemPane.AF_dest_folder = function(
                dir,    // direction: up/down
                it,     // to be moved: it is highest when up/lowest when down
                cont){  // father(container)
        var folder = null;  // new home(folder) for it
        if(dir==='up'){ // it.AF_index() ==0, need go up, to new folder
            if(it.type()[0]==='D'){ // doc to be moved up
                // sibling folder(s) exist, move into lowest(visible) one
                if(cont.folders().length > 0){
                    var fs = cont.folders();
                    for(var i=fs.length; i>0; i--){
                        if(concat[i].pk() !== 'M7118' || M.OA(concat[i])){ 
                            // not blocked for PV or OA(it)
                            folder = fs[i-1];
                            break;
                        }
                    }
                }
                if(!folder){ // failed to find folder: (visible)elder uncle?
                    if(cont.cid()!=='IP' && cont.AF_index()>0){
                        var con = cont.owner(); // grand-father
                        var fs = con.folders(); // father's sibling-list
                        for(var i=cont.AF_index()-1; i>=0; i--){
                            // if this uncle is visible: that's it!
                            if(concat[i].pk() !== 'M7118' || M.OA(concat[i])){
                                // unblocked for PV or OA(it)
                                folder = fs[i];
                                break;
                            }
                        }
                    }
                }
            } else {// it.type() =='F', it is highest folder under cont
                // father isn't IP, also not eldest among his siblings
                if(cont.cid() !== 'IP' && cont.AF_index()>0){
                    var con = cont.owner();// grand-father
                    var fs = con.folders();// father's sibling-list
                    for(var i=cont.AF_index()-1; i>=0; i--){
                        // if any elder uncle visible: that's it!
                        if(concat[i].pk() !== 'M7118' || M.OA(concat[i])){ 
                            // unblocked for PV or OA(it)
                            folder = fs[i];
                            break;
                        }
                    }
                }
            }
        } else { // dir==='down': it is last among its siblings: to new folder
            // if cont is ip, it is impossible to move down more
            if(cont.cid() !== 'IP'){ // cont is a it-f
                var con = cont.owner();// grand-father
                var fs = con.folders();// father sibling-list
                for(var i = cont.AF_index()+1; i<fs.length; i++){
                    // any younger uncle visible: that's it!
                    if(concat[i].pk() !== 'M7118' || M.OA(concat[i])){ 
                        // unblocked for PV or OA(it)
                        folder = fs[i];
                        break;
                    }
                }
            }
        }
        return folder;
    };// end of AC_ItemPane.prototype.AF_dest_folder method

    // ------------------------------------------------------------------------
    // Purpose: if cont is a it and of type F, make sure ul-folder
    // opens up. And if it has it(f) ancesters, all these folders open up too.
    // This is used when action happened to an it under cont, after the action 
    // had taken place on server successfully, the result is opened to be seen. 
    // ---------------------------------------------------------
    AC_ItemPane.AF_open_folders = function(cont){
        // collect all it(F) ancesters: eldest as the last
        var ances = [];
        var buf = cont;
        if (buf && typeof buf === 'object') {
            // if this folder to be expanded(open), collect all its
            // containing folder, for open them all
            while(buf.cid() === 'IT' &&  buf.type()[0] === 'F'){
                ances.push(buf.vmodel);
                buf = buf.owner();
            }
            // open all ances folders
            for (var i = ances.length - 1; i >= 0; i--) {
                ances[i].AF_toggle_state('expanded')
            }
        }
    };// end of AC_ItemPane.AF_open_folders static method

    // go thru all items rendered visible, cut names of those short if
    // too long for the browser window: abcwerwerwer => abcwerwe..
    AC_ItemPane.prototype.AF_trim_names = function(){
        $('span.item-title-box span.LS').each(
            function(index){
                U.AF_short_it_name($(this));
            }
        );
    };// end of AC_ItemPane.prototype.AF_trim_names
    
    return AC_ItemPane;
})(); // AC_ItemPane
