﻿/*
 * Copyright (C) 2014 Adacro Inc. - All Rights Reserved
 */
var AC_PathMgr = (function(){
    function AC_PathMgr(fd){
        this.fd = fd;
        this.paths = [];
        this.ci = -1;   // current-index: pointing to tpath in use
    };// end of AC_PathMgr constructor
        
    AC_PathMgr.prototype.AF_curr_tpath = function(){
        if(this.paths.length === 0) return null;
        return this.paths[this.ci];
    };// end of AC_PathMgr.prototype.AF_curr_tpath
    
    // clone current path, push it to be last path, set ci to it
    AC_PathMgr.prototype.AF_clone_new_path = function(new_focus_pid){
        this.paths.push(this.AF_curr_tpath().AF_clone(new_focus_pid));
        this.ci = this.paths.length - 1;
        this.fd.AV_tbar.AF_update_nav();
        return this.AF_curr_tpath();
    };// end of AC_PathMgr.prototype.AF_clone_new_path
    
    AC_PathMgr.prototype.AF_root = function(v, newpath){
        if(typeof(v) === 'undefined') 
            return this.AF_curr_tpath().AF_root();
        if(newpath){
            var npath = this.AF_curr_tpath().AF_clone();
            npath.AF_root(v);
            this.paths.length = this.ci+1;  // cut off all behinds curr
            this.paths.push(npath);         // add npath as last path
            this.ci = this.paths.length - 1;// curr point to npath
            this.fd.AV_tbar.AF_update_nav();
        } else {
            this.AF_curr_tpath().AF_root(v);
        }
        return this.AF_curr_tpath();
    };// end of AC_PathMgr.prototype.AF_root
    
    AC_PathMgr.prototype.AF_new_path = function(pa){
        this.paths.push(new AC_TPath(pa)); // create a tpath focus on pa
        // set current index to the new/last tpath
        this.ci = this.paths.length-1;
        this.fd.AV_tbar.AF_update_nav();
        return this.AF_curr_tpath();
    };// end of AC_PathMgr.prototype.AF_new_path
    
    AC_PathMgr.prototype.AF_back_possible = function(){
        return this.ci > 0;
    };// end of AC_PathMgr.prototype.AF_back_possible
    
    AC_PathMgr.prototype.AF_forward_possible = function(){
        return this.ci < this.paths.length - 1;
    };// end of AC_PathMgr.prototype.AF_forward_possible
    
    AC_PathMgr.prototype.AF_is_exp = function(id){
        if(this.AF_curr_tpath() && this.AF_curr_tpath().AF_is_exp(id)) 
            return true;
        return false;
    };// end of AC_PathMgr.prototype.AF_is_exp
    
    // set current-path-index forewards for 1, return current-path
    // if not possible moving forewards, return null
    AC_PathMgr.prototype.AF_forward = function(){
        var old = this.AF_curr_tpath();
        if(this.ci < (this.paths.length - 1)){
            this.ci++;
            return old;
        } else
            return null;
    };// end of AC_PathMgr.prototype.AF_forward
    
    // set current-path-index backwards for 1, return current-path
    // if not possible moving backwards, return null
    AC_PathMgr.prototype.AF_backward = function(){
        var old = this.AF_curr_tpath();
        if(this.ci > 0){
            this.ci--;
            return old;
        } else
            return null;
    };// end of AC_PathMgr.prototype.AF_backward

    // in case more than 1 paths, set to 0-th to be current, return current
    // if there is only 1 path, return null
    AC_PathMgr.prototype.AF_back2home = function(){
        var old = this.AF_curr_tpath();
        if(this.paths.length > 1){
            this.ci = 0;
            this.paths.length = 1;
            return old;
        } else
            return null;
    };// end of AC_PathMgr.prototype.AF_back2home
    
    AC_PathMgr.prototype.AF_add_expand = function(v, newpath){
        if(newpath){// make a new path with the new v
            var npath = this.AF_curr_tpath().AF_clone(v); // clone to npath
            this.paths.length = this.ci+1;  // cut off all behinds curr
            // add v to expands npath as last path
            this.paths.push(npath.AF_add_expand(v));
            this.ci = this.paths.length - 1;// curr point to npath
            this.fd.AV_tbar.AF_update_nav();
        } else { // keep the tpath as current, just add v to it
            this.AF_curr_tpath().AF_add_expand(v);
        }
        return this.AF_curr_tpath();
    };// end of AC_PathMgr.prototype.AF_add_expand
    
    AC_PathMgr.prototype.AF_remove_expand = function(v, propagate, newpath){
        if(newpath){// make a new path with the v removed
            var npath = this.AF_curr_tpath().AF_clone();// clone to npath
            npath.AF_remove_expand(v, propagate);    // remove from npath
            this.paths.length = this.ci+1;// cut off all behinds
            this.paths.push(npath);
            this.ci = this.paths.length - 1;
            this.fd.AV_tbar.AF_update_nav();
        } else { // keep the tpath as current, just remove v from it
            this.AF_curr_tpath().AF_remove_expand(v, propagate);
        }
        return this.AF_curr_tpath();
    };// end of AC_PathMgr.prototype.AF_remove_expand
    
    // in case this.paths = [pth1, pth2, pth3], ci=2(pointing to pth3)
    //   this will endup with: [pth1,pth2], ci=1(pointing to pth2)
    // in case [pth1, pth2, pth3, pth4], ci=2(point to pth3), this will
    //   result in: [pth1,pth2], ci=1. pth3/pth4 discarded.
    AC_PathMgr.prototype.AF_pop_last_path = function(){
        if(this.paths.length > 1){
            this.paths.length = this.ci;
            this.ci--;
        }
    };// end of AC_PathMgr.prototype.AF_pop_last_path
    
    AC_PathMgr.prototype.AF_chop_from_ci = function(){
        if(this.ci > -1){
            this.paths.length = this.ci + 1;
        }
        return this;
    };// end of AC_PathMgr.prototype.AF_chop_from_ci
    
    return AC_PathMgr;
})();