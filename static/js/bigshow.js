﻿/*
 * Copyright (C) 2014 Adacro Inc. - All Rights Reserved
 */
var AC_BigShow = (function(){
    function AC_BigShow(docpane){
        this.AV_docpane = docpane;
        // the left-up big div area
        this.AJ_container = this.AV_docpane.AJ_upper;
        this.AV_title_input = $('input#show-title-text');
    };// end of AC_BigShow constructor
        
    AC_BigShow.prototype.AF_anno_toggle = function(fcv){
        this.AJ_container.unbind('click');
        var self = this;
        if((['audio', 'video'].indexOf(fcv.fext) === -1) && 
           (['img', 'pdf'].indexOf(fcv.fext) > -1 || 
            !self.AV_docpane.AV_allow_write)){
            this.AJ_container.unbind('click').bind('click', function (e) {
                self.AV_docpane.AF_lower_updown();
                U.AF_stop_event(e);
            });
        }
    };// end of AC_BigShow.prototype.AF_anno_toggle
        
    AC_BigShow.prototype.AF_show = function(fcv){
        this.AV_docpane.AF_setup_dimension();
        this.AJ_container.css('background-image','none').empty();
        this.AV_docpane.AF_lower_updown(false); // flase: down
        
        this.fcv = fcv;
        var self = this;
        // set anno-toggle to diff trigger-eles depending on img/not
        this.AF_anno_toggle(fcv);
        
        var AV_title_input = $('input#show-title-text');
        AV_title_input.val(fcv.AF_title());
        if(this.AV_docpane.AV_allow_write){
            AV_title_input.removeAttr('readonly');
            AV_title_input.unbind('input').bind('input', function(e){
                fcv.AF_title(Base64.encode(AV_title_input.val()));
                U.AF_stop_event(e);
            });
        } else
            AV_title_input.attr('readonly', 'readonly');
        
        if (fcv.fext === 'img') {
            AC_BinObjMgr.AF_feed_media(this.AJ_container, 
                fcv.model.rs(), 'pics/docpane-bg3.jpg');
        } else if(fcv.fext === 'pdf'){
            var jpdf = $('<iframe />');
            if(fcv.model.rs().data())
                jpdf.attr('src', fcv.model.rs().data());
            else if(fcv.model.rs().signature())
                AC_BinObjMgr.AF_feed_media(jpdf, fcv.model.rs());
            this.AJ_container.append(jpdf);
        } else if(fcv.fext === 'audio'){
            if(!this.AV_audioplayer)
                this.AV_audioplayer = new AC_AudioPlayer(this);
            this.AV_audioplayer.AF_init(fcv);
            this.AV_docpane.AF_lower_updown(true);
        } else if(fcv.fext === 'video'){
            if(!this.AV_videoplayer)
                this.AV_videoplayer = new AC_VideoPlayer(this);
            this.AV_videoplayer.AF_init(fcv);
        } else if(fcv.fext === 'txt'){
            if(!this.AV_editable)
                this.AV_editable = new AC_Editable(this);
            this.AV_editable.AF_show()
        }
    };// end of AC_BigShow.AF_show method
    
    return AC_BigShow;
})(); // end of AC_BigShow class

// for txt fc: in.AV_editable text in bigshow
var AC_Editable = (function(){
    function AC_Editable(AV_bigshow){
        this.AV_docpane = AV_bigshow.AV_docpane;
        this.AJ_div = AV_bigshow.AJ_container;
        this.AJ_text = $("<div id='upper-text-area'></div?").appendTo(this.AJ_div);
        this.AF_set_text_height();
    };// end of AC_Editable constructor
    
    // only needed by IE/Edge. But won't harm other browsers
    AC_Editable.prototype.AF_set_text_height = function(){
        var h = this.AV_docpane.upper_height * 0.98;
        this.AJ_text.animate({'height': h + 'px'}, U.AV_animate_duration);
    };// end of AC_Editable.prototype.AF_set_text_height
    
    AC_Editable.prototype.AF_make_txt_rs = function(fc, plain_txt){
        var b64txt = Base64.encode(plain_txt);
        var rs = new RS({'_id': M.mid(fc.fid(),'RS'), 
                'mime': 'text/plain',
                'data': b64txt, 
                'holders': [fc.id()],         // fc as first holder
                'signature': hex_sha1(b64txt),// signature of saved b64 text 
                'size': b64txt.length,
                 // fc-id as name: used in rs.file_name call
                'name': fc.id(),
                'title': fc.name()
            });
            this.AV_docpane.AV_actbuffer.AF_add_save(rs);
            return rs;
    };// end of AC_Editable.prototype.AF_make_txt_rs
    
    AC_Editable.prototype.AF_show = function(){
        this.fcv = this.AV_docpane.AV_bigshow.fcv;
        this.fc = this.fcv.model; 
        if(this.AV_docpane.AV_allow_write){
            this.AJ_text.prop("contentEditable",true);
            this.AF_set_handler();
        } else {
            this.AJ_text.prop("contentEditable",false);
        }
        if($('div#upper-text-area').length === 0)
            this.AJ_text.appendTo(this.AJ_div);
        this.AJ_text.empty();
        var txtrs = this.fc.rs(), tx, content;
        if(txtrs){
            // txt has been saved in a blob referred by txtrs
            var txturl = txtrs.file_name(), self = this;
            // get the text-blob by ajax-get
            $.get('/blobs/' + txturl,'', function(e){
                var txt = Base64.decode(e); // e is base64 encoded. Decoded it
                self.AJ_text.html(txt); // fill in the text
                self.AV_orig_text = txt;
            })
        } else {
            // no txtrs, text(b64 encoded) is saved in fc.contdict_face
            content = this.fc.AF_contdict();
            // base64 decoded by LS.face
            this.AV_orig_text = LS.face(content, this.fcv.AV_lcode);
            this.AJ_text.html(this.AV_orig_text);
        }
    };// end of AC_Editable.prototype.AF_show
    
    AC_Editable.prototype.AF_set_handler = function(){
        var self = this;
        this.AJ_text.unbind('input').bind('input', function(e){
            var txt = self.AJ_text.html();
            if(self.AV_orig_text !== txt){
                if(self.fcv.newfc){ // fc is in dirt_dic already. set contdict
                    // since table-storage on azure max ~ 33k, a txt-fc
                    if(msg.length > 10000){ // may have 3 languages: 10K limit
                        // make new rs, also put it in actbuffer.saves
                        var rs = self.AF_make_txt_rs(self.fc, txt);
                        self.fc.contdict_face(self.fcv.AV_lcode, "");
                        self.fc.iddict['rs'] = rs.id();
                    } else {
                        // msg is small enough 2b in fc.AF_contdict
                        self.fc.contdict_face(self.fcv.AV_lcode, txt);
                    }
                } else { // fc is not a new creation
                    var rs = self.fc.rs();
                    if(!rs){ // txt was save in fc.AF_contdict, not in a blob, 
                        // fc doesn't have rs. The text is saved in contdict
                        var d = $.extend(true, {}, self.fc.AF_contdict());
                        var iddict = $.extend(true, {}, self.fc.iddict);
                        if(txt.length > 10000){ // msg is too long for contdict
                            // make a new rs, also adds it to actbuffer.saves.
                            // when actbuffer.AF_execute save this rs, it will
                            // send /saveblob to post rs.data to blob-storge.
                            rs = self.AF_make_txt_rs(self.fc, txt);
                            d[self.fcv.AV_lcode] = "";//cut in fc.AF_contdict
                            iddict['rs'] = rs.id();
                        } else {
                            d[self.fcv.AV_lcode] = Base64.encode(txt);
                            // fcv.dict refers to actbuffer.updates - dict.
                            iddict['rs'] = '';
                        }
                        self.fcv.dict['contdict'] = d;
                        self.fcv.dict.iddict = iddict; 
                    } else { // text was in a rs/blob. put update-data into 
                        // actbuffer. When executing actbuffer sends /saveblob 
                        // request for posting the rs.data to blob-storage.
                        var rsdic = FD.AV_actbuffer.AF_get_update_dic(rs);
                        var b64 = Base64.encode(txt);
                        rsdic['size'] = b64.length;
                        rsdic['signature'] = hex_sha1(b64);
                        rsdic['data'] = b64;
                        self.fcv.rsupdate = true;
                    }
                }
                self.AV_docpane.AF_update_save_btn();
            } else {
                delete self.fcv.dict['contdict'];
                self.fcv.rsupdate = false;
            }
            return U.AF_stop_event(e);
        });
    };// end of AC_Editable.AF_set_handler method

    return AC_Editable;
})();// end of AC_Editable