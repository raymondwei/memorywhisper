﻿var AC_DocPane = (function(){
    function AC_DocPane(fd){
        this.fd = fd;
        this.AV_actbuffer = fd.AV_actbuffer;
        this.AJ_container = $('div#doc_pane_container');
        this.AF_load_template();
        this.AV_bigshow = new AC_BigShow(this);
    };// end of AC_DocPane constructor
    AC_DocPane.prototype.cid = function(){ return 'DPV'; }

    AC_DocPane.prototype.AF_load_template = function(){
        var m, j;
        this.AJ_docpane = $('<div id="doc_pane"></div>');
        // title-bar:
    m = "ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix";
        this.AJ_ttlbar = $('<div id="doc-titlebar"></div>')
            .addClass(m).appendTo(this.AJ_docpane);
        m = '<span id="doc-title-text" class="ui-dialog-title"></span>';
        this.AJ_title_text = $(m).appendTo(this.AJ_ttlbar);

        // save button
        m = "ui-button ui-widget ui-state-default ui-corner-all";
        this.AJ_save_btn=$('<button type="button" id="doc-save-btn"></button>')
            .addClass(m).appendTo(this.AJ_ttlbar);
        m='<span id="doc-save-btn-label" class="LS" tag="M1004">Save</span>';
        $(m).appendTo(this.AJ_save_btn);

        // cancel-button
    m='<button type="button" role="button" aria-disabled="false"></button>';
        this.AJ_cancel_btn = $(m).attr('id',"doc-cancel-btn");
    m="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only";
        this.AJ_cancel_btn.addClass(m).appendTo(this.AJ_ttlbar);
    m='<span id="doc-cancel-btn-label" class="LS" tag="M1003">Cancel</span>';
        $(m).appendTo(this.AJ_cancel_btn);

        // close button
m="ui-button ui-widget ui-corner-all ui-button-icon-only ui-dialog-titlebar-close";
        this.AJ_close_btn=$('<button type="button" id="doc-close-btn"></button>')
            .addClass(m).appendTo(this.AJ_ttlbar)
            .attr('role','button').attr('aria-disabled','false');
    m='<span class="ui-button-icon-primary ui-icon ui-icon-closethick"></span>';
        $(m).appendTo(this.AJ_close_btn);
        $('<span class="ui-button-text">close</span>')
            .appendTo(this.AJ_close_btn);

        // upper with bigshow
        this.AJ_upper =$('<div id="doc-upper"><div id="bigshow"></div></div>')
            .appendTo(this.AJ_docpane);

        // lower part: button-bar + anno-textarea
        this.AJ_lower = $('<div id="doc-lower"></div>')
            .appendTo(this.AJ_docpane);
    m="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix";
        this.AJ_btnbar = $('<div id="button-bar"></div>')
            .appendTo(this.AJ_lower).addClass(m);
        j = $('<div id="atc"></div').appendTo(this.AJ_btnbar);
        $('<span class="LS" tag="M7130" id="anno-label">ANNO</span>')
            .appendTo(j);
        $('<span class="anno-icon">&dArr;</span>').appendTo(j);
        $('<input type="text" id="show-title-text" readonly />')
            .appendTo(this.AJ_btnbar);
        $('<div id="doc-lower-text-area"></div>').appendTo(this.AJ_lower);

        // item-fcs: the vertical area on the right
        this.AJ_itemfcs = $('<div id="item-fcs"></div>')
            .appendTo(this.AJ_docpane);
        this.AJ_ul = $('<ul id="fc-icons"></ul>').appendTo(this.AJ_itemfcs);
        this.AJ_addbtn = $('<button id="add-btn"></button>')
            .appendTo(this.AJ_docpane);
        this.AJ_file_input = $('<input id="the-file-input" type="file">')
            .appendTo(this.AJ_docpane);
    };// end of AC_DocPane.prototype.AF_load_template
    
    // context-menu for add-button
    AC_DocPane.prototype.AF_setup_ctxmenu = function(it){
        this.AV_ctxtmenu = new AC_ContextMenu(this, 
            this.AJ_addbtn, {title:'M7126'}, true, {x: -50, y: -28});
        this.AV_ctxtmenu.AF_modify_entry({
            label: 'M6016',  // M6016: 'editable'
            icon: 'pics/pencil.jpg', 
            func: this.AF_add_file, 
            func_ctx: this, arg_array: ['txt']});
        this.AV_ctxtmenu.AF_modify_entry({
            label: 'M6017',  // Picture
            icon: 'pics/picture-icon.jpg', 
            func: this.AF_add_file, 
            func_ctx: this, arg_aray: ['image']});
        this.AV_ctxtmenu.AF_modify_entry({
            label: 'M6018',  // Audio
            icon: 'pics/audio-icon.jpg', 
            func: this.AF_add_file, 
            func_ctx: this, arg_array: ['audio']});
        this.AV_ctxtmenu.AF_modify_entry({
            label: 'M6019',  // Video
            icon: 'pics/video-icon.jpg', 
            func: this.AF_add_file, 
            func_ctx: this, arg_array: ['video']});
        this.AV_ctxtmenu.AF_modify_entry({
            label: 'M6020', //  PDF document
            icon: 'pics/pdf-icon.png', 
            func: this.AF_add_file, 
            func_ctx: this, arg_array: ['pdf']});
    };// end of AC_DocPane.prototype.AF_setup_ctxmenu
    
    AC_DocPane.prototype.AF_add_file = function(ext){
        this.file_type = ext;
        if(ext === 'txt'){
            var ndic = {}, lc = this.AV_lcode;
            ndic[lc] = FD.AV_lang.face('M7514');  // M7514: Untitled
            var fc = new FC({ '_id':  M.mid(this.it.fid(), "FC"), 
                'name': ndic,'anno': {}, 'contdict': {}, 'type':'txt'});
            fc.owner(this.it);
            this.AV_actbuffer.AF_add_save(fc);
            var fcv = new AC_FCVModel( this, fc, true);
            this.fcvs.push(fcv.AF_attachme(true).AF_set_highlight());
            fcv.AF_set_actmenu(true);
            this.AF_update_save_btn();
        } else {
            this.AJ_file_input.val('').trigger('click');
        }
    };// end of AC_DocPane.prototype.AF_add_file
    
    AC_DocPane.prototype.process_rs = function(newrs, rs, self){
        // make new fc, its rs refering to rs, owner refering to it
        var fc = new FC({'_id': M.mid(self.it.fid(),'FC'),
            'name': {}, 'type': U.AV_fc_type(rs.mime())});
        self.AV_actbuffer.AF_add_save(fc.rs(rs).owner(self.it));
        
        if(newrs){ // no old rs found with the same signature
            self.AV_actbuffer.AF_add_save(
                rs.AF_add_holder(fc.id()).title(fc.name()));
        } else { // rs existed now with fc a new holder
            var iddict = $.extend(true, {}, rs.iddict);
            iddict['holders'].push(fc.id());
            // rs.iddict to be updated
            self.AV_actbuffer.AF_add_update(rs, {'iddict': iddict});
        }
        if(!self.AF_fc_inlist(fc)){
            var fcv = new AC_FCVModel(self, fc, true);//new-fc:true
            self.fcvs.push(fcv.AF_attachme(true));
            self.AF_update_save_btn();
        }
        self.AJ_file_input.val('');
    };// end of AC_DocPane.prototype.process_rs
    
    AC_DocPane.prototype.AF_init = function(){
        this.fcvs = [];
        this.AV_highlight_fcv = null;
        this.AV_show_anno = false;
        this.AV_bigshow.fcv = null;
        this.AF_setup_dimension();
        this.AV_actbuffer.AF_init();
        return this;
    };// end of AC_DocPane.prototype.AF_init
    
    AC_DocPane.prototype.AF_lower_updown = function(true_up_false_down){
        // if no input, set toggle
        if(typeof(true_up_false_down) === 'undefined')
            true_up_false_down = !this.AV_show_anno;
        
        // want up && already up, or want down && already down, or
        // highlight_fcv == null, then do nothing: return
        if(true_up_false_down && this.AV_show_anno ||
           !true_up_false_down && !this.AV_show_anno ||
           !this.AV_highlight_fcv)
           return this;
           
        var H_delta = 250;
        if(true_up_false_down){ // now: down, change => up/show-anno
            this.upper_height = this.upper_height - H_delta;
            if(this.AV_highlight_fcv.model.type()==='txt')
                this.AV_bigshow.AV_editable.AF_set_text_height();
            this.AJ_upper.animate({'height': this.upper_height + 'px'}, 
                                 U.AV_animate_duration);
            this.lower_height = this.lower_height + H_delta;
            this.AJ_lower.animate({'height': this.lower_height + 'px'}, 
                                U.AV_animate_duration);
            this.AV_show_anno = true;
        } else { // now: up, change => down/no-show-anno
            this.upper_height = this.upper_height + H_delta;
            if(this.AV_highlight_fcv.model.type()==='txt')
                this.AV_bigshow.AV_editable.AF_set_text_height();
            this.AJ_upper.animate({'height': this.upper_height + 'px'}, 
                                U.AV_animate_duration);
            this.lower_height = this.lower_height - H_delta;
            this.AJ_lower.animate({'height': this.lower_height + 'px'}, 
                                U.AV_animate_duration);
            this.AV_show_anno = false;
        }
        return this;
    };// end of AC_DocPane.prototype.AF_lower_updown
    
    AC_DocPane.prototype.AF_setup_handlers = function(){
        var self = this;
        self.AJ_cancel_btn.unbind('click').bind('click', function(e){
            self.AF_hideme();
            return U.AF_stop_event(e);
        });
        self.AJ_close_btn.unbind('click').bind('click', function(e){
            self.AF_hideme();
            return U.AF_stop_event(e);
        });
        self.AJ_btnbar.unbind('click').bind('click', function(e){
            self.AF_lower_updown();
            return U.AF_stop_event(e);
        });
        self.AJ_save_btn.unbind('click').bind('click', function(e){
            if(FD.AF_se()){
                self.AF_save();
            } else {
                self.AF_hideme();
            }
            return U.AF_stop_event(e);
        });
        // if write allowed, input-field will not trigger show-anno action
        self.AJ_addbtn.css('visibility', self.AV_allow_write?
                            'visible' : 'hidden');
        if(self.AV_allow_write){
            var input_field = $('input#show-title-text');
            input_field.unbind('click').bind('click', function(e){
                return U.AF_stop_event(e); // stop propagation to button-bar
            });
        }
        if(self.AJ_select)
        self.AJ_select.unbind('change').bind('change', function(e){
            var item = M.e(self.AJ_select.val());
            self.AV_act = 'switch-doc';
            self.AV_new_it = item;
            FD.AV_lserver.AF_get_info(item, self);
            return U.AF_stop_event(e);
        });
        // have to unbind/bind event, otherwise the callback will be there
        // multiple times, and called multiple times too.
        self.AJ_file_input.unbind('change').bind('change', function(e){
            var file = this.files[0];
            var ext = file.name.split('.')[1];
            var fctype = U.AV_fc_type(ext);
            if(!fctype){
                alert('file type '+ ext +' not supported');
                U.log(1, 'file type not supported');
                return U.AF_stop_event(e);
            }
            if(fctype !== 'video'){
                FD.AV_wwp.AF_send_req(
                    // data-object
                    { cmd: 'upload-media', fid: FD.fid(), obj: file},
                    // callctx: func and its calling-context
                    {func: self.AF_wworker_handler, ctx: self});
            } else { // video is too large, use web-worker approach
                FD.AV_wwp.AF_send_req(
                    // data-object
                    {cmd: 'upload-media', fid: FD.fid(), obj: file},
                    // callingctx: func and its calling-context
                    {func: self.AF_wworker_handler, ctx: self});
            }
            return U.AF_stop_event(e);
        });
    };// end of AC_DocPane.prototype.AF_setup_handlers

    AC_DocPane.prototype.AF_wworker_handler = function(data, ctx){
        //var self = this;
        if(data.result.indexOf('fail') === -1){
            var rs = new RS({_id: M.mid(FD.AV_fa.fid(), 'RS'), 
                            mime: data.type,
                            size: '' + data.size, 
                            signature: '' + data.sig});
            if(ctx.process_rs){
                ctx.process_rs(true, rs, ctx);
            }
        }
        U.log(0, "web-worker(cmd==upload-media) said: " + data.result);
    };// end of AC_DocPane.prototype.AF_wworker_handler
    
    AC_DocPane.prototype.AF_setup_dimension = function(){
        this.pane_width = U.INT(this.AJ_container.css('width'));
        this.pane_height = U.INT(this.AJ_container.css('height'));
        this.upper_width = this.pane_width - 153;//178; 
        this.upper_height = this.pane_height - 69;
        this.lower_height = this.pane_height - this.upper_height - 34;
        this.AJ_ttlbar.css({'width': this.upper_width});
        this.AJ_lower.css({'width': this.upper_width + 'px', 
                         'height': this.lower_height + 'px'});
        this.AJ_btnbar.css('width', this.upper_width);
        this.AJ_upper.css({'width':this.upper_width,'height':this.upper_height});
        this.AV_show_anno = false;
    };// end of AC_DocPane.prototype.AF_setup_dimension

    // when hiding docpane -> it === null: recover fd.AV_tbar 
    AC_DocPane.prototype.AF_set_doctitles = function(){
        this.AV_lcode = this.fd.AF_lcode();
        // title-bar: logoword replaced with <pa-name>: <cat-name>
        {
            var i, txt = '', lst = this.it.AF_get_containers();
            this.AV_ownerpa = this.it.AF_ownerpa();
            this.fd.AV_tbar.AJ_logoword.text(this.AV_ownerpa.tname_face() +
                        ': '+this.fd.AV_lang.face(lst[0].name()));
            // local title bar: folder-path till it.name_face(), > delimitor
            for(i = 0; i<lst.length; i++){
                if(lst[i].cid() === 'IP') continue;
                txt += lst[i].name_face() + ' > ';
            }
            this.AJ_select = $('<select id="doc-select"></select')
                .appendTo(this.AJ_title_text.text(txt));
            var siblingits = this.it.owner().docs();
            for(i = 0; i<siblingits.length; i++){
                var opstr = "<option>"+siblingits[i].name_face()+"</option>";
                var op=$(opstr).appendTo(this.AJ_select)
                    .val(siblingits[i].id());
                if(siblingits[i] === this.it) 
                    op.prop('selected', true);
            }
            this.AJ_addbtn.attr('title',this.fd.AV_lang.face('M7128'));
        }
        this.AF_setup_ctxmenu(this.it);
    };// end of AC_DocPane.prototype.AF_set_doctitles
    
    AC_DocPane.prototype.AF_showme = function(it){
        // hiden away the f-pane and p-pane
        this.fd.AV_landing.AV_tabs.detach();
        this.fd.AJ_person_pane.detach();
        
        this.fd.AV_lang.AJ_lang_btn.css('visibility','hidden');
        this.fd.AV_tbar.AJ_searchbtn.detach();
        this.fd.AV_tbar.AJ_nav_back.detach();
        this.fd.AV_tbar.AJ_nav_fore.detach();
        this.fd.AJ_shift_btn_bg.detach();
        this.fd.AV_synop.AJ_div.detach();
        this.it = it.AF_get_firstdoc();

        this.AJ_docpane.appendTo(this.AJ_container);
        this.AF_init().AF_set_doctitles();
        this.AJ_upper.css('background-image','none').empty();
        $('#doc-close-btn', this.AJ_docpane) // mouse-over help-info: Close
            .attr('title', this.fd.AV_lang.face('M7106')); // "Close"
        this.AF_populate().AF_setup_handlers();
        return this.AF_block_save_btn(false);
    };// end of AC_DocPane.prototype.AF_showme
    
    AC_DocPane.prototype.AF_hideme = function(){
        this.fd.AV_tbar.AF_set_tooltip(); // recover fgraph title
        // recover title on tbar
        //.his.fd.AF_pa_theme(this.AV_ownerpa);
        // hide doc-pane
        this.AJ_docpane.detach();
        if($("div#synop").length === 0)
            this.fd.AV_synop.AJ_div.appendTo($('div#mw-frame'));

        // bring back family-pane and person-pane
        var container = $('div#body_container');
        this.fd.AV_lang.AJ_lang_btn.css('visibility','visible');
        this.fd.AV_landing.AV_tabs.appendTo(container);
        this.fd.AJ_person_pane.appendTo(container);
        var barbg = $('div#bar-bg');
        this.fd.AV_tbar.AJ_searchbtn.appendTo(barbg);
        this.fd.AV_tbar.AJ_nav_back.appendTo(barbg);
        this.fd.AV_tbar.AJ_nav_fore.appendTo(barbg);
        this.fd.AJ_shift_btn_bg.appendTo(container);
    };// end of AC_DocPane.prototype.AF_hideme

     // if the given file already in icon-list: no need to add it again
    AC_DocPane.prototype.AF_fc_inlist = function(fc){
        for(var i=0; i< this.fcvs.length; i++){
            if(fc.id() === this.fcvs[i].model.id())
                return true;
        }
        return false;
    };// end of AC_DocPane.AF_fc_inlist method

    // check if .fcvs matches .it.iddict.facets id-list. if not
    // return id-list from .fcvs
    AC_DocPane.prototype.AF_facets_changed = function(){
        var fcs = [];
        for(var i=0; i<this.fcvs.length; i++){
            fcs.push(this.fcvs[i].model.id());
        }
        var s = JSON.stringify(this.it.iddict['facets']);
        if(s !== JSON.stringify(fcs))
            return fcs;
        return null;
    };// end of AC_DocPane.prototype.AF_facets_changed

    AC_DocPane.prototype.AF_block_save_btn = function(v){
        if(typeof(v) === 'undefined') return this._block_save_btn;
        this._block_save_btn = v;
        return this.AF_update_save_btn();
    };// end of AC_DocPane.prototype.AF_block_save_btn
    
    AC_DocPane.prototype.AF_update_save_btn = function(){
        var disable = true;
        if(!this.AF_block_save_btn()){
            // fc added/delete/moved will let s1 !== s2: enable
            var new_fcs = this.AF_facets_changed();
            if(new_fcs){
                disable = false;
            } else {
                for(var i=0; i<this.fcvs.length; i++){
                    if(this.fcvs[i].newfc){
                        disable = false; break;
                    } else {
                        var fcv = this.fcvs[i];
                        if(Object.keys(fcv.dict).length > 0){
                            disable = false; break;
                        }
                        if(fcv.rsupdate){
                            disable = false; break;
                        }
                    }
                }
            }
        }
        var disable_clss = "ui-button-disabled ui-state-disabled";
        if(disable) this.AJ_save_btn.addClass(disable_clss);
        else        this.AJ_save_btn.removeClass(disable_clss);
        return this;
    };// end of AC_DocPane.prototype.AF_update_save_btn
    
    AC_DocPane.prototype.AF_populate = function(){
        this.AV_allow_write = M.OA(this.it);
        if(this.AV_allow_write){
            this.AJ_save_btn.css('visibility','visible');  //M1004: Save
            this.AJ_cancel_btn.css('visibility','visible');//M1003: Cancel
            $('span#doc-save-btn-label').text(this.fd.AV_lang.face('M1004'));
            $('span#doc-cancel-btn-label').text(this.fd.AV_lang.face('M1003'));
        } else {
            this.AJ_save_btn.css('visibility','hidden');
            this.AJ_cancel_btn.css('visibility','hidden');
        }
        this.AJ_ul.empty();
        this.fcvs.length = 0;
        var fcv, i, facets = this.it.facets();
        this.AJ_upper.empty();
        for(i=0; i<facets.length; i++){
            if(!facets[i].vmodel){
                fcv = new AC_FCVModel(this, facets[i]);
            } else {
                fcv = facets[i].vmodel.AF_init();
            }
            // show fcv in icon-list
            this.fcvs.push(fcv.AF_attachme(true));
        }
        if(this.fcvs.length > 0){
            this.fcvs[0].AF_set_highlight(true);
        }
        $('span#anno-label').text(this.fd.AV_lang.face('M7130'));
        return this.AF_update_save_btn(); // returns this
    };// end of AC_DocPane.prototype.AF_populate
    
    AC_DocPane.prototype.AF_save = function(){
        var fcs = this.AF_facets_changed();
        if(fcs){
            var iddict = $.extend(true, {}, this.it.iddict);
            iddict['facets'] = fcs;
            this.AV_actbuffer.AF_add_update(this.it, { 'iddict': iddict });
        }
        var req_dict = this.AV_actbuffer.AF_get_reqdic('update-itd');
        this.AV_act = 'save';
        this.fd.AV_lserver.AF_post('putdocs', req_dict, this);
    };// end of AC_DocPane.prototype.AF_save
    
    AC_DocPane.prototype.AF_handle_err = function(data){ alert(data); }
    
    AC_DocPane.prototype.AF_handle_success = function(data, self){
        self.AV_actbuffer.AF_execute();
        if(self.AV_act === 'save'){
            self.AV_actbuffer.AF_init();
            self.AF_showme(self.it);
        } else if(self.AV_act === 'switch-doc') {
            self.AF_showme(self.AV_new_it);
        }
    };// end of AC_DocPane.AF_handle_success
    
    return AC_DocPane;
})();