/* Copyright (C) 2014 Adacro Inc. - All Rights Reserved
 * -----------------------------------------------------
 * the square under title-bar, above infov, with portrait on the left, and
 * synop-box with (2/3) lines of text (name, dob-dod, pob) of pa info.
 * A singleton inst contained in FD. when focus a pav/bav: to be updated.
 * ---------------------------------------------------------------------------*/
var AC_Synop = (function() {
  function AC_Synop() {
    this.AJ_div = $('<div id="synop"></div>');
    // slide shield when showing nutshell slide-out. click-outside / close
    // button will destroy slide-out and the shield
    this.AJ_shield = $('<div class="shield"></div>')
      .appendTo("body")
      .css("display", "none");
    this.AF_load_template();
    this.AV_synopwizard = new AC_SynopWizard(this);
    this._show_state = "hidden"; // shown  hidden
    this.AF_init();
  } // end of AC_Synop constructor

  AC_Synop.prototype.AF_load_template = function() {
    this.AJ_picframe = $('<div id="picframe"></div>').appendTo(this.AJ_div);
    var m = $('<div id="portrait_box"></div>').appendTo(this.AJ_picframe);
    this.AJ_portrait = $('<img id="portrait"/>').appendTo(m);
    this.AJ_synopbox = $('<div id="synopbox"></div>').appendTo(this.AJ_div);
    this.AJ_trigger = $('<img class="pencil" id="edit_synop"/>')
      .appendTo(this.AJ_synopbox)
      .css("visibility", "hidden")
      .attr("src", "pics/pencil.jpg");
    var d1 = $('<div id="info-line1"></div>').appendTo(this.AJ_synopbox);
    this.AJ_tname = $('<span class="LS"></span>').appendTo(d1);

    this.AV_parchive_title = new AC_SynopParchiveTitle(this);

    /* prepare slide2 layout, not yet attached to jdiv
         * synop has  2 slides: slide1 is a 3px thick line that stretch out 
         * when a person's picture frame is clicked, triggering slide 2 
         * dropping down from the line. both slides are attached to synop.AJ_div 
         * when a pa is in.
         */
    this.AJ_slide1 = $('<div class="slide1"></div>');
    this.AJ_slide2 = $('<div class="slide2"></div>');
    var upper = $('<div class="upper"></div>').appendTo(this.AJ_slide2);
    var lower = $('<div class="lower"></div>').appendTo(this.AJ_slide2);
    $('<span class="LS" tag="M7105" style="font-size:smaller;"></span>')
      .appendTo(upper)
      .text("About");
    $('<span id="title-name"></span>').appendTo(upper);
    var btn = $('<button class="sld-close"></button>').appendTo(upper);
    $('<span class="LS" tag="M7106">Close</span>').appendTo(btn);
  }; // end of AC_Synop.prototype.AF_load_template

  AC_Synop.prototype.AF_show_state = function(v) {
    if (typeof v === "undefined")
      // getter
      return this._show_state;
    if (v === this._show_state)
      // no change
      return this; // do nothing
    // setter
    if (v === "shown" && $("#synop").length === 0) {
      // need turn to show
      this.AJ_div.insertAfter("#mw-titlebar");
      this._show_state = v;
    } else if (v === "hidden" && $("#synop").length > 0) {
      this.AJ_div.detach();
      this._show_state = v;
    }
    return this;
  }; // end of AC_Synop.prototype.AF_show_state

  // ------------------------------------------------------------------------
  // Purpose: (renewed) attach jbgframe, jsynopbox, nutshell-slides;
  // init is NOT-pa specific, and is called for a login session,
  // before update_pa() for a pav assigned to this.pav
  // --------------
  AC_Synop.prototype.AF_init = function(pav) {
    this.pav = pav;
    if (pav) {
      this.AV_parchive_title.AF_update_title_showstate(pav);
      this.AF_update_pa();
      this.AF_setup_nutshell_slider(this.AJ_picframe);
      this.AF_show_state("shown").AF_set_tooltip();
    }
    return this;
  }; // end of AC_Synop.AF_init method

  AC_Synop.prototype.AF_set_tooltip = function() {
    this.AJ_picframe.attr("title", FD.AV_lang.face("M7138"));
  }; // end of AC_Synop.prototype.AF_set_tooltip

  // ------------------------------------------------------------------------
  // Purpose: update_pa synop-visuals for a person in focus
  // jsynopbox, jbgframe must be in jdiv now, and setup_nutshell_slider
  // has been called before. Also, depends on pa's access write,
  // synpwizard maybe setup for editing synop content.
  // -------------------------------------------------
  AC_Synop.prototype.AF_update_pa = function() {
    var pa = this.pav.model,
      keys = [
        "tname",
        "dob",
        "dod", // date of birth, date of death
        "pob",
        "pod", // place of birth, place of death
        "portrait", // portrait picture (RS)
        "nutshell"
      ]; // short-info (TD)
    // hide the pencil/trigger icon
    this.AJ_trigger.css("visibility", "hidden");
    if (M.OA(this.pav)) {
      // let dialog-trigger icon(pencil) appear
      if (this.AJ_synopbox.find("#edit_synop").length === 0) {
        this.AJ_trigger.appendTo(this.AJ_synopbox);
      }
      this.AJ_trigger.css("visibility", "visible");
      var self = this;
      this.AJ_trigger.unbind("click").bind("click", function(e) {
        if (pa) {
          // M7129: update (pav)
          self.AV_synopwizard.AF_bind2dlg("M7129", self.pav);
        } else {
          self.AV_synopwizard.AF_bind2dlg(
            // M7135: add-male; M7134: add-female
            self.pav.sex() === "male" ? "M7135" : "M7134",
            self.pav
          ); //
        }
        return U.AF_stop_event(e);
      });
    }
    if (!pa) {
      // pav == "unknown-male" or "unknown-female"
      this.AJ_tname.text(FD.AV_lang.face("M7137"));
      var sex = this.pav.sex();
      var picname = sex === "male" ? "pics/boy.jpg" : "pics/girl.jpg";
      this.AJ_portrait.attr("src", picname);
      return;
    }

    for (var i = 0; i < keys.length; i++) {
      switch (keys[i]) {
        case "tname":
          var tagstr = pa.id() + "@" + "tname_face";
          this.AJ_tname.attr("tag", tagstr).text(pa.tname_face(FD.AF_lcode()));
          break;
        case "dob": // date of birth
          this.AJ_synopbox.find('#info-line2').remove();
          if(pa.dob().length > 0){
            var d2= $('<div id="info-line2"></div>').appendTo(this.AJ_synopbox);
            this.AJ_padob = $("<span></span>")
              .appendTo(d2)
              .text(U.AF_date_dashed(pa.dob(), "."));  
          }
          break;
        case "pob": // place of birth
          this.AJ_synopbox.find('#info-line3').remove();
          if(pa.pob_face(FD.AF_lcode()).length > 0){
            var tagstr = pa.id() + "@" + "pob_face";
            var msg = pa.pob_face(FD.AF_lcode()).replace(",", " ");
            if (msg.length > 24) msg = msg.substr(0, 22);
            var d3= $('<div id="info-line3"></div>').appendTo(this.AJ_synopbox);
            this.AJ_papob = $('<span class="LS"></span>')
              .appendTo(d3)
              .text(msg)
              .attr("tag", tagstr);  
          }
          break;
        case "dod": // date of death
          this.AJ_synopbox.find('#info-line4').remove();
          if(pa.dod().length > 0){
            var d4= $('<div id="info-line4"></div>').appendTo(this.AJ_synopbox);
            this.AJ_padod = $('<span></span>')
              .appendTo(d4)
              .text(U.AF_date_dashed(pa.dod(), "."));  
          }
          break;
        case "pod": // place of death
          this.AJ_synopbox.find('#info-line5').remove();
          if(pa.pod_face(FD.AF_lcode()).length > 0){
            var tagstr1 = pa.id() + "@" + "pod_face";
            var msg = pa.pod_face(FD.AF_lcode());
            if (msg.length > 24) msg = msg.substr(0, 24);
            var d5= $('<div id="info-line5"></div>').appendTo(this.AJ_synopbox);
            this.AJ_papod = $('<span class="LS"></span>')
              .appendTo(d5)
              .attr("tag", tagstr1)
              .text(msg);
            }
          break;
        case "portrait":
          if (pa.sex() === "male") {
            this.AJ_portrait.attr("src", "pics/boy.jpg");
          } else {
            this.AJ_portrait.attr("src", "pics/girl.jpg");
          }
          var rs = pa.portrait();
          /* "https://royalfolder.blob.core.windows.net/tc6h8eu/N";
                 where N=<size>-<signature>.<ext>
                */
          if (rs && rs.signature()) {
            AC_BinObjMgr.AF_feed_media(this.AJ_portrait, rs);
          }
          break;
        case "tx":
          break; // no need to update_pa visual for tx
        case "nutshell":
          break; // td.content will be read when needed
        default:
          U.log(1, "synop update key unknow: " + keys[i]);
          break;
      }
    } // for(var i=0; i<keys.length; i++)

    // update slide2 title to be "About <pa.tname_face()>"
    this.AJ_div.find("span#title-name")
      .attr({ class: "LS", tag: pa.tname() })
      .text(pa.tname_face(FD.AF_lcode()));
  }; // end of AC_Synop.AF_update_pa method

  AC_Synop.prototype.AF_close_sliders = function(e) {
    var self = this;
    $("body").unbind("keydown");
    self.AJ_slide1.hide();
    self.AJ_slide2.hide();
    if (self.AJ_shield) self.AJ_shield.css("display", "none");
    return U.AF_stop_event(e);
  }; // end of AC_Synop.prototype.AF_close_sliders

  // ------------------------------------------------------------------------
  // setup the slid-out about pane, with animation. This does not depend
  // on the presence of pa, the init(pa) will update pa info in the slides
  // ---------------------------------------------------------------------
  AC_Synop.prototype.AF_setup_nutshell_slider = function(picframe) {
    var self = this;
    this.AJ_slide2.find("button")
      .unbind("click")
      .bind("click", function(e) {
        self.AF_close_sliders(e);
      });
    // bind the show-nutshell call to picture frame
    picframe.unbind("click").bind("click", function(e) {
      // first update nulshell_content to current  lcode, then slide out
      var pa = self.pav.model,
        td = pa.nutshell();
      if (td) {
        self.AV_nutshell_content = td.AF_content_face();
        FD.AV_lang.AF_set_all(self.AJ_slide2, FD.AF_lcode());
        self.AJ_slide2.find("#title-name").text(pa.tname_face());
        self.AF_show_nutshell(self.AV_nutshell_content);
      }
      return U.AF_stop_event(e);
    });
  }; // end of AC_Synop.AF_setup_nutshell_slider method

  // click event-handler: showing the nutshell info of the person
  // in the slide-out.
  AC_Synop.prototype.AF_show_nutshell = function(content) {
    var self = this;
    $("body").bind("keydown", function(e) {
      if (e.keyCode == 27) {
        self.AF_close_sliders(e);
      }
    });
    self.AJ_slide1.appendTo(self.AJ_div);
    self.AJ_slide2.appendTo(self.AJ_div);

    if (!content) return;
    //if(content.indexOf('[') === -1) // not base64? then use it directly
    //    content = Base64.decode(content).replace(/\n/g,'<br>');
    self.AJ_slide2.find("div.lower").html(content);
    self.AJ_slide1.show(
      "slide", // first slide a rod
      { direction: "right" }, // towards left
      200, // in 200 msecs, when done,
      function() {
        // then sld2's slide starts
        self.AJ_slide2.show("slide", { direction: "up" }, 700, function() {
          // after sld2 finished, set jshield to be
          // 1. visible; 2. handle click event to close sld2
          // that means, a click outside of sld2 will close it
          if (self.AJ_shield && self.AJ_shield.css("display") === "none") {
            self.AJ_shield.css("display", "block");
            self.AJ_shield.unbind("click").bind("click", function(e) {
              self.AF_close_sliders(e);
            });
          }
        });
      }
    );
  }; // end of AC_Synop.AF_show_nutshell method
  return AC_Synop;
})();

// personal-archive-title under synop-box
var AC_SynopParchiveTitle = (function() {
  AC_SynopParchiveTitle = function(AV_synop) {
    this.AV_syn = AV_synop;
    this.AJ_div = $('<div id="parchive-title"></div>').appendTo(
      AV_synop.AJ_div
    );
    this.AJ_text = $(
      '<span id="parchive-title-text" class="LS" tag="M7000"></span>'
    ).appendTo(this.AJ_div);
    this.AJ_back2left = $(
      '<img id="back2left" src="pics/arrow_left.png"/>'
    ).appendTo(this.AJ_div);
    this.AJ_slide_button = $('<div id="ppane-slide-button"></div>').appendTo(
      AV_synop.AJ_div
    );
  }; // end of AC_SynopParchiveTitle constructor

  // set this.AV_shown to
  // true: pa does have ip(s) to show, or pa is logged in - show-up
  // false: pa not logged-in, and has no ip for showing   - hidden
  // return this
  AC_SynopParchiveTitle.prototype.AF_update_title_showstate = function(pav) {
    this.AV_shown = false;
    if (!pav.AF_is_virtual()) {
      var pa = pav.model;
      if (FD.AV_loginpv && FD.AV_loginpv.model.id() === pa.id()) {
        this.AV_shown = true;
      } else {
        this.AV_shown =
          pa.ips().length > 1 ||
          (pa.ips().length === 1 && pa.ips()[0].name() !== "M7014");
      }
    }
    return this.AF_update_slide_button();
  }; // end of AC_SynopParchiveTitle.prototype.AF_update_title_showstate

  // initial setting for a personal-archive-title of a new synp.pav.
  // if pa has ip(s) to show, or pa is logged in: show slide-button,
  // and the title-bar (under slide-button, z-index-wise), return true
  // otherwise, hiden slide-button/parchive-title div, and return false
  AC_SynopParchiveTitle.prototype.AF_update_slide_button = function() {
    // AF_update_showstate sets .AV_shown to be true/false
    if (this.AV_shown) {
      if ($("div#parchive-title").length === 0)
        this.AJ_div.appendTo(this.AV_syn.AJ_div);
      this.AJ_slide_button.css("visibility", "visible");
      this.AF_reset();
    } else {
      this.AJ_div.detach();
      this.AJ_slide_button.css("visibility", "hidden");
    }
    return this;
  }; // end of AC_SynopParchiveTitle.prototype.AF_update_slide_button

  // 1. !title: set to initial: M7000(personal-archive), and
  //    hiden this.AJ_back2left, show this.AJ_slide_button
  // 2. title is M7100(Category) or ip.name(), show it as title and,
  //    show this.AJ_back2left, hiden this.AJ_slide_button
  AC_SynopParchiveTitle.prototype.AF_reset = function(AV_title) {
    if (this.AV_shown) {
      var self = this;
      if (!AV_title) {
        // case 1: AV_title not given - set inital state
        // M7000: personal archive
        this.AJ_text.attr("tag", "M7000").text(FD.AV_lang.face("M7000"));
        this.AJ_back2left.css("visibility", "hidden");
        this.AJ_slide_button.css("visibility", "visible");
        this.AJ_slide_button.unbind("click").bind("click", function(e) {
          FD.AV_infov.AF_init(self.AV_syn.pav.model, self);
          return U.AF_stop_event(e);
        });
      } else {
        // case 2: AV_title given
        this.AJ_text.text(FD.AV_lang.face(AV_title));
        this.AJ_slide_button.css("visibility", "hidden");
        this.AJ_back2left.css("visibility", "visible");
      }
    }
    return this;
  }; // end of AC_SynopParchiveTitle.prototype.AF_reset

  return AC_SynopParchiveTitle;
})();
