/*
 * Copyright (C) 2014 Adacro Inc. - All Rights Reserved
 */
var AC_Lang = (function(){
    function AC_Lang(fd, lcode){
        this.AV_lcode = lcode;
        this.fd = fd;
        this.AJ_container = $('<div class="list-container"></div>');
        this.AJ_container.css('z-index','1000001');
        this.AJ_lang_btn = $('button.lang-trigger');
        this.AJ_btn_img = this.AJ_lang_btn.find('img');
        this.AJ_ul = $('<ul></ul>').appendTo(this.AJ_container);
        this.entries = {};

        var self = this;
        this.AJ_lang_btn.click(function(e){
            self.AF_toggle_visibility();
            return U.AF_stop_event(e);
        });
    };// end of AC_Lang constructor

    AC_Lang.prototype.AF_all_lcodes = function(){
        return [ // arry specifies the order of flags in offering list
            'en',  /** 1:  Englisg             */
            'zh',  /** 2:  Chinese             */
            'de',  /** 3:  German              */
            'fr',  /** 4:  French              */
            'it',  /** 5:  Italian             */
            'es',  /** 6:  Spanish             */
            'pt',  /** 7:  Portuguese          */
            'ru',  /** 8:  Russina             */
            'hi',  /** 9:  Hindi               */
            'ja',  /** 10: Japanese            */
            'ko',  /** 11: Korean              */
            'vi',  /** 12: Vietnamese          */
            'tl',  /** 13: Filippino/Tagalog   */
            'th',  /** 14: Thai                */
            'id',  /** 15: Idonesian           */
            'ar',  /** 16: Arabic              */
            'ur',  /** 17: Urdu                */
            'he',  /** 18: Hebrew              */
            'fa',  /** 19: Farci               */
            'hu',  /** 20: Hungarian           */
            'nl',  /** 21: Dutch               */
            'sv',  /** 22: Swedish             */
            'fi',  /** 23: Finish              */
            'no',  /** 24: Norwegen            */
            'pl',  /** 25: Polish              */
            'el',  /** 26: Greek               */
            'cs',  /** 27: Czech               */
            'tr',  /** 28: Turkish             */
        ];
    };// end of AC_Lang.prototype.AF_all_lcodes

    // after LS-icons is in , language choices(flag-name) entries 
    // a drop down list, can be shown
    AC_Lang.prototype.AF_populate_choices = function(){
        if(Object.keys(this.entries).length > 0 && this.ls)
            return this;
        var flags_ls = this.fd.AV_store.AF_getEntity('FTSUPER-LS-icons'),
            i, flags = this.AF_all_lcodes(); 
        // set English as default lang first    
        this.ls = this.fd.AV_store.AF_getEntity('FTSUPER-LS-en'); 
        if(flags_ls){
            var dic = flags_ls.icon_dic(), fn, i;
            for(i=0; i<flags.length; i++){
                if(flags[i] in dic){
                    fn = 'pics/' + dic[flags[i]];
                    this.entries[flags[i]] = new AC_LangBox(this, flags[i], fn);
                }
            }
        }
        return this;
    };// end of AC_Lang.prototype.AF_populate_choices

    AC_Lang.prototype.AF_icon_filepath = function(lc){
        if(lc) 
            return this.entries[lc].AJ_imgname;  
        else if(this.AV_lcode && this.AV_lcode in this.entries)
            return this.entries[this.AV_lcode].AJ_imgname;
        return null;
    };// end of AC_Lang.prototype.AF_icon_filepath

    // toggle the visibility of lang-choice box
    AC_Lang.prototype.AF_toggle_visibility = function(){
        var list = $('div.list-container');
        if(list.length > 0){
            this.AJ_container.detach();
        } else {
            var self = this, 
                container = $('div#body_container');
            this.AJ_container.appendTo(container);
            var h = U.INT(container.css('height')) - 4,
            h1 = U.INT(this.AJ_container.css('height'));
            if(h1 > h)
                this.AJ_container.css('height', h + 'px');
            this.AJ_lang_btn.unbind('click').bind('click', function(e){
                self.AF_toggle_visibility();
                return U.AF_stop_event(e);
            });            
            var bg = $('<div class="contextMenuPluginShield"></div>')
                .css({left:0, top:0, width:'100%', height:'100%', 
                    position:'absolute', zIndex:1000000})
                .appendTo(document.body)
                .unbind('click').bind('click', function() {
                    bg.remove();
                    self.AJ_container.detach();
                    return false;
                });
        }
        return this;
    };// end of AC_Lang.prototype.AF_toggle_visibility
    
    AC_Lang.prototype.AF_switch_lang = function(AV_lcode){
        this.AV_lcode = AV_lcode;
        var ids = [];
        FD.AV_store.AF_add2lst('FTSUPER-LS-'+AV_lcode, ids);
        this.fd.AV_lserver.AF_getdics_helper(ids, 'get-lang-ls', this);
    };// end of AC_Lang.AF_switch_lang method

    // new lang-ls fetched
    AC_Lang.prototype.AF_handle_success = function(data, self){
        var lsid = 'FTSUPER-LS-' + self.AV_lcode;
        var src = self.AF_icon_filepath()? self.AF_icon_filepath() : null;
        self.ls = M.e(lsid);
        if(src) 
            self.AJ_lang_btn.css(
                'background-image', 'url("'+ self.AF_icon_filepath() +'")');
        // set text-title for all language choice (dropdown) entries
        for(var lc in self.entries){
            self.entries[lc].AF_set_lang_title();
        }
        self.AF_set_all();
        // switch lang for all pav>patag/bav>sgrps that are shown
        if(self.fd.AF_tpath()){
            var vs = self.fd.AF_tpath().AF_all_viewables();
            for(var i=0; i< vs.length; i++){
                if(vs[i].cid() == 'BAV'){
                    if(vs[i].AF_sgroup().AV_siblings.length > 0)
                        vs[i].AF_sgroup().AF_set_tooltip();
                } else if(vs[i].cid() === 'PAV'){
                    vs[i].AV_patag.AF_set_tooltip().AF_tname();
                }
            }
        }
        if(self.fd.AV_tbar)
            self.fd.AV_tbar.AF_set_tooltip();
        if(self.fd.AV_synop)
            self.fd.AV_synop.AF_set_tooltip();
        if(self.fd.AV_infov)
            self.fd.AV_infov.AF_set_tooltip();
    };// end of AC_Lang.prototype.AF_handle_success
    
    // ------------------------------------------------------------------------
    // mid is msg-id like M1001, or <ent-id>@<face-function-name>
    // alt offers a msg string in case not in ls
    // --------------------------------------
    AC_Lang.prototype.face = function(mid, alt){
        if($.isArray(mid)){
            var i, AV_msg = '';
            for(i=0; i<mid.length; i++){
                AV_msg += this.face(mid[i]) + ' ';
            }
            return AV_msg;
        } else if(typeof(mid) === 'string'){
            var splt = mid.split(' ');
            if(splt.length === 1)
                return this.ls.face(mid, alt);
            else
                return this.face(splt);
        }
    };// end of AC_Lang.prototype.face method
    
    AC_Lang.prototype.AF_set_all = function(AV_ctxt){
        if(!this.ls) return;
        var self = this, i, ent;
        for(i=0; i<5; i++){
            ent = this.fd.AV_landing.AF_page(true, i);
            if(ent && ent.AV_lcode && ent.AV_lcode !== this.AV_lcode)
                ent.AV_lcode = this.AV_lcode;
            ent = this.fd.AV_landing.AF_page(false, i);
            if(ent && ent.AV_lcode && ent.AV_lcode !== this.AV_lcode)
                ent.AV_lcode = this.AV_lcode;
        }        
        $('.LS', AV_ctxt).each(function(){
            // get tag, or if not existing, text content if $(this)
            var AV_ltag = $(this).attr('tag') || $(this).innerHTML;
            var AV_msg = 'unknown';
            AV_msg = self.face(AV_ltag);
            if($(this).hasClass('ui-button')){
                // in case of ui-button, it is inside .LS element
                var ele = $(this).find('.ui-button-text');
                ele.text(AV_msg);
            } else {
                // $(this) is the .LS element
                // don't do for for tname of patag(has fullname class)
                // that are done in this.AF_switch_lang taking care of short_name
                if($(this).hasClass('fullname'))
                    $(this).text(U.AF_shorten_name(AV_msg));
                else
                    //$(this).text(AV_msg);
                    $(this).html(AV_msg);
            }
        });
        ent = this.fd.AV_landing.AF_page();
        if(ent.AF_init)
            ent.AF_init();
    };// end of AC_Lang.AF_set_all method

    /* ------------------------------------------------------------------------
     * localize jquery-ui dialog. id is the id set to the div.ui-dialog, which 
     * can found by 1. jdiv.dialog('widget'); or 2. jdiv.parent(), where
     * jdiv is the app-defined content-div.
     * dict can have two kinds of keys: 1. '.ui-dialog-title' which is the
     * class of a span for title text container; 2. '#*button*' which I can
     * define when defining buttons in the dialog
     * -------------------------------------------*/
    AC_Lang.prototype.AF_set_dlg = function(AV_dlg_id, dict, given_ls){
        var root = $('div#'+AV_dlg_id),
            ls = given_ls? given_ls : this.ls;
        for(var k in dict){
            if(k === '.ui-dialog-title'){
                var jv = root.find(k);
                jv.text(ls.face(dict[k]));
            } else if(k.indexOf('button') > -1){
                var jv = root.find(k + ' span');
                jv.text(ls.face(dict[k]));
            }
        }
        // find all eles of class LS, get its tag, set text with face(tag)
        root.find('.LS').each(function(){
            var t = $(this).attr('tag');
            $(this).text(ls.face(t));
        })
        var close_face = $('button.ui-dialog-titlebar-close');
        close_face.attr('title', ls.face('M7106'));
    };// end of AC_Lang.AF_set_dlg method
    
    return AC_Lang;
})();

var AC_LangBox = (function(){
    function AC_LangBox(lang,      // parent: inst of AC_Lang
                        lcode,     // en | zh | de |...
                        filepath){ // icon file path
        this.AV_lang = lang;
        this.lang_id = lcode;
        var jli = $('<li class="country"></li>').appendTo(lang.AJ_ul);
        this.jbox = $('<div class="country-box"></div>').appendTo(jli);
        this.AJ_imgname = filepath;
        $('<img src="'+ this.AJ_imgname +'"/>').appendTo(this.jbox);
        this.jlang_name = $('<span class="lang-name"></span>')
            .appendTo(this.jbox);
        this.AF_set_lang_title();
            
        var self = this;
        jli.click(function(){
            self.AV_lang.AJ_lang_btn.css('background-image',
                                    'url("'+self.AJ_imgname+'")');
            self.AV_lang.AF_switch_lang(self.lang_id);
            self.AV_lang.AF_toggle_visibility();
        });
    };// end of AC_LangBox constructor

    // ------------------------------------------------------------------------
    // set the title text in box
    // ----------------------------------------------
    AC_LangBox.prototype.AF_set_lang_title = function(){
        var txt = 'unknown'; 
        txt = this.AV_lang.ls.face(this.lang_id);
        this.jlang_name.text(txt);
    };// end of .AC_LangBox.AF_set_lang_title method
        
    return AC_LangBox;
})();
