/*
 * Copyright (C) 2014 Adacro Inc. - All Rights Reserved
 */
var AC_ItemWizard = (function(){
    function AC_ItemWizard(fd, itempane){//itempane: Cat-Content-VModel: parent
        this.fd = fd;
        this.AV_itempane = itempane;
        this.AV_lcode = this.fd.AF_lcode();
        this.it;        // for holding newly created
        this.cont;      // container of it
        this.AV_actbuffer = this.fd.AV_actbuffer;
        this.AJ_lcpicker_img = $('<img class="language-picker"></img>');
        this.delta_dic = {};
        var self = this;
        if(!this.AJ_div){
            $.get("/dlgs/iwz.html",'',
                function(data){
                    self.AJ_div = $(data); // mark-up for content part
                    self.AJ_name = self.AJ_div.find('input#add-entry-name');
                    self.AJ_pk = self.AJ_div.find('select#add-entry-pk');
                    self.AJ_descr = self.AJ_div.find('input#add-entry-descr');
                    self.AJ_folder_div = self.AJ_div.find("div#folder-pk-icon");
                    self.AJ_folder_lock = self.AJ_div.find("img#lock-icon");
                    self.AJ_folder_lock.attr('src','pics/lockIcon.png');
                    self.AJ_pwd_label = self.AJ_div.find('span#pwd-label');
                    self.AJ_pwd = self.AJ_div.find('input#entry-password');
                    self.AJ_expdate = self.AJ_div.find('input#pwd-expire-date');
                    self.AJ_selpostexp_label= self.AJ_div.find('span#postpk-label');
                    self.AJ_selpostexp = self.AJ_div.find('select#sel-postpk');
                }
            ).fail(function(e){
                alert("server error on getting itemwizard.html");
            });
        }
    };// end of AC_ItemWizard constructor

    AC_ItemWizard.prototype.AF_lang_pick = function(lcode, self){
        self.AJ_lcpicker_img.attr('src',FD.AV_lang.AF_icon_filepath(lcode));
        self.AV_lcode = lcode;
        // show the name and title of the chosen lcode, if existing; if not
        // then the current text in input box will remain, for to be saved.
        if(lcode in self.it.name()){
            self.AJ_name.val(Base64.decode(self.it.name()[self.AV_lcode]));
        }
        if(lcode in self.it.title()){
            self.AJ_descr.val(Base64.decode(self.it.title()[self.AV_lcode]));
        }
        // clear existing keys in delta_dic. In case it is not the first time
        // the user choose the saving-language choice, there may be 
        // .name and .title delta from prev language choice(s)
        // (Only name and title have language choices). clear out them in 
        // delat-dic. Use AV_lcode for testing delta
        self.AF_update_delta(['name','title']);
    };// end of AC_ItemWizard.prototype.AF_lang_pick

    AC_ItemWizard.prototype.AF_reset_visibilities = function(pk, lock){
        this.AJ_folder_div.css('visibility','hidden');// folder-icon hidden
        this.AJ_pk.css('visibility','hidden');
        $('#pk-label').css('visibility','hidden');  // pk label hidden
        this.AJ_pwd.css('visibility','hidden');   // hide pwd input
        this.AJ_pwd.attr('autocomplete','off');
        this.AJ_pwd_label.css('visibility','hidden'); // hide pwd input
        this.AJ_folder_lock.css('visibility','hidden'); // no lock icon
        this.AJ_expdate.css('visibility','hidden');// no exp-date input
        this.AJ_selpostexp.css('visibility','hidden');// no post.pk
        this.AJ_selpostexp_label.css('visibility','hidden'); // no label
        $('span#share-url').css('visibility','hidden').text(this.url);
        $('span#share-url-label').css('visibility','hidden');
        $('#doc-iw-info').css('visibility','hidden');
        $('#doc-introduction').css('visibility','hidden')

        if(this.it.type()[0] === 'D'){
            $('#doc-iw-info').css('visibility','visible');
            $('#doc-introduction').css('visibility','visible')
                .html(this.fd.AV_lang.face('M6005'));        
        } else { // type === "F"
            this.AJ_folder_div.css('visibility','visible'); // folder-icon shown
            $('#pk-label').css('visibility','visible');   // pk label shown
            this.AJ_pk.val(pk).css('visibility','visible');
            var iconurl ='';
            if(pk === 'M7117') { // public access
                iconurl = 'url(/pics/folder-icon.png)'; // public folder icon
                $('span#share-url').css('visibility','visible').text(this.url);
                $('span#share-url-label').css('visibility','visible');
            } else if(pk === 'M7118'){ // private access
                iconurl = 'url(/pics/folder-picon.png)'; // private folder icon
            } else if(pk === 'M1513'){ // pwd-protection
                iconurl = 'url(/pics/folder-icon.png)';// public folder with
                this.AJ_folder_lock.css('visibility','visible'); // lock icon on
                this.AJ_pwd.css('visibility','visible');
                this.AJ_pwd_label.css('visibility','visible'); 
                if(lock && lock.length === 3){
                    this.AJ_pwd.val(lock[0]);
                    if(lock[0].length > 0){// when pwd exists, show exp-date
                        this.AJ_expdate.val(lock[1]).css('visibility','visible');
                        this.AJ_pwd.css('background-color','white');
                    } else {
                        this.AJ_pwd.css('background-color','pink');
                    }
                    if(lock[1].length > 0){ // expiry date given
                        this.AJ_selpostexp.css('visibility','visible');//post.pk
                        this.AJ_selpostexp_label.css('visibility','visible');
                    } else {
                        this.AJ_expdate.attr('placeholder', 
                                              this.fd.AV_lang.face('M6004'));
                    }
                }
            }
            this.AJ_folder_div.css('background-image', iconurl);//show right icon
        }
        return this;
    };// end of AC_ItemWizard.prototype.AF_reset_visibilities
    
    AC_ItemWizard.prototype.AF_enable_OK_btn = function(force_onoff){
        if(force_onoff && typeof(force_onoff) === 'boolean'){
           $('#itemwizard-ok-button').button(force_onoff? 'enable' : 'disable');
        } else {
            if( this.mode === 'add' && this.AJ_name.val().trim().length > 0 ||
                Object.keys(this.delta_dic).length > 0 &&
               !(this.AJ_pk.val() == 'M1513' && this.lock[0].length === 0)){
                $('#itemwizard-ok-button').button('enable');

            } else
                $('#itemwizard-ok-button').button('disable');
        }
        return this;
    };// end of AC_ItemWizard.prototype.AF_enable_OK_btn
    
    AC_ItemWizard.prototype.AF_folder_event_handlers = function(){
        var self = this;
        this.AJ_pk.unbind("change").bind("change", function(e){
            var pk = self.AJ_pk.val();
            self.lock = pk === 'M1513'? ['','',''] : [];
            self.AF_reset_visibilities(pk, self.lock).AF_update_delta();
        });
        this.AJ_expdate.datepicker({dateFormat: "yy-mm-dd", // 2013-04-17
            changeYear: true,
            yearRange: "+0:+0", 
            changeMonth: true,
            regional: this.fd.AF_lcode()
        });
        // password input event: if input not empty: show expire-date/label
        // otherwise hide them
        this.AJ_pwd.unbind('input').bind('input',function(e){
            if(self.AJ_pwd.val().length > 0 && self.AJ_pwd.val()[0] === '*')
                self.AJ_pwd.val('');
            self.lock[0] = $.trim(self.AJ_pwd.val());
            self.AF_reset_visibilities(self.AJ_pk.val(), self.lock)
                .AF_update_delta();
        });
        // expire-date event: if in input date in future:
        // show post expire act sel(and the label). otherwise, hiden them
        var nowstr = TS(true);    // short=true: 20141108
        this.AJ_expdate.unbind('change').bind('change', function(e){
            var dstr = $.trim(U.AF_date_undashed(self.AJ_expdate.val()));
            if(dstr === '' || dstr <= nowstr){ // not in the future?
                self.AJ_expdate.val('');         // set back then
                self.AJ_selpostexp.val('');
            }
            self.lock = [self.AJ_pwd.val(), 
                         self.AJ_expdate.val(),
                         self.AJ_selpostexp.val()];
            self.AF_reset_visibilities($.trim(self.AJ_pk.val()), self.lock)
                .AF_update_delta();
        });
        return this;
    };// end of AC_ItemWizard.prototype.AF_folder_event_handlers 
    
    // set up dialog visuals when opening the popup
    AC_ItemWizard.prototype.AF_opening_setups = function(mode){
        var self = this;
        if(mode === 'add'){ // adding a new item
            this.AJ_name.val('');
            this.AJ_descr.val('');
            this.AJ_pwd.val('');
            this.AJ_expdate.val('');
        } else {            // edit existing item
            this.AJ_name.val(Base64.decode(this.it.name()[this.AV_lcode]));
            this.AJ_descr.val(Base64.decode(this.it.title()[this.AV_lcode]));
        }
        this.url = self.AF_get_url();
        this.fd.AV_lang.AF_set_all(self.AJ_layout, self.AV_lcode);
        this.AF_reset_visibilities(self.it.pk(), self.it.lock());
        if(self.it.type()[0] === 'F'){
            this.AF_folder_event_handlers();
            this.lock = this.it.lock().slice();
        }
        this.AJ_name.unbind('input').bind('input',function(e){
            self.AF_update_delta();
        });
        this.AJ_descr.unbind('input').bind('input',function(e){
            self.AF_update_delta();
        });
        return this.AF_enable_OK_btn(false); // disable OK button: no change(yet?)
    };// end of AC_ItemWizard.prototype.AF_opening_setups
    
    AC_ItemWizard.prototype.AF_get_url = function(){
        return document.location.host + '/?docview=' +
                    this.it.id()+'&lcode='+this.fd.AF_lcode();
    };// end of AC_ItemWizard.prototype.AF_get_url 
    
    // ------------------------------------------------------------------------
    // popup dialog for creating a new it(folder/document) or edit old one
    // with mode==add,itemtype can be: M7127(new folder) or M7128(new document)
    // with mode==edit, itemtype: M7107(folder) or M7108(Document)
    // ent can be it(F) or itD, where a new it is created/old one editted
    // -------------------------------------------------------------------
    AC_ItemWizard.prototype.AF_popup_dialog = function(
             ent,     // mode==edit: it for update; mode==add: container ip/it
             itemtype,// mode==add: M7127(New folder), M7128(New document)
                      // mode==edit: M7107(Folder), M7108(Document)
             mode){   // edit/add
        this.mode = mode; // "edit" or "add"
        this.AV_actbuffer.AF_init();

        // make sure dialog language is the same as global lcode
        this.AV_lcode = this.fd.AF_lcode();
        var self = this, title_key = itemtype;
        self.dlg = self.AJ_div.dialog({
            title: '',    // will be replace by this.fd.AV_lang.AF_set_dlg
            autoOpen: false,
            width: 650,
            height: 420,
            modal: true,
            show: false,         // animated going away
            close: function(e){ self.dlg.dialog('destroy'); },
            open: function(e){
                if(mode === 'edit'){
                    self.it = ent;                 // M7129:Update
                    title_key = ['M7129',itemtype];// Update + Folder/Document
                } else {
                    self.it = self.AF_new_itfc(ent, itemtype);
                }
                self.AF_opening_setups(mode);
                self.AJ_lcpicker_img.attr('src', 
                    self.fd.AV_lang.AF_icon_filepath(self.AV_lcode));
                $('div.ui-dialog-buttonset').prepend(self.AJ_lcpicker_img);
                self.AJ_lcpicker_img.languagePicker(self.AF_lang_pick, self);
                return U.AF_stop_event(e);
            },
            buttons: {
                'ok-button': {
                    id: 'itemwizard-ok-button',
                    text: 'OK',
                    click: function(e){
                        if(FD.AF_se()){
                            self.AF_save_updates();                            
                        } 
                        self.AJ_div.dialog('close');
                        return U.AF_stop_event(e);
                    },
                },
                'cancel-button': {
                    id: 'itemwizard-cancel-button',
                    text: 'CANCEL',
                    click: function(e){ 
                        self.AJ_div.dialog('close'); 
                        return U.AF_stop_event(e);
                    }
                }
            }
        });
        self.dlg.dialog('open');

        self.dlg.keyup(function(e){
            if(e.keyCode === 13 && self.AJ_name.val().trim().length > 0){
                // ENTER -> OK
                $('#itemwizard-ok-button').trigger('click');
            }
            if(e.keyCode === 27){ // ESCAPE -> Cancel
                $('#itemwizard-cancel-button').trigger('click');
            }
        });
        self.dlg.dialog('widget').attr('id', 'item-wizard-popup');
        this.fd.AV_lang.AF_set_dlg('item-wizard-popup',{
            '.ui-dialog-title': title_key,
            '#itemwizard-ok-button': 'M0002',
            '#itemwizard-cancel-button': 'M1003'
        });
    };// end of AC_ItemWizard.AF_popup_dialog method
    
    // ------------------------------------------------------------------------
    // Purpose:construct a item of type F(M7127:New folder) or D(M7128:New doc),
    // if it-D, also make a new FC. container_ent as its owner(IP/itF)
    // -----------------
    AC_ItemWizard.prototype.AF_new_itfc = function(container_ent, itemtype){
        var type = (itemtype === 'M7127') ? 'F':'D';//M7127: New folder
        var it = new IT({ '_id': M.mid(container_ent.fid(),'IT'), 'lock':[], 
                        'type': type, 'name': {}, 'title': {}, 'pk': "M7117"});
        it.iddict['owner'] = container_ent.id();
        if(FD.AV_loginpv)
            it.iddict['ownerpa'] = FD.AV_loginpv.model.id();
        else
            it.iddict['ownerpa'] = "FTSUPER-PA-admin";
        var iddict = $.extend(true, {}, container_ent.iddict);
        var msg = type==='F'? 'folders' : 'docs';
        var arr = iddict[msg]; // reference to folders|docs array in iddict
        arr.push(it.id());
        this.AV_actbuffer.AF_add_save(it);
        if(type === 'D'){
            // a new it always has a txt-fc as its first fc
            var fc = new FC({'_id': M.mid(container_ent.fid(),'FC'),
                name: {en: Base64.encode('Untitled')}, 
                anno:{}, contdict:{},type:'txt'});
            it.AF_add_facet(fc); // will also set fc.owner to be it
            this.AV_actbuffer.AF_add_save(fc);
        }
        if(container_ent.cid() === 'LP'){ // LandingPage is the container
            this.AV_actbuffer.AF_add_call(
                container_ent.AF_render, // func ref
                container_ent,           // func ctx
                []);                     // params
        } else {
            this.AV_actbuffer.AF_add_update(container_ent, {'iddict': iddict})
            this.AV_actbuffer.AF_add_call(this.AV_itempane.AF_populate, 
                                          null, [container_ent]);
        }
        return it;
    };// end of AC_ItemWizard.AF_new_itfc method
    
    // feed data into actbuffer from input widgets
    AC_ItemWizard.prototype.AF_update_delta = function(resetkeys){
        if(this.mode === 'edit'){   // editing existing it
            // check inputs against this.it. capture updates into actbuffer
            this.delta_dic = this.AV_actbuffer.AF_get_update_dic(this.it);
            if(resetkeys)
                for(var i=0; i<resetkeys.length; i++){
                    delete this.delta_dic[resetkeys[i]];
                }
            var name = $.extend(true, {}, this.it.name());
            var curb64_name = Base64.encode(this.AJ_name.val().trim());
            if(this.it.name()[this.AV_lcode] !== curb64_name){ // name changed?
                name[this.AV_lcode] = curb64_name;           // put name into
                this.delta_dic['name'] = name;              // dict
            } else {                                    // not changed
                delete this.delta_dic['name'];          // del name from dict 
            }
            var title = $.extend(true, {}, this.it.title());
            var curb64_title = Base64.encode(this.AJ_descr.val());
            if(this.it.title()[this.AV_lcode] !== curb64_title){
                title[this.AV_lcode] = curb64_title;
                this.delta_dic['title'] = title;
            } else { 
                delete this.delta_dic['title']; 
            }
            if(this.it.pk() !== this.AJ_pk.val()){
                this.delta_dic['pk'] = this.AJ_pk.val();
                if(this.AJ_pk.val() === 'M1513'){
                    this.delta_dic['lock'] = this.lock;
                } else {
                    this.lock = [];
                }
            } else { 
                delete this.delta_dic['pk'];
            }
            if(!U.AF_array_equal(this.it.lock(), this.lock)){
                this.delta_dic['lock'] = this.lock;
            } else {
                delete this.delta_dic['lock'];
            }
            // set update for sending req and update after response, and
            // re-render the vmodel's look, as response-call
            this.AV_actbuffer.AF_add_update(this.it, this.delta_dic)
                .AF_add_call(this.it.vmodel.AV_iconv.AF_reset,
                          this.it.vmodel.AV_iconv, ["shrunk"]);
        } else { // mode === add: collect data for new item from inputs
            // read all GUI inputs into this.it
            this.it.name_face(this.AV_lcode, this.AJ_name.val().trim())
              .title_face(this.AV_lcode, this.AJ_descr.val())
              .pk(this.AJ_pk.val());
            if(this.it.pk() === 'M1513')
              this.it.lock([this.AJ_pwd.val(), 
                            this.AJ_expdate.val(),
                            this.AJ_selpostexp.val()]);
            this.AV_actbuffer.AF_add_save(this.it);
        }
        return this.AF_enable_OK_btn();
    };// end of AC_ItemWizard.prototype.AF_update_delta
    
    AC_ItemWizard.prototype.AF_handle_success = function(data, self){
        var open_cont = self.it.owner();
        this.AV_actbuffer.AF_execute();
        // redraw the whole cat-cont-pane
        self.AV_itempane.AF_update_ipv(self.AV_itempane.ip);
        // if something is done inside a folder-it, open all 
        // the folders(set check-box to checked) containing it
        AC_ItemPane.AF_open_folders(open_cont);
        // trim names of all items rendered, not over the right window-edge
        self.AV_itempane.AF_trim_names();
    };// end of AC_ItemWizard.AF_handle_success method
    
    AC_ItemWizard.prototype.AF_handle_err = function(data){
        alert(data);
    };// end of AC_ItemWizard.AF_handle_success method
    
    // ------------------------------------------------------------------------
    // Purpose: save (to server/DB) the updates in dic(itemdic). 
    // itemdic={'C:<id>':<doc of new it>,'U:<id>':<change-dic of it>,...}
    // the container can be ip or it;
    // ----------------
    AC_ItemWizard.prototype.AF_save_updates = function(){
        var req_dict = this.AV_actbuffer.AF_get_reqdic('item');
        this.fd.AV_lserver.AF_post('putdocs', req_dict, this);
    };// end of AC_ItemWizard.AF_save_updates method

    return AC_ItemWizard;
})()