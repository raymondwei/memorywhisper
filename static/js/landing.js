var AC_Landing = (function(){
    function AC_Landing(fd){
        this.AV_is_public = false;
        this.fd = fd;
        this.AJ_heads = [ 
            $('a[href="#tab1"]'), $('a[href="#tab2"]'), $('a[href="#tab3"]'), 
            $('a[href="#tab4"]'), $('a[href="#tab5"]')];
        this.AJ_containers = [ 
            $('div#tab1'), $('div#tab2'), $('div#tab3'), $('div#tab4'), 
            $('div#tab5')];
        this.AV_active_index = 0;//active tab index. also set by AF_activate_tab
        this.AV_public_pages = [
            {ent: null, title_text: "M5001", // About MW: landingpage100
             jbox: $('<div id="lp100" class="lp-tab"></div>')},
            {ent: null, title_text: "M8003", // Example families:landingpage200
             jbox: $('<div id="lp200" class="lp-tab"></div>')},
            {ent: null, title_text: "M8004", // How tos: landingpage300
             jbox: $('<div id="lp300" class="lp-tab"></div>')},
            {ent: null, title_text: "M6001", // forum 
             jbox: $('<div id="lp400" class="lp-tab"></div>')},
            {ent: null, title_text: "M8006", // personal account:landingpage500
             jbox: $('<div id="lp500" class="lp-tab"></div>')}
        ];
        this.AV_private_pages = [
            {   ent: null, title_text: 'M3001',  // Family Tree
                jbox: $('<div id="ftgraph" class="ft-tab"></div>'), },
            {   ent: null, title_text: 'M7144',  // Calendar
                jbox: $('<div id="calendar" class="ft-tab"></div>')},
            {   ent: null, title_text: 'M4001', // Family Bulletin
                jbox: $('<div id="tab3box" class="ft-tab"></div>')},
            {   ent: null, title_text: 'M6001', // Forum
                jbox: $('<div id="tab4box" class="ft-tab"></div>')},
            {   ent: null, title_text: 'M8004', // How tos
                jbox: $('<div id="tab5box" class="ft-tab"></div>')}
        ];
        var self = this;
        this.AV_tabs = $('#tabs').tabs({
            activate: function(e, ui){
                var new_aid = e.currentTarget.id;
                if (new_aid === 'ui-id-1'){ // AC_FGraphPane
                    self.AF_activate_tab(0);
                } else {
                    //self.fd.AV_synop.AJ_div.detach();
                    if(new_aid === 'ui-id-2'){ // calendar-pane
                        self.AF_activate_tab(1);
                    } else if(new_aid === 'ui-id-3'){ // bulletin-pane
                        self.AF_activate_tab(2);
                    } else if(new_aid === 'ui-id-4'){ // forum-pane
                        self.AF_activate_tab(3);
                    } else if(new_aid === 'ui-id-5'){ // help-pane
                        self.AF_activate_tab(4);
                    }
                }
            }
        });// tabs definition
        this.AV_tabs.tabs('enable');
    };// end of AC_Landing constructor

    AC_Landing.prototype.AF_activate_tab = function(index){
        $('a span').removeClass('title-shadow');
        this.AJ_heads[index].find('span').addClass('title-shadow');
        this.AF_page(null, index);
        // show/hide away nav-arrows and detach synop from page
        this.fd.AV_tbar.AF_show_hide_nav(!this.AV_is_public && index === 0);
        switch(index | 0){
            case 0: 
                if(!this.AV_is_public){
                    this.fd.AV_lang.AJ_lang_btn.css('visibility','visible');
                    var pav = this.fd.AF_tpath()? 
                                this.fd.AF_tpath().AF_focus() : null;
                    this.fd.AV_synop.AF_init(pav); // show synop with pav
                }
                break;
            case 1: if(!this.AV_is_public){// calendar tab no-show land-btn
                    this.fd.AV_lang.AJ_lang_btn.css('visibility','hidden');    
                }
                break;
            case 2: if(!this.AV_is_public){
                    this.fd.AV_lang.AJ_lang_btn.css('visibility','hidden');    
                }
                break;
            case 3: if(!this.AV_is_public){
                    this.fd.AV_lang.AJ_lang_btn.css('visibility','hidden');    
                }
                break;
            case 4: if(!this.AV_is_public){
                    this.fd.AV_lang.AJ_lang_btn.css('visibility','visible');
                }
                break;
        }
        this.AV_active_index = index | 0; // |0 force it to be integer
        var ent = this.AF_page();
        if(ent.AF_init)
            ent.AF_init();
        return this.AF_resize();
    };// end of AC_Landing.prototype.AF_activate_tab

    AC_Landing.prototype.AF_resize = function(tabs_div){
        tabs_div = tabs_div? tabs_div : $('#tabs');    // tbs
        var w = parseInt(tabs_div.css('width')), 
            h = (parseInt(tabs_div.css('height'))-30); // w/h for tab1,2,..
        this.AJ_containers[this.AV_active_index].css({ //<div#tab1,2,3,4,5
            position: 'absolute', width: w + 'px', height: h + 'px'});
        var page_ent = this.AF_page();
        if(page_ent.AF_resize)
            page_ent.AF_resize(w, h);
        return this;
    };// end of AC_Landing.prototype.AF_resize

    // return page entity. 
    // if not existing, create it (AF_init will be called), return it 
    AC_Landing.prototype.AF_page = function(is_public, index){
        var el; // page-entry
        is_public = typeof(is_public) === 'undefined'? 
            this.AV_is_public : is_public;
        index = typeof(index)==='undefined'? this.AV_active_index : index;
        if(!is_public){ // private(ft specific) tabs
            el = this.AV_private_pages[index];
            if(!el.ent){
                switch(index | 0){ // | 0 force index to be integer
                    case 0: // ft-graph-pne
                        el.ent = new  AC_FGraphPane(this.fd, el.jbox);
                        break;
                    case 1: // calendar pane
                        el.ent = new AC_CalendarPane(this.fd, el.jbox);
                        break;
                    case 2: // bulletin pane
                        el.ent = new AC_Bulletin(this.fd, el.jbox);
                        break;
                    case 3:  // ft forum
                        el.ent = new AC_ForumGroup(this.fd, el.jbox);
                        break;
                    case 4: // Help/How-Tos - shared with public[2].ent
                        if(this.AV_private_pages[2].ent)
                            el.ent = this.AV_public_pages[2].ent;
                        else
                            el.ent = new AC_LandingPage300(this.fd, el.jbox);
                        break;
                    default: break;
                }
            } else 
                return el.ent;
        } else { // public landing pages
            el = this.AV_public_pages[index];
            if(!el.ent){
                var ctls = {
                    up: $('<div class="lp-showpane"></div>').appendTo(el.jbox),
                    dn: $('<div class="LS lp-msgpane"></div>').appendTo(el.jbox),
                    lb: $('<img class="show-prev"/>').appendTo(el.jbox)
                        .attr('src','pics/navigate-backward.png'),
                    rb: $('<img class="show-next"/>').appendTo(el.jbox)
                        .attr('src','pics/navigate-forward.png'),
                    vc: $('<span class="lp-vtimer"></span>').appendTo(el.jbox)
                };                
                switch(index | 0){ // | 0 force index to be integer
                    case 0: // About MW
                        el.ent = new AC_LandingPage100(this.fd, el.jbox, ctls);
                        break;
                    case 1: // Example families
                        el.ent = new AC_LandingPage200(this.fd, el.jbox, ctls);
                        break;
                    case 2: // How to
                    /*
                        if(this.AV_public_pages[2].ent)
                            el.ent = this.AV_public_pages[2].ent;
                        else
                        */
                            el.ent= new AC_LandingPage300(this.fd,el.jbox,ctls);
                        break;
                    case 3: // public forum
                        el.ent = new AC_LandingPage400(this.fd, el.jbox, ctls);
                        break;
                    case 4: 
                        el.ent = new AC_LandingPage500(this.fd, el.jbox, ctls);
                        break;
                    default: break;
                }
            } else 
                return el.ent;
        }// private / public
        return el.ent;
    };// end of AC_Landing.prototype.AF_page

    // 1. set all tab-titles for public/private landing-pages
    // 2. attach target page under div#tabN
    AC_Landing.prototype.AF_init = function(AV_is_public){
        this.AV_is_public = AV_is_public;
        var i, t, span;
        // set tab-titles, then clear <div#tabN>, append jbox to it
        for(i=0; i<5; i++){
            div = this.AJ_containers[i]; // div#tab1,2,3,4,5
            span = this.AJ_heads[i].find('span');
            if(this.AV_is_public){
                t = this.AV_public_pages[i].title_text;
                // append <div#lp100,200,.. > to div#tab1,2,..
                div.empty().append(this.AV_public_pages[i].jbox);// div#lp100             
            } else {
                t = this.AV_private_pages[i].title_text;
                div.empty().append(this.AV_private_pages[i].jbox);//div#ftgrapph     
            }
            span.attr('tag', t).text(FD.AV_lang.face(t));
        }
        // have the first tab triggered
        this.AF_activate_tab(0);            
        return this;
    };// end of AC_Landing.prototype.AF_init

    return AC_Landing;
})();
