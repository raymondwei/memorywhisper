﻿/*
 * Copyright (C) 2014 Adacro Inc. - All Rights Reserved
 * ----------------------------------------------------------------------------
 * AC_BinObjMgr class for handling binary files
 * ------------------------------------------
 * 0. how a binary/media file is stored on server
 *    my azure storage account name is royalfolder. The root url for azure blob
 *    is saved in U.BUCKET_URL = 'https://royalfolder.blob.core.windows.net/'
 *    the file-path of a media(binary) file rs.filename():
 *    <fid-in-lower-case>/<file-size>-<signature>.<ext>, for example:
 *      tc6h8eu//105320-e1a4a8be323f3c3cd39f1458b6fa935ca354bc3c.jpg
 *    the whole url:
 *    U.BUCKET_URL + rs.filename(), e,g,
 *     https://royalfolder.blob.core.windows.net/
 *        tc6h8eu/105320-e1a4a8be323f3c3cd39f1458b6fa935ca354bc3c.jpg
 *    This url can be used as <img src=<url>> directly on a html page
 * 1. Reading access of a media file from blob
 *    feed_media(jimg, rs, [bg-img]) is a class method, for assigning url from
 *    rs to jimg. If bg-img given, put that as div background picture.
 * 2. Upload: a picture file is chosen from synopwizard for a
 *    portrait, 
 *    a).add_media is called with the file object from input element
 *    b).add_media starts a file-reader(HTML5 file-i/o): 
 *    file_reader.readAsDataURL(file); in its onloadend call-back, it get file 
 *    data, and calc signature, and
 *    c). test if a rs with the same signature exists in store, if yes,
 *    call synopwizard.AF_handle_rs. if no, get all rss from idb and find rs in
 *    the rss: if one rs exists with the same sig, call synopwizard.AF_handle_rs
 *    if not found, make a new rs. call synopwizard.AF_handle_rs(rs)
 */
function _arrayBufferToBase64( buffer ) {
    var binary = '';
    var bytes = new Uint8Array( buffer );
    var len = bytes.byteLength;
    for (var i = 0; i < len; i++) {
        binary += String.fromCharCode( bytes[ i ] );
    }
    return window.btoa( binary );
}
 
var AC_BinObjMgr = (function(){
    function AC_BinObjMgr(fd){
        this.fd = fd;
    }
    
    AC_BinObjMgr.prototype.error = function(msg){ alert(msg); }
        
    // ------------------------------------------------------------------------
    // make sure _rs.data (if not empty) is persisted in Azure DB(Blob storage)
    // --------------------------------------------
    AC_BinObjMgr.prototype.AF_ensure_data = function(rs){
        if(rs.data().length < 100) return;
        this.rs = rs;
        var req_dic = {data:rs.data(), 
                       mime:rs.mime(),
                       filepath:rs.file_name()}; 
        FD.AV_lserver.AF_post('saveblob', req_dic, this);
    };// end of AC_BinObjMgr.prototype.AF_ensure_data
    
    // handle 'searchrs' response
    AC_BinObjMgr.prototype.AF_handle_success = function(data, self){
        if(self.reqverb === 'saveblob') {
            // data succeddfully to DB, set url to rs.data
            var url = U.AV_BUCKET_ROOTURL + self.rs.file_name();
            self.rs.data(url);
            if(self.AJ_img)
                AC_BinObjMgr.AF_feed_media(self.AJ_img, self.rs)
            self.AJ_img = null;
            self.rs = null;
        }
    };// end of AC_BinObjMgr.prototype.AF_handle_success
    
    // get file object from caller, a input/type=file element.files[0]
    AC_BinObjMgr.prototype.AF_add_media = function(file, caller){
        function __makers(file, data){
            var rs = new RS({'_id':M.mid(FD.fid(),'RS'), name: self.file.name,
                             mime:self.file.type, size: ""+self.file.size});
            rs.data(data).signature(hex_sha1(data)).title(self.file.name);
            return rs;
        }
        this.caller = caller;
        this.file = file;
        var self = this, fid = FD.fid(), i;
        var store_rdic = FD.AV_store.AV_repos[fid].entdict['RS'];
        var file_reader = new FileReader;
        file_reader.onloadend = function(e){
            U.log(2, "pushing file "+file.name);
            self.data = e.target.result;
            if(self.data.length > 0){
                self.sig = hex_sha1(self.data);
                // see if we have rs in store with the same signature, yes: rs
                for(var k in store_rdic){ // {<rs-id>:<rs>,...}
                    if(store_rdic[k].signature() === self.sig){
                        self.rs = store_rdic[k]; // copy rs to self.rs
                    }
                }
                if(self.rs){
                    self.caller.AF_handle_rs(self.rs);
                } else {
                    // look for rs with the same sig in idb
                    var found = false;
                    FD.AV_lserver.AF_get_idb(fid, function(){
                        var idb = FD.idbs[fid];
                        idb.AF_get_cid_ents('RS').done(function(e){
                            if(e && e.length > 0)
                                for(i=0; i<e.length; i++){
                                    if(e[i].signature === self.sig){
                                        found = true;
                                        self.rs = e[i];
                                        self.caller.AF_handle_rs(self.rs);
                                        break;
                                    }
                                }
                            else{
                                self.caller.AF_handle_rs(  // with is_new=true
                                    __makers(self.file, self.data), true);
                            }
                        }).fail(function(e){
                            self.caller.AF_handle_rs(  // with is_new=true
                                __makers(self.file, self.data), true);
                        });
                    });
                }
                //self.reqverb = 'searchrs';
                //FD.AV_lserver.AF_post(self.reqverb, {'sig': self.sig }, self);
            }
            return U.AF_stop_event(e);
        }
        file_reader.readAsDataURL(file);
    };// end of AC_BinObjMgr.prototype.AF_add_media
    
    // -------------------------------
    // class methods
    // -------------------------------
    AC_BinObjMgr.AF_feed_media = function(jimg, //target element for assigning src
                                       rs,     // src 
                                       bgimg){ // optional background image
        var url;
        if(rs.data().length > 1000){
            url = rs.data();
        } else {
            url = U.AV_BUCKET_ROOTURL + rs.file_name();
        }
        var tagname = jimg.get(0).tagName.toLowerCase();
        if(['img','iframe','source','audio','video'].indexOf(tagname) > -1){
            jimg.attr('src', url);
        } else if(tagname == 'div'){
            // multiple bg-images: front url stays front.
            var msg = bgimg?  'url("' + url + '"), url("' + bgimg + '")' :
                'url("' + url + '")'
            jimg.css('background-image', msg);
        }
    };// end of AC_BinObjMgr.AF_feed_media
    
    return AC_BinObjMgr;
})();

/**
 * This will communicate with web-worker thread defined in webworker.js
 */
var AC_WebWorkerPortal = (function(){
    function AC_WebWorkerPortal(){
        var self = this;
        this.worker = new Worker('js/webworker.js');
        this.cmd_handlers = {
            'upload-media': null,
            'update-idb': null
        };
        this.worker.addEventListener('message',
            function(e){
                var data = e.data;
                // if calling-ctx exist, make the call back
                if(self.cmd_handlers[data.cmd]){
                    var func = self.cmd_handlers[data.cmd].func,
                        ctx = self.cmd_handlers[data.cmd].ctx;
                    func(data, ctx);
                }
            },
            false
        );
    };// end of AC_WebWorkerPortal constructor

    // callctx:{func: <callback-function name>, ctx: <function-calling-context>}
    AC_WebWorkerPortal.prototype.AF_send_req = function(dic, callctx){
        if(callctx)
            this.cmd_handlers[dic.cmd] = callctx;
        this.worker.postMessage(dic);
    };// end of AC_WebWorkerPortal.prototype.AF_send_req

    return AC_WebWorkerPortal;
})();