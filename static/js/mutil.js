/*
 * Copyright (C) 2014 Adacro Inc. - All Rights Reserved
 */
var M = (function(){
    function M(){}
    M.AF_clear_dict = function(d){
        for(var k in d){   delete[k];  }
        return d;
    };
    // update delta_dic into dic: change-values or add new values in dic
    // issue: it is not done for delete entries from dic. 
    M.AF_update_dict = function(dic, delta_dic){
        for(var k in delta_dic){
            var v = delta_dic[k];
            if(k in dic){
                if($.type(v) === 'string' || $.type(v) === 'array'){
                    dic[k] = delta_dic[k];
                } else if($.type(v) === 'object'){
                    M.AF_update_dict(dic[k], delta_dic[k]);
                }
            } else {
                dic[k] = delta_dic[k];
            }
        }
    };// end of M.AF_update_dict

    // test if 2 objects have the same contents
    M.AF_object_equal = function(o1, o2){
        if(typeof(o1) !== typeof(o2)) return fals;
        if(['number','string'].indexOf(typeof(o1)) > -1)
            return o1 === o2;
        if(typeof(o1) === 'object'){
            if($.isArray(o1)){
                return U.AF_array_equal(o1,o2);
            } else {
                for(var k in o1){
                    if(!(k in o2)) return false;
                    return U.AF_object_equal(o1[k], o2[k]);
                }
            }
        }
        return true;
    };// end of M.AF_object_equal

    M.AF_random_string = function(N){
        // alphbet chars without t/T. 50 in count(T used in TS 20151212T123456
        var randChars = "abcdefghijklmnopqrsuvwxyzABCDEFGHIJKLMNOPQRSUVWXYZ";
        var s = [];
        for (var i = 0; i < N; i++) {
            s[i] = randChars.substr(Math.floor(Math.random() * 50), 1);
        }
        return s.join("");
    };// end of M.AF_random_string
    
    // make id
    M.mid = function(fid,cid,seed,no_ts){
        if(!seed || seed.length !== 6){
            seed = M.AF_random_string(6);
        }
        if(no_ts) // 3TXE45N-CE-201510
            return fid+'-'+cid+'-'+seed;
        var milsec_since_19700101 = __UTC() + '';
        var uuid = seed + milsec_since_19700101;
        return fid+'-'+cid+'-'+uuid; // 3TXE45N-PA-bfDceS1354941698601
    };// end of M.mid
    
    // from a list of id strings, return a list of ids that belong to fid
    M.AF_fid_filter = function(lst,fid){
        var res = [];
        for(var i=0; i<lst.length; i++){
            if(lst[i].indexOf(fid) == 0)
                res.push(lst[i]);
        }
        return res;
    };// end of M.AF_fid_filter
    
    // get ent from store with eid/id or a copy of ent
    // if input is an array of ids, get array of ents
    M.e = function(id_or_eid_or_ent){
        if(!id_or_eid_or_ent) return null;
        if($.isArray(id_or_eid_or_ent)){
            var ents = [];
            for(var i=0; i<id_or_eid_or_ent.length; i++){
                var ent = M.e(id_or_eid_or_ent[i])
                if(ent)
                    ents.push(ent);
                else
                    U.log(1, // error 
                          id_or_eid_or_ent[i] + ' not in store')
            }
            return ents;
        } else { // id_or_eid_or_ent is not an array
            var tid = typeof(id_or_eid_or_ent) === 'object'?
                id_or_eid_or_ent.id() : id_or_eid_or_ent;
            return FD.AV_store.AF_getEntity(tid); 
        }
    };// end of M.e

    // get list of ents of cid (!= RL)
    M.ces = function(cid){ return FD.AV_store.AF_getEntsByCid(cid); }
        
    // add entity to store, if not already in. return ent from store with e.id
    M.ae = function(e){ 
        return FD.AV_store.AF_put_ent(e); 
    }
    
    M.de = function(id){
        if($.isArray(id)){
            for(var i=0; i<id.length; i++){
                M.de(id[i]);
            }
        } else
            FD.AV_store.AF_delEntity(id); 
    }
    
    // get the 2 char cid from id/eid string
    M.cid = function(eid){ 
        if(eid.length>10) return eid.substr(8,2); 
        return null;
    }
    // get fid fro eid or id
    M.fid = function(eid){
        if(eid.length>6) return eid.substr(0,7); 
        return null;
    }

    // docs: single ent-json - return entity;
    //       or array of ent-json return array of entities
    // lst is collector-array: accumulative.
    M.AF_load = function (docs, lst) { 
        if(!docs) return;
        var e, outlst = lst? lst : [];
        if($.isArray(docs)){
            for (var i = 0; i < docs.length; i++) {
                M.AF_load(docs[i], outlst);
            }
            return outlst;
        } else { // docs is a json object
            if('_id' in docs){
                // regardless json-obj or FTBase-ent, put (new)ent into store
                // return to entity e. This is synch-call return
                e = FD.AV_store.AF_put_ent(docs);
                outlst.push(e);
                return e;// docs:single json, return ref of entity stored
            } else {
                U.log(2, 'disgarding doc with no id:' + docs);
                return null;
            }
        }
    };// end of M.AF_load method

    M.AF_fadmin = function(){
        if(FD.AV_loginpv &&
            parseInt(FD.AV_loginpv.model.role()) & 256)
            return true;
        return false;
    };// end of M.AF_fadmin
    
    // OA(owner-access) means logged-in pa is the ownerpa; or
    // logged-in pa is the trustee of ownerpa
    M.OA = function(obj){ // return: false | true
        if(!FD.AV_loginpv || !obj)
            return false; // no logged-in pa
        // if logged-in user has fadmin-bit(256) set, allow everything
        if(M.AF_fadmin())
            return true;
        var pas = M.AF_ownerpa(obj), i, oa;
        if(!pas) return false;
        if($.isArray(pas)){
            for(i=0; i<pas.length; i++){
                oa =  M.OA(pas[i]);
                if(oa) return true;
            }
            return false;
        } else { // pas is pa (not plural)
            if(pas.id() === FD.AV_loginpv.model.id())
                return true;
            var AV_trusteepids = ('M7534' in pas.AF_cfg())?
                    pas.AF_cfg()['M7534'] : [];
            for(var i=0; i<AV_trusteepids.length; i++){
                if(FD.AV_loginpv.model.id() === AV_trusteepids[i])
                    return true;
            }
            return false;
        }
    };// end of M.OA

    // ------------------------------------------------------------------------
    // returning the pa who is the owner pa, for internal use in
    // allow_read/allow_write. me can only be inst of IT,IP,FC
    // -------------------------------------
    M.AF_ownerpa = function (me) {
        var pa;
        switch (me.cid()) {
            case 'FC':
            case 'IT':
            case 'IP':
                pa = me.owner();
                if(pa.cid() === 'PA')
                    return pa;
                return M.AF_ownerpa(pa);
            case 'ITV': return M.AF_ownerpa(me.model);
            case 'PA': return me;
            case 'PAV':
                if(me.AF_is_virtual()){
                    return M.AF_ownerpa(me.mhost);
                } else
                    return me.model;
            case 'BA': if(me.AF_is_virtual())  return me.AF_children();
                return [me.male(), me.female()];
            case 'BAV': 
                if(me.AF_is_virtual()) return me.model.AF_children();
                return M.AF_ownerpa(me.model);
            default: U.log(1, 'AC_Store.AF_ownerpa: ' + me.cid() +
                    ' does not have owner');
        }; // end of switch(me.cid())
        return pa;
    };// end of M.AF_ownerpa 
    
    // create a pa instance and all its contained objs(td,rs,ip,it,it,fc)
    M.AF_create_pa_set = function(fid,  
                                  seed){ // !null only when ftwizard('as init')
        var ps = {}; // pa-set
        // use seed given password, otherwise '_abc' as password
        var pid = M.mid(fid,'PA');
        var pwd = seed['pwd']? seed['pwd'] : '_abc';
        ps.pa = new PA({'_id':pid, 'sex': seed['sex'],
                    cfg: { M1504: {'login': [pwd,'','']},
                           M7534: seed['trustee'],
                           M1502: seed['email']? seed['email'] : '',
                           M7519: seed['theme']? seed['theme'] : 'default',
                           M1508: seed['lang']? seed['lang'] : FD.AF_lcode()
                    }
                });
        ps.pa.tname_face(seed['lang'], seed['name']);

        ps.td = new TD({'_id':M.mid(fid,'TD'),'name': 'M7111'}); // nutshell
        ps.pa.nutshell(ps.td.AF_add_holder(ps.pa.id()))
             .pob_face(FD.AF_lcode(),'');
        ps.rs = new RS({'_id':M.mid(fid,'RS')});
        ps.pa.portrait(ps.rs.AF_add_holder(ps.pa.id()).title('portrait'));
        return ps;
    };// end of M.AF_create_pa_set
    
    // ------------------------------------------------------------------------
    // mode can be:
    // 1."as init"(M1512) - the very first pa in fa. itor: null; 
    //    seed: pa-dict from ftwizard.AF_create_ft
    // 2."as child"(M7133):adding a child for ba. itor: bav; seed: null
    // 3."as spouse"(M7132): hosting pa add a spouse. itor: pa; seed: null 
    // 4."as real"(M7134(female)/7135(male)): fill in a parent
    //    (lettin vitual ba -> real).
    //    itor: bav; seed: null
    M.AF_create_pa = function(mode, itor, seed, abuf){
        var fid;
        if(mode === "M1512"){       // initial person
            fid = seed['pcode'];
            seed['trustee'] = [];   // init person has no trustee
        } else {
            fid = FD.AV_fa.fid();
        }
        var ps = M.AF_create_pa_set(fid, seed)
        abuf.AF_add_save([ps.pa, ps.td, ps.rs]);
        
        // case switch
        switch(mode){
        case "M1512": // "Initial Person" by ftwizard: first pa in a new fa 
            // virtual parents-ba of the new pa
            ps.ba = new BA({'_id': M.mid(fid, 'BA')});
            ps.ba.iddict['children'].push(ps.pa.id());
            ps.pa.role('257')  // initial pa is fadmin + login
                 .iddict['parents'].push(ps.ba.id());
            ps.ct = new CT({'_id':fid+'-CT-'+fid, version: '1', versid: {}});
            ps.fa = new FA({_id: fid+'-FA-'+fid, ctver: '1'});
            var sss = {};// snapshots of current fid-db
            sss[ps.pa.id()] = ps.pa.tx();
            sss[ps.td.id()] = ps.td.tx();
            sss[ps.rs.id()] = ps.rs.tx();
            sss[ps.ba.id()] = ps.ba.tx();
            sss[ps.fa.id()] = ps.fa.tx();
            sss[ps.ct.id()] = ps.ct.tx();
            ps.ct.AF_snapshots(sss);
            ps.fa.add_fadmin(ps.pa.id());
            abuf.AF_add_save([ps.ba, ps.fa, ps.ct]);
            break;
        case "M7133"://add-child: itor is an existing bav adding pa as a child
            ps.pa.iddict['parents'].push(itor.model.id()); 
            // itor.model(ba) will have pa in its children, as aftermath
            var iddict = $.extend(true,{},itor.model.iddict);
            if(iddict['children'].indexOf(ps.pa.id()) === -1)
                iddict['children'].push(ps.pa.id());
            abuf.AF_add_update(itor.model, {'iddict': iddict});
            break;
        case "M7132":// "add-spouse": itor is hosting pa adding pa as a spouse
            ps.ba = new BA({'_id': M.mid(fid, 'BA')});
            if(itor.sex()==='male'){
                ps.ba.female(ps.pa); ps.ba.male(itor.model);
            } else {
                ps.ba.male(ps.pa); ps.ba.female(itor.model);
            }
            ps.pa.AF_add_spouse(ps.ba); // handle_succ: itor.AF_add_spouse(ba)
            
            // virtual ba as parents-ba for the new pa
            ps.pb = new BA({'_id': M.mid(fid, 'BA')});
            ps.pa.iddict['parents'].push(ps.pb.id());
            ps.pb.iddict['children'].push(ps.pa.id());
            abuf.AF_add_save([ps.ba, ps.pb]);
            // itor.iddict['spouses'] to be updated - adding ba into it
            var iddict = $.extend(true,{},itor.model.iddict);
            if(iddict['spouses'].indexOf(ps.ba.id()) === -1)
                iddict['spouses'].push(ps.ba.id());
            abuf.AF_add_update(itor.model, {'iddict': iddict});
            break;
        case "M7134": // add female, for filling in a virtual parent pav
        case "M7135": // add male, for filling in a virtual parent pav
            // itor is the pav to be filled with ps.pa
            // bv, the mhost of pav (new pa has been created in ps.pa)
            var bv = itor.mhost; // itor's mhost (bav)
            
            // create a new ba for pa's parents-ba
            ps.ba = new BA({'_id': M.mid(fid, 'BA')});
            ps.pa.iddict['parents'].push(ps.ba.id());
            // pa add to children-array of ps.ba
            ps.ba.iddict['children'].push(ps.pa.id());
            
            // put bv.model as pa's spouse
            ps.pa.iddict['spouses'].push(bv.model.id());
            
            // setup call for modifying itor.model (ba).mlae/femal,
            var iddict = $.extend(true,{}, bv.model.iddict);
            iddict[ps.pa.sex()] = ps.pa.id();
            abuf.AF_add_update(bv.model, {'iddict': iddict});
            
            abuf.AF_add_save(ps.ba);
            abuf.AF_add_assignment(itor,'model',ps.pa); // pv.model = pa
            abuf.AF_add_call(itor.AF_set_id,itor,[]);
            break;
        }
        return ps.pa;
    };// end of M.AF_create_pa
        
    // when a new pa created (op==C), or existing pa (op==U | D)
    // collect all months (2chars string) that are touched 
    // p is pa-dic(op==C); p is pid(op==D); p is  pa-delta-dic (op==U)
    M.AF_collect_dirty_ces = function(op, p, ce_ids){
        function collect_months(p, key, months){
            if(!p) return;
            if(key in p && p[key].length === 8){ // p.key: 19261026
                var m = p[key].substr(4,2);      // m: 10
                if(months.indexOf(m) == -1)      // m not in months
                    months.push(m);              // put m into months
            }
        };// end of collect_months

        var ms = [];
        if(!ce_ids)
            ce_ids = [];
        if(op === 'C'){ // p is pa-dic
            collect_months(p, 'dob', ms);
            collect_months(p, 'dod', ms);
        } else if(op === 'D'){
            var pa = M.e(p);
            collect_months(pa, 'dob', ms);
            collect_months(pa, 'dod', ms);
        } else { // op === U, p:  [oldpa, delta-dic]
            if('dob' in p[1]){ // p[1] is delta-dic. dob in delta-dic
                // in case 'dob' in delta-dic(dob being updated), collect both
                // oldpa's dob and delta-dic's dob
                collect_months(p[0], 'dob', ms);
                collect_months(p[1], 'dob', ms);
            }
            if('dod' in p[1]){ // p[1] is delta-dic. dob in delta-dic
                collect_months(p[0], 'dod', ms);
                collect_months(p[1], 'dod', ms);
            }
        }
        if(FD.fid() !== "FTSUPER"){
            var fid = FD.fid(), 
                ce_id;
            // ms:['09','11'] -> [<fid>-CE-09, <fid>-CE-11]
            for(var i=0; i<ms.length; i++){
                ce_id = fid + '-CE-' + ms[i];
                if(ce_ids.indexOf(ce_id) === -1){
                    ce_ids.push(ce_id);
                }
            }
        }
        return ce_ids;
    };// end of M.AF_collect_dirty_ces

    // before save pa or it into store(actbuffer.saves/updates)
    // pa.AF_cfg().M1504: {<key>:[<pwd>,<>,<>]} - pwd to be '******'
    // it.lock: [<pwd>,<>,<>] - pwd to be '******'
    M.AF_blur_pwd = function(pa_or_it){
        var M1504_dic, lock;
        if(pa_or_it.cid() === 'PA'){
            M1504_dic = pa_or_it.AF_cfg().M1504;
            for(var k in M1504_dic){
                if($.isArray(M1504_dic[k])){
                    M1504_dic[0] = '******';
                }
            }
        } else if(pa_or_it.cid() === 'IT' &&
                  pa_or_it.type()==='F'){ // only itF can have lock
            lock = pa_or_it.lock();
            lock[0] = '******';
        }
    };// end of M.AF_blur_pwd

    return M;
})();