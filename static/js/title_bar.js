/*
 * Copyright (C) 2014 Adacro Inc. - All Rights Reserved
 */
var AC_TitleBarVModel = (function(){
    function AC_TitleBarVModel(fd){
        this.fd = fd;
        /**
         * <div #mwlogo
         *   <span #mwlogo-text  //family name tag - "<tname> family"
         *   <div #home-icon     //user icon on right of family tag
         *     <div #loginuser   //a tag right of icon "Login: <tname>""
         */
        this.AJ_homepage_icon = $('#home-icon').css('visibility','hidden');
        var self = this;
        this.AV_user_ctxmenu = new AC_ContextMenu(this, 
            this.AJ_homepage_icon,   // element to click upon
            {   title: '',
                items:[
                {label: 'M1002', icon: 'pics/logout.png',
                 action: function(e){
                    self.reqverb = 'logout';
                    if (fd.AV_loginpv){
                        fd.AV_lserver.AF_post('logout',
                            {_sid: self.fd.AF_sid()}, self);
                    }
                    return U.AF_stop_event(e);
                }},
                {label: 'M7121', icon: 'pics/settings.png',
                 action: function(e){
                    self.fd.AV_cfg.AF_popup_dialog(self.fd.AV_loginpv.model);
                    return U.AF_stop_event(e);
                }},
            ]}, 
            true);  // left-click = true
        this.AJ_logo = $('div#mwlogo');
        this.AJ_logoword = $('#mwlogo-text');
        this.AJ_loginuser = $('#loginuser');
        fd.AV_lang.AF_switch_lang(fd.AV_lang.AV_lcode);
        this.AF_loginout(fd.AV_loginpv);
        this.AF_load_login_template();
        this.AF_load_invite_template();
        this.AJ_nav_back = $('img#navback');
        this.AJ_nav_fore = $('img#navfore');
        this.AJ_nav_back.attr('src','pics/navigate-backward.png');
        this.AJ_nav_fore.attr('src','pics/navigate-forward.png');
        this.AJ_searchbtn = $('img#search-icon');
        this.AF_show_hide_nav(false);
        this.AF_setup_navhandler();
        this.swz = new AC_SearchWizard(fd);
        //this.AF_set_tooltip();
    };// end of AC_TitleBarVModel constructor

    AC_TitleBarVModel.prototype.cid = function(){ return 'TBV'; }

    AC_TitleBarVModel.prototype.AF_load_login_template = function(){
        var t, tr1,tr2,td11,td12,td21,td22;
        this.AJ_login = $('<div id="Dlg_login"></div>');
        t = $('<table id="login-dlg-table"></table>').appendTo(this.AJ_login);
        tr1 = $('<tr></tr>').appendTo(t);
        td11 = $('<td></td>').appendTo(tr1);
        $('<span class="LS login-label" tag="M1501">UserID</span>')
            .appendTo(td11);
        td12 = $('<td></td>').appendTo(tr1);
        this.AJ_username = $('<input id="login_userid"/>').appendTo(td12);
        tr2 = $('<tr></tr>').appendTo(t);
        td21 = $('<td></td>').appendTo(tr2);
        $('<span class="LS login-label" tag="M1504"></span>')
            .appendTo(td21);
        td22 = $('<td></td>').appendTo(tr2);
        this.AJ_loginpwd = $('<input type="password" id="login_password"/>')
            .appendTo(td22);
    };// end of AC_TitleBarVModel.prototype.AF_load_login_template
    
    AC_TitleBarVModel.prototype.AF_load_invite_template = function(){
        this.AJ_invite = $('<div id="dlg-invite"></div>');
        $('<span class="LS to-label" tag="M1498">To</span>')
            .appendTo(this.AJ_invite);
        $('<span class="LS subj-label" tag="M1499">Subject</span>')
            .appendTo(this.AJ_invite);
        this.AJ_addr = $('<input type="text" id="email-address" />')
            .appendTo(this.AJ_invite);
        this.AJ_subj = $('<input type="text" id="subject" />')
            .appendTo(this.AJ_invite);
        this.AJ_itxt = $('<textarea id="invite-text" />')
            .appendTo(this.AJ_invite);
    };// end of AC_TitleBarVModel.prototype.AF_load_invite_template
    
    AC_TitleBarVModel.prototype.AF_show_hide_nav = function(sh){
        if(sh){
            this.AJ_nav_fore.css('visibility','visible');
            this.AJ_nav_back.css('visibility','visible');
            this.AJ_searchbtn.css('visibility','visible');
        } else {
            this.AJ_nav_fore.css('visibility','hidden');
            this.AJ_nav_back.css('visibility','hidden');
            this.AJ_searchbtn.css('visibility','hidden');
            if(this.fd.AV_synop)
                this.fd.AV_synop.AF_show_state('hidden');
        }
    };// enf of AC_TitleBarVModel.prototype.AF_show_hide_nav
    
    AC_TitleBarVModel.prototype.AF_set_tooltip = function(){
        this.AJ_nav_back.attr('title',this.fd.AV_lang.face('M0010'));
        this.AJ_nav_fore.attr('title',this.fd.AV_lang.face('M0009'));
        this.AJ_searchbtn.attr('title',this.fd.AV_lang.face('M7520'));
        this.AF_set_logoword();
    };// end of AC_TitleBarVModel.prototype.AF_set_tooltip

    AC_TitleBarVModel.prototype.AF_set_logoword = function(){
        var face = FD.AV_lang.face('M1000');// Memory Whisper
        if(this.fd.AF_centerpa()){
            face = this.fd.AF_centerpa().tname_face() + ' ' + 
                       FD.AV_lang.face('M7119'); // M7119: family   
        }
        this.AJ_logoword.text(face);
        if(FD.AV_loginpv){
            this.AJ_loginuser.text(FD.AV_lang.face('M1001') + ': ' +
                FD.AV_loginpv.model.tname_face());
        }
    };// end of AC_TitleBarVModel.prototype.AF_set_logoword
    
    AC_TitleBarVModel.prototype.AF_setup_navhandler = function(){
        var self = this;
        this.AJ_logo.unbind('click').bind('click', function(e){
            var last_path = self.fd.AV_path_mgr.AF_back2home();
            if(last_path){
                self.fd.AV_landing.AF_page().AF_delta_render(last_path)
                .AF_update_focus_ppane();                  
            }
            self.AF_update_nav();
        });
        this.AJ_nav_back.unbind('click').bind('click', function(e){
            var last_path = self.fd.AV_path_mgr.AF_backward();
            if(last_path){
                self.fd.AV_landing.AF_page().AF_delta_render(last_path)
                .AF_update_focus_ppane();                  
            }
            self.AF_update_nav();
        });
        this.AJ_nav_fore.unbind('click').bind('click', function(e){
            var last_path = self.fd.AV_path_mgr.AF_forward();
            if(last_path){
                self.fd.AV_landing.AF_page().AF_delta_render(last_path)
                .AF_update_focus_ppane();  
            }
            self.AF_update_nav();
        });
        this.AJ_searchbtn.unbind('click').bind('click', function(e){
            if(self.fd.AF_centerpa())   // only when a fa is chosen, can search be
                self.swz.AF_open_dlg(); // offered
        });
    };// end of AC_TitleBarVModel.prototype.AF_setup_navhandler
    
    AC_TitleBarVModel.prototype.AF_update_nav = function(){
        if(this.fd.AV_path_mgr.AF_back_possible()){
            this.AJ_nav_back.attr('src','pics/navigate-backward1.png');
            this.AJ_nav_back.css('cursor','pointer');
        } else {
            this.AJ_nav_back.attr('src','pics/navigate-backward.png');
            this.AJ_nav_back.css('cursor','default');
        }
        if(this.fd.AV_path_mgr.AF_forward_possible()){
            this.AJ_nav_fore.attr('src','pics/navigate-forward1.png');
            this.AJ_nav_fore.css('cursor','pointer');
        } else {
            this.AJ_nav_fore.attr('src','pics/navigate-forward.png');
            this.AJ_nav_fore.css('cursor','default');
        }
    };// end of AC_TitleBarVModel.prototype.AF_update_nav

    AC_TitleBarVModel.prototype.AF_lang_select = function(){
        this.fd.AV_lang.AF_switch_lang(this.fd.AV_lang.AV_lcode);
    };// end of AC_TitleBarVModel.AF_lang_select method

    AC_TitleBarVModel.prototype.AF_loginout = function(logintrue){
        if(logintrue){
            // if login-dialog still open, close it
            if(this.login_dlg.dialog('isOpen'))
                this.login_dlg.dialog('close');
            // show homepage-icon with login-user name
            var col, pa = FD.AV_loginpv? FD.AV_loginpv.model : null;
            if(pa){
                col = pa.sex()==='male'?  'darkblue' : 'darkred';
                this.AJ_homepage_icon.css( // show up homepage login icon
                    {'visibility': 'visible', 'border-color': col});
                // on right side of homepage icon(contained) - Login: <tname>
                this.AJ_loginuser // M1001: login
                    .text(FD.AV_lang.face('M1001') + ': '+pa.tname_face());
            }
        } else {
            this.AJ_homepage_icon.css('visibility', 'hidden');            
        }
        this.fd.AV_lang.AF_set_all();
    };// end of AC_TitleBarVModel.prototype.AF_loginout
    
    // handle-logout
    AC_TitleBarVModel.prototype.AF_handle_success = function(data, self){
        U.log(2, self.reqverb);
        if(self.reqverb === 'invite'){
            alert('Invitation sent');
        } else { // logout
            // self may be not AC_TitleBarVModel obj. use FD
            this.fd.AV_tbar.AF_loginout(false); // no param-pa: hide logout button
            this.fd.AF_se('logout');
            this.fd.AV_acticon_pic = "pics/login2.png";
            var AV_loginpav = this.fd.AV_loginpv;
            this.fd.AV_loginpv = null;
            if(AV_loginpav){ // after loginpav logging out, update all pavs
                var AV_viewables = this.fd.AF_tpath().AF_all_viewables();
                for(var i=0; i<AV_viewables.length; i++){
                    // non-virtual pav's ctxt-menu update
                    if(AV_viewables[i].cid() === 'PAV' &&
                       AV_viewables[i].model){
                        AV_viewables[i].AF_update_lclick_items();
                    }
                }
                this.fd.AF_tpath().AF_focus().AV_patag
                    .AF_setlove_icon(true)
                    .AF_set_focus_bg(true);
            }
            this.fd.AV_synop.AJ_trigger.css('visibility','hidden');
            if($('div#doc_pane').length > 0){
                this.fd.AV_infov.AV_docpane.AF_hideme();
            }
            this.fd.AV_infov.AF_show_state('hidden');
        }
    };// end of AC_TitleBarVModel.AF_handle_success method
    
    // setup invite dialog: invitation from login-pa to pa
    AC_TitleBarVModel.prototype.AF_setup_invite = function(pa){
        this.AJ_addr.val(pa.AF_cfg()["M1502"]);// email addr
        // localized string: "invite <name>"
        this.AJ_subj.val(this.fd.AV_lang.face('M0007')+' '+pa.tname_face());
        var txt = this.fd.AV_lang.face("M1495") + " " + pa.tname_face() + ",\n\n";
        txt += this.fd.AV_lang.face("M1496") +":\n\n";
        txt += window.location.origin + "/?private=" + pa.id() +"\n\n";
        txt += this.fd.AV_lang.face("M1497") +"!\n\n" + 
                this.fd.AV_loginpv.model.tname_face();
        txt += '\n'+ this.fd.AV_loginpv.model.AF_cfg()["M1502"];
        this.AJ_itxt.val(txt);

        var self = this;
        var btn = $('#send-invite-button');
        var addr = self.AJ_addr.val();
        U.AF_validateEmail(addr)? btn.button('enable') : btn.button('disable');
        this.AJ_addr.unbind('input').bind('input', function(e){
            addr = self.AJ_addr.val();
            var msg = U.AF_validateEmail(addr)? 'enable' : 'disable';
            $("#send-invite-button").button(msg);
        });
    };// end of AC_TitleBarVModel.prototype.AF_setup_invite
    
    AC_TitleBarVModel.prototype.AF_send_invite_req = function(){
        this.reqverb = 'invite';
        var b64msg = Base64.encode(this.AJ_itxt.val());
        var myemail = FD.AV_loginpv.model.AF_cfg()['M1502'];
        if(myemail.length < 5)
            myemail = "thememorywhisper@gmail.com";
        this.fd.AV_lserver.AF_post(this.reqverb,
                    {'email_address': this.AJ_addr.val(),
                     'email_subj': this.AJ_subj.val(),
                     'email_text': b64msg,
                     'sender_address': myemail
                    }, this);
        //alert('inviting...');
    };// end of AC_TitleBarVModel.prototype.AF_send_invite_req
    
    AC_TitleBarVModel.prototype.AF_invite_popup = function(pav){
        var self = this;
        self.AV_invite_dlg = self.AJ_invite.dialog({
            autoOpen: false,
            title: self.fd.AV_lang.face('M0007','M0007'), 
            modal: true,
            resizable: false,
            width: 680,
            height: 460,
            show: true,     // animated showing up
            hide: true,     // animated hideout
            close: function(){ },
            buttons: {
                'send-invite': {
                    id: 'send-invite-button',
                    text: '',
                    click: function(e){
                        self.AF_send_invite_req();
                        self.AV_invite_dlg
                            .closest('.ui-dialog-content').dialog('close');
                        return U.AF_stop_event(e);
                    }
                },
                'cancel-invite': {
                    id: 'cancel-invite-button',
                    text: '',
                    click: function(e){
                        self.AV_invite_dlg
                            .closest('.ui-dialog-content').dialog('close');
                        return U.AF_stop_event(e);
                    }
                }
            }
        });
        this.AJ_invite.dialog('open');
        self.AF_setup_invite(pav.model);
        self.AV_invite_dlg.keyup(function(e){
            if(e.keyCode === 13){
                $('#send-invite-button').trigger('click');
            }
        });
        this.AJ_invite.dialog('widget').attr('id','invite-dialog-popup');
        
        this.fd.AV_lang.AF_set_dlg('invite-dialog-popup', {
            '.ui-dialog-title': 'M0007',
            '#send-invite-button': 'M0008',
            '#cancel-invite-button': 'M1003'
        });
    };// end of AC_TitleBarVModel.prototype.AF_invite_popup
    
    AC_TitleBarVModel.prototype.AF_login_popup = function(pav){
        var self = this, uname = pav.model.tname_face();
        self.AJ_username.val(uname);
        self.AJ_username.attr('readonly','readonly');
        this.fd.AV_lang.AF_set_all(this.AJ_login, this.AV_lcode);
        this.login_dlg = this.AJ_login.dialog({
            autoOpen: false,
            title: self.fd.AV_lang.face('M1001','M1001'), 
            modal: true,
            resizable: false,
            width: 380,
            height: 250,
            show: true,     // animated showing up
            hide: true,     // animated hideout
            // see coments before FD.AV_lang.AF_set_dlg
            close: function(e){ 
                self.login_dlg.closest('.ui-dialog-content').dialog("destroy");
            },
            open: function(e){  
                self.AJ_username.val(uname);
                self.AJ_loginpwd.val('');
                var ttltxt = self.fd.AV_lang.face('M1001','M1001');
                self.AJ_loginpwd.css('background-color','white');
                self.login_dlg.dialog('option', 'title', ttltxt);
                self.AJ_loginpwd.unbind('click').bind('click', function(e){
                    // in case prev login failed, pwd input turned pink and
                    // the title showed 'login failed', now clear that away
                    // when the user click/focus mouse on input
                    self.AJ_loginpwd.css('background-color','white');
                    self.login_dlg.dialog('option', 'title', ttltxt);
                });
                $('.ui-dialog-buttonpane > button:first').focus();
            },
            buttons: {
                'login-button': {
                    id: 'login-submit-button',
                    text: '', 
                    click: function(e){
                        $('#login-submit-button').button("disable")
                        self.fd.AF_authenticate(pav, self.AJ_loginpwd.val());
                        return U.AF_stop_event(e);
                    }
                },
                'cancel-button': {
                    id: 'login-cancel-button',
                    text: self.fd.AV_lang.face('M0003','M0003'), 
                    click: function(e){ 
                        self.login_dlg.dialog('close');
                        return U.AF_stop_event(e);
                    }
                }
            }
        });
        this.login_dlg.dialog('open');
        /**/
        this.login_dlg.keyup(function(e){
            if(e.keyCode === 13){   // ENTER key pressed: OK button click
                U.AF_stop_event(e)
                $('#login-submit-button').trigger('click');
                return false;
            }
        });
        /**/
        this.login_dlg.dialog('widget').attr('id','login-dialog-popup');
        
        // dialog has to be destroyed after close (see close:function..)
        // otherwise a new dialog may use the same id, and then id will
        // not be unique, and the wrong one would be localized.
        this.fd.AV_lang.AF_set_dlg('login-dialog-popup', {
            '.ui-dialog-title': 'M1001',
            '#login-submit-button': 'M1001',
            '#login-cancel-button': 'M1003'
        });
    };// end of AC_TitleBarVModel.AF_login_popup method
    
    return AC_TitleBarVModel;
})();
