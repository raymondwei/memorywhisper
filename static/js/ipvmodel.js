﻿/*
 * Copyright (C) 2014 Adacro Inc. - All Rights Reserved
 */
var AC_IPVModel = (function(){
    function AC_IPVModel(ip, infov){
        this.AV_infov = infov;
        // div.right container with all children-its of ip
        this.AJ_container = infov.AJ_right;
        this.ip = ip;
        ip.vmodel = this;
        this.AJ_ul = $('<ul class="right-pane" id="'+this.ip.id()+'_ipv"></ul>');
    }
    AC_IPVModel.prototype.cid = function(){ return 'IPV'; }
    
    // attach ipv to ccp: set title with ip.name, attach jul
    AC_IPVModel.prototype.AF_attachme = function(){
        if(this.AJ_container.find('ul#'+this.ip.id()+'_ipv').length === 0)
            this.AJ_ul.appendTo(this.AJ_container);
        var iw = this.AV_infov.AV_itempane.AV_iw;
        var self = this;
        this.AV_infov.AV_itempane.AJ_addfolder_btn_span.unbind('click')
            .bind('click', function(e){
                iw.AF_popup_dialog(self.ip,'M7127','add');
                return U.AF_stop_event(e);
            });
        this.AV_infov.AV_itempane.AJ_adddoc_btn_span.unbind('click')
            .bind('click', function(e){
                iw.AF_popup_dialog(self.ip,'M7128','add');
                return U.AF_stop_event(e);
            }); 
        return this.AF_refresh_ctxtmenu();
    };// end of AC_IPVModel.prototype.AF_attachme
    
    AC_IPVModel.prototype.AF_refresh_ctxtmenu = function(){
        var concat = this.ip.folders().concat(this.ip.docs());
        for(var i=0; i<concat.length; i++){
            if(concat[i].vmodel){
                concat[i].vmodel.AF_update_clickables();
                // only show the top level. Next level demands expand(click)
                // action which set/update the actions(expand/ctxt-menu) of
                // the containees.
                if(concat[i].type()[0] === 'F')
                    concat[i].vmodel.AF_toggle_state('shrunk');
            }
        }
        return this;
    };// end of AC_IPVModel.prototype.AF_refresh_ctxtmenu
    
    return AC_IPVModel;
})();