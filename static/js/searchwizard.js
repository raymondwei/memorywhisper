/*
 * Copyright (C) 2014 Adacro Inc. - All Rights Reserved
 */
var AC_SearchWizard =(function(){
    function AC_SearchWizard(fd){
        this.fd = fd;
        this.offering = [];
        this.purpose = 'find pa';
        this.AV_lcode = fd.AF_lcode();
        // when this dialog pops up, the f-graph-pne is covered by a
        // div shield (.jbg_cover) with z-index=99. The dialog pop-up
        // from jquery-ui has z-index=100, so it can still be accessed.
        // this shield is detached when the dialog closes.
        this.jbg_cover = $('<div class="contextMenuPluginShield"></div>')
                .css({left:0, top:0, width:'100%', height:'100%', 
                    position:'absolute', zIndex:99});
        this.AF_load_template();
        var self = this;
        FD.AV_lserver.AF_get_idb(FD.fid(), function(){
            self.idb = FD.idbs[FD.fid()];
        });
        this.lwz = new AC_RelationWizard(this);
    };// end of AC_SearchWizard constructor
    
    AC_SearchWizard.prototype.AF_load_template = function(){
        this.AJ_layout = $("<div id='search-dlg'></div>");
        this.jsbutton = $('<img id="s-button" src="pics/magnifier0.png"/>')
            .appendTo(this.AJ_layout);
        this.jsinput = $('<input type="text" id="s-input" />')
            .appendTo(this.AJ_layout);
        this.jrel_startbox = $('<div id="relation-start"></div>')
            .appendTo(this.AJ_layout).css('visibility', 'hidden');
        this.jofferings = $('<div id="offerings"></div>')
            .appendTo(this.AJ_layout);
        this.jloaded = $('<span id="number-loaded"/>').appendTo(this.AJ_layout);
        var m = '<span class="LS" id="pas-loaded" tag="M0015">Loaded</span>';
        this.jpasloaded = $(m).appendTo(this.AJ_layout);
        this.jpa_tname = $('<span id="search-pa-tname" />')
            .appendTo(this.AJ_layout);
        this.jpa_dates = $('<span id="search-pa-dates" />')
            .appendTo(this.AJ_layout);
        this.jpa_pob = $('<span id="search-pa-pob" />').appendTo(this.AJ_layout);
    };// end of AC_SearchWizard.prototype.AF_load_template
    
    /**
     * Find pa by name substring, for purpose of customer issued name search
     * first in store, if no found in idb. return the found pa or null 
     */
    AC_SearchWizard.prototype.AF_findpa = function(namestr){
        var k, p, lst = [],
            pas_dic = FD.AV_store.AF_getEntsByCid('PA');
        for(k in pas_dic){
            p = pas_dic[k];
            if(p.tname_face().indexOf(namestr) > -1){
                lst.push(p);
            }
        }
        this.offering = lst;
        if(this.offering.length > 0)
            this.offering.sort(U.AF_compare_pas);
        this.render_offering();
    };// AC_SearchWizard.prototype.AF_findpa
    
    // clear offering[], add newpas(array of pa) to it - except excl_pa
    // if newpas==null, add all pas 
    AC_SearchWizard.prototype.AF_populate_offering = function(){
        this.offering.length = 0;   // clear up offering list
        var self = this, i;
        this.idb.AF_get_cid_ents('PA').done(function(e){// e:array of pa ent
            for(i=0; i<e.length; i++){
                self.offering.push(e[i]);
            }
        }).fail(function(err){
            U.log(1, "idb:getcid failed: "+err);
        })
    };// end of AC_SearchWizard.prototype.AF_populate_offering
    
    // when switching to find-rel mode, hide jsbutton/jsinput, show
    // pa as name-tag in stead, and other changes
    AC_SearchWizard.prototype.set_find_rel = function(pa){
        // detach li from ul
        pa.rela.parent().parent().detach();//get/detach <li>
        pa.pdiv.appendTo(this.jrel_startbox);
        pa.rela.text('  => ?').css({'font-size':'20px','color':'black'})
            .appendTo(this.jrel_startbox);
        var msg = this.fd.AV_lang.face("M0014") + ' ';
        msg += this.fd.AV_lang.face("M0011")
        this.jofferings.find('.pa-relation').text(msg);
    }
    // set patag in search-dlg be hight-lighted/not-high-lighted
    // hl=true: p be hightlighted, false: not highlighted
    AC_SearchWizard.prototype.highlight_pdiv = function(p, hl){
        if(hl){
            p.pdiv.css('background-image',
                       'url(/pics/goldtag2.png)'); //highlight tag
            if(this.AV_highlight) 
                this.highlight_pdiv(this.AV_highlight, false)
            this.AV_highlight = p;
            p.rela.active = true;
            p.rela.css({'color':'darkblue','font-weight':'bold'});
            p.rela.css('cursor','pointer');
            this.jpa_tname.text(p.tname_face());
            this.jpa_dates.text(U.AF_date_dashed(p.dob(),'.')+' - '
                    +U.AF_date_dashed(p.dod(),'.'));
            this.jpa_pob.text(p.pob_face());
            this.jloaded.css('visibility','hidden');
            this.jpasloaded.css('visibility','hidden');
        } else {
            p.pdiv.css('background-image',
                       'url(/pics/goldtag.png)');//normal-not highlighted
            p.rela.active = false;
            p.rela.css({'color':'white','font-weight': 'normal'});
            p.rela.css('cursor','default');
        }
    };// end of AC_SearchWizard.prototype.highlight_pdiv
    
    // <li.pa-li-offering><div.li-pa-box>
    //   <div.pa-tag>                       // pa.pdiv
    //     <span.LS></span><img/>
    //   </div>
    //   <div.pa-relation></div>            // pa.rela
    // </div></li>
    AC_SearchWizard.prototype.render_pa = function(pa, jul, front){
        var i, self = this;
        var li = $('<li class="pa-li-offering"></li>');
        var box = $('<div class="li-pa-box"></div>').appendTo(li);
        pa.rela = $('<div class="pa-relation"></div>').appendTo(box);
        pa.rela.text(this.fd.AV_lang.face("M0011")); // relationship-text
        
        pa.rela.unbind('click').bind('click', function(e){
            if(!pa.rela.active) return;
            if(self.jrel_startbox.css('visibility') === 'hidden'){
                if(pa === self.AV_highlight){
                    //highlighted pa as rel-search start pa, will sets 
                    self.AF_populate(self.AV_highlight); // purpose:'find rel'
                    self.pa = self.AV_highlight;
                    self.AV_highlight = null;
                }
            } else {
                if(pa === self.AV_highlight){
                    self.idb.AF_get_cid_ents('BA').done(function(e){
                        var AV_path = [];//[['c|s',pa],...] rel:self.pa to pa
                        var found = U.AF_related_pas(self.pa, pa, AV_path);
                        if(found){
                            self.jofferings.empty();
                            self.lwz.AF_render(self.pa, AV_path);
                        } 
                    });
                }
            }
        });
        pa.pdiv = $('<div class="pa-tag"></div>').appendTo(box);
        var imgsrc = pa.sex()==='male'? 
            "pics/male-head.png":"pics/female-head.png"
        var tname = pa.tname_face(this.AV_lcode), 
            LSclss = 'fullname LS',
            ls_tag = pa.id() + '@tname_face'; 
        var msg ='<span class="'+LSclss+'" tag="'+ls_tag+'">'+tname+'</span>';
        $(msg).appendTo(pa.pdiv);
        imgsrc = '<img class="head" src="'+imgsrc+'"/>';
        $(imgsrc).appendTo(pa.pdiv);
        if(front)   jul.prepend(li);
        else        li.appendTo(jul);
        pa.pdiv.unbind('click').bind('click', function(e){
            if(pa !== self.AV_highlight){
                self.highlight_pdiv(pa, true);
            }
            self.reqverb = 'getdics';
            self.purpose = 'pa-center';
            self.fd.AV_lserver.AF_pa_center(pa, self);
        });
    };// end of AC_SearchWizard.prototype.render_pa
    
    AC_SearchWizard.prototype.AF_handle_success = function(data, self){
        if(self.reqverb !== 'getdics'){
            self.jpa_pob.text('');
            self.jpasloaded.css('visibility','visible')
        }
        var pas = data? data.ents : null; // pa.rooting fed by lserver
        //if(self.reqverb === 'getdics'){ 
        if(self.purpose === 'pa-center'){
            if(self.AV_init_path_index < self.fd.AV_path_mgr.ci){
                if(self.fd.AV_path_mgr.ci-self.AV_init_path_index > 1)
                    U.log(1, 'search-wizard tpath overusage.');
                else
                    self.fd.AV_path_mgr.AF_pop_last_path();
            }
            // render f-graph for new (highlight)center-pa
            $('div#tab1').scrollTop(0).scrollLeft(0);
            
            // chop off all paths behind curr_index
            self.fd.AV_path_mgr.AF_chop_from_ci();
            
            self.fd.AV_path_mgr.AF_new_path(self.AV_highlight)
                .AF_center_pa(self.AV_highlight);
            self.fd.AV_landing.AF_page()
                .AF_render().AF_update_focus_ppane();
        }
        //}
    };// end of AC_SearchWizard.prototype.AF_handle_success
    
    AC_SearchWizard.prototype.render_offering = function(){
        var ul = $("ul#pa-list").empty();
        for(var i=0; i<this.offering.length; i++){
            this.render_pa(this.offering[i], ul);
        }
        return this;
    };// end of AC_SearchWizard.prototype.render_offering
    
    AC_SearchWizard.prototype.AF_setup_handlers = function(){
        var self = this;
        self.found_local = 0;
        if(self.purpose === 'find pa'){
            self.jsinput.val('');
            // search-button event-handler: if input field not empty. 
            self.jsbutton.unbind('click').bind('click', function(e){
                // only act when input field not empty(active icon shown up)
                if(self.jsbutton.attr('src') === 'pics/magnifier1a.png'){
                    // find pas using namestr put result array in offering
                    self.AF_findpa(self.jsinput.val());
                }
            });
            // set search button active/inactive depending on if input's empty
            self.jsinput.unbind('input').bind('input', function(e){
                if(self.jsinput.val() !== ''){
                    self.jsbutton.css('cursor','pointer');
                    self.jsbutton.attr('src','pics/magnifier1a.png');
                } else {
                    self.jsbutton.css('cursor','default');
                    self.jsbutton.attr('src','pics/magnifier0.png');
                }
            });
        }
    };// end of AC_SearchWizard.prototype.AF_setup_handlers
    
    AC_SearchWizard.prototype.AF_populate = function(pa){
        this.jpa_tname.text('');
        this.jpa_dates.text('');
        this.jpa_pob.text('');
        $('<ul id="pa-list"></ul>').appendTo(this.jofferings.empty());
        this.AF_populate_offering();
        if(pa){
            this.jrel_startbox.empty().css({'visibility':'visible'});
            this.jsbutton.css('visibility', 'hidden');
            this.jsinput.css('visibility', 'hidden');
            this.purpose = 'find rel';
            this.set_find_rel(pa);
        } else {
            // pa not given. next op: find pa with name partial-str: 'find pa'
            this.jrel_startbox.css('visibility', 'hidden');
            this.jsbutton.css('visibility', 'visible');
            this.jsinput.css('visibility', 'visible');
            this.purpose = 'find pa';
            this.AV_highlight = null;
        }
    };// end of AC_SearchWizard.prototype.AF_populate
    
    // dialog pop up
    // 1. pa == null: offer search-pa; 
    // 2. pa != null: search relationship for pa
    AC_SearchWizard.prototype.AF_open_dlg = function(pa){
        this.pa = pa;
        this.AV_lcode = this.fd.AF_lcode();
        // make sure dialog language is the same as global lcode
        FD.AV_lang.AF_set_all(this.AJ_layout, this.AV_lcode);
        var self = this;
        self.AV_init_path_index = this.fd.AV_path_mgr.ci;
        var ttlmsg = this.fd.AV_lang.face('M7520');
        if(pa){
            ttlmsg = this.fd.AV_lang.face('M0011');
        }
        //this.AV_actbuffer.init();
        self.dlg = self.AJ_layout.dialog({
            title: ttlmsg,
            autoOpen: false,
            width: 350,
            height: 780,
            position: {my: "top", at:"right"},// location: bottom/right side
            modal: false, // background inactive; false: inter-action w. bg
            resizable: false,
            show: true,
            hide: true,
            close: function(e){ 
                self.dlg.dialog('destroy'); 
                self.fd.AV_tbar.AF_show_hide_nav(true);
                self.jbg_cover.detach();
            },
            open: function(){
                self.AF_populate(pa);
                self.AF_setup_handlers();
                self.fd.AV_tbar.AF_show_hide_nav(false);
                self.jbg_cover.appendTo(document.body);
            }
        });
        self.dlg.dialog('open');
        self.dlg.keyup(function(e){
            // ENTER pressed and search is ready: do search
            if(e.keyCode === 13 && 
               self.jsbutton.attr('src') === 'pics/magnifier1a.png'){
                self.jsbutton.trigger('click');
            }
        });
        self.dlg.dialog('widget').attr('id', 'search-wizard-popup');
    };// end of AC_SearchWizard.prototype.dlg
    
    return AC_SearchWizard;
})()