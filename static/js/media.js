﻿/*
 * Copyright (C) 2014 Adacro Inc. - All Rights Reserved
 * ----------------------------------------------------
 * AC_AudioPlayer class
 */
var AC_AudioPlayer = (function(){
    function AC_AudioPlayer(bigshow){
        this.AV_bigshow = bigshow;
        this.AJ_container = bigshow.AJ_container;
    };// end of AC_AudioPlayer constructor
    
    AC_AudioPlayer.prototype.AF_init = function(fcv){
        this.fcv = fcv;
        this.tags = [];
        this.htag = null;
        // clone of existing contdict
        this.AV_curr_contdict = $.extend(true, {}, fcv.model.AF_contdict());
        
        this.player = $('<audio>',{autoPlay:'autoplay', loop: 'none'})
          .appendTo(this.AJ_container.css('background-image','none').empty());
        this.vol_label = $('<span id="vol-label" tag="M6021"></span>')
            .text(FD.AV_lang.face('M6021')).appendTo(this.AJ_container);
        this.vol_value = $('<span id="vol-value"></span>')
            .text('5%').appendTo(this.AJ_container);
        var self = this;
        this.jvslider = $('<div id="audio-vol-slider"></div>')
            .appendTo(this.AJ_container);
        this.vslider = this.jvslider.slider({
                orientation: "vertical",
                range: "min",
                min: 0,
                max: 100,
                value: 5,
                slide: function(e, ui){
                    self.vol_value.text(ui.value+'%');
                    self.player[0].volume = ui.value/100;
                }
            });
        this.player[0].volume = 0.05;
        this.jprogress = $('<div id="audio-progress-slider"></div>')
            .appendTo(this.AJ_container);
        this.progress = this.jprogress.slider({
                orientation: "horizontal",
                range: "min",
                min: 0,
                max: 100,
                value: 0,
                slide: function(e, ui){
                    self.player[0].currentTime =
                        (ui.value/100) * self.player[0].duration;
                }
            });
        this.ppbutton = $('<div id="play-pause"></div>')
            .appendTo(this.AJ_container);
        this.stopbutton = $('<div id="stopplay"></div>')
            .appendTo(this.AJ_container);
        this.fjumpbutton = $('<div id="forward-jump"></div>')
            .appendTo(this.AJ_container);
        this.bjumpbutton = $('<div id="backward-jump"></div>')
            .appendTo(this.AJ_container);
        this.ppstate = 'playing'; // paused
        this.timeinfo = $('<span id="playtime-info"></span>')
            .appendTo(this.AJ_container);
        // audio tag mgr
        this.tag_container = $('<div id="audio-tag-container"></div>')
            .appendTo(this.AJ_container);
        this.AJ_tag_descr = $('<div id="audio-tag-description"></div>')
            .appendTo(this.AJ_container).css('visibility','hidden');
        this.AJ_player_handler = $('div#audio-progress-slider a.ui-slider-handle');
        if(this.AV_bigshow.AV_docpane.AV_allow_write)
            this.thunder = $('<img id="tag-thunder"></img>')
                .appendTo(this.AJ_container).attr('src','pics/thunder.png');
        // populate tags, if exist, un-highlighted
        var tag_count = this.AV_curr_contdict['tags'].length;
        for(var i=0; i<tag_count; i++){
            var t = new AC_AVTag(this, this.AV_curr_contdict['tags'][i]);
            // isnew=false: dont put in curr_contdict. It already existed.
            var ind = this.AF_insert_newtag(t, false);
            t.name(''+(ind+1));
        }
        AC_BinObjMgr.AF_feed_media(this.player, fcv.model.rs());
        this.AF_setup_handlers();
    };// end of AC_AudioPlayer.prototype.AF_init
    
    AC_AudioPlayer.prototype.AF_setup_handlers = function(){
        var self = this;
        this.ppbutton.unbind('click').bind('click', function(e){
            if(self.ppstate === 'playing'){
                self.player[0].pause();
                self.ppstate = 'paused';
                self.ppbutton.css('background-image',
                    'url(../pics/play-button.png)');
            } else {
                self.player[0].play();
                self.ppstate = 'playing';
                self.ppbutton.css('background-image',
                    'url(../pics/pause-button.png)');
            }
            return U.AF_stop_event(e);
        });
        this.stopbutton.unbind('click').bind('click', function(e){
            self.player[0].pause();
            self.player[0].currentTime = 0;
            self.ppbutton.css('background-image','url(../pics/play-button.png)');
            self.ppstate = 'paused';
            return U.AF_stop_event(e);
        });
        this.player.unbind('ended').bind('ended', function(e){
            self.player[0].pause();
            self.player[0].currentTime = 0;
            self.ppbutton.css('background-image','url(../pics/play-button.png)');
            self.ppstate = 'paused';
            return U.AF_stop_event(e);
        });
        this.player.unbind('timeupdate').bind('timeupdate', function(e){
            var msg = U.AF_time_count(U.INT(self.player[0].currentTime)) +"/"+
                      U.AF_time_count(U.INT(self.player[0].duration));
            self.timeinfo.text(msg);
            self.jprogress.slider({value:
                100*(self.player[0].currentTime/self.player[0].duration)});
        });
        if(this.AV_bigshow.AV_docpane.AV_allow_write){
            this.AJ_player_handler.dblclick(function(e){
                self.AF_add_tag();
            });
            this.thunder.unbind('click').bind('click', function(e){
                self.AF_add_tag();
            });
            self.AJ_tag_descr.unbind('keyup').bind('keyup',function(e){
                if(self.htag){
                    self.htag.src_pair[1]= Base64.encode(self.AJ_tag_descr.html());
                    self.AF_update_save_btn();
                }
            });
        }
    };// end of AC_AudioPlayer.prototype.AF_setup_handlers
    
    AC_AudioPlayer.prototype.AF_add_tag = function(){
        if(!('tags' in this.AV_curr_contdict))
            this.AV_curr_contdict['tags'] = [];
        var pair = [''+this.player[0].currentTime,''];
        var t = new AC_AVTag( this, pair);
        var index = this.AF_insert_newtag(t, true);// put into curr_contdict: true
        t.AF_set_highlight(true).name(''+(index+1));
        this.AF_update_save_btn();
    };// end of AC_AudioPlayer.prototype.AF_add_tag

    // .tags should be sorted along the time_offset, so that a tag with
    // earlier time_offset should stay front. This may change when adding new
    // tag, or when a tag's time_offset is changed
    AC_AudioPlayer.prototype.AF_insert_newtag = function(thetag, isnew){
        // find index of tag in .tags with time_offset bigger than tag's
        var ind = 0;
        for(ind=0; ind<this.tags.length; ind++){
            if(thetag.time_offset < this.tags[ind].time_offset){
                break;
            }
        }
        // insert thetag before ind-th element, thetag is now ind-th
        this.tags.splice(ind, 0, thetag);
        if(isnew) // not already in curr_contdict
            this.AV_curr_contdict['tags'].splice(ind, 0, thetag.src_pair);
        
        // if inserted in front of tags, rename all tags behind thetag
        if(ind < (this.tags.length - 1)){
            for(var ii=ind+1; ii<this.tags.length; ii++){
                this.tags[ii].name(''+(ii+1));
            }
            // insert before 
            thetag.div.insertBefore(this.tags[ind+1].div);
        } else {
            // append as last under tag_container in DOM
            thetag.div.appendTo(this.tag_container);
        }
        return ind;
    };// end of AC_AudioPlayer.prototype.AF_insert_newtag
    
    AC_AudioPlayer.prototype.AF_update_save_btn = function(){
        var str0 = JSON.stringify(this.fcv.model.AF_contdict());
        var str1 = JSON.stringify(this.AV_curr_contdict);
        if(str0 === str1){
            this.fcv.AF_contdict({});
        } else {
            this.fcv.AF_contdict(this.AV_curr_contdict);
        }
    };// end of AC_AudioPlayer.prototype.AF_update_save_btn
    
    return AC_AudioPlayer;
})();

var AC_VideoPlayer = (function(){
    function AC_VideoPlayer(bigshow){
        this.AV_bigshow = bigshow;
        this.AJ_container = bigshow.AJ_container;
    };// end of AC_VideoPlayer constructor
    
    AC_VideoPlayer.prototype.AF_init = function(fcv){
        this.fcv = fcv;
        this.tags = [];
        this.AV_curr_contdict = $.extend(true, {}, fcv.model.AF_contdict());
        var vbox = $('<div id="video-box"></div>').appendTo(this.AJ_container);
        var msg = '<video width="320" height="240" controls="controls" '; 
        msg += 'preload="none" type="video/mp4"></video>';
        this.player = $(msg).appendTo(vbox);
        AC_BinObjMgr.AF_feed_media(this.player, fcv.model.rs());
    };// end of AC_VideoPlayer.prototype.AF_init
    
    return AC_VideoPlayer;
})();