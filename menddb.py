# repair mysql db data identified by fid
# usage: python menddb.py <fid>
import sys
from modules import ftsuper, utils, tmp

if __name__ == '__main__':
    if len(sys.argv) == 2: # cmd-line: python menddb.py <fid>
        ft = ftsuper.FTSUPER()
        fid = sys.argv[1]
        ft.db._del_cid(fid, 'LE')
        ct = utils.CT(fid, ft)
        ct.update()
    else:
        print("usage: python menddb.py <fid>")