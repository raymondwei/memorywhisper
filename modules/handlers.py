﻿#
# Copyright (C) 2014 Adacro Inc. - All Rights Reserved
# -----------------------------------------------------------------------------
# handlers for http requests
# -----------------------------------------------------------------------------
import base64, random
from hashlib import sha1
import time
from modules import utils
from pdb import set_trace

# client sent pid/pwd and 'C:<se-dic>', client already has sid
def login(req, ft):
    utils.action_log('debugA', "request: /login", ft.db)
    #set_trace()
    msg = {'result': '__failed__'} 
    try:
        pid = req.values.get('_pid',None)
        se = req.values.get('se',None)
        if (not pid) or len(pid) != 30: return msg
        se = utils.json.loads(se)
        ft.db.put_ent(se)
        hashed_pwd = req.values.get('_pwd','')
        pwd = utils.tostr(base64.b64decode(utils.tobytes(hashed_pwd)))[30:]
        utils.action_log('debugA', "pwd: "+pwd, ft.db)
        #set_trace()
        if pwd and utils.verify_pwd([pid], 'login', pwd, ft.db):
            utils.action_log('debugA',"login pwd verified.",ft.db)
            try:
                ft.db.put_ent(se) # save log-entry
            except:
                utils.action_log('debugE',"request: /login exception-1", ft.db)
                #set_trace()
            msg = {'result': '__OK__'}
            utils.action_log('debugA', "request: /login __OK__", ft.db)
        return msg
    except:
        utils.action_log('debugE',"request: /login exception-2", ft.db)
        return {'result':'__failed__'}

def logout(request, ft):
    utils.action_log('debugA', "request: /logout", ft.db)
    try:
        #set_trace()
        sid = request.values.get('_sid', None)
        if sid:
            se = utils.SE(ft.db.get_ent(sid), ft.db)# find se in T_FTSUPER
            se.die()    # terminate it
            utils.action_log('debugA', "request: /logout __OK__", ft.db)
            return {'result':'__OK__'}
        utils.action_log('debugB', "request: /logout __OK__sid_missing", ft.db)
        return {'result':'__OK__sid_missing'}
    except:
        utils.action_log('debugE', "request: /logout exception-1", ft.db)
        return {'result':'__failed__'}

def putdocs(req, ft):
    utils.action_log('debugA', "request: /putdocs", ft.db)
    try:
        #set_trace()
        pid = req.values.get('_pid', None)
        res = ft.transact.doit(req.values)
        # res: {<cid>:[('C',{<ent>}),('D','<id>'),(),..], <cid>:[],..}
        if not res:
            utils.action_log('debugB',"request: /putdocs __fail__", ft.db)
            return {'result':'__failed__'}
        dels = []
        for cid, lst in res.items():
            for tu in lst:
                if tu[0] == 'D':
                    # tu[1]: (id string "<fid>-<cid>-<eid>"")
                    dels.append(tu[1])
        ct = res['CT'][0][1]
        utils.action_log('debugA', "request: /putdocs __OK__", ft.db)
        return {'result':'__OK__', 'ctver': ct['version'], 'dels': dels }
    except:
        utils.action_log('debugE', "request: /putdocs exception-1", ft.db)
        return {'result':'__failed__'}

def verifypwd(req, ft):
    utils.action_log('debugA', "request: /verifypwd", ft.db)
    try:
        #set_trace()
        pwd = req.values.get('_pwd','')
        pid = req.values.get('_pid',None)
        pid2 = req.values.get('_pid2',None)
        key = req.values.get('_key',None)
        if utils.verify_pwd([pid,pid2],key,pwd,ft.db):
            utils.action_log('debugA', "request: /verifypwd __OK__", ft.db)
            return {'result':'__OK__'}
        else:
            utils.action_log('debugB', "request: /verifypwd __fail__", ft.db)
            return {'result':'__filed__'}
    except:
        utils.action_log('debugE', "request: /verifypwd exception-1", ft.db)
        return {'result':'__filed__'}

def newft(req, ft):
    utils.action_log('debugA', "request: /newft", ft.db)
    try:
        #set_trace()
        pcode = req.values.get('_pcode', None)
        if pcode:
            coll = None
            rp = ft.db.get_ent('FTSUPER-RP-'+pcode)
            # regardless if pcode has F or T, drop/create table, wipeout
            ft.db.load_table_names() # refresh table-names
            if pcode in ft.db.table_names:
                ft.db.drop_fid_table(pcode) # drop fid table if existed
                del ft.db.table_names[pcode]
            tname = ft.db._fid_table_name(pcode)
            if tname:
                coll = ft.transact.doit(req.values)
            # coll is dic collecting what's done
            # {'<cid>':[(op,dic),..]}, op:'C|D',dic:entity|id
            if coll:
                pa = coll['PA'][0][1] # only 1 in 'PA':[('C',{pa})]
                email = pa['cfg']['M1502']
                msg = "Hello! Your Memory-Whisper account is ready to use.\n"
                msg+= "Please go to http://memorywhisper.com/?private="
                msg+= pa['_id']+"\nYour login password: "+ft.transact.pwd
                msg+= "\n\nYour Memory-Whisper team.\n\n"
                utils.send_email(email,                  # target address
                                "Your account is ready", # subject
                                msg, ft.db)

                rp['inuse'] = 'T'
                msg =  'initial PA: ' + pa['_id'] + ', email: '+email
                rp['descr'] = msg
                ft.db.put_ent(rp)
                utils.action_log('debugA', "request: /newft __OK__", ft.db)
                return {'result':'__OK__','msg': msg}
        utils.action_log('debugB', "request: /newft __OK__", ft.db)
        return {'result':'__failed_wrong_pcode'}
    except:
        return {'result': '__failed__'}

def docview(req, ft): # get fcs in doc and ents along way up to pa
    utils.action_log('debugA', "request: /docview", ft.db)
    try:
        #set_trace()
        docid = req.values.get('docid', None)
        doc = ft.db.get_ent(docid)
        if not doc:
            return {'result':'__OK__', 'docs': []}
        # collecting doc and all its containees(facets,docs,folders,ownerpa)

        # ids: list of ids of all containees
        ids = [docid, 'FTSUPER-LS-icons','FTSUPER-LS-en']

        pa = ft.db.get_ent(doc['iddict'].get('ownerpa',None))
        if pa:
            # get pa's prefered lcode (cfg.M1508)
            lsid = 'FTSUPER-LS-' + pa['cfg']["M1508"]
            if lsid not in ids:
                ids.append(lsid)
            lst = [pa]
            utils.collect_treenodes(lst, ids, ft.db)

        # go upwards
        oid = doc['iddict'].get('owner', None)
        while oid and oid[8:10] != 'PA':
            o = utils.collect_treenodes(lst, oid, ft.db)
            oid = o['iddict'].get('owner', None) 
        utils.action_log('debugA', "request: /docview __OK__", ft.db)
        return {'result':'__OK__', 'docs': lst}
    except:
        utils.action_log('debugE', "request: /docview exception", ft.db)
        return {'result':'__failed__'}
    # END OF --------------- def docview(req, ft)

def invite(req, ft):
    utils.action_log('debugA', "request: /invite __OK__", ft.db)
    msg = {'result': '__failed__'} 
    #set_trace()
    try:
        address = req.values.get('email_address', None)
        text = req.values.get('email_text',None)
        subj = req.values.get('email_subj',None)
        sender_address = req.values.get('sender_address',None)
        if address and text and subj and sender_address:
            txt =  utils.tostr(base64.b64decode(utils.tobytes(text)))
            utils.send_email(address, subj, txt, sender_address)
            utils.action_log('debugA', "request: /invite __OK__", ft.db)
            msg = {'result': '__OK__'}
    except:
        utils.action_log('debugE', "request: /invite exception", ft.db)
    return msg

def handle_merge(req, ft):
    utils.action_log('debugA',"handling /merge", ft.db)
    try:
        pid = req.values.get('_pid', None)
        p0id = req.values.get('p0id', None)
        p1id = req.values.get('p1id', None)
        op0 =  req.values.get('op0', None)
        op1 =  req.values.get('op1', None)
        if merge.mergepa(p0id, p1id, op0, op1, repo):
            return {"result": "__OK__","ids": []}
        else:
            return {"result": "__failed__"}
    except:
        return {"result": "__failed__"}
        
def getcid(req, ft): # /getcid?fid=<fid>&cid=<cid>
    utils.action_log('debugA', "request: /getcid", ft.db)
    msg = {'result': '__failed__'}
    try:
        fid = req.values.get('fid', None)
        cid = req.values.get('cid', None)
        #set_trace()
        if fid and cid:
            recs  = ft.db._get_cid_recs(fid, cid)
            lst = []
            for rec in recs:
                lst.append(rec[0])
            msg = {'result':'__OK__', 'lst': lst}
            utils.action_log('debugA', "request: /getcid __OK__", ft.db)
        return msg
    except:
        utils.action_log('debugE', "request: /getcid exception", ft.db)
        return msg

# ----------------------------------------------------------------------------
# save under blobs/<fid>/binary or text depending on mime
# client post a /req/saveblob request, for saving a media file which can be 
# an images, audio, pdf, video,.. data: 'data:image/jpeg;base64,/9j/4AAQS...';
# or text file data: base64-encoded("<text>")
def saveblob(req, ft):
    utils.action_log('debugA', "request: /saveblob", ft.db)
    try:
        data = req.values.get('data', None)
        mime = req.values.get('mime', None)
        if data and mime:
            filepath = req.values.get('filepath', None)
            fid, name = filepath.split('/')
            if mime != 'text/plain':
                # data: 'data:image/jpeg;base64,/9j/4AAQS...'
                header, body = data.split('base64,') # body: /9j/4AAQS...
                # because webworker sends req with 
                # Content-Type==application/x-www-form-urlencoded, which
                # converts +(in b64 string) to space, I convert it back to +
                # other char / in b64 string seems to be safe.
                body = body.replace(' ','+') 
                bdata = base64.b64decode(utils.tobytes(body)) # binary
                if ft.db.put_blob(fid, name, bdata):
                    return {"result": "__OK__"}
            else: # text/plain
                # for text/plain, data is already b64 encoded. Just save it
                if ft.db.put_blob(fid, name, data):
                    utils.action_log('debugA',"request: /saveblob __OK__",ft.db)
                    return {"result": "__OK__"}
        utils.action_log('debugB', "request: /saveblob __fail__", ft.db)
        return {"result": "__failed__"}
    except:
        utils.action_log('debugB', "request: /saveblob exception", ft.db)
        return {"result": "__failed__"}

def getdics(req, ft):
    utils.action_log('debugA', "request: /getdics", ft.db)
    msg = {'result': '__failed__'}
    try:
        ids = utils.json.loads(req.values.get('ids', []))
        lst = []
        for id in ids:
            dic = ft.db.get_ent(id)
            if dic and id[8:10] == 'CT':  # ct: remove from ct['snapshots']
                dic = utils.filter_ct(dic)# UR|VR|EP|RP|CT|RC
            if dic:
                lst.append(dic)
                # for fc, always get rs, if the fc has rs-id
                if id[8:10] == 'FC':
                    rs_id = dic['iddict'].get('rs',None)
                    if rs_id:
                        rs = ft.db.get_ent(rs_id)
                        if rs:
                            lst.append(rs)
            elif id[8:10] == 'CE':
                #set_trace()
                ce = utils.make_ce(id, ft.db)
                ft.db.put_ent(ce)
                lst.append(ce)
        utils.action_log('debugA', "request: /getdics __OK__", ft.db)
        msg = {'result':'__OK__', 'docs': lst}
        return msg
    except:
        utils.action_log('debugE', "request: /getdics exception", ft.db)
        return msg

def verifypurl(req, ft):
    utils.action_log('debugA', "request: /verifypurl", ft.db)
    msg = {'result': '__failed__'}
    try:
        pid = req.values.get('pid',None)
        if pid:
            pa = ft.db.get_ent(pid)
            if pa:
                msg['result'] = '__OK__'
        utils.action_log('debugA', "request: /verifypurl __OK__", ft.db)
        return msg
    except:
        utils.action_log('debugE', "request: /verifypurl exception", ft.db)
        return msg