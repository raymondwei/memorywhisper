# Copyright (C) 2014 Adacro Inc. - All Rights Reserved
# -----------------------------------------------------------------------------
# FTSUPER is the dispatcher between business logic and dal(DB access)
# -------------------------------------------------------------------
from modules import handlers, dal, utils, transact, tmp
from time import time
from base64 import b64encode
from shutil import copy
import re, os, hashlib, zipfile

class FTSUPER(object):
    def __init__(self):
        self.db = dal.DB()
        self.tname = self.db._fid_table_name('FTSUPER') # T_FTSUPER
        # self.resp_cache = {} # response-cache {<key>:[<ts>,<resp-str>], ...}
        # each client identified by ip-pairs: "<public-ip>#<localip>"
        # where local ip is by using browser's WebRTC (U.AF_localip call)
        # in case WebRTC not supported by client-browser, <localip>="badapple"
        self.ipmap = self.load_eps()   # {<ip-pair>:<EP-id>}
        self.transact = transact.Transact(self)
        if self.load_version_record():
            ver = self.db.get_ent('FTSUPER-VR-'+self.version)
            if ver:
                ver['runcount'] += 1
                self.db.put_ent(ver)
            else:
                rec = {'_id': 'FTSUPER-VR-'+self.version,
                       'runcount': 1, 
                       'tx': utils.get_tx10() }
                self.db._put_rec('FTSUPER','VR',self.version,None,
                                 utils.json.dumps(rec))

    def load_version_record(self):
        p = re.compile(r'build\s+(?P<ver>\d+\.\d+\.\d+\.\d+)')
        # line in index.html: build0.16.1583.20171114
        data = open('static/index.html').read()
        m = p.search(data)
        if m:
            self.version = m.group('ver')
            return True
        return False
    
    def load_eps(self):
        the_map = {}
        #utils.set_trace()
        recs = self.db._get_cid_recs('FTSUPER','EP') #(rec,eid,ts)
        for rec in recs:
            ep = utils.json.loads(rec[0])
            if 'ippair' in ep:
                the_map[ep['ippair']] = ep['_id']
        return the_map

    # given ip-address, find/return the EP-ent in FTSUPER 
    def find_endpoint(self, ippair):
        if ippair in self.ipmap: 
            # found cached ep-id in ipmap
            epid = self.ipmap[ippair]
            dic = self.db.get_ent(epid)
            return dic
        else: # not fount in ipmap cache, keyed by ippair
            # in case ep does exist in DB, but missing in map: fill it in map
            # get all rows/rec of all ep-ents
            recs = self.db._get_cid_recs('FTSUPER','EP')
            # recs: (('{}','<eid>',<ts>),('{}','<eid>',<ts>))
            if recs: # recs is a tuple of rec
                for rec in recs: # rec :('{..}','<eid>',ts), use '{..}'
                    dic = utils.json.loads(rec[0])
                    if 'ippair' in dic and dic['ippair'] == ippair:
                        self.ipmap[ippair] = dic['_id']
                        return dic
            # ep not existing in DB. create one
            _id = utils.make_id('FTSUPER','EP')
            self.ipmap[ippair] = _id
            dic = {'_id': _id, 'ippair': ippair}
            self.db.save_ent(dic)
            return dic

    # load pcodes into FTSUPER-RP-(eid=<serial#><pcode>)
    def load_rp(self, lst): 
        # lst is a list of format: [(<s#>,<pcode>,<descr>),(),...]
        # from pcode source file (tools/__pcodes.py)
        try:
            # get existing fids from DB
            recs = self.db._get_cid_recs('FTSUPER', 'RP') 
            eids = [rec[1] for rec in recs] # eid is fid existing in DB already
            for sn, eid, used, descr in lst:
                if eid not in eids: # if eid not already in DB
                    # make rec in FTSUPER-RP-* record
                    dic = {'_id': 'FTSUPER-RP-'+eid, 
                           'inuse':used, 'descr': descr,'sn': sn}
                    # save it into DB
                    self.db._put_rec('FTSUPER','RP',
                                      eid, None, utils.json.dumps(dic))
                    eids.append(eid) #
            return True
        except:
            return False

    # download all rows of class FTSUPER-RP-* into <ROOT>/tools/__pcodes.py
    # the file will be over-written.
    def download_rp(self):
        try:
            recs = self.db._get_cid_recs('FTSUPER','RP')
            lst = []
            for rec in recs:
                dic = utils.json.loads(rec[0])
                lst.append([str(dic['sn']),     # col-1: serial#
                            str(rec[1]),        # col-2: fid
                            str(dic['inuse']),  # col-3: T|F - inuse
                            str(dic['descr'])]) # col-4: description
            # lst sorted by serial#
            lst.sort()

            # output to tools/__pcodes.py
            ofile = open('tools/__pcodes.py','w')
            ofile.write('# downloaded from DB:' + utils.get_ts18()[0:-3] +'\n')
            ofile.write('pcodes=[\n')
            for e in lst:
                ofile.write(str(e)+',\n')
            ofile.write(']')
            ofile.close()
            return True
        except:
            return False

    def backup_db(self, fid, pid='FTSUPER-ADMIN'):
        if not os.path.exists('dbbackups/'+fid):
            os.makedirs('dbbackups/'+fid)
        logfile = open('dbbackups/'+fid+'/log.txt','a')
        ct_obj = utils.CT(fid, self)
        ct = ct_obj.ent
        fa = ct_obj.fa
        #fa['history'] = fa['history'][:1] # temp for testing
        snapshots = ct['snapshots'] # {<id>:<ts>,...}
        last_mhash = fa['history'][-1].split(':')[-1]

        # ts format: 2017-11-14T19:23:57 => 20171114T192357
        ts = utils.get_ts18()[0:15] # 20171216T103954
        logfile.write('[' + ts + ']\n')
        tx = utils.get_tx10()
        filebasename = 'dbbackups/' + fid +'/'+ fid + ct['version'] + '-' + ts
        tmpname = 'dbbackups/tmp.data'

        # save data into tmp.data, then open a new file, then
        # put header-line(with sig) + tmp.data content

        mfilenames = []
        media_size = pa_cnt = 0
        # put every ent in string-line format(with \n) into ofile
        ofile = open(tmpname, 'w')  # openfile(tmp.data) for writing data
        data_str = utils.json.dumps(ct)
        data_size = len(data_str)
        ofile.write(b64encode(data_str.encode()).decode() + '\n')
        for key in snapshots: # key in snapshots are ids of all ents in DB
            ent = self.db.get_ent(key)
            if ent:
                if ent['_id'][8:10] == 'PA':
                    pa_cnt += 1
                data_str = utils.json.dumps(ent)
                data_size += len(data_str)
                line = b64encode(data_str.encode()).decode() + '\n'
                ofile.write(line)
                if ent['_id'][8:10] == 'RS' and \
                   (isinstance(ent['size'],int) or ent['size'].isdigit()):
                    media_size += int(ent['size'])
                    #utils.set_trace()
                    if ent['name'].find('-FC-') > -1:
                        name = ent['name'] + '.txt'
                    elif ent['_id'][11:18] == 'landing':
                        name = ent['name']
                    else:
                        #utils.set_trace()
                        name = str(ent['size']) + '-' + ent['signature'] + '.'
                        name += utils.mime_dict[ent['mime']]
                    if name not in mfilenames:
                        mfilenames.append(name)
        mfilenames.sort()
        md5 = hashlib.md5()
        md5.update('|'.join(mfilenames).encode())
        mhash = md5.hexdigest()
        his = ct['version' ] +':'+ str(pa_cnt) + ':' + str(data_size) + ':'
        his += ':' + str(media_size) + ':' + mhash
        fa['history'].append(his)
        fa['tx'] = tx
        self.db.put_ent(fa)
        data_str = utils.json.dumps(fa)
        ofile.write(b64encode(data_str.encode()).decode() + '\n')
        ofile.close()

        # sha1 hash value of all data lines, from tmpname file -> signature
        sha1 = hashlib.sha1()
        data = open(tmpname).read()
        sha1.update(data.encode()) # sha1 needs byte-string. encode(data) -> bstr
        signature = sha1.hexdigest() # signature is of type string, not bstr
        
        # header-line format:
        # '#0.16.1583.20171115:<pid>:<ts>:<sig>\n'
        headline = '#'+self.version+':'+pid+':'+ts+':'+signature+'\n@:@'

        ofile = open(filebasename + '.data','w')
        ofile.write(headline)
        ofile.write(data)
        ofile.close()
        logfile.write('Save: '+filebasename + '.data\n')

        # history[-1]: "<ctver>:<pc-cnt>:<dsize>:<msize>:<mhash>"
        # compare with last_mhash with current mhash: if diff, call:
        zipfname = filebasename+'.zip'
        if mhash != last_mhash or (not os.path.exists(zipfname)):
            self.backup_mediazip(fid, filebasename+'.zip', mfilenames, logfile)
        else:
            logfile.write('Use prev zip file.')
        logfile.close()
        return filebasename

    # make a zip file
    def backup_mediazip(self, fid, filename, names, logfile):
        # create and save all files in ctmediafiles into it
        existings = os.listdir('blobs/'+fid) # all blob-files existing for fid
        z = zipfile.ZipFile(filename,'w') # create a zipfile
        logfile.write('Save: '+filename+'\n')#log action of saving zip file
        for name in names: # <size>-<signature>.<ext> under blobs/<fid>/
            fname = 'blobs/' + fid + '/' + name
            if os.path.exists(fname):
                existings.remove(name)
            else:
                logfile.write('Missing: '+fname+'\n')
                if fname.split('.')[-1] == 'txt':
                    copy('blobs/FTSUPER/dummy.txt',fname)
                elif fname.split('.')[-1] == 'jpg':
                    copy('blobs/FTSUPER/dummy.jpg',fname)
                elif fname.split('.')[-1] == 'png':
                    copy('blobs/FTSUPER/dummy.png',fname)
            z.write(fname)
        if len(existings) > 0:
            for fn in existings:
                logfile.write('Unused: ' + fn + '\n')
        z.close()

    # for every req, update stats/count in ep for the ipaddr
    def update_req_count(self, req, verb):
        try:
            #utils.set_trace()
            req_id = req.values.get('_req_id',None)#<ip>#<lip>@<cname>_<counter>
            if not req_id:
                return None
            clientid = req_id.split('@')[0] # "<ip>#<lip>" or "<lip>"
            twoips = ep = new_client_ipaddr = None
            if clientid.find('#') > -1:
                twoips = clientid  # "<ip>#<lip>"
            else:                  # "<lip>" (local-ip)
                new_client_ipaddr = req.environ['REMOTE_ADDR']
                twoips = new_client_ipaddr + '#' + clientid
            ep = self.find_endpoint(twoips)
            fid = req.values.get('_fid', None)
            # for some reason, ep set to None? this happens mostly on the very
            # beginning where maybe DB connections timed out, so that
            # using etn-id, ent cannot be fetched (self.find_endpoint). WEIRD
            if fid and ep:
                fid_dic = ep.setdefault(fid, {})
                month_ts = utils.get_ts18()[:6] # 201602
                ts_dic = fid_dic.setdefault(month_ts, {})
                if verb in ts_dic:
                    ts_dic[verb] += 1
                else:
                    ts_dic[verb] = 1
                self.db.put_ent(ep)
            return new_client_ipaddr
        except:
            utils.set_trace()
            return None

    def get_respcache_string(self, req_id):
        try:
            rec_ts = self.db._get_rec_ts('FTSUPER','RC', req_id)
            if rec_ts:
                return rec_ts[0] # (<rec>,<ts>), use rec (cached-string) only
            return False
        except:
            utils.set_trace()
            return False

    # -----------------------------------------------------------------------
    # a cache holding the response(s) for 1 min, 
    # keyed under <EP-eid> This is useful when req has been responded
    # but the client did not get it and re-sent the same req(same req-id). In 
    # that case(within 1 min), the saved resp will be sent to client
    # resp-caching is done in DB: T_FTSUPER: cid=RC, eid is req_id(<19 long)
    # with ts recording the caching time.
    # a cron job sends get-req /req/_purge every minute, triggering
    # call of this, going thru all cache-items, purge the expired ones
    # ----------------------------------------------------------------
    def purge_expired_resp_cache(self):
        try:
            recs = self.db._get_cid_recs('FTSUPER','RC')
            if len(recs) == 0: 
                return 0
            now_ts = int(time()) # now in epoch sec
            exps = [] # holds eids of all rcs that are expired
            for rec in recs: # rec: (<cached-resp-str>, <eid>, <ts>)
                if rec[2] + 60 < now_ts:
                    exps.append(rec[1]) # save the eid of this RC ent-dic
            n = len(exps) 
            for exp in exps: # delete rc using <fid>,<cid>,<eid>
                self.db._delete_rec('FTSUPER','RC',exp)
            return n
        except:
            return False

    # request dispatcher: call the handlers.<verb>. update ep-counts, also do
    # resp-cache. for debugging verb starting with _, just call handler.
    def process_req(self, req, verb):
        utils.action_log('debugA',"ftsuper: handling "+verb, self.db)
        debug_req = False
        gateip = None
        #utils.set_trace()
        try:
            if not verb.startswith('_'): # not debug/test request

                # req_id for checking if the req has saved resp
                req_id = req.values.get('_req_id', None)
                if not req_id:
                    utils.action_log('debugB','__failed_req-id missing',self.db)
                    return utils.json.dumps(
                        {'result': '__failed_req-id missing'})
                utils.action_log('debugA',
                                 "all_reqa on:"+verb +" #:"+req_id, self.db)
                # collect verb-count
                gateip = self.update_req_count(req, verb)
                cached_resp = self.get_respcache_string(req_id) 
                if cached_resp:
                    utils.action_log('debugA',
                        verb + " has been cached. Used saved resp",self.db)
                    return cached_resp
            elif verb.startswith('_purgerc'):
                # purge response cache
                pcount = self.purge_expired_resp_cache()
                return "Purged resp-cache:" + str(pcount)
            elif verb.startswith('_purgess'):
                utils.purge_sessions(self.db)
                return "Purged session"
            elif verb.startswith('_resetur'):
                #utils.set_trace()
                # reset all urs for pa(or every pa) in fid-db to _abc
                fid = req.values.get('fid', None)
                pid = req.values.get('pid', None)
                if fid:
                    tmp.reset_ur(fid, self.db, pid)
                return "UR reset"
            else:
                debug_req = True
                verb = verb.strip('_')
            thecall = eval("handlers." + verb)
            resp = thecall(req, self)
            if not debug_req:
                resp['_req_id'] = req_id
            if gateip:
                resp['gateip'] = gateip
            resp_str = utils.json.dumps(resp)
            ts = int(time())
            if not (debug_req or req_id.startswith('##')):
                # saveblob req has req_id == ##, no response-caching
                self.db._insert_rec('FTSUPER','RC',req_id, ts, resp_str)
                
            return resp_str
        except:
            if not utils.action_log(
                'debugE','ft-process_req exception', self.db): 
                utils.set_trace()
