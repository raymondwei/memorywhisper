from modules import utils
from pdb import set_trace
import copy, time
import ujson as json

class Transact(object):
    def __init__(self, ft):
        self.ft = ft
    
    # do the transaction and return result. dic is req.values. called in:
    # handlers.newft and handlers.putdocs
    def doit(self, dic):
        acts = self.prepare(dic)
        return self.ft.db.transaction(acts)

    # convert dic to acts(list) used for transaction: [<act>,<act>,..] where
    # act: [<op>, <id>, <dic>], when op==D, dic:'{_id:<id>}'
    # see actbuffer.js:get_reqdic(line 25)
    # when op==U, it will be converted to C, filling delta-dic to be full dic
    def prepare(self, dic): #dic:{<key>:<dic-string>,...}
        #set_trace()
        self.collect = {}
        acts = []
        for k, d in dic.items(): # k: 'C|U|D:<id>'
            if k[0] == '_' or ':' not in k: # K starting with _ is not transact
                continue
            # op: C|U|D, theid: TC6H8EU-PA-zpReSK1497559799099
            op, theid = k.split(':')
            fid, cid, eid = theid.split('-')
            d = json.loads(d) 
            if '_id' not in d:   # when op==U sometimes _id is not in d
                d['_id'] = theid
            # op==D, d:{_id:<id>}; op==U, d is delta-dic, both not complete
            if op == 'C':  # d for a new ent
                dd = d     # dd set to d: it is a complete ent-dic
            else:          # dd: complete dic by filling in missings from db
                dd = self.ft.db.complete_dic(d)

            # cid specific pwd-handlings for PA and IT-F. op can be C|U|D
            if cid == 'IT' and dd['type'] == 'F' and theid[:7] != 'FTSUPER':
                # handle ur of ownerpa, for updating it-F's pwd-protection
                opid = dd['iddict']['ownerpa'] # dd is it-F complete version
                ur_id = opid.replace('-PA-','-UR-') # ur-eid == pa-eid
                self.handle_it_pwd(acts, op, dd, ur_id)
            elif cid == 'PA':
                # handle ce changes for pa.dob/dod changes. op can be C|U|D
                self.add_ce2acts([op, theid, dd], acts)
                # handle pwd in pa.cfg.M1504
                self.handle_pa_pwd(acts, op, dd, fid + '-UR-' + eid)
            elif cid == 'SE':
                # se[tx] contains all ents-updates in form of "C:<id>,D:<id>,.."
                # update ct with these, and increase ct.version, save ct to DB
                pafid = dd['pid'][:7]
                setx = dd['tx']    # get session-tx (time-stamp)
                m = dd['actlog'][setx] # get acts-str:"C:<id>,D:<id>,U:<id>,.."
                # get ct of the fid
                ct_id = pafid + '-CT-' + pafid
                fa_id = pafid + '-FA-' + pafid
                ct = utils.CT(pafid, self.ft)
                ct.update(dd, False) # save=F: no-save into DB, use transaction
                self.into_acts(['C',ct_id, ct.ent],acts)
                self.into_acts(['C',fa_id, ct.fa], acts)

            # op-specific handlings
            if op == 'C': # no special ahndling needed, just put into acts
                self.into_acts([op, theid, d], acts)
            elif op == 'U':
                # use dd (complete dic, and set op=C)
                self.into_acts(['C', theid, dd], acts)
            elif op == 'D':
                self.collect_deletes(acts, theid)
        return acts
    # -------------------------------------------------------------------------
    # end of def prepare
    # -------------------------------------------------------------------------

    # handle possible pwd change. new pwd can be in dic[cfg][M1504]. op: C|U|D
    # C: dic is a new pa, with pwd in dic[cfg][M1504][0]: add act creating ur
    # D: add act for deleting the ur, or
    # U: if delta-dic has cfg.M1504
    def handle_pa_pwd(self, acts, op, dic, ur_id):
        if op == 'D':
            self.into_acts(('D', ur_id, {'_id': ur_id}), acts)
        elif op == 'C':
            self.pwd = dic['cfg']['M1504']['login'][0]
            pwd_str = utils.sha1_sig(dic['_id'] + self.pwd)
            ur = {'_id': ur_id,  'usage':{},
                  'pwds': {'login': pwd_str}, 
                  'tx': str(int(time.time()))} # 10 digits ts string
            self.into_acts(('C',ur_id, ur), acts)
            dic['cfg']['M1504']['login'][0] = '******'
        elif op == 'U' and 'cfg' in dic and 'M1504' in dic['cfg']:
            ur = self.ft.db.get_ent(ur_id)
            ur_dirty = False
            if not ur:
                ur = {'_id': ur_id, 'pwds': {}, 'usage':{}, 
                      'tx': str(int(time.time()))} # 10 digits ts string
                self.into_acts(('C',ur_id, ur), acts)
            pwds = ur.setdefault('pwds',{})
            M1504 = dic.get('cfg',{}).get('M1504',{})
            # go thru: login, blood-lineage, spouses, children
            for k in ('login','M7516','M7517','M7518'):
                if k in M1504:
                    # M1504[k]'s first ele is pwd-string, 
                    # if it pwd-string[0] != '*', new value is given. use it
                    if isinstance(M1504[k], list) and M1504[k][0][0] != '*':
                        # set ur.pwds[k] with sha1-sig of pid + pwdstring
                        pwds[k] = utils.sha1_sig(dic['_id']+M1504[k][0])
                        # blur out pa's M1504[k][0]: overwrite with ******
                        M1504[k][0] = '******'
                        ur_dirty = True
                else: # k missing in pa.M1504 means, to delete it in ur.pwds
                    if k in pwds:
                        del pwds[k]
                        ur_dirty = True
            if ur_dirty:
                self.into_acts(('C',ur_id, ur), acts)
    # -------------------------------------------------------------------------
    # end of handle_pa_pwd
    # -------------------------------------------------------------------------
 
    # it-F may have dic['lock'] = ['<pwd>','<exp-date>','<post-exp-act>']
    # 1.it[pk]==M7117, lock is absent or will be ignored: it is public
    # 2.pk==M7118, lock is also ignored: it is private - only for logged-in pa
    # 3.pk==1513(pwd-protect): lock[1]: exp-date(string like 2020-12-27);
    #   lock[2] tells M7117 or M7118: after exp-date. lock[0] can have pwd
    #   when it is created, or user is modifying pwd for it. This will be 
    #   blurred to be ****** after pwd-string is saved into ur
    def handle_it_pwd(self, acts, op, dic, ur_id):
        ur = self.ft.db.get_ent(ur_id)
        if not ur:
            ur = {'_id': ur_id, 'pwds': {}, 'usage':{}, 
                  'tx': str(int(time.time()))} # 10 digits ts string
            self.into_acts(('C', ur_id, ur),acts)
        if op in ('C', 'U'): # it may have new pwd in lock[0]
            lock = dic.get('lock',[])
            pid = dic['iddict'].get('ownerpa','')
            if len(lock) > 0:
                if lock[0] != '*':
                    # take the new pwd into ur
                    ur['pwds'][dic['_id']] = utils.sha1_sig(pid+lock[0]) 
                    # blur pwd to be * in it
                    lock[0] = '******'
                    # overwrite ur (Create ur)
                    self.into_acts(('C', ur_id, ur), acts)
            else: # lock is empty
                if dic['_id'] in ur['pwds']:   # it was pwd-protexted before
                    del ur['pwds'][dic['_id']] #  delete it-id from ur.pwds
                    self.into_acts(('C', ur_id, ur), acts)
        elif op == 'D' and dic['_id'] in ur['pwds']:
            # it to be deleted, so delete its pwd in ur
            del ur['pwds'][dic['_id']]
            # over-write ur with deleted pwd for the it
            self.into_acts(('C', ur_id, ur), acts)
    # -------------------------------------------------------------------------
    # end of handle_it_pwd
    # -------------------------------------------------------------------------

    # if act not in acts(identified by act[0]+act[1]), put it in;
    # if act already in acts, overwrite existing atcs[i][2] with act[2]
    def into_acts(self, act, # e.G. ['C',"UK8FJGH-IT-zpReSK1497559799099",{..}]
                        acts): # list [<act>,>act>,..]
        for a in acts:
            # over-write dict(ent) with new if the same op/id existed
            if act[0] == a[0] and a[1] == act[1]: 
                a[2] = act[2]
                return
        acts.append(act)

    # handle calendar-entry(ce) changes caused by pa-act:['C|U|D',<pid>,p-dic]
    # check if ce(calendar-entry) needs to be updated/created:put ce into acts
    def add_ce2acts(self, act, acts): # if yes add ce operation to acts
        def fetch_ce(ce_id, acts, db):
            ce = None
            for a in acts: # if ce already in act ('C',ce_id, ce), return ce
                if a[1] == ce_id:
                    return a[2] # (it is a reference to the ce in acts)
            ce = db.get_ent(ce_id)  # get from DB
            if not ce: # not in DB: make ce
                ce = utils.make_ce(ce_id, db)
            acts.append(['C',ce_id, ce]) # put ce into acts-list
            return ce # return ce (it is a reference to the ce in acts)

        # vars for holding 2 chars for bd-month, bd-day, dd-month, dd-day
        mdob = ddob = mdod = ddod = oldpa = ce = None
        pa_dic = act[2]
        pid = pa_dic['_id']
        fid = pid[:7]
        if act[0] in ('U', 'D'): # updating pa, or deleting pa 
            # U|D, delete ce entries of oldpa's dob/dod
            oldpa = self.ft.db.get_ent(pid)
            if len(oldpa['dob']) == 8: # oldpa has month/day for dob
                # D case, or U case with dob in delta-dic: del entry in ce
                if act[0] == 'D' or 'dob' in pa_dic:
                    omdob = oldpa['dob'][4:6]
                    ce_id = fid + '-CE-' + omdob
                    ce = fetch_ce(ce_id, acts, self.ft.db)
                    # find oldpa.dob entry in ce
                    if ce:
                        k = 'D' + oldpa['dob'][6:8] # entry-key: e.g. D16
                        entries = ce['B'].setdefault(k, {}) # ce dob-entries
                        if pid in entries: # find pid birth-entry and
                            del entries[pid]
            if len(oldpa['dod']) == 8:    # death-date 8 char long
                # D case, or U case with dod in delta-dic: del ce entry
                if act[0] == 'D' or 'dod' in pa_dic:
                    omdod = oldpa['dod'][4:6] # get 2 chars death month
                    # find oldpa.dod entry in ce
                    ce_id = fid + '-CE-' + omdod
                    ce = fetch_ce(ce_id, acts, self.ft.db)
                    if ce:
                        k = 'D' + oldpa['dod'][6:8]
                        entries = ce['D'].setdefault(k, {})
                        if pid in entries:      # find pid death-entry and
                            del entries[pid] # delete the pid
            if act[0] == 'D':  # keep working for C | U case only
                return         # for D, it is done.
        # now handle op == C or U
        if 'dob' in pa_dic and len(pa_dic['dob']) == 8: # yyyymmdd
            mdob = pa_dic['dob'][4:6]   # mm
            ddob = pa_dic['dob'][6:8]   # dd
            ce_id = fid + '-CE-' + mdob
            ce = fetch_ce(ce_id, acts, self.ft.db)
            if ce:
                k = 'D' + ddob
                entries = ce['B'].setdefault(k, {}) # entries for birth-date
                entries[pid] = 'M7102'
        if 'dod' in pa_dic and len(pa_dic['dod']) == 8: # yyyymmdd
            mdod = pa_dic['dod'][4:6]   # mm
            ddod = pa_dic['dod'][6:8]   # dd
            ce_id = fid + '-CE-' + mdod
            ce = fetch_ce(ce_id, acts, self.ft.db)
            if ce:
                k = 'D' + ddod
                entries = ce['D'].setdefault(k, {}) # entries for death-date
                entries[pid] = 'M7103'
    #--------------------------------------------------------------------------
    # end of def add_ce2acts
    #--------------------------------------------------------------------------

    # when deleting ent, there maybe contained other ents to be deleted
    # put those deletees into acts.
    # when ent is fc/pa, handle deletion of contained rs
    def collect_deletes(self, acts, # act-list for collecting deletees
                entid,              # id of ent 2b deleted. can be list of ids
                rs_holder_id=None): # id of rs-holder. pa-id or fc-id
        if not entid: return
        if isinstance(entid, list): # in case of list of ids: recursive-call
            for single_id in entid:
                self.collect_deletes(acts, single_id, rs_holder_id)
        else: # entid is a id-string, not a list
            fid, cid, eid = entid.split('-')
            
            # put ent into acts for deletion. Exception: RS-ent
            if cid != 'RS': # rs will be deleted only when... see below
                self.into_acts(('D', entid, {'_id': entid}), acts)
            
            # only collect more deletees for these cids; Otherwise done.
            if cid not in ('RS','FC','PA','IP','IT'): 
                return
            
            ent = self.ft.db.get_ent(entid)
            iddict = ent['iddict']
            if cid == 'FC': # see if contained rs should be deleted too
                rs_id = iddict['rs']
                self.collect_deletes(acts, rs_id, entid)
            elif cid == 'RS':
                # a rs_holder can be pa/fc
                # holder is in, but not the only one: modify rs: remove holder
                if rs_holder_id and len(iddict['holders']) > 1 and \
                   rs_holder_id in iddict['holders']:
                    # update rs.holders and put rs back in db
                    iddict['holders'].remove(rs_holder_id)
                    self.into_acts(('C', entid, ent), acts) # put updated rs
                else: 
                    # rs_holder is the only holder: add rs to deletes
                    self.into_acts(('D', entid, {'_id': entid}), acts)
            elif cid == 'PA':
                rs_id = iddict['portrait']
                self.collect_deletes(acts, rs_id, entid) # rs-holder:pid(entid)
                self.collect_deletes(acts, 
                    iddict['ips'] + [iddict['nutshell']])
            elif cid == 'IP':
                self.collect_deletes(acts, iddict['folders']+iddict['docs'])
            elif cid == 'IT':
                if ent['type'] == 'F':
                    # if it-F has pwd-protection, del from ownerpa-ur
                    opid = ent['iddict']['ownerpa']     # ownerpa-id
                    ur_id = opid.replace('-PA-','-UR-') # ownerpa's ur-id
                    self.handle_it_pwd(acts, 'D', ent, ur_id)
                self.collect_deletes(acts, 
                    iddict['folders']+iddict['docs']+iddict['facets'])
    #--------------------------------------------------------------------------
    # end of def collect_deletes
    #--------------------------------------------------------------------------