# when merging p0 and p1, the parents-bas of p0 and p1 are merged here.
# -------------
# when p0/p1 with ops=='same', p1's parents-bas  are to be deleted.
# if it has children, find a ba among p0.parents with the same adp-state,
#  merge the children to that ba.
# when p0's op == +, p1.op== -, keep p1.parent b1, add b1 to p0.parents,
# find p1 among b1.children, change that to p0-id
# -------------------------------------------------------------------------
def modify_ba(
    bps0,       # [(<b0-id>,<b0-adp-state>),..] from p0.parents
    bp1,        # (<b1-id>,<b1-adp-state>)
    dels,       # collection of ents to be deleted
    updates,    # collection of ents to be updated
    repo,       # DB/tablerepo
    delete_b1=True,
    pids=None): # in case delete_b1=False, pids is a list [p0,p1], where p1
                # in b1[iddict][children] to be replaced by p0
    #---------------------------------------------------------------------------
    if delete_b1:  # b0/b1 are both bio-parents of p0, b1 to be deleted
        # merge all children of b1 into b0. Merging means here:
        # -----------------------------------------------------
        # if a child(id) from b1['iddict']['children'] list is not already in
        # b0['iddict']['children'], put that chid into b0; otherwise do not.
        b0 = None
        b1 = None
        for bp in bps0:
            if bp[1] == bp1[1]: # having the same adp-state
                b0 = repo.get_dic(bp[0], False, True)# cache:F, from-DB-only:T
                b1 = repo.get_dic(bp1[0],False, True)
                merge_children(b0, b1, updates, repo)
                break
        if b1:
            set_delete(b1, dels, repo)
        else:
            print("the 2 ba are of diff adp-states.")

        # if b1 has male-pa or/and female pa, delete it/them
        if b1 and b1['iddict']['male']:
            set_delete(repo.get_dic(bp1[0]['iddict']['male']), dels, repo)
        if b1 and b1['iddict']['female']:
            set_delete(repo.get_dic(b1['iddict']['female']), dels, repo)
            
    else: # b0 is the adopting-in/taking ba; b1 the giving/adopt-out ba
        # p1/pids[1] needs to be replaced by p0/pids[0] in b1's children 
        # id-list
        b1 = repo.get_dic(bp1[0], False, True) # no-cache, from-DB-only: T
        updates[b1['_id']] = b1
        chs, css = get_pairs(b1['iddict']['children'], 2)
        if len(chs) == 0 or not pids:
            raise Exception("modify_ba has no target.")
        for i in range(len(chs)):
            if pids[1] == chs[i]:
                b1['iddict']['children'][i] = pids[0] + bp1[1]
        debug = 1

# collect under a container(ip/it): all it-F/-D and fc and rs under fc
def collect_dels(dic, col, repo):
    col[dic['_id']] = ('D', dic['_id'][8:])
    if dic['_id'][8:10] == 'FC': # facet
        # collect rs under iddict or contdict
        if 'rs' in dic['iddict'] and dic['iddict']['rs']:
            col[dic['iddict']['rs']] = ('D', dic['iddict']['rs'][8:])
        if dic['type'] == 'txt':
            for k, v in dic['contdict'].items():
                if len(v) > 29 and v[8:10] == 'RS':
                    col[v] = ('D', v[8:])
        return
    # collect all subtree nodes from under itF/itD, and facests
    for id in dic['iddict']['folders']:
        d = repo.get_dic(id)
        collect_dels(d, col, repo)
    for id in dic['iddict']['docs']:
        d = repo.get_dic(id)
        collect_dels(d, col, repo)
    if 'facets' in dic['iddict']: # IT has facets, ip not
        for id in dic['iddict']['facets']:
            d = repo.get_dic(id)
            collect_dels(d, col, repo)

# union of children-ids into b0.children. 
# b0 should be a clone avoiding sideeffect
def merge_children(b0, b1, updates, repo):
    children0, states0 = get_pairs(b0['iddict']['children'], 2)
    children1, states1 = get_pairs(b1['iddict']['children'], 2)
    for i in range(len(children1)):
        if not children1[i] in children0:
            # add child to b0
            updates[b0['_id']] = b0         # b0 to be updated
            b0['iddict']['children'].append(children1[i] + states1[i]) 

            # b0-id into child's parents list, replacing b1-id, keep adp-state
            c1 = repo.get_dic(children1[i],
                              False, # no cahce
                              True)  # from-DB-only: True
            ps1, ss1 = get_pairs(c1['iddict']['parents'], 2)
            ind1 = ps1.index(b1['_id'])
            c1['iddict']['parents'][ind1] = b0['_id'] + ss1[ind1]
            updates[c1['_id']] = c1
    return 

def set_delete(dic, dels, repo):
    eid = dic['_id'][8:]
    dels[dic['_id']] = ('D', eid)
    if eid[:2] == 'PA':
        dels[dic['iddict']['portrait']] = ('D', dic['iddict']['portrait'][8:])
        dels[dic['iddict']['nutshell']] = ('D', dic['iddict']['nutshell'][8:])
    return dels
    
# see if ba is among p0.spouses: if English tname of osex-partner equals
def ba_among_bas(osex, spouses, ba, repo):
    bp = repo.get_dic(ba['iddict'][osex])
    for sp in spouses:
        p = repo.get_dic(sp['iddict'][osex])
        if p['_id'] != bp['_id'] and p['tname']['en'] == bp['tname']['en']:
            return True
    return False

# ba-id in p.parents is sufixed with adp-state char: -|+ or ''(nothing)
# kind==1: return list of (<ba-id>,<adp-state>) from p.parents
# kind==2: return 2 lists: [<id>,..],[<adp-state>,..]
def get_pairs(parents, #[<id>(-|+)?,..] => [(<id>,<adp-state>),(), ...]
              kind=1):
    res = []
    if kind == 1:
        for id in parents:
            if id[-1] in ('-','+'):
                res.append((id.strip('-+'), id[-1]))
            else:
                res.append((id,''))
        return res
    else:
        res2 = []
        for id in parents:
            if id[-1] in ('-','+'):
                res.append(id.strip('-+'))
                res2.append(id[-1])
            else:
                res.append(id)
                res2.append('')
        return res, res2
        
def letgo_pa(p0dic,  # originating pa(adopt-in/+), not letgo of. collect the
             pdic,   # spouse in the pa to be let go of
             repo,   # DB/fidtable
             # if not-None, collect sibling-pas in bdic['iddict']['children']
             same=True): # when not "same", p0:adopt-in(+), p1 adopt-out(-)
    # put p1 and all its subtree-node in dels
    dels = set_delete(pdic, {}, repo)# {<id>: ('D',<eid>),...}

    # merge pdic.spouses into p0dic.spouses, if not-None
    if p0dic:
        osex = ['female','male'][p0dic['sex'] == 'female'] # p0 partner-sex
        sps0 = [] # collect list of spouse-ba dics from p0
        for id in p0dic['iddict']['spouses']:
            sps0.append(repo.get_dic(id))
        sps1 = [] # collect list of spouse-ba dics from p1
        for id in pdic['iddict']['spouses']:
            sps1.append(repo.get_dic(id, True, True))# getclone=True
        # if a ba from p1 existed in p0, delete, else, put into p0.spouses
        for sp in sps1: # in means: 
            if ba_among_bas(osex, sps0, sp, repo):
                # sp represent a partner that is already represented in sps0
                dels[sp['_id']] = sp # delete that ba(sp)
            else:
                updates[p0dic['_id']] = p0dic # p0 update: received a spouse-ba
                updates[sp['_id']] = sp  # sp need update: male/female = p0
                p0dic['iddict']['spouses'].append(sp['_id'])
                if p0dic['sex'] == 'male':
                    sp['iddict']['male'] = p0dic['_id']
                else:
                    sp['iddict']['female'] = p0dic['_id']
    # pdic's siblings
    bps0 = get_pairs(repo.get_dic(p0dic['iddict']['parents']))
    bps1 = get_pairs(repo.get_dic(pdic['iddict']['parents']))
    for bp in bps1:
        if same: # delete b(id==bp[0]) and its male/female. And for a b0 in 
            # p0.parents, with the same adp-state, marge children from b1 to b0
            modify_ba(bps0, bp, dels, updates, repo, True)
        else: # keep b1(id==bp[0]). 
            # among b1.children modify child-id: replace p1-id with p0-id
            modify_ba(bps0, bp, 
                      dels, updates, repo, 
                      False,        # delete b: NO (False)
                      [p0dic['_id'],# this child-id string to 
                       pdic['_id']])# replace this child-id-string
            # add b as going-out-from parents-ba to p0-parents-list
            p0dic['iddict']['parents'].append(bp[0]['_id'] + '-')
            updates[p0dic['_id']] = p0dic
    
    commit(fid, dels, updates, repo)
    # end of letgo_pa

def commit(fid, dels, updates, repo):
    t = repo.fidtable(fid)
    acts = []
    for tups in list(dels.values()):
        acts.append(tups)
    for k, v in updates.items():
        acts.append(('C', k[8:], 
                    t.construct_entity(fid, k[8:], v)))
    return t.transaction(acts)
    
def mergepa(p0id, p1id, op0, op1, repo):
    try:
        p0 = repo.get_dic(p0id, True, True) # getclone=True
        p1 = repo.get_dic(p1id)
        if op0 == 'same' and op1 == 'same':
            letgo_pa(p0, p1, repo, True)
        else:
            if op0 == "in" and op1 == "out":
                letgo_pa(p0, p1, repo, False)
            elif op0 == "out" and op1 == "in":
                letgo_pa(p1, p0, repo, False)
            else:
                print("wrong combination of "+op0+" "+op1)
        return True
    except:
        return False