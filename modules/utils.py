﻿#
# Copyright (C) 2014 Adacro Inc. - All Rights Reserved
#
import time, smtplib
from datetime import datetime
from random import randrange
from flask_mail import Message
import base64, re, hashlib, zipfile, os
import ujson as json
from pdb import set_trace
DEBUG = True
mail = ''

randchars = "abcdefghijklmnopqrsuvwxyzABCDEFGHIJKLMNOPQRSUVWXYZ"
mime_dict = {'gif': 'image/gif',
             'jpg': 'image/jpeg',
             'jpe': 'image/jpeg',
             'png': 'image/png',
             'bmp': 'image/bmp',
             'mp3': 'audio/mpeg3',
             'wav': 'audio/wav',
             'ogg': 'audio/ogg',
             'aif': 'audio/x-aiff',
             'aifc':'audio/x-aiff',
             'aiff':'audio/x-aiff',
             'pdf': 'application/pdf',
             'doc': 'application/msword',
             'txt': 'text/plain',
             'mp4': 'video/mp4',
             'webm':'video/webm',
             'avi': 'video/avi',
             # -------------------
             'image/gif':           'gif',
             'image/jpeg':          'jpg',
             'image/pjpeg':         'jpg',
             'image/png':           'png',
             'image/bmp':           'bmp',
             'image/x-windows-bmp': 'bmp',
             'audio/mpeg3':         'mp3',
             'audio/mp3':           'mp3',
             'audio/x-mpeg':        'mp3',
             'audio/mpeg':          'mp3',
             'audio/wav':           'wav',
             'audio/x-wav':         'wav',
             'audio/ogg':           'ogg',
             'application/x-troff-msvideo': 'avi',
             'video/avi':           'avi',
             'video/msvideo':       'avi',
             'video/x-msvideo':     'avi',
             'video/webm':          'webm',
             'video/mp4':           'mp4',
             'application/pdf':     'pdf',
             'application/msword':  'doc'
             }
# get 18 char long iso-format time stamp acurrite to msec: 
# 20171216T130945287 (287 is milli-sec)
def get_ts18():
    now = datetime.now()
    m = now.isoformat().replace('-','').replace(':','').replace('.','')
    # m format: '20171216T130945287317' 21 chars long, last 3 chars: micro-secs
    return m[0:-3] # cut micro-secs: return 18 char-long ts

def get_tx10():
    return str(int(time.time()))
    
# SendGrid account:
# service name: SendGrid
# 

def mime_from_file_extention(ext):
    return mime_dict.get(ext.lower(),'text/plain')

# make a id in format: TC6H8EU-PA-AMrrIJ1409021272252 :<fid>-<cid>-<seid>
# where seid = 6 random chars + milisecs epoc time (int-string)
# this is a 30 long string, as long as milisec-epoc is 13 digits, which
# will be true for the next 240 years. Now(2015-09-01): 1441127179147
def make_id(fid, cid, seid=None):
    if seid:
        return fid + '-' + cid + '-' + seid
    seid = ''
    for i in range(6):
        eid_index = randrange(50)
        seid += randchars[eid_index]
    return fid + '-' + cid + '-' + seid + str(int(time.time()*1000))

def tobytes(msg):
    if isinstance(msg, str):
        return msg.encode()
    return msg

def tostr(msg):
    if isinstance(msg, bytes):
        return msg.decode('utf-8')
    return str(msg)

def sha1_sig(msg):
    sha1 = hashlib.sha1()
    sha1.update(tobytes(msg)) # python3's sha1 demands input be bytes
    return sha1.hexdigest()

# verify pwd in FIDCODE table possible keys:
# login - for loggin in. only 1 pid in pids
# M7517(spouses), M7516(ancestry) 
# - for expanding spouses/ancestry. only 1 pid in pids
# M7518(children) 
# - expanding children. can have 2 pids(either parent pwd-match)
# <it-ids> - protect folder(s)
def verify_pwd(pids, key, pwd, db):
    def verify(pid, pwdkey, pwd):
        if not pid: return True
        ur = db.get_ent(pid.replace('-PA-','-UR-'))
        if pwdkey in ur['pwds']:
            ur_pwd = ur['pwds'].get(pwdkey,'')
            return ur_pwd == sha1_sig(pid + pwd)
        else:
            return True

    if  key == 'M7518': # children - check both pids
        res0 = verify(pids[0],key,pwd, db)
        res1 = verify(pids[1],key,pwd, db)
        return res0 and res1
    else: # key == 'M7517' or key == 'M7516' or key == <it-id>
        # M7517/spouses or M7516/ancestry or <it-id>
        # check pids[0]-pa.cfg.M1504.M7517.pwd, or pa.cfg.M1504.<it-id>
        return verify(pids[0], key, pwd)

# pattern matching info block in db-file header line, like
# "# TC6H8EU-PA-rKAaKS1407852727235@@0 GFT 0.5.984"
pp = re.compile(r"""
    (?P<cpid>\w{7,7}-\w{2,2}-\w{6,6}\d{13,14})  # pid: centerpa-id
    @@
    (?P<private>\d{1,1}) # 0: public, 1: private
    \s+
    (?P<app>\w{3,3})     # GFT or AFT
    \s+
    (?P<version>\d{1,2}\.\d{1,3}\.\d{1,4}) # 0.5.984
    """, re.VERBOSE)

# construct a ce entity, return it without putting it into db
# ce-id: <fid>-CE-<month>, e.g. TC6H8EU-CE-11
def make_ce(ce_id, db): # month: '01'..'12' (1 based month number string)
    recs = db._get_cid_recs(ce_id[:7], 'PA')# rows of pas:[[<rec>,<eid>,<ts>],...]
    ce = {'_id': ce_id, 'tx': str(int(time.time())),
          'B':{}, 'D':{}, 'E':{}}     # births, deaths, events - dics
    for rec in recs: # row: [<rec>,<eid>,<ts>]
        pa = json.loads(rec[0]) # pa-dic
        if len(pa['dob']) == 8 and pa['dob'][4:6] == ce_id[11:]:
            k = 'D' + pa['dob'][6:8]
            entries = ce['B'].setdefault(k, {})
            entries[pa['_id']] = 'M7102'
        if len(pa['dod']) == 8 and pa['dod'][4:6] == ce_id[11:]:
            k = 'D' + pa['dod'][6:8]
            entries = ce['D'].setdefault(k, {})
            entries[pa['_id']] = 'M7103'
    return ce

# collect all ents(itF/itD/fc/rs) under it/ip subtree in lst. 
# no duplication in lst
def collect_treenodes(lst, itid, db):
    def exists_already(lst,id):
        for e in lst:
            if e.get('_id','') == id:
                return True
        return False 
    if isinstance(itid, list):
        for i in itid:
            collect_treenodes(lst, i, db)
        return lst
    else:
        if not exists_already(lst, itid):
            e = db.get_ent(itid)
            if e:
                lst.append(e)
                if e['_id'][8:10] == 'FC':
                    if e['iddict']['rs']:
                        collect_treenodes(lst, e['iddict']['rs'], db)
                elif e['_id'][8:10] in ('IT','IP'):
                    ids = e['iddict'].get('folders',[]) + \
                        e['iddict'].get('docs',[]) + \
                        e['iddict'].get('facets',[])
                    collect_treenodes(lst, ids, db)
                return e
        return None

def send_email( target,           # target email address 
                subj,             # email subject
                text_content,     # clear text content
                the_sender=None): # sender email address
    try:
        if not the_sender:
            the_sender = "thememorywhisper@gmail.com"
        msg = Message(subj,
            sender=the_sender,
            recipients=[target, "thememorywhisper@gmail.com"])
        body = text_content + "\n\n"+ datetime.now().date().isoformat()
        msg.body = body
        mail.send(msg)
        action_log('debugA','send-email',)
        return "Email sent."
    except Exception as e:
        return str(e)

class SE(object):
    # dic can be se-ent-dic, or a se-id( a string)
    def __init__(self, dic, db): 
        self.db = db
        self.exp_time = 1800
        try:
            if isinstance(dic, str):    # dic is se-id string
                self.se = db.get_ent(dic)
            elif isinstance(dic, dict): # dic is an instance of SE from DB
                self.se = dic # just save the dic
            else:
                if not action_log('debugE','session exception-1'):
                    set_trace()
        except:
            if not action_log('debugE','session exception-2'):
                set_trace()

    def purge(self):  # if expired, purge
        # load se from db
        self.se = self.db.get_ent(self.se['_id'])
        # within session exp-time?
        if self.se['lu'] + self.exp_time < int(time.time()):
            self.die()
    
    # SE instances in FTSUPER table are the ones alive. When a se die,
    # a new se is copied from the alive one, and modified id to have fid in id
    # this e will be saved (in fid-table) and the one in FTSUPER deleted. 
    # So, in fid-table, all ses are records of past sessions.
    # from their crtime one can get its start time; 
    # from se.tx the last active time. And pid is also saved in it
    def die(self):
        action_log('cronjb','session died', self.db)
        # alive se is in FTSUPER table. dead ses (for record in fid table)
        fid = self.se['pid'][:7]  # get fid of the initiator pid
        tx = str(int(time.time()))
        actlog = self.se['actlog']
        actlog[tx] = "D:"+self.se['_id'] # logout time-stamp(del from FTSUPER)
        se_id = self.se['_id'].replace('FTSUPER',fid) # se update fid
        se = {'_id': se_id, 'pid': self.se['pid'], 'actlog': actlog, 'tx':tx}
        self.db.put_ent(se)
        self.db.delete_ent(self.se['_id']) # delelete se in FTSUPER table

def purge_sessions(db):
    #set_trace()
    se_rows = db._get_cid_recs('FTSUPER', 'SE')
    action_log('cronjb','purge-session')
    for row in se_rows:
        se = SE(db, json.loads(row[0])) # row[0] is rec, load it as dic
        se.purge()   # update == False, as it defaults: kill it if expired 

# if client request ct entity, it should not have UR,VR,EP,RP,AL,SE,CT in it
def filter_ct(ct): # return a copy of reduced version of ct
    res_ct = {'_id': ct['_id'], 'version': ct['version'],'snapshots': {}}
    for key,val in ct['snapshots'].items():
        if key[8:10] not in ('UR','VR','EP','RC','RP','AL','SE','CT'):
            res_ct['snapshots'][key] = val
    return res_ct

def action_log(key, msg, db):
    if not DEBUG: return 
    ts = str(int(1000*time.time()))
    id = 'FTSUPER-AL-' + key + ts
    db.put_ent({'_id': id, 'tx': ts[:10], 'msg': msg})
    return key[-1] != 'E'

class CT(object):
    def __init__(self, fid, ft):
        self.ft = ft
        self.fid = fid
        self.ct_dirty = False
        self.fa_dirty = False
        try:
            self.ent = self.ft.db.get_ent(self.fid + '-CT-' + self.fid)
            self.fa = self.ft.db.get_ent(self.fid + '-FA-' + self.fid)
            #if self.fa['ctver'] != self.ent['version']:
            #    raise Exception("fa - ct out of synch")
        except:
            raise Exception("CT construction failed.")

    def reset(self, ts):
        self.fa['ctver'] = '1'
        self.fa['tx'] = ts
        self.ent = {'_id':self.fid+'-CT-'+self.fid, 'version':'1',
                    'snapshots': {}, 'versid': {},'tx': ts}
        self.ct_dirty = True
        self.fa_dirty = True

    def save(self):
        if self.ct_dirty:
            self.ft.db.put_ent(self.ct)
        if self.fa_dirty:
            self.ft.db.put_ent(self.fa)

    # from se.actlog[se.tx]: "C:<id>,D:<id>,U:<id>,..."
    # if se not given, make/save initial ct/fa
    def update(self, se=None, save=True): 
        try:
            if se:
                new_version = str(int(self.ent['version']) + 1)
                self.fa['ctver'] = new_version
                self.ent['version'] = new_version
                ts = se['tx']
                self.ent['tx'] = ts
                self.fa['tx'] = ts

                self.ent['versid'][new_version] = se['_id'] + ':' + ts
                lst = se['actlog'][ts].split(',') # ['C:<id>','D:<id>',...]
                for e in lst: # 'C:<id>' | 'D:<id>' | 'U:<id>'
                    pair = e.split(':') # ['C',<id>]
                    # modify ct.snapshots
                    if pair[0] in ('C','U'): # put/update ts for this id
                        self.ent['snapshots'][pair[1]] = ts
                    elif pair[0] == 'D':     # remove <id> entry
                        self.ent['snapshots'].pop(pair[1],None)
                # update time-stamps of ct and fa in snapshots
                self.ent['snapshots'][self.ent['_id']] = ts
                self.ent['snapshots'][self.fa['_id']] = ts
            else: # init ct/fa
                utils.action_log('debugA','initialize ct',self.ft.db)
                # all possible cids (except FA, CT)
                cids = ( #'FA',   not caring FA because it is being updated
                         #'CT'    not caring CT because it is being generated
                    'PA',# persona
                    'BA',# bond: spouse-pair
                    'IP',#information pane
                    'IT',#item, tyep: F|D
                    'FC',#facet
                    'RS',#resource
                    'TD',#text doc
                    'CE',#calendar entry
                    'BB',#bulletin board
                    'UR',#user record: never go to client
                    'SE',#session record
                    'EP',#end point
                    'LS',#language selection
                    'RP',#repository
                    'AL',#actionlog on server
                    'VR')#version record
                    #'RC' never cared about - it should be temps in FTSUPER
                pa_count = data_size = media_size = 0
                ts = str(int(time.time()))# 10 char time-stamp-string(secs)
                self.reset(ts)
                for cid in cids:
                    #print('cid: '+cid)
                    #rows: [(<rec>,<eid>,<ts>),...]
                    rows = self.ft.db._get_cid_recs(self.fid, cid)
                    for row in rows:
                        data_size += len(row[0])
                        dic = json.loads(row[0])
                        if cid == 'PA': 
                            pa_count += 1
                        elif cid == 'RS':
                            if str(dic['size']).isdigit():
                                media_size += int(dic['size'])
                        self.ent['snapshots'][dic['_id']] = ts
                msg = '1:'+str(pa_count)+':'+str(data_size)+':'+str(media_size)
                self.fa['history'] = [msg]
            #set_trace()
            if save:
                # save the updated ct, fa into DB
                self.ft.db.put_ent(self.fa)
                self.ft.db.put_ent(self.ent)
        except:
            if not action_log('debugE','ct exception', self.ft.db):
                set_trace()
    
