#
# Copyright (C) 2014 Adacro Inc. - All Rights Reserved
#
from modules import utils
import re, base64, pdb, time
import ujson as json
# pattern of unicode in python27, like u66f9
pattern = re.compile(r'(u[0-9a-fA-F]{4,4})')
b64shouldhave = re.compile(r'[0-9=]')

# used in ent_b64localize which is used in db_b64localize
def __is_base64(string):
    if len(string) % 4 == 0 and \
        re.match('^[A-Za-z0-9+\/=]+\Z', string):
        # if string has pattern in it, it is not b64 string
        if pattern.match(string): 
            return False
        # after 1st letter,there exist(s) A-Z0-9= or a space: not b64 encoded
        if len(re.findall(r'[0-9A-Z= ]',string[1:])) == 0:
            return False
        return True
    else:
        return False

def db_b64localize(fid, db):
    for cid in ('PA','IP','IT','FC','TD'):
        rows = db._get_cid_recs(fid, cid) # return [[rec, eid, ts],..]
        for r in rows:
            #print("doing: " + cid + '-' + r[1])
            try:
                e = json.loads(r[0])
                ent_b64localize(e, cid)
                db.put_ent(e)
            except:
                print("FAILED:" + r[0])

# used in db_b64localize, for ent of ('PA','IP','IT','FC','TD')
# make sure all strings in ent's {en:<string>,zh:<string>,..}'
# are b64 encoded
def ent_b64localize(ent, cid):
    def b64(dic): # dic: {'en':<string>,'zh':<string>,...}
        if not dic: return
        for k in dic.keys():
            msg = dic[k] # get the lang-specific <string>
            if __is_base64(msg):
                if ent['_id'] == "UK8FJGH-IT-SeMsig1441558398211":
                    debug = 1
                #print(ent['_id'] +'.'+ k + ' passed.')
                #print('msg:'+msg)
            else: # if <string> is not b64 encoded: b64 encode it
                ustr = convert_to_ustring(msg) # make sure it is u'..'
                # to b64 string, and save it in dic[k]
                dic[k] = base64.b64encode(ustr.encode('utf-8'))

    if cid == 'PA':
        b64(ent['tname'])
        if 'cfg' not in ent:
            #print(ent['_id']+" missed cfg.")
            ent['cfg'] = {"M1508": "en", "M7534": [], "M7519": "default", 
                          "M1502": "", "M1504": {"login": ["******", "", ""]}}
        b64(ent.get('pob', None)) # make sure all langs in pob -> b64
        b64(ent.get('pod', None)) # make sure all langs in pod -> b64
    elif cid == 'IP':
        b64(ent.get('title', None)) # make sure all langs in title -> b64
    elif cid == 'IT':
        b64(ent.get('name', None))  # make sure all langs in name -> b64
        b64(ent.get('title', None)) # make sure all langs in title -> b64
    elif cid == 'TD':
        b64(ent.get('content', None))# make sure all langs in content->b64
    elif cid == 'FC':
        b64(ent.get('name', None)) # make sure all langs in name -> b64
        b64(ent.get('anno', None)) # make sure all langs in anno -> b64
        if ent['type'] == 'txt':
            b64(ent.get('contdict', None))

# used in ent_b64localize
# turn "u66f9u8018u5c71"  to u'\u66f9\u8018\u5c71'
def convert_to_ustring(msg):
    # pattern = re.compile(r'(u[0-9a-fA-F]{4,4})')

    # func used in pattern.sub(<func>,<string>)
    # if pattern did not match original <string> returned
    def __repl(match):
        n = int(match.group()[1:], 16) # '66f9'' -> 26361
        return unichr(n) # 26361 -> u'\u66f9'
    return pattern.sub(__repl, msg)

# some pa's dob/dod are '1099****', replace such with '', so that
# Dec.16 calendar is not crowded (too many 10991216 as dob/dod)
def fix_dob_dod(fid, db):
    rows = db._get_cid_recs(fid, 'PA')  # return [[rec, eid, ts],..]
    for r in rows:  # r: [rec, eid, ts]
        try:
            dirty = False
            d = json.loads(r[0]) # get ent-dic from rec
            if d['dob'].startswith('1099'):
                d['dob'] = ''
                dirty = True
            if d['dod'].startswith('1099'):
                d['dod'] = ''
                dirty = True
            if dirty:
                print(d['_id'] + " dob/dod reset.")
                db.put_ent(d)
        except:
            print("fix_dob_dod(PA) failed on: "+eid)

# some pob pod string have "sdfds,sdf"
# turn it to "sdfds sdf"
def fix_pob_pod(fid, db):
    p = re.compile(r"\b,\b")
    rows = db._get_cid_recs(fid, 'PA')  # return [[rec, eid, ts],..]
    for r in rows:  # r: [rec, eid, ts]
        try:
            #pdb.set_trace()
            dirty = False
            d = json.loads(r[0]) # get ent-dic from rec
            dic = d.setdefault('pob', {})
            for k in dic:
                pob = utils.tostr(base64.b64decode(utils.tobytes(dic[k])))
                m = p.search(pob)
                if m:
                    dic[k] = base64.b64encode(
                        re.sub(p, ' ', pob).encode('utf-8'))
                    dirty = True
            dic = d.setdefault('pod',{})
            for k in dic:
                pod =  utils.tostr(base64.b64decode(utils.tobytes(dic[k])))
                m = p.search(pod)
                if m:
                    dic[k] = base64.b64encode(
                        re.sub(p, ' ', pod).encode('utf-8'))
                    dirty = True

            if dirty:
                print(d['_id'] + " pob/pod reset.")
                db.put_ent(d)
        except:
            print("fix_pob_pod(PA) failed on: "+d['_id'])

def repair(fid, report):
    pass

def reset_ur(fid, db, pid=None):
    if pid:
        splt = pid.split('-')
        urid = pid.replace('-PA-','-UR-')
        ts = int(time.time())
        encrypted_pwd = utils.sha1_sig(pid + '_abc')
        ur = {'_id': urid,'tx': str(ts), 'usage': {},
              'pwds':{'login': encrypted_pwd}}
        db.put_ent(ur)
    else:
        rows = db._get_cid_recs(fid, 'PA')  # return [[rec, eid, ts],..]
        for r in rows:  # r: [rec, eid, ts]
            d = json.loads(r[0]) # get ent-dic from rec
            reset_ur(fid, db, d['_id'])

def display(inputs, repo): #[display-<fid>-<cid>]
    t = repo.fidtable(inputs[1])
    lst = t.get_cid_dics(inputs[2])
    html_msg = "dics of class "+inputs[2]+"<br/><hr/>"
    for e in lst:
        html_msg += utils.json.dumps(e) + '<br/><hr/>'
    return html_msg

def delete_le(fid, db):
    db._del_cid(fid, 'LE')
