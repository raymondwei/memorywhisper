#
# Copyright (C) 2014 Adacro Inc. - All Rights Reserved
# -----------------------------------------------------------------------------
# dal.py: data access layer
#   offer api to db
# -----------------------------------------------------------------------------

from string import Template
from pdb import set_trace
import sys
# -----------------------------------------------------------------------------
# sql script: create table: 1 var = FID (uppercase)
_q1 = """CREATE TABLE IF NOT EXISTS __table_name(
  cid    CHAR(2)  NOT NULL,
  eid    CHAR(20) NOT NULL,
  ts     INT(10)  UNSIGNED NOT NULL DEFAULT 0,
  rec    MEDIUMBLOB,
  PRIMARY KEY(cid,eid)
);"""

_q2  = "SELECT * FROM information_schema.tables WHERE table_schema = 'mw'"
_q2 += "  AND table_name = 'testtable' LIMIT 1;"

_slike = Template(
    "SELECT rec,ts FROM $tname WHERE cid='$cid' AND eid like '$eid%';")

# sql script: select a single rec using fid,cid,eid
_sel1 = Template("SELECT rec,ts FROM $tname WHERE cid='$cid' AND eid='$eid';")

# sql script: put a row, needed input: fid, cid, eid, rec
# if a row with key= cid+eid already existed, overwrite; if not insert it
m = "REPLACE INTO $tname(cid,eid,ts,rec) VALUES('$cid','$eid',$ts,'$rec');"
_repl=Template(m)

m = "INSERT INTO $tname(cid,eid,ts,rec)VALUES('$cid','$eid',$ts,'$rec');"
_insert=Template(m)

# sql script: delete a single record
_del1 = Template("DELETE FROM $tname WHERE cid='$cid' AND eid='$eid';")

# select all recs in a fid table
_selfid = Template("SELECT rec, ts FROM $tname;")

# sql script for getting rows of the same cid: fid, cid needed
_selcid = Template("SELECT rec, eid, ts FROM $tname WHERE cid='$cid';")

# 
_batget =Template("SELECT rec FROM $tname WHERE cid='$cid' LIMIT $l OFFSET $o;")

# sql for deleting all records of cid class
_del_cid = Template("DELETE FROM $tname where cid='$cid';")
# -----------------------------------------------------------------------------
if sys.version_info.major == 3:
    import pymysql as MySQLdb
else:
    import MySQLdb
import os, time
import ujson as json

class DB(object):
    def __init__(self, _host="localhost",
                 _user="dorf",
                 _passwd="spotty",
                 _db="mw"):
        self.db = MySQLdb.connect(_host,_user,_passwd,_db)
        # fid may start with non-char. table name: T_<fid>
        self.load_table_names()

    # load names of all fid-tables that exist in DB into self.table_names dict
    def load_table_names(self):
        self.table_names = {} # {<fid>:<table-name>,..} purge olds
        self.db.ping(True)
        cursor = self.db.cursor()
        count = cursor.execute("SHOW TABLES;")
        if count > 0:
            lst = cursor.fetchall()
            for t in lst:
                tname = t[0]
                if tname.startswith('T_'):
                    fid = tname.split('_')[-1]
                    if len(fid) == 7:
                        self.table_names[fid] = tname
        cursor.close() 

    def drop_fid_table(self, fid): # fid - 7 char uppercase
        table_name = 'T_' + fid
        q = 'DROP TABLE IF EXISTS mw.'+table_name+';'
        self.db.ping(True)
        cursor = self.db.cursor()
        cursor.execute(q)
        cursor.close()

    # if no table for fid existed, create fid-table. 
    # return table-name
    def _fid_table_name(self, fid):
        try:# fid has to be 7 char long. table-name = T_<fid>
            if len(fid) != 7:
                return False
            fid = fid.upper()
            if fid in self.table_names:
                # if fid in here, table and blob-dir both exist already
                return self.table_names[fid]
            else:
                # create a table for fid every fid has its table and blob-dir
                tname = 'T_'+fid
                q = _q1.replace('__table_name', tname)
                self.db.ping(True)
                cursor = self.db.cursor()
                cursor.execute(q)
                cursor.close()
                self.table_names[fid] = tname
                # create blob-dir: a fid has its own blob-dir: blobs/<fid>/
                dirname = './blobs/' + fid
                if not os.path.exists(dirname):
                    os.mkdir(dirname)
            return tname
        except:
            return False
    
    def _del_cid(self, fid, cid):
        try:
            tname = self._fid_table_name(fid)
            q = _del_cid.substitute(tname=tname,cid=cid)
            self.db.ping(True)
            cursor = self.db.cursor()
            res = cursor.execute(q)
            cursor.close()
            return 
        except:
            return

    def _get_like_recs(self, fid, cid, eid_like):
        try:
            tname = self._fid_table_name(fid)
            if not tname:
                return False
            q = _slike.substitute(tname=tname,cid=cid,eid=eid_like)
            self.db.ping(True)
            cursor = self.db.cursor()
            if cursor.execute(q):
                rows = cursor.fetchall()
                cursor.close()
                return rows
            return []
        except:
            return []

    # get rec-text, ts of a single row identified by fid/cid/eid
    def _get_rec_ts(self, fid, cid, eid):
        try:
            tname = self._fid_table_name(fid)
            if not tname:
                return False
            q = _sel1.substitute(tname=tname, cid=cid,eid=eid)
            self.db.ping(True)
            cursor = self.db.cursor()
            if cursor.execute(q): # 1 if hit one, 0 if no hit
                rec = cursor.fetchone()
                cursor.close() 
                return rec # rec is a tuple:(<rec>,<ts>)
            return False
        except:
            return False

    # get N(limit) rows from fid-table, from offset
    # return[0]: if so many recs has been retrieved as wished(limit): 
    #            ready for moret(True); if len(recs) < limit,return[0]= False 
    # return[1]: list of recs
    def _batch_get(self, fid, cid, offset=0, limit=50):
        lst = []
        try:
            self.db.ping(True)
            cursor = self.db.cursor()
            tname = self._fid_table_name(fid)
            q = _batget.substitute(
                    tname=tname,cid=cid,
                    l=str(limit),
                    o=str(offset))
            res = cursor.execute(q)
            if res > 0:
                lst = cursor.fetchall()
            cursor.close()
            return (res == limit, lst)
        except:
            return (False, lst)

    # insert rec-text, identified by fid/cid/eid. row must not have existed.
    def _insert_rec(self, fid, cid, eid, ts, rec, commit_it=True):
        try:
            tname = self._fid_table_name(fid)
            if not ts: 
                ts = int(time.time())
            if not tname:
                return False
            q = _insert.substitute(tname=tname,cid=cid,eid=eid,ts=ts,rec=rec)
            self.db.ping(True)
            cursor = self.db.cursor()
            res = cursor.execute(q) # res should be 1L
            if commit_it:
                self.db.commit()
            cursor.close()
            return res == 1
        except:
            return False

    # put rec-text, identified by fid/cid/eid, regardless if the row existed
    def _put_rec(self, fid, cid, eid, ts, rec, commit_it=True):
        try:
            tname = self._fid_table_name(fid)
            if not ts: 
                ts = int(time.time()) # 10 digits(secs since epoch in UTC)
            if not tname:
                return False
            #set_trace()
            q = _repl.substitute(tname=tname,cid=cid,eid=eid,ts=ts,rec=rec)
            self.db.ping(True)
            cursor = self.db.cursor()
            res = cursor.execute(q)
            if commit_it:
                self.db.commit()
            cursor.close()
            return res > 0
        except:
            return False 

    # delete the row identified by fid/cid/eid
    def _delete_rec(self, fid, cid, eid, commit_it=True):
        try:
            tname = self._fid_table_name(fid)
            if not tname:
                return False
            self.db.ping(True)
            q = _del1.substitute(tname=tname,cid=cid,eid=eid)
            cursor = self.db.cursor()
            res = cursor.execute(q)
            if commit_it:
                self.db.commit()
            cursor.close()
            return res == 1
        except:
            return False
    
    # get all rec,ts from fid-table
    def _get_fid_recs(self, fid):
        try:
            tname = self._fid_table_name(fid)
            if not tname: return False
            q = _selfid.substitute(tname=tname) #[(rec,ts),(rec,ts),...]
            self.db.ping(True)
            cursor = self.db.cursor()
            cursor.execute(q)
            rows = cursor.fetchall()
            cursor.close()
            return rows            
        except:
            return False

    # all recs selected by fid and cid, return: [(rec, eid, ts),...]
    def _get_cid_recs(self, fid, cid):
        try:
            tname = self._fid_table_name(fid)
            if not tname:
                return False
            # "SELECT rec, eid, ts FROM $tname WHERE cid='$cid';"
            q = _selcid.substitute(tname=tname, cid=cid)
            self.db.ping(True)
            cursor = self.db.cursor()
            cursor.execute(q)
            recs = cursor.fetchall()
            cursor.close()
            return recs
        except:
            return False
    # ------------------------------------------------------------------------
    def get_ent(self,         # get ent-dict(or str) id-ed by ent-id
                ent_id,       # e.G. TC6H8EU-PA-etg5fv1430123642190 
                to_dic=True): # T: convert text to dict; F: return text str
        if not ent_id: return None
        fid, cid, eid = ent_id.split('-')
        rec_ts = self._get_rec_ts(fid, cid, eid) # returns [<rec>,<ts>]
        if rec_ts:
            if to_dic:
                return json.loads(rec_ts[0])     # rec -> dic/return it
            else:
                return rec_ts[0]                 # return rec (string)
        else:
            return None

    def reset_tx(self, ent):
        ts = int(time.time())
        ent['tx'] = str(ts)
        return ts

    # if ent has tx, extract it, if not put now-ts into ent, return it
    def ts_from_ent(self, ent, update_ts=False): # ent is a dict
        ts = 0
        if update_ts or 'tx' not in ent or not ent['tx']:
            ts = int(time.time())
            ent['tx'] = str(ts)
        elif 'tx' in ent and ent['tx']: # has tx, and ent[tx] != None
            if len(ent['tx']) >= 30:
                ts = int(ent['tx'].split('-')[-1][6:16])
            elif len(ent['tx']) == 10:
                ts = int(ent['tx'])
        return ts

    # delta- or delete-dic(must have _id) has only changed k/v or _id
    # fill it to be whole ent, return it
    def complete_dic(self, delta_dic): 
        import collections
        def deep_update(source, overrides):
            for key, value in overrides.items():# python3:items(); p2:iteritems
                if isinstance(value, collections.Mapping) and value:
                    returned = deep_update(source.get(key, {}), value)
                    source[key] = returned
                else:
                    source[key] = overrides[key]
            return source

        ent = self.get_ent(delta_dic['_id'])
        if not ent: return None
        return deep_update(ent, delta_dic)

    def save_ent(self,  # save entity into DB. entity must not have existed
                 ent):  # ent is a dict
        fid, cid, eid = ent['_id'].split('-')
        ts = self.ts_from_ent(ent)
        return self._insert_rec(fid, cid, eid, ts, json.dumps(ent))

    def put_ent(self,       # save/overwrite
                ent,        # ent is a dict
                update_ts=False): # update ts here
        fid, cid, eid = ent['_id'].split('-')
        ts = self.ts_from_ent(ent, update_ts)
        if self._put_rec(fid, cid, eid, ts, json.dumps(ent)):
            return ent
        else:
            return None

    def put_blob(self,      # put bin-file under blobs/fid-table-name 
                 fid,       # fid
                 name,      # file name e.g. 11429-as23hjkjhj..sdfds.jpg
                 bin_data): # binary data. Can be text too
        try:
            tname = self._fid_table_name(fid)
            if not tname:
                return False
            fname = "./blobs/" + fid + "/" + name 
            ofile = open(fname,'wb')
            ofile.write(bin_data)
            ofile.close()
            return True
        except:
            return False

    def delete_ent(self,    # delete entity from DB 
                   ent_id): # e.G. TC6H8EU-PA-etg5fv1430123642190
        fid, cid, eid = ent_id.split('-')
        return self._delete_rec(fid, cid, eid)

    # acts: [('C|D',<id>,<ent-dic>),(),..]
    def transaction(self, acts): #
        # collect for reporting creates/deletes
        collect = {} # {<cid>:[(),(),..], ...}
        # () can be ('C',<ent-id>,{<ent-dic>}), or ('D','<ent-id>')
        try:
            if len(acts) > 0:
                self.db.autocommit(False)
                for act in acts: # act: ('C|D',<id>,dic)
                    fid, cid, eid = act[1].split('-')
                    lst = collect.setdefault(cid, [])
                    if act[0] == 'D':
                        self._delete_rec(fid, cid, eid, 
                                        False) # dont commit
                        lst.append(('D',act[1]))
                    elif act[0] == 'C':
                        ts = int(act[2]['tx']) # 
                        self._put_rec(fid, cid, eid, ts, 
                                    json.dumps(act[2]),
                                    False)    # dont commit
                        lst.append(('C',act[2]))
                self.db.commit()
            return collect
        except:
            self.db.rollback()
            return False
# --- end of class DB(object)

def test():
    db = DB()
    ent = db.get_ent('TC6H8EU-BA-AKmFZi1408857744219')
    print(str(ent))

if __name__ == '__main__':
    test()
