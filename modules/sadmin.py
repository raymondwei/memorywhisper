from modules import utils
import os

def handle(req, cmd, db):
    res = {'result': '__failed__'}
    try:
        # for cmd switching to show-DB
        if cmd == 'getfids':
            db.load_table_names()
            lst = list(db.table_names.keys())
            res['result'] = '__OK__'
            res['docs'] = lst
            res['info'] = 'count: ' + str(len(lst))
            return utils.json.dumps(res)
        elif cmd in ('getdfiles','getzfiles'):
            #utils.set_trace()
            sel = cmd[3] # 'd' or 'z'
            if sel == 'd':
                ext = 'data'
            else:
                ext = 'zip'
            fid = req.values.get('fid', None)
            if fid:
                fns = os.listdir('dbbackups/'+fid)
                res['result'] = '__OK__'
                res['log'] = open('dbbackups/'+fid+'/log.txt').read()
                lst = []
                for fn in fns:
                    if fn.endswith(ext):
                        lst.append(fn)
                res['lst'] = lst
            return utils.json.dumps(res)
        elif cmd == 'getzfiles':
            fid = req.values.get('fid', None)
            if fid:
                pass
            return utils.json.dumps(res)
        elif cmd == 'getdics':
            fid = req.values.get('fid', None)
            cid = req.values.get('cid', None)
            eid = req.values.get('eid', None)
            if not cid:
                rows = db._get_fid_recs(fid)
                lst = []
                for row in rows:
                    lst.append(row[0])
                res['result'] = '__OK__'
                res['docs'] = lst
                res['info'] = "count: " + str(len(lst))
                return utils.json.dumps(res)
            elif not eid:
                rows = db._get_cid_recs(fid, cid)
                lst = []
                for row in rows:
                    lst.append(row[0])
                res['result'] = '__OK__'
                res['docs'] = lst
                if cid == 'CT':
                    dic = utils.json.loads(row[0])
                    res['info'] = "count: " + str(len(lst)) + ', ' +\
                        'snapshots: ' + str(len(dic['snapshots']))
                else:
                    res['info'] = "count: " + str(len(lst))
                return utils.json.dumps(res)
            else:  # fid, cid, eid all given
                if len(eid) < 19: # eid is a partial id-string
                    rows = db._get_like_recs(fid, cid, eid)
                    lst = []
                    for row in rows:
                        lst.append(row[0])
                    res['result'] = '__OK__'
                    res['docs'] = lst
                    res['info'] = "count: " + str(len(lst))
                    return utils.json.dumps(res)
                else:
                    rec_ts = db._get_rec_ts(fid, cid, eid)  # (rec, ts)
                    res['result'] = '__OK__'
                    res['docs'] = [rec_ts[0]]
                    res['info'] = '1 entity fetched.'
                    return utils.json.dumps(res)
        elif cmd == 'dels':
            # utils.set_trace()
            ids = utils.json.loads(req.values.get('ids', "[]"))
            for id in ids:
                db.delete_ent(id)
            res['result'] = '__OK__'
            res['info'] = 'deleted: ' + str(len(ids))
            return utils.json.dumps(res)
        elif cmd == 'modifyent':
            pass
        elif cmd == 'verifydb':
            #utils.set_trace()
            fid = req.values.get('fid',None)
            if fid:
                os.system("python dbbackup.py "+fid+" verify")
                o = __import__(fid+'_report')
                res['result'] = '__OK__'
                res['sum'] = str(o.sum)
                res['pas'] = str(o.pas)
                res['bas'] = str(o.bas)
                res['ips'] = str(o.ips)
                res['its'] = str(o.its)
                res['fcs'] = str(o.fcs)
                res['tds'] = str(o.tds)
                res['rss'] = str(o.rss)
            return utils.json.dumps(res)
        elif cmd == 'backupdb':
            #utils.set_trace()
            fid = req.values.get('fid',None);
            if fid:
                os.system("python dbbackup.py "+fid)
                res['result'] = '__OK__'
                nb = open('tmp.txt').read()
                m = 'datafile: '+nb+'.data;<br/> zipfile: '+nb+'.zip'
                res['info'] = m
            return utils.json.dumps(res)
        elif cmd in ('recoverdfile','recoverzfile'):
            #utils.set_trace()
            fid = req.values.get('fid', None)
            fpath = req.values.get('fpath',None)
            if fid and fpath:
                path = 'dbbackups/'+fid+'/'+fpath
                os.system('python loaddb.py '+path)
                res['result'] = '__OK__'
                res['info'] = open('tmp.txt').read()
                res['log'] = open('dbbackups/'+fid+'/log.txt').read()
            return utils.json.dumps(res)
        elif cmd == 'genpcode':
            os.system("python loaddb.py pcode")
            res['result'] = '__OK__'
            return utils.json.dumps(res)
        elif cmd == 'resetct':
            #utils.set_trace()
            fid = req.values.get('fid',None)
            if fid:
                os.system("python menddb.py "+fid)
                res['result'] = '__OK__'
                #res['info'] = 'OK'
            return utils.json.dumps(res)
        elif cmd == 'loadldict':
            os.system("python loaddb.py ldict")
            res['result'] = '__OK__'
            return utils.json.dumps(res)
    except:
        res['info'] = '__failed_exception'
        utils.action_log('debugE', 'sadmin exception', db)
        return utils.json.dumps(res)
