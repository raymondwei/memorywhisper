#
# landingpage100 used fcs: _fc090 (creator), _fc100(landingpage), _fc101(video),
# _fc102(feature-list),_fc102_<n>(n in 1-20): feature pages
# -----------------------------------------------------------------------------
_a090en = 'This software is under development, not released; It '+\
'is for presentations and discussions only.'
# -------------------------------------------------------------------
_a090zh = '本软件产品尚处于开发阶段，不是公开产品。这里仅是为了内部演示与讨论。'
# -------------------------------------------------------------------
_c090en = '<img id="raymondwei" src="pics/portrait2014.jpg"/>'+\
'<p>About the creator</p>'+\
'Raymond Wei is a senior Software Engineer.<br/> He holds a Ph.D. degree in '+\
 'Computer Science from University Karlsruhe, Germany. <br/>He has over '+\
 '25 years of working experience as Sr. software developer '+\
 'both with established companies and startups, in Germany, and in silicon '+\
 'valley California. </p><p>2005 he moved to live in Canada. '+\
 'Now he lives in Houston Texas US.</p>'
# -------------------------------------------------------------------
_c090zh='<img id="raymondwei" src="pics/portrait2014.jpg"/>'+\
 '<p>关于创始人</p>'+\
 '<p>芮孟德是一位资深软件工程师。<br/>他曾作为哈尔滨工业大学电子工程专业优秀毕业生被国家'+\
 '选送前往德国深造。后毕业于德国卡尔斯鲁尔大学信息工程系，并获取计算机科学博士学位。<br/>'+\
 '作为软件开发工程师，他有多于二十五年的工作经验，他曾就职于德国，美国硅谷的大小高科技公司，'+\
 '以及创业公司。</p><p>2005年，他移居于加拿大。目前他住在美国德州休斯顿。</p>'
# -------------------------------------------------------------------
_a100en = 'This software is under development, not released; It '+\
        'is for presentations and discussions only.'
# -------------------------------------------------------------------
_c100en = "<p>About Memory Whisper</p>"+\
"<p>Our heritage, our life and emotional paths define who we are. Documenting"+\
" our stories of life and family becomes more important as we go along. "+\
"Life is increasingly fast "+\
"paced, globalized and sophisticated, making it rich and colorful, "+\
"but also hard to keep track of.</p>"+\
"<p>Memory Whisper offers a centralized way for users to keep track of family "+\
"relationships and manage personal documents. It makes connecting and sharing "+\
"with family members easy, and lets users set detailed privacy and safety "+\
" control.</p><div class='lp100-navbox'>"+\
"<span class='lp100-nav' id='nav-item-0'>About the creator</span>"+\
"<span class='lp100-nav' id='nav-item-2'>Product tour video</span>"+\
"<span class='lp100-nav' id='nav-item-3'>Feature list</span></div>"
# -------------------------------------------------------------------
_a100zh = '本软件产品尚处于开发阶段，不是公开产品。这里仅是为了内部演示与讨论。'
# -------------------------------------------------------------------
_c100zh = "<p> 关于《足迹 族记》 </p><p>我们的血缘，人生经历和心迹历程，"+\
"定义了我们是谁。所以，每一个人独特的人生故事，家庭故事，随着人生之路的延伸，"+\
"越来越被珍视。如今，生活节奏日趋加速，全球化，技术化的趋势，使生活更加丰富多彩。"+\
"而能否收集，保存好自己特有的人生，和家庭的故事，对于每个人，也变得越来越重要。</p>"+\
"<p>《足迹 族记》提供了一个平台，让使用者能够系统地编辑，保存好自己珍贵的多媒体文件，"+\
"以及家庭关系的记录；也使家庭成员间的通信，联络，分享更加方便。"+\
"同时，个人信息的隐私性也可以按用户要求设置控制。</p>"+\
"<div class='lp100-navbox'>"+\
"<span class='lp100-nav' id='nav-item-0'>创始人的简介</span>"+\
"<span class='lp100-nav' id='nav-item-2'>产品介绍视频</span>"+\
"<span class='lp100-nav' id='nav-item-3'>功能清单</span></div>"
# -------------------------------------------------------------------
_a101en="The choice of the example family tree has no political, "+\
"or personal interest. It was only because the persons here are "+\
"public figures and information about them is publicly available."
# -------------------------------------------------------------------
_a101zh = "这里选择的例子家谱，没有任何政治或个人偏好与倾向。选择它们是由于其中的人物信息"+\
"都可以在网上获取(并非“族记-足迹”所编)。"
# -------------------------------------------------------------------
_a102en="Icons: Double(blue/red)-heart-ring-pairs represent spousal bonds; "+\
"red round disk left side of a name tag: connection to upper generation; "+\
"white disk with (-)/(+\) sign: expand/shrink spouses and siblings, "+\
"clicking on them will expand/shrink them."
# -------------------------------------------------------------------
_a102zh = "关注人名片周围的标符： 红／蓝色心双环代表配偶关系；左侧红色圆点：上一代连结；"+\
"白色，带有 (-)/(+\) 的圆点：配偶／兄妹结点，点击它会展开／缩回。"
# -------------------------------------------------------------------
lp100_tips = [
  # index 0: #1 feature */
 {'en':"A personal family tree homepage can is accessed with a "+\
 "personalized URL (long and cryptic looking, not made known to the public "+\
 "eyes). This URL is hard to book-mark - it disappears after "+\
 "landing. However the owner can send this URL to anyone trust-worthy, "+\
 "who then can access the page.",
 'zh':"每个人的个人家谱首页链接是一个难以记忆的，不公开的长字符串，只有自己知道。这个链接字符"+\
 "串在浏览器显示首页后随即消失，难以存下网址。当然，拥有者可以将其电邮寄给可信赖的他人，让他"+\
 "人也能够阅读"},
  # index 1: #2 feature */
 {'en':"28 languages are supported as operational language. On the upper-left "+\
"corner, there is a language (flag) sign. Clicking on it, the user can switch.",
 'zh':"每个人的首页由自己设置优先语言选择。点击首页左上角的语言（国旗）标符，可以随时切换。"
 },
  # index 2: feature 3 */
 {'en':"Navigate around family tree by clicking on action icons of a focus "+\
  "person name tag, read all people's not-protected information",
 'zh':"点击页面上关注人的不同的互动标符，可以以互动方式展开／回缩，以浏览家谱中所关心部分"
 },
  # index 3: feature 4 */
 {'en':"Navigate to spouse-related family trees by clicking on focus person "+\
  "spouse's bloodline node(left side of name tag).",
 'zh':"点击关注人配偶名片左侧的血缘节点，即可转换到不同姓氏（配偶）的家谱"},
  # index 4: feature 5 */
 {'en':"Post personal basic information into family tree, including:"+\
  "Name, birth-day/place, death-day/place, short introduction, portrait",
  'zh':""},
  # index 5: feature 6 */
 {'en':"A person's information, and all folders and documents can be saved in "+\
  "different languages. The displayed language follows language switch.",
  'zh':"个人信息以及个人上载，编辑的文件，文件夹，都可用多种语言存储。这些信息都可以支持"+\
  "显示语言切换。"
 },
  # index 6: feature 7 */
 {'en':"User can add other family members to the tree, and edit their personal"+\
  "information and manage their documents (user then becomes their trustee).",
  'zh':"用户可以添加家庭其他成员，他们的个人信息和文件（用户将成为被添加人的信托人）。"},
 # index 7: feature 8 */
 {'en':"User can browse family calendar, seeing B-day/D-day events of all "+\
  "people in the family tree. And navigate to their home page",
 'zh':"在家庭日历上，用户可以浏览任何月日的家人生卒纪念日，并阅读他们的家谱首页。"},
 # index 8: feature 9 */
 {'en':"User can subscribe/receive notifications of anniversary events "+\
  "of all family members in the tree.",
 'zh':"订阅，自动收取家人近日的纪念日预告"},
 # index 9: feature 10 */
 {'en':"User can invite anyone in the family tree, to accessing his/her "+\
  "family tree personal-home-page",
  'zh':"用户可以向家庭成员发出邀请，去浏览，登陆他们本人的家谱首页。"},
 # index 10: feature 11 */
 {'en':"Set/update login password, page-background color-theme, language choice",
  'zh':"设置自己的首页选择：登陆密码；首页背景图案；优先操作语言"},
 # index 11: feature 12 */
 {'en':"Set/update password, password-schedules, for protected folders, and "+\
  "family relationship (spouses, children) nodes; manage truestees.",
  'zh':"设置文件，家庭关系的隐私保护，及其密码；管理信托人"},
 # index 12: feature 13 */
 {'en':"Leave comments or add documents to other family member personal archive",
  'zh':"对家庭成员留言，或填送文件"},
 # index 13: feature 14 */
 {'en':"Construct personal archives：categories， folders，documents; write or "+\
 "upload multi media files.",
  'zh':"构建自己的文件类别，文件夹和文件；上传，管理个人的多媒体文件"},
 # index 14: feature 15 */
 {'en':"Elevated size limit for personalarchive",
  'zh':"扩充上载文件上限"},
 # index 15: feature 16 */
 {'en':"Using  name or name-part/ pattern, for searching with family tree",
  'zh':"用名字，或不完整的名字，搜寻家谱中人物"},
 # index 16: feature 17 */
 {'en':"Set privacy control in personal archive for personal archive folders: "+\
 "private - visible only after login; or password protection, where password "+\
 "can be forever or scheduled (after expiration: becomes private or public).",
 'zh':"设置个人文件夹的隐私保护: 完全隐私 －只有登陆后，方可显现；密码保护 － 需用密码开启。"+\
 "密码可以是无期限，或设以期限（过期后可成为公开，或完全隐私）。"},
 # index 17: feature 18 */
 {'en':"Set privacy control for spousal or chidren relationships nodes: "+\
 "private - visible only after login; or password protection, where password "+\
 "can be forever or scheduled (after expiration: becomes private or public).",
 'zh':"设置个人配偶／后代关系节点的隐私保护: 完全隐私 －只有登陆后，方可显现；密码保护 － "+\
 "需用密码开启。密码可以是无期限，或设以期限（过期后可成为公开，或完全隐私）。"},
 # index 18: feature 19 */
 {'en':"Participate in family forum.",
 'zh':"有权在家庭论坛上读写."},
 # index 19: feature 20 */
 {'en':"Send anyone (can be non-family member) URLs of personal folders, so "+\
 "the receiver can read all documents in the folder, without see family "+\
 "tree graph. Password-protections for folders still apply.",
 'zh':"个人的每一个文件夹，可以产生网上链接，然后将其送给他人。收者可以打开这个文件夹。"+\
 "如果文件夹，或子文件夹有密码保护，收者还需密码。"}
] #-- end of lp100_tips ----------------

lp100_flist = {'en':'<h4>Feature List</h4><ol><li>'+\
 '<div id="lp-item-1" class="lpitem">'+\
 'Personalized homepage'+\
 # -----------------------------------------------------
 '</div></li><li><div id="lp-item-2" class="lpitem">'+\
 'Multiple operational languages'+\
 # -----------------------------------------------------
 '</div></li><li><div id="lp-item-3" class="lpitem">'+\
 'Interactive browsing of the family tree'+\
 # -----------------------------------------------------
 '</div></li><li><div id="lp-item-4" class="lpitem">'+\
 'Switching to different family trees'+\
 # -----------------------------------------------------
 '</div></li><li><div id="lp-item-5" class="lpitem">'+\
 'Post personal information'+\
 # -----------------------------------------------------
 '</div></li><li><div id="lp-item-6" class="lpitem">'+\
 'Multi language support for user information and documents'+\
 # -----------------------------------------------------
 '</div></li><li><div id="lp-item-7" class="lpitem">'+\
 'Adding family members to the tree'+\
 # -----------------------------------------------------
 '</div></li><li><div id="lp-item-8" class="lpitem">'+\
 'Calendar'+\
 # -----------------------------------------------------
 '</div></li><li><div id="lp-item-9" class="lpitem">'+\
 'Events subscription'+\
 # -----------------------------------------------------
 '</div></li><li><div id="lp-item-10" class="lpitem">'+\
 'Sending invitation to family members'+\
 # -----------------------------------------------------
 '</div></li><li><div id="lp-item-11" class="lpitem">'+\
 'Basic account configuration'+\
 # -----------------------------------------------------
 '</div></li><li><div id="lp-item-12" class="lpitem">'+\
 'Advanced account configuration'+\
 # -----------------------------------------------------
 '</div></li><li><div id="lp-item-13" class="lpitem">'+\
 'Posting comments and documents to other family members'+\
 # -----------------------------------------------------
 '</div></li><li><div id="lp-item-14" class="lpitem">'+\
 'Construct Personal Archive'+\
 # -----------------------------------------------------
 '</div></li><li><div id="lp-item-15" class="lpitem">'+\
 'Elevated file uploading limit'+\
 # -----------------------------------------------------
 '</div></li><li><div id="lp-item-16" class="lpitem">'+\
 'Search name in family tree'+\
 # -----------------------------------------------------
 '</div></li><li><div id="lp-item-17" class="lpitem">'+\
 'Set privacy controls to personal archive folders'+\
 # -----------------------------------------------------
 '</div></li><li><div id="lp-item-18" class="lpitem">'+\
 'Set privacy control to spouse-node and children-node'+\
 # -----------------------------------------------------
 '</div></li><li><div id="lp-item-19" class="lpitem">'+\
 'Participating in family forum'+\
 # -----------------------------------------------------
 '</div></li><li><div id="lp-item-20" class="lpitem">'+\
 'Personal archive folder public exposure'+\
 '</div></li></ol>',
 # -----------------------------------------------------
 # -----------------------------------------------------
 'zh':'<h4>功能清单</h4><ol><li>'+\
 '<div id="lp-item-1" class="lpitem">'+\
 '个人家谱首页'+\
 # -----------------------------------------------------
 '</div></li><li><div id="lp-item-2" class="lpitem">'+\
 '多重操作语言支持'+\
 # -----------------------------------------------------
 '</div></li><li><div id="lp-item-3" class="lpitem">'+\
 '互动式家谱浏览'+\
 # -----------------------------------------------------
 '</div></li><li><div id="lp-item-4" class="lpitem">'+\
 '切换不同姓氏家谱'+\
 # -----------------------------------------------------
 '</div></li><li><div id="lp-item-5" class="lpitem">'+\
 '编辑个人信息'+\
 # -----------------------------------------------------
 '</div></li><li><div id="lp-item-6" class="lpitem">'+\
 '对个人信息，文件的多重语言支持'+\
 # -----------------------------------------------------
 '</div></li><li><div id="lp-item-7" class="lpitem">'+\
 '添加家庭成员及其个人信息'+\
 # -----------------------------------------------------
 '</div></li><li><div id="lp-item-8" class="lpitem">'+\
 '家庭日历'+\
 # -----------------------------------------------------
 '</div></li><li><div id="lp-item-9" class="lpitem">'+\
 '订阅家庭成员的纪念日'+\
 # -----------------------------------------------------
 '</div></li><li><div id="lp-item-10" class="lpitem">'+\
 '向家庭成员发出邀请'+\
 # -----------------------------------------------------
 '</div></li><li><div id="lp-item-11" class="lpitem">'+\
 '个人家谱首页基本设置'+\
 # -----------------------------------------------------
 '</div></li><li><div id="lp-item-12" class="lpitem">'+\
 '个人家谱首页深层设置'+\
 # -----------------------------------------------------
 '</div></li><li><div id="lp-item-13" class="lpitem">'+\
 '对家庭成员留言，添加文件'+\
 # -----------------------------------------------------
 '</div></li><li><div id="lp-item-14" class="lpitem">'+\
 '建构个人文档'+\
 # -----------------------------------------------------
 '</div></li><li><div id="lp-item-15" class="lpitem">'+\
 '提高上载文件上限'+\
 # -----------------------------------------------------
 '</div></li><li><div id="lp-item-16" class="lpitem">'+\
 '搜寻家谱中人物'+\
 # -----------------------------------------------------
 '</div></li><li><div id="lp-item-17" class="lpitem">'+\
 '设置个人文件隐私控制'+\
 # -----------------------------------------------------
 '</div></li><li><div id="lp-item-18" class="lpitem">'+\
 '设置浏览配偶及后代的隐私控制'+\
 # -----------------------------------------------------
 '</div></li><li><div id="lp-item-19" class="lpitem">'+\
 '参与家庭论坛'+\
 # -----------------------------------------------------
 '</div></li><li><div id="lp-item-20" class="lpitem">'+\
 '公开分享个人文件夹阅读权'+\
 '</div></li></ol>'
}
# ============================ lp300 ==========================================
lp300_h2list = {'en':'<h4>How To use</h4><ol><li>'+\
 '<div id="lp-item-1" class="lpitem">'+\
 'Open a new user account and create a family tree'+\
 # -----------------------------------------------------
 '</div></li><li><div id="lp-item-2" class="lpitem">'+\
 'Submit, update basic personal information '+\
 # -----------------------------------------------------
 '</div></li><li><div id="lp-item-3" class="lpitem">'+\
 'Add father and mother， and their basic personal information'+\
 # -----------------------------------------------------
 '</div></li><li><div id="lp-item-4" class="lpitem">'+\
 'Add spouse(s) and basic personal information. Make order if necessary'+\
 # -----------------------------------------------------
 '</div></li><li><div id="lp-item-5" class="lpitem">'+\
 'Add children and their information, order them if necessary'+\
 # -----------------------------------------------------
 '</div></li><li><div id="lp-item-6" class="lpitem">'+\
 'Add parents for a spouse'+\
 # -----------------------------------------------------
 '</div></li><li><div id="lp-item-7" class="lpitem">'+\
 'add or modify adopt parents'+\
 # -----------------------------------------------------
 '</div></li><li><div id="lp-item-8" class="lpitem">'+\
 'Make personal archive'+\
 # -----------------------------------------------------
 '</div></li><li><div id="lp-item-9" class="lpitem">'+\
 'Make a new folder, and make a new document'+\
 # -----------------------------------------------------
 '</div></li><li><div id="lp-item-10" class="lpitem">'+\
 'Edit document, add new sub-documents'+\
 # -----------------------------------------------------
 '</div></li><li><div id="lp-item-11" class="lpitem">'+\
 'Setup privacy control for a folder'+\
 # -----------------------------------------------------
 '</div></li><li><div id="lp-item-12" class="lpitem">'+\
 'Update personal account configuration'+\
 # -----------------------------------------------------
 '</div></li><li><div id="lp-item-13" class="lpitem">'+\
 'Invite family member to access their home page'+\
 # -----------------------------------------------------
 '</div></li><li><div id="lp-item-14" class="lpitem">'+\
 'Multi-language support for user data'+\
'</div></li></ol>',
 # -----------------------------------------------------
 'zh':'<h4>使用说明</h4><ol class="lp100-features"><li>'+\
 # ---------------1--------------------------------------
 '<div id="lp-item-1" class="lpitem">'+\
 '如何注册一个新用户，建立一个新的家谱'+\
 # ---------------2--------------------------------------
 '</div></li><li><div id="lp-item-2" class="lpitem">'+\
 '填写，修改个人基本信息'+\
 # ---------------3--------------------------------------
 '</div></li><li><div id="lp-item-3" class="lpitem">'+\
 '添加父母,以及他们的基本信息'+\
 # ---------------4--------------------------------------
 '</div></li><li><div id="lp-item-4" class="lpitem">'+\
 '添加配偶，以及他们的基本信息。如需要，修改排序'+\
 # ---------------5--------------------------------------
 '</div></li><li><div id="lp-item-5" class="lpitem">'+\
 '添加孩子，以及他们的基本信息。如需要，修改排序'+\
 # ---------------6--------------------------------------
 '</div></li><li><div id="lp-item-6" class="lpitem">'+\
 '添加配偶的父母，以及他们的基本信息'+\
 # ---------------7--------------------------------------
 '</div></li><li><div id="lp-item-7" class="lpitem">'+\
 '添加，修改：寄养父母，收养父母'+\
 # ---------------8--------------------------------------
 '</div></li><li><div id="lp-item-8" class="lpitem">'+\
 '建立个人文档'+\
 # ---------------9--------------------------------------
 '</div></li><li><div id="lp-item-9" class="lpitem">'+\
 '添加新文件夹，添加新文件'+\
 # ---------------10--------------------------------------
 '</div></li><li><div id="lp-item-10" class="lpitem">'+\
 '编辑文件，添加子文件'+\
 # ----------------11-------------------------------------
 '</div></li><li><div id="lp-item-11" class="lpitem">'+\
 '设置文件夹的隐私保护'+\
 # ----------------12-------------------------------------
 '</div></li><li><div id="lp-item-12" class="lpitem">'+\
 '设置个人账号的各种选择'+\
 # -----------------13------------------------------------
 '</div></li><li><div id="lp-item-13" class="lpitem">'+\
 '邀请其他家庭成员使用自己的家谱首页'+\
 # -----------------14------------------------------------
 '</div></li><li><div id="lp-item-14" class="lpitem">'+\
 '用户文件的多重语言支持'+\
 '</div></li></ol>'
} # ----- end of lp300_h2list

 # -----------------------------------------------------
lp300_h2tips = [
 # -- 1 -------------------------------------------------
 {'en': 'Create a new user account with a promo code, start a new family tree.',
  'zh': '使用注册码建立一个新的用户账号，并建立一个新的家谱。收到（电邮）个人首页网址'},
 # --- 2 ------------------------------------------------
 {'en': 'Fill in, or update basic personal information, upload portrait',
  'zh': '填写，修改个人基本信息：姓名，生卒时间，地点，上载个人肖像'},
 # --- 3 ------------------------------------------------
 {'en': 'Create node for father and mother, with their basic personal '+\
        'information and portraits',
  'zh': '添加父母结点。填写，修改父亲，母亲的基本信息：姓名，生卒时间，地点，上载个人肖像'},
 # --- 4 ------------------------------------------------
 {'en': 'Create spouse node(s). If multiple, adjust order among them',
  'zh': '添加配偶结点 － 基本信息，个人肖像。如果多个，然后按需要整理排行顺序'},
 # --- 5 ------------------------------------------------
 {'en': 'Create children nodes under a mariage with a spouse. If multiple, '+\
        'order them afterwards',
  'zh': '添加与配偶出生的孩子结点 － 基本信息，个人肖像。然后按需要整理排行顺序'},
 # --- 6 ------------------------------------------------
 {'en': 'Create parent nodes for a spouse, fill in their basic personal '+\
        'information and upload their portraits',
  'zh': '添加配偶的父母结点。填写，修改父亲，母亲的基本信息：姓名，生卒时间，地点，上载个人肖像'},
 # --- 7 ------------------------------------------------
 {'en': 'modify parents to be adopt-in or -out parents; add adopt-out/-in parents',
  'zh': '修改目前双亲成：寄养（孩出），或收养（孩入）双亲；添加：寄养（孩出），或收养（孩入）双亲'},
 # --- 8 ------------------------------------------------
 {'en': 'Construct personal archive center, choose and add categories to it.'+\
        ' Set privacy for a category: public - visible without login, or '+\
        'private - visible only after login',
  'zh': '建立个人文档中心：选择所要建立文档的类别。设定文档隐私：公开 － 不登录可见； '+\
        '或，只限私用 － 不登录就不显现'},
 # --- 9 ------------------------------------------------
 {'en': 'Create folder(s) under a personal-archive category. Create '+\
        'document(s) under a folder',
  'zh': '在存在的文档类别里，或文件夹里建立文件夹。在文件夹里建立新文件'},
 # --- 10 -----------------------------------------------
 {'en': 'A new document has a default text sub-document. Edit it, annoted it.'+\
      ' Adding new (multimedia)sub-documents for this document, add annotation',
  'zh': '每个新文件都有一个空的字符子文件。如何填写，编辑字子符文件內容。添加新的子文件，'+\
        '添加多媒体子文件。如何为子文件添加注释'},
 # --- 11 -----------------------------------------------
 {'en': 'Set up privacy protection for a folder: A: visible only private; '+\
        'B: password protection; C: scheduled password protection',
  'zh': '设定（公开文件类别的）文件夹隐私：1. 只限私用 － 不登录就不显现； 或'+\
        ' 2. 公开显现，但打开需要密码；3. 同 2. 但密码设有过期时限； 4. 没有隐私保护'},
 # --- 12 -----------------------------------------------
 {'en': 'make choices for personal account configurations: default language; '+\
        'theme choice, login-password; passwords for other protected items, '+\
        'and trustee mamangement.',
  'zh': '设置账号的个人选择：登录密码，其他隐私文件密码；色调风格，优先操作语言，信托人管理'},
 # --- 13 -----------------------------------------------
 {'en': 'After login, user can invite other family members for accessing '+\
        'their own personal family tree home page (by sending email)',
  'zh': '登录后，如何邀请其他家庭成员使用他们自己的家谱首页（发出电邮）'},
 # --- 14 -----------------------------------------------
 {'en': 'Any user constructed personal folders and documents, can be multi-'+\
        'language, so that when switching language, the documents can switch '+\
        'to the target language',
  'zh': '所有个人文件夹命名，文件命名，内容以及文件注释，个人信息，都可以用多种语言存贮 － '+\
        '它们就都会支持语言切换'}
] # ----------- end of lp300_h2tips --------------------
# --------- lp300 item 1 page, and picture file names. for en and zh
_c301en = '<div id="lp300-steps-root">'+\
      '<div class="lp300-steps" id="lp300-01en0"></div>'+\
      '<div class="lp300-steps" id="lp300-01en1"></div>'+\
      '<div class="lp300-steps" id="lp300-01en2"></div>'+\
      '<div class="lp300-steps" id="lp300-01en3"></div>'+\
      '<div class="lp300-steps" id="lp300-01en4"></div></div>'
_c301zh = '<div id="lp300-steps-root">'+\
      '<div class="lp300-steps" id="lp300-01zh0"></div>'+\
      '<div class="lp300-steps" id="lp300-01zh1"></div>'+\
      '<div class="lp300-steps" id="lp300-01zh2"></div>'+\
      '<div class="lp300-steps" id="lp300-01zh3"></div>'+\
      '<div class="lp300-steps" id="lp300-01zh4"></div></div>'
_a301en = 'lp300-01en0.jpg||lp300-01en1.jpg||lp300-01en2.jpg||'+\
      'lp300-01en3.jpg||lp300-01en4.jpg'
_a301zh = 'lp300-01zh0.jpg||lp300-01zh1.jpg||lp300-01zh2.jpg||'+\
      'lp300-01zh3.jpg||lp300-01zh4.jpg'
# --------- end of lp300 item 1 page ----------------------
# --------- lp300 item 2 page, and picture file names. for en and zh
_c302en = '<div id="lp300-steps-root">'+\
      '<div class="lp300-steps" id="lp300-02en1"></div>'+\
      '<div class="lp300-steps" id="lp300-02en2"></div>'+\
      '<div class="lp300-steps" id="lp300-02en3"></div>'+\
      '<div class="lp300-steps" id="lp300-02en4"></div>'+\
      '<div class="lp300-steps" id="lp300-02en5"></div></div>'
_c302zh = '<div id="lp300-steps-root">'+\
      '<div class="lp300-steps" id="lp300-02zh1"></div>'+\
      '<div class="lp300-steps" id="lp300-02zh2"></div>'+\
      '<div class="lp300-steps" id="lp300-02zh3"></div>'+\
      '<div class="lp300-steps" id="lp300-02zh4"></div>'+\
      '<div class="lp300-steps" id="lp300-02zh5"></div></div>'
_a302en = 'lp300-02en1.jpg||lp300-02en2.jpg||lp300-02en3.jpg||'+\
      'lp300-02en4.jpg||lp300-02en5.jpg'
_a302zh = 'lp300-02zh1.jpg||lp300-02zh2.jpg||lp300-02zh3.jpg||'+\
      'lp300-02zh4.jpg||lp300-02zh5.jpg'
# --------- end of lp300 item 2 page ----------------------
# --------- lp300 item 3 page, and picture file names. for en and zh
_c303en = '<div id="lp300-steps-root">'+\
      '<div class="lp300-steps" id="lp300-03en1"></div>'+\
      '<div class="lp300-steps" id="lp300-03en2"></div>'+\
      '<div class="lp300-steps" id="lp300-03en3"></div>'+\
      '<div class="lp300-steps" id="lp300-03en4"></div>'+\
      '<div class="lp300-steps" id="lp300-03en5"></div>'+\
      '<div class="lp300-steps" id="lp300-03en6"></div>'+\
      '<div class="lp300-steps" id="lp300-03en7"></div>'+\
      '<div class="lp300-steps" id="lp300-03en8"></div></div>'
_c303zh = '<div id="lp300-steps-root">'+\
      '<div class="lp300-steps" id="lp300-03zh1"></div>'+\
      '<div class="lp300-steps" id="lp300-03zh2"></div>'+\
      '<div class="lp300-steps" id="lp300-03zh3"></div>'+\
      '<div class="lp300-steps" id="lp300-03zh4"></div>'+\
      '<div class="lp300-steps" id="lp300-03zh5"></div>'+\
      '<div class="lp300-steps" id="lp300-03zh6"></div>'+\
      '<div class="lp300-steps" id="lp300-03zh7"></div>'+\
      '<div class="lp300-steps" id="lp300-03zh8"></div></div>'
_a303en = 'lp300-03en1.jpg||lp300-03en2.jpg||lp300-03en3.jpg||'+\
      'lp300-03en4.jpg||lp300-03en5.jpg||lp300-03en6.jpg'+\
      'lp300-03en7.jpg||lp300-03en8.jpg'
_a303zh = 'lp300-03zh1.jpg||lp300-03zh2.jpg||lp300-03zh3.jpg||'+\
      'lp300-03zh4.jpg||lp300-03zh5.jpg||lp300-03zh6.jpg||'+\
      'lp300-03zh7.jpg||lp300-03zh8.jpg'
# --------- end of lp300 item 3 page ----------------------
# --------- lp300 item 4 page, and picture file names. for en and zh
_c304en = '<div id="lp300-steps-root">'+\
      '<div class="lp300-steps" id="lp300-04en1"></div>'+\
      '<div class="lp300-steps" id="lp300-04en2"></div>'+\
      '<div class="lp300-steps" id="lp300-04en3"></div>'+\
      '<div class="lp300-steps" id="lp300-04en4"></div></div>'
_c304zh = '<div id="lp300-steps-root">'+\
      '<div class="lp300-steps" id="lp300-04zh1"></div>'+\
      '<div class="lp300-steps" id="lp300-04zh2"></div>'+\
      '<div class="lp300-steps" id="lp300-04zh3"></div>'+\
      '<div class="lp300-steps" id="lp300-04zh4"></div></div>'
_a304en = 'lp300-04en1.jpg||lp300-04en2.jpg||'+\
      'lp300-04en3.jpg||lp300-04en4.jpg'
_a304zh = 'lp300-04zh1.jpg||lp300-04zh2.jpg||'+\
      'lp300-04zh3.jpg||lp300-04zh4.jpg'
# --------- end of lp300 item 4 page ----------------------
# --------- lp300 item 5 page, and picture file names. for en and zh
_c305en = '<div id="lp300-steps-root">'+\
      '<div class="lp300-steps" id="lp300-05en1"></div>'+\
      '<div class="lp300-steps" id="lp300-05en2"></div>'+\
      '<div class="lp300-steps" id="lp300-05en3"></div></div>'
_c305zh = '<div id="lp300-steps-root">'+\
      '<div class="lp300-steps" id="lp300-05zh1"></div>'+\
      '<div class="lp300-steps" id="lp300-05zh2"></div>'+\
      '<div class="lp300-steps" id="lp300-05zh3"></div></div>'
_a305en = 'lp300-05en1.jpg||lp300-05en2.jpg||lp300-05en3.jpg'
_a305zh = 'lp300-04zh1.jpg||lp300-04zh2.jpg||lp300-05zh3.jpg'
# --------- end of lp300 item 5 page ----------------------
# --------- lp300 item 6 page, and picture file names. for en and zh
_c306en = '<div id="lp300-steps-root">'+\
      '<div class="lp300-steps" id="lp300-06en1"></div>'+\
      '<div class="lp300-steps" id="lp300-06en2"></div>'+\
      '<div class="lp300-steps" id="lp300-06en3"></div>'+\
      '<div class="lp300-steps" id="lp300-06en4"></div>'+\
      '<div class="lp300-steps" id="lp300-06en5"></div></div>'
_c306zh = '<div id="lp300-steps-root">'+\
      '<div class="lp300-steps" id="lp300-06zh1"></div>'+\
      '<div class="lp300-steps" id="lp300-06zh2"></div>'+\
      '<div class="lp300-steps" id="lp300-06zh3"></div>'+\
      '<div class="lp300-steps" id="lp300-06zh4"></div>'+\
      '<div class="lp300-steps" id="lp300-06zh5"></div></div>'
_a306en = 'lp300-06en1.jpg||lp300-06en2.jpg||'+\
      'lp300-06en3.jpg||lp300-06en4.jpg||lp300-06en5.jpg'
_a306zh = 'lp300-06zh1.jpg||lp300-06zh2.jpg||'+\
      'lp300-06zh3.jpg||lp300-06zh4.jpg||lp300-06zh5.jpg'
# --------- end of lp300 item 6 page ----------------------
