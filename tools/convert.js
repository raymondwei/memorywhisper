var fs = require('fs');
var et = require('./etstore.js');
var et_count = 0;

function process_files(release_dirpath){
    var i, 
        data,        // string from input file 
        data1,       // translated string for output
        map = {},    // {<old-name>: <new-name>, ...}
        filename,    // read-in file name 
        ofilename,   // write-out file name
        options = {  // file attributes for written js files 
            encoding: 'utf8', 
            mode: 438 /*=0666*/, 
            flag: 'w' 
        },
        tmppath = release_dirpath + '/tmpjs',      // read dir-path
        dstpath = release_dirpath + '/static/js/', // write dir-path
        lst = fs.readdirSync(tmppath);
    for(i=0; i<lst.length; i++){
        if(lst[i].slice(-3) === '.js'){
            filename = tmppath + '/' + lst[i];
            ofilename = dstpath + lst[i];
            try {
                console.log("reading file: ", filename);
                data = fs.readFileSync(filename,'utf8');

                // translate
                data1 = translate(data, map);
                //data1 = data;

                console.log('writing file: ', ofilename);
                fs.writeFileSync(ofilename, data1, options);
            } catch(error){
                console.log("Failed to read/write file:", filename);
            }
        }
    }
    var map_str = JSON.stringify(map, null, 2);
    fs.writeFileSync(release_dirpath+'/map.log', map_str, options);
};// end of function process_files

function translate(msg, map){
    var data1 = '';
    var replacement = '';
    var len, pos = 0;
    var pat = /\b(A[FVCJ]_\w+)\b/; // AC_*, AV_* AF_*, AJ_*
    var m = pat.exec(msg);
    while(m){
        data1 += msg.substr(pos, m.index);
        var v = m[0];     // v is the matching string
        if(!(v in map)){  // is v not already in map?
            map[v] = et.test1(et_count++); // make a manp entry
        }
        replacement = map[v];
        data1 += replacement;
        msg = msg.substr(m.index + m[0].length);
        m = pat.exec(msg);
    }
    data1 += msg;
    return data1;
};// end of translate

var args = process.argv;// ['node', 'convert.js', '<release-path>']
//console.log("arg: ", args);
if(args[2]){
    process_files(args[2]); // feed release-path
} else {
    process_files('deploy/0.13.1341');
}
console.log('et-count:', et_count)

