import jsmin, sys
def minify(filename):
    ifile = open(filename)
    data = ifile.read()
    ofile = open(filename.rsplit('.',1)[0] + '_min.js', 'w')
    ofile.write(jsmin.jsmin(data))
    ofile.close()

if __name__ == '__main__':
    if len(sys.argv) != 2:
        print("usage: python test_1.py <filename>")
    else:
        minify(sys.argv[1])


