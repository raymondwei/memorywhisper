#
# Copyright (C) 2014 Adacro Inc. - All Rights Reserved
# -----------------------------------------------------------------------------
import base64, hashlib, os, zipfile
from modules import utils
from tools import __pcodes as pcode_repo

# called by loaddb: python loaddb.py <fid-backup-db-file-name>
# read db-backup data file, write into db
def deserialize(datafilename, test, db):
    if datafilename.endswith('zip'):
        return recover_zip(datafilename, test, db)
    info = {}
    if test: # if python loaddb.py <data-file-name>.data test
        test_db = {}  # write to a <data-file-name>.py file all the dics
        dicfilename = datafilename.rsplit('.',1)[0] + '.py'
        ofile = open(dicfilename,'w')
    da = open(datafilename).read()
    head, data = da.split('@:@')
    # head line format:
    #0.16.1605.20171215:<pid of saver pa>:<time>:<signature>
    #<version# N.M.K>.<build-time>
    hlst = head.split(':') # [0]:version,[1]:pid,[2]:ts,[3]:sig
    info['version'] = hlst[0].strip('#')
    info['pid'] = hlst[1]
    info['time-stamp'] = hlst[2]
    info['sig'] = hlst[3].strip() # cut the trailing \n
    sha1 = hashlib.sha1()
    sha1.update(data.encode())    # sha1.update demands byte-string. encode
    sig = sha1.hexdigest()        # hexdigest return a string not byte-string
    if sig == info['sig']:
        utils.action_log("debugA", "signature matched", db)
    else:
        utils.action_log("debugB","signature mismatch", db)
        return "signature mismatch:"+sig+'!='+info['sig']
    lst = data.split() # lines separated by \n
    for line in lst:
        try:
            msg = utils.tostr(base64.b64decode(utils.tobytes(line)))
            dic = utils.json.loads(msg)
            if test:
                test_db[dic['_id']] = dic
            else:
                db.put_ent(dic)
        except:
            utils.action_log('debugE','line cuasing issue: '+ line, db)
            return False
            #utils.set_trace()
    if test:
        msg = 'test_db = '
        msg += utils.json.dumps(test_db).replace('null','None')
        ofile.write(msg)
    return True

def  recover_zip(datafilename, test, db):
    try:
        if test:
            os.makedirs(test)
        fh = open(datafilename, 'rb')
        z = zipfile.ZipFile(fh)
        for name in z.namelist():
            if test:
                nam = test + '/' + name.split('/')[-1]
            else:
                nam = name
            fid = name.split('/')[1]
            if not os.path.exists('blobs/'+fid): # fid-blob may not already exist
                os.makedirs('blobs/'+fid)
            ofile = open(nam, 'wb')
            ofile.write(z.read(name))
            ofile.close()
        fh.close()
        utils.action_log('debugA', datafilename + ' recovered', db)
        return True
    except:
        utils.action_log('debugE', datafilename + ' recovery exception', db)
        return False

def loadpcodes(ft):
    # pcodes from __pcodes.py is a list of list of format:
    # [<serial number>, <fid>, <T|F: used ot not>, <description>]
    if ft.load_rp(pcode_repo.pcodes):
        utils.action_log('debugA',
                         str(len(pcode_repo.pcodes))+" loaded.", ft.db)
    else:
        utils.action_log('debugE',"load pcode failed", ft.db)

__lcs = { # lcode : <icon file name>
    'en': u'USA.gif',
    'zh': u'China.gif',
    'de': u'Germany.gif',
    'fr': u'France.gif',
    'it': u'Italy.gif',
    'es': u'Spain.gif',
    'pt': u'Portugal.gif',
    'ru': u'Russia.gif',
    'ja': u'Japan.gif',
    'ar': u'Saudi.jpg',
    'hi': u'India.gif',
    'ko': u'Korea.gif',
    'th': u'Thailand.gif',
    'ur': u'Pakistan.jpg',
    'hu': u'Hungary.gif',
    'nl': u'dutch.jpg',
    'sv': u'sweden.jpg',
    'fi': u'Finland.gif',
    'no': u'Norway.gif',
    'pl': u'Poland.gif',
    'id': u'Indonesia.jpg',
    'vi': u'Vietnam.gif',
    'el': u'Greece.jpg',
    'tr': u'Turkey.gif',
    'tl': u'Philippines.jpg',
    'cs': u'czech.jpg',
    'he': u'isreal.jpg',
    'fa': u'iran.jpg'
}
# 1. For every lang in __lcs, make a new ent
# 2. load every lang-string entry from ldict into the ent, save iter
# This is a over-writing act
def loadldict(db):
    # ldict contains all lang-strings in format of
    # {'M6001': {en: <string in English>, de: <string in Germany>, ...},
    #  'M6002': {}, ...
    # }
    from tools.locale_dict import ldict
    # FTSUPER-LS-icons record: words:{<lcode>:<icon-file-name>,..}
    icons_ent = {'_id': 'FTSUPER-LS-icons', 'words': {}}
    icons_ts = db.ts_from_ent(icons_ent)

    for lc, icon in __lcs.items(): # lcode/icon-file for every languages
        eid = lc
        #print('===== lc =======: ' + lc)
        # create a rec for each language
        ent = {'_id': 'FTSUPER-LS-'+eid, 'icon': icon, 'words': {}}
        ts = db.ts_from_ent(ent)

        icons_ent['words'][lc] = icon
        # fill in strings of all languages into ent
        for key, dic in ldict.items():
            #print('key:' + key)
            txt = dic[lc]
            ent['words'][key]= utils.tostr(base64.b64encode(utils.tobytes(txt)))
        # save(overwrite mode) lang-ent into DB
        db._put_rec('FTSUPER', 'LS', eid, ts, utils.json.dumps(ent))

    # save FTSUPER-LS-icons record
    db._put_rec('FTSUPER', 'LS', 'icons', icons_ts, utils.json.dumps(icons_ent))

# load only the string-entries from ldict that are listed in 
# delta list in local_dict.py
def load_ldict_delta(db):
    from tools.locale_dict import ldict, delta
    for key in delta:
        #print('key:' + key)
        dic = ldict[key]
        for k, v in dic.items():
            dic[k] = utils.tostr(base64.b64encode(utils.tobytes(v)))

        for eid in __lcs.keys():
            #print('===== lc =======: ' + eid)
            ent = db.get_ent('FTSUPER-LS-'+eid)
            ent['words'][key] = dic[eid]
            db.put_ent(ent, True) # update_ts = True
    

