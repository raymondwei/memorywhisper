﻿# ------------------------------------------------------------------------------
# pcodegen.py raymond wei 20140218
# ------------
# run this to generate new promotional codes, appended into promocodes.py
# if promocodes.py does not exists, it will be generated.
#   python pcodegen.py [110]
# will generate 110 new fids into __pcodes.py. if 110 not given, 20 fids
# are generated, by default.
# ------------------------------------------------------------------------------

import random, os, pprint, string, sys
from datetime import datetime
    
rfcharset = (  # 33 chars. 7 digits of 33 chars gives over 42 billion codes
    '1','2','3','4','5','6','7','8','9','A','B','C','D','E','F','G','H','J','K',
    'L','M','N','P','Q','R','S','T','U','V','W','X','Y','Z',) # not use 0,O,I
__file_name = '__pcodes.py'

def put_timestamp(file): # file must be a file opened for writing
    import os
    now = datetime.now()
    tstamp_string = now.strftime('%Y-%m-%d %H:%M:%S')

    # !!!! this line is VERY IMPORTANT !!!!
    file.write('# FID REPOSITORY '+'#'*63+'\n')

    file.write('#\tUpdated:\t '+ tstamp_string+'\n')
    #file.write('#\tFrom Computer: ' + os.environ['COMPUTERNAME'] + '\n')
    info = '#   entry:\n#   serial-N,    fid       used   description\n'
    file.write(info)
    file.write('#\tFrom User: ' + os.environ['USER'] + '\n')
    file.write('#'*80+'\n')

class PCodeGen:
    def __init__(self, pcode_file_name='__pcodes.py'):
        self.filename = pcode_file_name
        
        # get a fresh read of reserved codes from pcode_file_name
        try:
            codes = __import__(self.filename.strip('.py'))
            self.reserved_codes = codes.pcodes
        except:
            self.reserved_codes = []
        self.fids = [e[1] for e in self.reserved_codes]
            
    def writeout(self):
        'save/update reserved code into pcodes_file '
        pprint.pprint(self.reserved_codes)
        ofile = open(self.filename,'w')
        put_timestamp(ofile)
        ofile.write('pcodes = [\n')
        for n, c, u, d in self.reserved_codes:
            ofile.write('\t["'+n+'", "'+c+'", "'+u+'", "' +d+'"],\n')
        ofile.write("]")
        ofile.close()
        
    def gen(self, n=20): # add n more new codes into self.reserved_codes
        while n: # make n new codes
            # make a new code - 7 random chars
            code = ''
            code += random.choice(rfcharset) # [0] char
            code += random.choice(rfcharset) # [1] char
            code += random.choice(rfcharset) # [2] char
            code += random.choice(rfcharset) # [3] char
            code += random.choice(rfcharset) # [4] char
            code += random.choice(rfcharset) # [5] char
            code += random.choice(rfcharset) # [6] char
            # make sure code not in reserved/inuse-codes
            if code not in self.fids:
                #number = string.zfill(len(self.fids)+1, 9)
                number = str(len(self.fids)+1).zfill(9)
                self.reserved_codes.append([number, code, "F", ""])
                self.fids.append(code)
                n -= 1
            if n == 0: 
                break
        return self

    def consume_code(self):
        pass
        
if __name__ == '__main__':
    cgen =  PCodeGen()
    delta = 20  # defaultwise 20 new pcode to be generated
    if len(sys.argv) > 1:
        delta = int(sys.argv[1])  # user gives number
    cgen.gen(delta)
    cgen.writeout()