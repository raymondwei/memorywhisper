# Copyright (C) 2014 Adacro Inc. - All Rights Reserved
# -----------------------------------------------------------------------------
import os
from pdb import set_trace
from flask import Flask, redirect, request, Response
from modules import ftsuper, utils, sadmin
from flask_mail import Mail, Message
import socket

app = Flask(__name__)

# On the Digital-Ocean hosting Ubuntu vm, hostname has 'nyc' in its name
# if 'nyc' not found in hostname, it is localhost, set utils.DEBUG=True
hostname = socket.gethostname()
if hostname.find('nyc') == -1: 
    utils.DEBUG = True
# flask-mail setups
# -------------------------------------------------------
app.config.update(
    DEBUG=utils.DEBUG,
    # Email settings, using the gmail account:
    #    thememorywhisper@gmail.com/spotty2801
    #---------------------------------------------
    MAIL_SERVER='smtp.gmail.com',
    MAIL_PORT=465,
    MAIL_USE_SSL=True, #False,
    MAIL_USERNAME='thememorywhisper@gmail.com',
    MAIL_PASSWORD='spotty2801'
)
utils.mail=Mail(app)

dispatcher = ftsuper.FTSUPER()
# -----------------------------------------------------------------------------
# routes for statics, this block of processors are used only in test mode
# in operational mode, these will be served by nginx as static files
# -----------------------------------------------------------------------------
@app.route('/')
@app.route('/index.html')
def home():
    data = open('static/index.html').read()
    return data

@app.route('/sadmin.html')
def sadmin_page():
    data = open('static/sadmin.html').read()
    return data

@app.route('/sadmin/<cmd>', methods=['GET','POST'])
def handle_sadmin(cmd):
    res = sadmin.handle(request, str(cmd), dispatcher.db)
    return Response(res, mimetype='text/plain')

@app.route('/sendmail-test')
def sendmail():
    try:
        return Response(utils.sendmail())
    except Exception as e:
        return str(e)

@app.route('/favicon.ico')
def get_favicon():
    return open('static/favicon.ico','rb').read()

@app.route('/pics/<filename>')
def get_image(filename):
    #set_trace()
    data = open('static/pics/'+filename, 'rb').read()
    mime = utils.mime_from_file_extention(filename.split('.')[1])
    return Response(data, mimetype=mime)

@app.route('/css/images/<filename>')
def get_jqueryui_image(filename):
    data = open('static/css/images/'+filename, 'rb').read()
    return Response(data,
           mimetype=utils.mime_from_file_extention(filename.split('.')[1]))

@app.route('/dlgs/<filename>')
def get_dlg(filename):
    #set_trace()
    data = open('static/dlgs/'+filename).read()
    return Response(data, mimetype='text/plain')

@app.route('/css/<filename>')
def get_css(filename):
    #set_trace()
    data = open('static/css/'+filename).read()
    return Response(data, mimetype='text/css')

@app.route('/js/<filename>')
def get_js(filename):
    pth = 'static/js/'+filename
    #set_trace()
    if (not filename.endswith('.map')) and os.path.exists(pth):
        data = open(pth).read()
        return Response(data, mimetype='text/javascript')

@app.route('/blobs/<fid>/<blobpath>', methods=['GET'])
def get_blob(fid, blobpath):
    #set_trace()
    data = open('blobs/' + fid + '/' + blobpath, 'rb').read()
    mime = utils.mime_from_file_extention(blobpath.split('.')[1])
    return Response(data, mimetype=mime)

# -----------------------------------------------------------------------------
# end of static block
# -----------------------------------------------------------------------------

@app.route("/req/<requrl>", methods=['GET','POST'])
def all_reqs(requrl):
    #set_trace()
    msg = dispatcher.process_req(request, str(requrl))
    return Response(msg)

if __name__ == "__main__":
    app.run(host='0.0.0.0')