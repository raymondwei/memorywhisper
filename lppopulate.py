# Copyright (C) 2014 Adacro Inc. - All Rights Reserved
# -----------------------------------------------------------------------------
# Python3. load data for lp100 into DB.T_FTSUPER table
# -------------------------------------------------------------------
from tools import lptexts
from modules import utils, ftsuper
from base64 import b64encode
import ujson as json

# ----------------------------------------------------------------------------
_fc090 = {'_id': utils.make_id('FTSUPER','FC','landingpage_fc090'), 
    'type': 'txt',
    'iddict': {'rs':'', 'owner': 'FTSUPER-PA-sadmin'},
    'anno': {'en': b64encode(lptexts._a090en.encode('utf-8')),
             'zh': b64encode(lptexts._a090zh.encode('utf-8'))},
    'contdict': {
        'en': b64encode(lptexts._c090en.encode('utf-8')),
        'zh': b64encode(lptexts._c090zh.encode('utf-8'))
    }
}
# ----------------------------------------------------------------------------
_fc100 = {'_id': utils.make_id('FTSUPER','FC','landingpage_fc100'), 
    'type': 'txt',
    'iddict': {'rs':'', 'owner': 'FTSUPER-PA-sadmin'},
    'anno': {'en': b64encode(lptexts._a100en.encode('utf-8')),
             'zh': b64encode(lptexts._a100zh.encode('utf-8'))},
    'contdict': {
        'en': b64encode(lptexts._c100en.encode('utf-8')),
        'zh': b64encode(lptexts._c100zh.encode('utf-8'))
    }
}
# ----------------------------------------------------------------------------
_rs101en = {'_id': utils.make_id('FTSUPER','RS','landingpage_rs101en'), 
    'mime': 'video/mp4',
    'name': 'english-tour.mp4','title':'lp101en.jpg','size': 2954623
}
# ----------------------------------------------------------------------------
_rs101zh = {'_id': utils.make_id('FTSUPER','RS','landingpage_rs101zh'), 
    'mime': 'video/mp4',
    'name': 'chinese-tour.mp4','title':'lp101en.jpg','size': 2954623
}
# ----------------------------------------------------------------------------
_fc101 = {'_id': utils.make_id('FTSUPER','FC','landingpage_fc101'), 
    'type': 'video',
    'iddict': {'rs':'', 'owner': 'FTSUPER-PA-sadmin'},
    'anno': {'en': b64encode(lptexts._a101en.encode('utf-8')),
             'zh': b64encode(lptexts._a101zh.encode('utf-8'))},
    'contdict': {'en': _rs101en['_id'], 'zh': _rs101zh['_id'] }
}
# ----------------------------------------------------------------------------
_fc102 = {'_id': utils.make_id('FTSUPER','FC','landingpage_fc102'), 
    'type': 'txt',
    'iddict': {'rs':'', 'owner': 'FTSUPER-PA-sadmin'},
    'anno': {}, # feed come later
    'contdict':{'en': b64encode(lptexts.lp100_flist['en'].encode('utf-8')),
                'zh': b64encode(lptexts.lp100_flist['zh'].encode('utf-8'))}
}
# feed _fc102.anno from lptexts.tips
anno_en = ""
anno_zh = ""
for pair in lptexts.lp100_tips:
    anno_en += pair['en'] + '||'
    anno_zh += pair['zh'] + '||'
_fc102['anno']['en'] = b64encode(anno_en.strip('||').encode('utf-8'))
_fc102['anno']['zh'] = b64encode(anno_zh.strip('||').encode('utf-8'))
# ----------------------------------------------------------------------------
_it100 = {'_id': utils.make_id('FTSUPER','IT','landingpage100'), 'type': 'D',
    'iddict': { 'ownerpa': 'FTSUPER-PA-sadmin', 'owner':'FTSUPER-IP-sadmin',
        'facets': [_fc090['_id'],_fc100['_id'],_fc101['_id'],_fc102['_id']]
    }
}
# ----------------------------------------------------------------------------
_fc300 = {'_id':utils.make_id('FTSUPER','FC','landingpage_fc300'), 
    'type': 'txt',
    'iddict': {'rs':'', 'owner': 'FTSUPER-PA-sadmin'},
    'anno': {}, # feed come later
    'contdict':{'en': b64encode(lptexts.lp300_h2list['en'].encode('utf-8')),
                'zh': b64encode(lptexts.lp300_h2list['zh'].encode('utf-8'))}
}
# feed _fc102.anno from lptexts.tips
anno_en = ""
anno_zh = ""
for pair in lptexts.lp300_h2tips:
    anno_en += pair['en'] + '||'
    anno_zh += pair['zh'] + '||'
_fc300['anno']['en'] = b64encode(anno_en.strip('||').encode('utf-8'))
_fc300['anno']['zh'] = b64encode(anno_zh.strip('||').encode('utf-8'))
# ---------------------------------------------------------------------------
_fc300_01 = {'_id':utils.make_id('FTSUPER','FC','landingpagefc300_01'), 
    'type': 'txt',
    'iddict': {'rs':'', 'owner': 'FTSUPER-PA-sadmin'},
    'anno': {'en':b64encode(lptexts._a301en.encode('utf-8')), 
             'zh':b64encode(lptexts._a301zh.encode('utf-8'))},
    'contdict':{'en': b64encode(lptexts._c301en.encode('utf-8')),
                'zh': b64encode(lptexts._c301zh.encode('utf-8'))}
}
# ---------------------------------------------------------------------------
_fc300_02 = {'_id':utils.make_id('FTSUPER','FC','landingpagefc300_02'), 
    'type': 'txt',
    'iddict': {'rs':'', 'owner': 'FTSUPER-PA-sadmin'},
    'anno': {'en':b64encode(lptexts._a302en.encode('utf-8')), 
             'zh':b64encode(lptexts._a302zh.encode('utf-8'))},
    'contdict':{'en': b64encode(lptexts._c302en.encode('utf-8')),
                'zh': b64encode(lptexts._c302zh.encode('utf-8'))}
}
# ---------------------------------------------------------------------------
_fc300_03 = {'_id':utils.make_id('FTSUPER','FC','landingpagefc300_03'), 
    'type': 'txt',
    'iddict': {'rs':'', 'owner': 'FTSUPER-PA-sadmin'},
    'anno': {'en':b64encode(lptexts._a303en.encode('utf-8')), 
             'zh':b64encode(lptexts._a303zh.encode('utf-8'))},
    'contdict':{'en': b64encode(lptexts._c303en.encode('utf-8')),
                'zh': b64encode(lptexts._c303zh.encode('utf-8'))}
}
# ----------------------------------------------------------------------------
_fc300_04 = {'_id':utils.make_id('FTSUPER','FC','landingpagefc300_04'), 
    'type': 'txt',
    'iddict': {'rs':'', 'owner': 'FTSUPER-PA-sadmin'},
    'anno': {'en':b64encode(lptexts._a304en.encode('utf-8')), 
             'zh':b64encode(lptexts._a304zh.encode('utf-8'))},
    'contdict':{'en': b64encode(lptexts._c304en.encode('utf-8')),
                'zh': b64encode(lptexts._c304zh.encode('utf-8'))}
}
# ----------------------------------------------------------------------------
_fc300_05 = {'_id':utils.make_id('FTSUPER','FC','landingpagefc300_05'), 
    'type': 'txt',
    'iddict': {'rs':'', 'owner': 'FTSUPER-PA-sadmin'},
    'anno': {'en':b64encode(lptexts._a305en.encode('utf-8')), 
             'zh':b64encode(lptexts._a305zh.encode('utf-8'))},
    'contdict':{'en': b64encode(lptexts._c305en.encode('utf-8')),
                'zh': b64encode(lptexts._c305zh.encode('utf-8'))}
}
# ----------------------------------------------------------------------------
_fc300_06 = {'_id':utils.make_id('FTSUPER','FC','landingpagefc300_06'), 
    'type': 'txt',
    'iddict': {'rs':'', 'owner': 'FTSUPER-PA-sadmin'},
    'anno': {'en':b64encode(lptexts._a306en.encode('utf-8')), 
             'zh':b64encode(lptexts._a306zh.encode('utf-8'))},
    'contdict':{'en': b64encode(lptexts._c306en.encode('utf-8')),
                'zh': b64encode(lptexts._c306zh.encode('utf-8'))}
}
# ----------------------------------------------------------------------------
def main():
    ft = ftsuper.FTSUPER()
    for ent in [_fc090,_fc100,_fc101,_rs101en,_rs101zh, _fc102, _it100,
                _fc300, _fc300_01, _fc300_02, _fc300_03,
                _fc300_04,_fc300_05,_fc300_06]:
        ft.db.put_ent(ent)

if __name__ == '__main__':
    main()