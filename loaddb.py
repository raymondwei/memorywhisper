# load backuped db data into mysql db
# usage: 
#   python loaddb.py dbbackups/<fid>/<db-file-name>           (1)
#   python loaddb.py dbbackups/<fid>/<db-file-name> test      (2)
#   python loaddb.py pcode                                    (3)
#   python loaddb.py ldict                                    (4)
#   python loaddb.py ldict-delta                              (5)
# -----------
# (1) upload data from db-file into DB, overwriting pre-existing recs
# (2) testing: convert data file to a python dict file, write to <data-file>.py
# (3) upload pcodes(fids) saved in tools/__pcodes.py
# (4) upload lang-string saved in locale_dict in DB(FTSUPER-LS-*)
# (5) update the lang-string entries saved in locale_dict.py marked
#     as delta
# ------------------------------------------------
import sys, os
from modules import ftsuper, utils
from tools.read_data import deserialize, loadpcodes, loadldict, load_ldict_delta

if __name__ == '__main__':
    if len(sys.argv) >= 2:
        ft = ftsuper.FTSUPER()
        if sys.argv[1] == 'pcode': # case (3)
            loadpcodes(ft)
        elif sys.argv[1] == 'ldict': # case (4)
            # load tools/locale_dict.py into FTSUPER-LS-<lc>
            loadldict(ft.db)
        elif sys.argv[1] == 'ldict-delta': # case (5)
            load_ldict_delta(ft.db)
        else:
            if len(sys.argv) > 2:
                # case (2): test
                deserialize(sys.argv[1], sys.argv[2], ft.db)
            else: 
                # case (1):python loaddb.py dbbackups/<fid>/<db-file-name>
                path = sys.argv[1]
                res = deserialize(path, None, ft.db) # test = None
                # put a log record for recovering DB from data file
                lst = path.split('/')
                # log file is in the same directory
                logfile_name = lst[0]+'/'+lst[1]+'/log.txt'
                logfile = open(logfile_name,'a')
                logfile.write('['+utils.get_ts18()[0:15]+']\n')
                if res == True:
                    logfile.write('Recover: ' + path + '\n')
                    open('tmp.txt','w').write('OK')
                else:
                    logfile.write('Recover: ' + str(res) + '\n')
                    open('tmp.txt','w').write('failed')
                logfile.close()

    else:
        print("usage: python loaddb.py pcode|<data-file-name>")