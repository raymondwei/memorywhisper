import sys
from modules.ftsuper import FTSUPER
trial = False

def repair(fid, M):
    ft = FTSUPER()
    # pas
    for k, lst in M.pas.items():
        for e in lst:
            if e[1] == 'NF':
                pa = ft.db.get_ent(k)
                if not pa:
                    print("pa not existing: "+k)
                else:
                    if e[0] == 'nutshell':
                        print("wipe-out nutshell "+e[2]+" in "+k)
                        pa['iddict']['nutshell'] = ''
                        if not trial: 
                            ft.db.put_ent(pa)
                    elif e[0] == 'portrait':
                        print("wipe-out portrait "+e[2]+" in "+k)
                        pa['iddict']['portrait'] = ''
                        if not trial: 
                            ft.db.put_ent(pa)
                    # cases of parents-ba/spouses-ba will be handled manually
    # bas 
    for k, lst in M.bas.items():
        for e in lst:
            if e[1] == 'NF':
                ba = ft.db.get_ent(k)
                if not ba:
                    print("ba not existing: "+k)
                else:
                    if e[0] == 'male':
                        print("wipe-out male "+e[2]+" in "+k)
                        ba['iddict']['male'] = ''
                    elif e[0] == 'female':
                        print("wipe-out female "+e[2]+" in "+k)
                        ba['iddict']['female'] = ''
                    elif e[0] == 'children':
                        print("wipe-out child "+e[2]+" in "+k)
                        ind = ba['iddict']['children'].index(e[2])
                        del ba['iddict']['children'][ind]
                    if not trial:
                        ft.db.put_ent(ba)
    # ips
    for k, lst in M.ips.items():
        for e in lst:
            if e[1] == 'NF':
                ip = ft.db.get_ent(k)
                if not ip:
                    print("ip not existing: "+k)
                else:
                    if e[0] == 'owner':
                        print("delete ip "+k)
                        if not trial:
                            ft.db.delete_ent(k)
                    else:
                        if e[0] == "folders":
                            print("wipe-out folder it "+e[2]+" in "+k)
                            ind = ip['iddict']['folders'].index(e[2])
                            del ip['iddict']['folders'][ind]
                        else: # docs
                            print("wipe-out doc it "+e[2]+" in "+k)
                            ind = ip['iddict']['docs'].index(e[2])
                            del ip['iddict']['docrs'][ind]
                        if not trial:
                            ft.db.put_ent(ip)
    # its
    for k, lst in M.its.items():
        for e in lst:
            if e[1] == 'NF':
                it = ft.db.get_ent(k)
                if not it:
                    print("it not existing: "+k)
                else:
                    if e[0] in ('ownerpa','owner'):
                        print("delete orphanated it "+k)
                        if not trial:
                            ft.db.delete_ent(k)
                    else:
                        if e[0] == 'facets':
                            print("wipe-out fc "+e[2]+" in "+k)
                            ind = it['iddict']['facets'].index(e[2])
                            del it['iddict']['facets'][ind]
                        elif e[0] == 'folders':
                            print("wipe-out itF "+e[2]+" in "+k)
                            ind = it['iddict']['folders'].index(e[2])
                            del it['iddict']['folders'][ind]
                        elif e[0] == 'docs':
                            print("wipe-out itD "+e[2]+" in "+k)
                            ind = it['iddict']['docs'].index(e[2])
                            del it['iddict']['docs'][ind]
                        if not trial:
                            ft.db.put_ent(it)
    # fcs
    for k, lst in M.fcs.items():
        for e in lst:
            if e[1] == 'NF':
                fc = ft.db.get_ent(k)
                if not fc:
                    print("fc not existing: "+k)
                else:
                    if e[0] == 'rs':
                        print("wipe-out rs "+e[2]+" in "+k)
                        fc['iddict']['rs'] = ''
                        if not trial: 
                            ft.db.put_ent(fc)
                    elif e[0] == "owner":
                        print("delete orphonated fc "+k)
                        if not trial:
                            ft.db.delete_ent(k)
                        if fc['iddict']['rs']:
                            print("delete rs "+fc['iddict']['rs'])
                            if not trial:
                                ft.db.delete_ent(fc['iddict']['rs'])
    # tds
    for k, lst in M.tds.items():
        for e in lst:
            if e[1] == 'NF':
                td = ft.db.get_ent(k)
                if not td:
                    print("td not existing: "+k)
                else:
                    if len(td['iddict']['holders']) == 1:
                        print("delete td: "+k)
                        if not trial:
                            ft.db.delete_ent(k)
                    else:
                        print("wipe-out "+e[2]+" in holders-list of "+k)
                        ind = td['iddict']['holders'].index(e[2])
                        del td['iddict']['holders'][ind]
                        if not trial:
                            ft.db.put_ent(td)
    # rss
    for k, lst in M.rss.items():
        for e in lst:
            if e[1] == 'NF':
                rs = ft.db.get_ent(k)
                if not rs:
                    print("rs not existing: "+k)
                elif e[0] == "holders":
                    if len(rs['iddict']['holders']) == 1:
                        print("delete rs: "+k)
                        if not trial:
                            ft.db.delete_ent(k)
                    else:
                        print("wipe-out "+e[2]+" in holders-list of "+k)
                        ind = rs['iddict']['holders'].index(e[2])
                        del rs['iddict']['holders'][ind]
                        if not trial:
                            ft.db.put_ent(rs)

if __name__ == '__main__':
    if len(sys.argv) < 2:
        print("usage: python repair_db.py report_on_<fid>.py")
    else:
        module_name = sys.argv[1].split('.')[0]
        fid = module_name.split('_')[-1]
        m = __import__(module_name)
        repair(fid, m)
