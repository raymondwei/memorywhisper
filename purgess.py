# Copyright (C) 2014 Adacro Inc. - All Rights Reserved
# -----------------------------------------------------------------------------
# To be called by crontab - every 20 minute.
# purpose: to clear/purge expired SE record in DB (FTSUPER-SE-<eid>)
# After an user logged in, a session is created with eid == pa.eid
# every action, the user does, will update se-rwo.ts. When the crontab call is
# called, all se rows are checked to see if se is expired:
#  (now - se-rwo.ts > 20min)
# A session, if the user is idle, can expire between [20-40) minutes
# if expired, ftsuper.terminate_session is called.
# -----------------------------------------------------------------------------
import httplib
import os
if os.uname()[1].find('vaio') > -1:
    port = "5000"
else:
    port = "8000"
# send purge request every minute
conn = httplib.HTTPConnection("localhost",port)
conn.request("GET","/req/_purgess")
#res = conn.getresponse()
#print res.status, res.reason
