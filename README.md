# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###
Source tree of memorywhisper.com web app.
www.memoryWhisper.com is a 1. family-tree and 2. personal documentation hub.


* A single page web-app
- family tree: render family tree graphically and dynamically
  offspring-subtrees and ancestery subtrees can be expanded/shrunk dynamically
  and women(wife) family tree or man(husband) family tree can be switched too.
  offspring/spousal/ancestery sub trees can also be privacy protected. If wanted
  user can a. show those sub trees only when he/she personally logged in; or
  b.expand them asking a pwd, that a not-logged-in user needs to input
- personal documentation hub
  user can organize his/her personal documents in folders/file that can be created
  and named in any languages
  the folder/file structures are user's own creation. Any folder can privacy-
  protected too: A. only visible for logged in user; B. pwd protection, where
  pwd can be permenant or timed.
  Document can have multi media contents(picture, audio, video, pdf)
* Version
  as for 2016-07-13: 0.13.1311. NOT YET GA.
  nginx/gunicorn/flask(python server, mySQL DB
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact