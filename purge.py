# Copyright (C) 2014 Adacro Inc. - All Rights Reserved
# -----------------------------------------------------------------------------
# To be called by crontab - every 1 minute.
# purpose: to clear/purge expired RC record in DB
#  RC(response cache): for every request(with req_id), the response is cached
#  in DB(id: FTSUPER-RC-<eid>) where rec is stringified version of response of 
#  response identified by req_id.
#  The rc row is for the case where the same request is sent multiple times 
#  by routers/client, so the server, when recognizing the same req_id, uses the
#  pre-saved response, in stead of handling it again.
# ---------------------------------------------------
# But the cache is kept only for 1 minute. So this crontab call will purge all
# entries that are older than 1 minute
# -----------------------------------------------------------------------------
import httplib
import os
if os.uname()[1].find('vaio') > -1:
    port = "5000"
else:
    port = "8000"

# send purge request every minute
conn = httplib.HTTPConnection("localhost", port)
#print('purgerc...')
conn.request("GET","/req/_purgerc")
#print('purgess...')
conn.request("GET","/req/_purgess")
#res = conn.getresponse()
#print res.status, res.reason
