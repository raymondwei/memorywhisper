import os, re, pdb, shutil, glob, md5, sys
from jsmin import jsmin
from datetime import datetime
from modules.ftsuper import FTSUPER

# language-icon (flag) file name will not be md5 encoded
ft = FTSUPER()
lang_icon_dic = ft.db.get_ent('FTSUPER-LS-icons')
lang_icons = lang_icon_dic['words'].values()

# process static/index.html
# save infor into dic{}, return dic
def extract_homepage():
    dic = {} # things in index.html
    # [<orig-filename>, ...], use list for keeping order
    csslst = dic.setdefault('csslst', []) 
    jslst = dic.setdefault('jslst',[])
    bodylst = dic.setdefault('bodylst',[])
    ver_pattern = re.compile(r"\<title\>MW\s+(?P<ver>\d+\.\d+\.\d+)\</title\>")
    cssline_pattern = re.compile(r'css/(?P<fn>.+)\"')
    jsline_pattern = re.compile(r'js/(?P<fn>.+)\"')
    body_start_pattern = re.compile(r'\<body\>')
    body_end_pattern = re.compile(r'\</body\>')
    lines = open('static/index.html').readlines()
    body = False
    for line in lines:
        if body:
            m = body_end_pattern.search(line)
            if m:
                break
            else:
                bodylst.append(line)
                continue
        m =ver_pattern.search(line)
        if m:
            dic['ver'] = m.groups('ver')[0]
            continue
        m = cssline_pattern.search(line)
        if m:
            csslst.append(m.groups('fn')[0])
            continue
        m = jsline_pattern.search(line)
        if m:
            jslst.append(m.groups('fn')[0])
            continue
        if not body:
            m = body_start_pattern.search(line)
            if m:
                body = True
                continue
    return dic

# make new (md5) file-names. put the mapping into mapdic
def mapfn(dirname,  # /static/pics, static/js, static/css, static/dlgs 
          mapdic):
    lst = glob.glob(dirname + '/*')
    if dirname.endswith('js'):
        ma = mapdic['jsmap']
    elif dirname.endswith('pics'):
        ma = mapdic['picmap']
    elif dirname.endswith('css'):
        ma = mapdic['cssmap']
    elif dirname.endswith('dlgs'):
        ma = mapdic['dlgmap']
    for filepath in lst:
        if os.path.isfile(filepath):
            fn = os.path.basename(filepath)
            if fn in lang_icons: # lang-icon file names remain unchanged
                ma[fn] = fn
            else:
                # make md5 file names mapping to the original file names
                name, ext = fn.rsplit('.', 1) # 'a.b.c.js'' => ['a.b.c','js']
                m5 = md5.md5(name)
                m5.update(name)
                ma[fn] = '_' + m5.hexdigest() + '.' + ext

# 1.make structured directories
# 2.map file-names to new file-names, for js/*, css/*, dlgs/*, pics/* 
def prepare(dic):
    ver_root = 'deploy/' + dic['ver'] # root for the deployed release version
    if os.path.exists(ver_root):      # if tree exists, delete it
        shutil.rmtree(ver_root)
    # create tree structure
    os.makedirs(ver_root + '/modules')
    os.makedirs(ver_root + '/tmpjs')
    os.makedirs(ver_root + '/static/js')
    os.makedirs(ver_root + '/static/pics')
    os.makedirs(ver_root + '/static/dlgs')
    os.makedirs(ver_root + '/static/css/images')
    mapdic = {'jsmap':{}, 'cssmap':{}, 'picmap':{}, 'dlgmap':{}}

    mapfn('static/pics', mapdic)
    mapfn('static/js', mapdic)
    mapfn('static/css', mapdic)
    mapfn('static/dlgs', mapdic)
    return ver_root, mapdic


def transform_file(type, file, mdic):
    lines = file.readlines()
    res = ''
    if type in ('html', 'css'): # a line may have pic-file name - replace them
        ma = mdic['picmap']
        for line in lines:
            replaced = False
            for k in ma.keys():
                if line.find('/'+k) > -1:
                    replaced = True
                    if k in ma:
                        res += line.replace(k, ma[k])
                        break
                    else:
                        print('===>' + k + ' not in map')
            if not replaced:
                res += line
    elif type == 'js':
        m1 = mdic['dlgmap']
        m2 = mdic['picmap']
        for line in lines:
            replaced = False
            for k in m1.keys():
                if line.find('/'+k) > -1:
                    replaced = True
                    if k in m1:
                        res += line.replace(k, m1[k])
                        break
                    else:
                        print('===>' + k + ' not in map')
            for k in m2.keys():
                if line.find('/'+k) > -1:
                    replaced = True
                    if k in m2:
                        res += line.replace(k, m2[k])
                        break
                    else:
                        print('===>'+ k + ' not in map')
            if not replaced:
                res += line
    return res

def homepage(r, dic, mdic):
    ofile = open(r + '/static/index.html','w')
    ofile.write('<!DOCTYPE html><html><head><meta charset="utf-8"/><title>MW ')
    ofile.write(dic['ver']+'</title><link rel="shortcut icon" href="favicon.ico"/>')
    for cssfn in dic['csslst']:
        fn = "css/" + mdic['cssmap'][cssfn]
        msg = '<link rel="stylesheet" type="text/css" href="' + fn + '"/>'
        ofile.write(msg)
    for jsfn in dic['jslst']:
        fn = "js/" + mdic['jsmap'][jsfn]
        msg = '<script src="' + fn + '"></script>'
        ofile.write(msg)
    ofile.write('<body>')
    for line in dic['bodylst']:
        replaced = False
        pickeys = mdic['picmap'].keys() # all old pic-file names
        for k in pickeys:
            if line.find(k) != -1:
                replaced = True
                if k in mdic['picmap']:
                    ofile.write(line.replace(k, mdic['picmap'][k]))
                    break
                else:
                    print(k+' not in map')
        if not replaced:
            ofile.write(line)
    ofile.write('</body></html>')
    ofile.close()

# place files to be deployed: 
# *.py files, (newly-named) minified *.html files (also replace fn in them),
# css files, pics files. minified js file under /static/tmpjs, for obfuscation
def generate_files(r, dic, mdic, cfg):
    no_js_minify = cfg.setdefault('no-js-minify', False)
    # python files
    shutil.copyfile('flaskserver.py', r+'/flaskserver.py')
    shutil.copyfile('modules/__init__.py', r+'/modules/__init__.py')
    shutil.copyfile('modules/dal.py', r+'/modules/dal.py')
    shutil.copyfile('modules/ftsuper.py', r+'/modules/ftsuper.py')
    shutil.copyfile('modules/handlers.py', r+'/modules/handlers.py')
    shutil.copyfile('modules/merge.py', r+'/modules/merge.py')
    shutil.copyfile('modules/tmp.py', r+'/modules/tmp.py')
    shutil.copyfile('modules/transact.py', r+'/modules/transact.py')
    shutil.copyfile('modules/utils.py', r+'/modules/utils.py')
    # pics files
    lst = glob.glob('static/pics/*')
    for f in lst:
        bn = os.path.basename(f)
        if bn in mdic['picmap']:
            shutil.copyfile(f, r+'/static/pics/' + mdic['picmap'][bn])
        else:
            print('====>' + bn + ' not in map! ')
    # copy css/images/*
    lst = glob.glob('static/css/images/*')
    for f in lst:
        bn = os.path.basename(f)
        shutil.copyfile(f, r+'/static/css/images/' + bn)
    
    # index.html
    homepage(r, dic, mdic)

    # html files from dlgs: replace pic file-names, minify and save them
    lst = glob.glob('static/dlgs/*')
    for f in lst:
        bn = os.path.basename(f)
        if bn in mdic['dlgmap']:
            msg = transform_file('html', open(f), mdic)  # do replacement
            ofile = open(r + '/static/dlgs/' + mdic['dlgmap'][bn], 'w')
            msg1 = jsmin(msg)
            ofile.write(msg1)
            ofile.close()
        else:
            print('====>' + bn + ' not in map')

    # css: replace pic file-names, minify and save them
    lst = glob.glob('static/css/*')
    for f in lst:
        if f.endswith('css'): # avoid images directory under css/
            bn = os.path.basename(f)
            if bn in mdic['cssmap']:
                msg = transform_file('css', open(f), mdic)  # do replacement
                ofile = open(r + '/static/css/'+mdic['cssmap'][bn], 'w')
                #msg1 = jsmin(msg) # jsmin has issue minfying css file
                #ofile.write(msg1)
                ofile.write(msg)
                ofile.close()
            else:
                print('====>'+bn+' not in map')

    # js: replace filenames of pic/dlg files, minify and save under tmpjs
    lst = glob.glob('static/js/*')
    for f in lst:
        bn = os.path.basename(f)
        if bn in mdic['jsmap']:
            #pdb.set_trace()
            msg = transform_file('js',open(f), mdic)
            ofile = open(r + '/tmpjs/' + mdic['jsmap'][bn], 'w')
            if no_js_minify:
                ofile.write(msg)
            else:
                msg1 = jsmin(msg)
                ofile.write(msg1)
            ofile.close()
        else:
            print('=====>'+ bn + ' not in map') 
    # write log under root
    ofile = open(r + '/log.txt','w')
    write_log(r, ofile, mdic)

def write_log(root, ofile, maps):
    ts = datetime.now().isoformat()
    pyver = os.environ['VERSIONER_PYTHON_VERSION']
    user = os.environ['USER']
    ofile.write('#'*80)
    ofile.write('\n# build time: ' + ts + '\n')
    ofile.write('# user: '+user+'\n')
    ofile.write('# Python version: '+pyver+'\n')
    ofile.write('#'*80)
    ofile.write('\n\nPicture files:\n')
    for k, v in maps['picmap'].items():
        ofile.write(k + ': ' + v +'\n')
    ofile.write('\nJavascript files:\n')
    for k, v in maps['jsmap'].items():
        ofile.write(k + ': ' + v + '\n')
    ofile.write('\nCSS files:\n')
    for k, v in maps['cssmap'].items():
        ofile.write(k + ': ' + v + '\n')
    ofile.write('\nDlg files:\n')
    for k, v in maps['dlgmap'].items():
        ofile.write(k + ': ' + v + '\n')
    ofile.close()

def build(cfg):
    #pdb.set_trace()
    dic = extract_homepage()
    release_root, mapdic = prepare(dic)
    generate_files(release_root, dic, mapdic, cfg)
    print('running: node ./tools/convert.js ' + release_root)
    os.system('node ./tools/convert.js ' + release_root)

    # make s link for test-running the build
    os.system('ln -s ../../blobs ' + release_root + '/blobs')
    debug = 1

if __name__ == '__main__':
    cfg = {}
    if len(sys.argv) > 1:
        opts = sys.argv[1:]
        if 'no-js-minify' in opts:
            cfg['no-js-minify'] = True
    build(cfg)